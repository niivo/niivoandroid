package com.niivo.com.niivo.Fragment.BeginnerFragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.util.Locale;


/**
 * Created by andro on 22/5/17.
 */

public class FundDetailFragment extends Fragment {
    Activity _activity;
    TextView fundNav, fundSchemeName, fundFundClass,
            fundSipText, pref1M, pref3M, pref6M, pref1Y, pref2Y, pref3Y, pref5Y, pref7Y, pref10Y, fundMinimumPurchase, fundMinimumSIPAmnt,subcategory,maincategory;

    Context contextd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        View parentView = inflater.inflate(R.layout.activity_fund_detail, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(getArguments().getString("schemeName").trim().toUpperCase());
        fundNav = (TextView) parentView.findViewById(R.id.fundNAV);
        fundSchemeName = (TextView) parentView.findViewById(R.id.fundShcemeName);
        //fundFundClass = (TextView) parentView.findViewById(R.id.fundFundClass);
        fundSipText = (TextView) parentView.findViewById(R.id.fundSIPTxt);
        pref1M = (TextView) parentView.findViewById(R.id.fund1MonthPerf);
        pref3M = (TextView) parentView.findViewById(R.id.fund3MonthPref);
        pref6M = (TextView) parentView.findViewById(R.id.fund6MonthPref);
        pref1Y = (TextView) parentView.findViewById(R.id.fund1YearPref);
        pref2Y = (TextView) parentView.findViewById(R.id.fund2YearPref);
        pref3Y = (TextView) parentView.findViewById(R.id.fund3YearPref);
        pref5Y = (TextView) parentView.findViewById(R.id.fund5YearPref);
        pref7Y = (TextView) parentView.findViewById(R.id.fund7YearPref);
        pref10Y = (TextView) parentView.findViewById(R.id.fund10YearPref);
        fundMinimumPurchase = (TextView) parentView.findViewById(R.id.fundMinimumPurchaseAmnt);
        fundMinimumSIPAmnt = (TextView) parentView.findViewById(R.id.fundMinimumSIPAmnt);
        subcategory=(TextView) parentView.findViewById(R.id.subcategory);
        maincategory=(TextView) parentView.findViewById(R.id.maincategory);

        fundNav.setText(Common.currencyString(String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("nav"))), true));

        subcategory.setText(getArguments().getString("subcategory"));
        maincategory.setText(getArguments().getString("maincategory"));
        fundSchemeName.setText(getArguments().getString("schemeName"));
       // fundFundClass.setText(getArguments().getString("fundClass"));
        fundSipText.setText((getArguments().getString("isSIP").equals("Y") ? contextd.getResources().getString(R.string.yes) : contextd.getResources().getString(R.string.no)));
        fundSipText.setTextColor(Color.parseColor((getArguments().getString("isSIP").equals("Y") ? "#3fd37e" : "#d74c48")));
        pref1M.setText((getArguments().getString("1MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("1MonthPref"))) + "%"));
        pref3M.setText((getArguments().getString("3MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("3MonthPref"))) + "%"));
        pref6M.setText((getArguments().getString("6MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("6MonthPref"))) + "%"));
        pref1Y.setText((getArguments().getString("1YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("1YearPref"))) + "%"));
        pref2Y.setText((getArguments().getString("2YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("2YearPref"))) + "%"));
        pref3Y.setText((getArguments().getString("3YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("3YearPref"))) + "%"));
        pref5Y.setText((getArguments().getString("5YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("5YearPref"))) + "%"));
        pref7Y.setText((getArguments().getString("7YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("7YearPref"))) + "%"));
        pref10Y.setText((getArguments().getString("10YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("10YearPref"))) + "%"));
        fundMinimumPurchase.setText(Common.currencyString((int) (Float.parseFloat(getArguments().getString("minimumPurchase").trim())) + "",false));
        fundMinimumSIPAmnt.setText(getArguments().getString("minimumSIPAmnt").equals("")
                ? contextd.getResources().getString(R.string.na)
                : Common.currencyString((int) (Float.parseFloat(getArguments().getString("minimumSIPAmnt").trim())) + "",false));

        return parentView;
    }
}
