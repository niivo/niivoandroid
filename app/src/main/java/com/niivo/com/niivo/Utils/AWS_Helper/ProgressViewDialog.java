package com.niivo.com.niivo.Utils.AWS_Helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.niivo.com.niivo.R;


/**
 * Created by deepak on 7/5/18.
 */

public class ProgressViewDialog {
    Context contextd;
    Dialog progressdialog;

    ProgressViewDialog(Context context) {
        contextd = context;
        progressdialog = new Dialog(contextd);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.view_progress);
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public static ProgressViewDialog with(Context context) {
        return new ProgressViewDialog(context);
    }

    public void setCancelable(boolean cancelable) {
        progressdialog.setCancelable(cancelable);
    }

    public void show() {
        if (!progressdialog.isShowing())
            progressdialog.show();
    }

    public void dismiss() {
        progressdialog.dismiss();
    }
}
