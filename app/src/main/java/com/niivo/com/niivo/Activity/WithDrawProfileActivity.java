package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.R;

public class WithDrawProfileActivity extends AppCompatActivity {
    TextView tv_withdrawfundname, tv_withamount, tv_withdrawfolio, tv_bsemsg,tv_units,tv_fullwithdraw, tv_home,amount,withdraw,unit;
    ImageView back;
    String foliono, msg, fundname, Ramount, Runits, RWithdraw,fullwithdraw;
    LinearLayout amountlayout,unitlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_draw_profile);
        amountlayout = (LinearLayout)findViewById(R.id.amountlayout);
        unitlayout =(LinearLayout)findViewById(R.id.unitlayout);
        back = (ImageView) findViewById(R.id.back);
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_units=(TextView) findViewById(R.id.tv_units);
        tv_withdrawfundname = (TextView) findViewById(R.id.tv_withdrawfundname);
        tv_withamount = (TextView) findViewById(R.id.tv_withamount);
        tv_withdrawfolio = (TextView) findViewById(R.id.tv_withdrawfolio);
        tv_bsemsg = (TextView) findViewById(R.id.tv_bsemsg);
        amount = (TextView) findViewById(R.id.amount);
        unit = (TextView) findViewById(R.id.unit);

        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WithDrawProfileActivity.this, MainActivity.class);
                startActivity(intent);

                finish();
            }
        });

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();

        if (bd != null) {

            foliono = (String) bd.get("folioNo");
            tv_withdrawfolio.setText(foliono);

            fundname = (String) bd.get("fundname");
            tv_withdrawfundname.setText(fundname);

            RWithdraw = (String) bd.get("fullWithdraw");
            fullwithdraw = (String) bd.get("fullamount");
            Runits = (String) bd.get("units");
            Ramount = (String) bd.get("amount");
            if(RWithdraw.equals("Y"))
            {
                tv_withamount.setText(fullwithdraw);
                unitlayout.setVisibility(View.GONE);
            }else if(Runits.equals("0")){
                tv_withamount.setText(Ramount);
                unitlayout.setVisibility(View.GONE);
            }else{
                tv_units.setText(Runits);
                amountlayout.setVisibility(View.GONE);
            }
            msg = (String) bd.get("order_status");
            tv_bsemsg.setText(msg);
        }

    }
}
