package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niivo.com.niivo.Model.InvestmentFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.util.Locale;

/**
 * Created by Deepak.
 */

public class IntermidiateAdapter extends RecyclerView.Adapter<IntermidiateAdapter.MyViewHolder> {

    InvestmentFunds list;
    Context contextd;
    boolean isLongTerm = false;
    String lang = "";

    public IntermidiateAdapter(InvestmentFunds moviesList, Context context) {
        this.list = moviesList;
        contextd = context;
        lang = Common.getLanguage(contextd);
    }

    public IntermidiateAdapter(InvestmentFunds moviesList, boolean isLongTerm, Context context) {
        this.list = moviesList;
        this.isLongTerm = isLongTerm;
        contextd = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, oneY, twoY, fiveY, oneYLbl, twoYLbl, fiveYLbl;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.fundTitle);
            oneY = (TextView) view.findViewById(R.id.oneYrt);
            twoY = (TextView) view.findViewById(R.id.twoYrt);
            fiveY = (TextView) view.findViewById(R.id.fiveYrt);
            oneYLbl = (TextView) view.findViewById(R.id.oneYrtLbl);
            twoYLbl = (TextView) view.findViewById(R.id.twoYrtLbl);
            fiveYLbl = (TextView) view.findViewById(R.id.fiveYrtLbl);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_intermidiate, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(lang.equals("en")
                ? list.getData().get(position).getSCHEME_NAME()
                : lang.equals("hi")
                ? list.getData().get(position).getFUNDS_HINDI()
                : lang.equals("gu")
                ? list.getData().get(position).getFUNDS_GUJARATI()
                : lang.equals("mr")
                ? list.getData().get(position).getFUNDS_MARATHI()
                : list.getData().get(position).getSCHEME_NAME());
        if (!this.isLongTerm) {
            if (list.getData().get(position).getONE_Y_PERF().trim().equals("N.A.")) {
                holder.oneY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.oneY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getONE_Y_PERF().trim())) + "%");
            }
            if (list.getData().get(position).getTWO_Y_PERF().trim().equals("N.A.")) {
                holder.twoY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.twoY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getTWO_Y_PERF().trim())) + "%");
            }
            if (list.getData().get(position).getFIVE_Y_PERF().trim().equals("N.A.")) {
                holder.fiveY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.fiveY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getFIVE_Y_PERF().trim())) + "%");
            }
        } else if (this.isLongTerm) {
            holder.oneYLbl.setText(contextd.getResources().getString(R.string.y3));
            holder.twoYLbl.setText(contextd.getResources().getString(R.string.y5));
            holder.fiveYLbl.setText(contextd.getResources().getString(R.string.y7));
            if (list.getData().get(position).getONE_Y_PERF().trim().equals("N.A.")) {
                holder.oneY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.oneY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getTHREE_Y_PERF().trim())) + "%");
            }
            if (list.getData().get(position).getTWO_Y_PERF().trim().equals("N.A.")) {
                holder.twoY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.twoY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getFIVE_Y_PERF().trim())) + "%");
            }
            if (list.getData().get(position).getFIVE_Y_PERF().trim().equals("N.A.")) {
                holder.fiveY.setText(contextd.getResources().getString(R.string.na));
            } else {
                holder.fiveY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getSEVEN_Y_PERF().trim())) + "%");
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.getData().size();
    }
}
