package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemState;
import com.niivo.com.niivo.R;

import java.util.ArrayList;

/**
 * Created by deepak on 9/6/17.
 */

public class EquityFundsAdapter extends ArrayAdapter<ItemState> {
    Context contextd;
    ArrayList<ItemState> list;

    public EquityFundsAdapter(@NonNull Context context, ArrayList<ItemState> resource) {
        super(context, 0, resource);
        this.contextd = context;
        this.list = resource;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_fund_type, parent, false);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView desc = (TextView) convertView.findViewById(R.id.itemDesc);
            desc.setText(list.get(position).getStateCode());
            title.setText(list.get(position).getStateName());
        }
        return convertView;
    }
}
