package com.niivo.com.niivo.Fragment.InvestHelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Activity.PaymentHelperActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.CartHelper.CartDBHelper;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 12/12/17.
 */

public class PendingRedirectingFragment extends Fragment implements ResponseDelegate {
    TextView notifyTxt;

    //Non ui Vars
    Context contextd;
    CartDBHelper db;
    RequestedServiceDataModel requestedServiceDataModel;
    String userID = "", orders = "", amount = "", payment_type = "";
    boolean proceedPayment = false;
    MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_redirectoring, container, false);
        contextd = container.getContext();
        activity = (MainActivity) getActivity();
        db = new CartDBHelper(contextd);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.redirecting));
        notifyTxt = (TextView) rootView.findViewById(R.id.notifyText);
        notifyTxt.setText(contextd.getResources().getString(R.string.pleaseWait));
        userID = getArguments().getString("userid");
        orders = getArguments().getString("orders");
        amount = getArguments().getString("amount");
        payment_type = getArguments().getString("orderType");
        proceedPayment(userID, orders, amount, payment_type, getArguments().getString("due_date"));
        return rootView;
    }


    void proceedPayment(String userid, String orders, String amount, String payment_type, String due_date) {
        notifyTxt.setText(contextd.getResources().getString(R.string.redirectingPaymentGateWay));
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-payment.php");
        baseRequestData.setTag(ResponseType.DoPayment);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("orders", orders.trim());
        requestedServiceDataModel.putQurry("amount", amount.trim());
        requestedServiceDataModel.putQurry("paymemt_type", payment_type);
        requestedServiceDataModel.putQurry("due_date", due_date);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("sip_registration_no", orders);
        requestedServiceDataModel.setWebServiceType("payment");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.DoPayment:
             //   Log.d("Response", json);
             //   Log.d("PendingRedirectionfgmnt", json);
                proceedPayment = true;
                startActivityForResult(new Intent(contextd, PaymentHelperActivity.class).putExtra("rawHtml", message), 113);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.DoPayment:
                try {
                    JSONObject obj = new JSONObject(json);
                 //   Log.d("faill PendingRedirect", json);
                    if (obj.getString("retry").equals("1"))
                        retryOnWebError(obj.getString("msg"));
                    else
                        errorMsg(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    void errorMsg(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(message);

        builder.setPositiveButton(contextd.getResources().getString(R.string.goBack), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        builder.setCancelable(false);
        builder.create().show();
    }


    private void retryOnWebError(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(contextd.getResources().getString(R.string.retry),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        proceedPayment(userID, orders, amount, payment_type, getArguments().getString("due_date"));
                    }
                });
        builder.setNegativeButton(contextd.getResources().getString(R.string.backToHome),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.getBackToHome();
                        dialog.dismiss();
                    }
                });

        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 113 && resultCode == 113) {
            db.deleteFund(getArguments().getString("schemeCode"));
            activity.getBackToHome();
            //rate here
        }
    }
}
