package com.niivo.com.niivo.Fragment.KYCnew;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.CountryListAdapter;
import com.niivo.com.niivo.Adapter.PepListAdapter;
import com.niivo.com.niivo.Adapter.SalaryDependentAdapter;
import com.niivo.com.niivo.BuildConfig;
import com.niivo.com.niivo.Model.ItemCountryList;
import com.niivo.com.niivo.Model.ItemPanDetails;
import com.niivo.com.niivo.Model.ItemPepList;
import com.niivo.com.niivo.Model.ItemSalaryDependent;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.AWS_Helper.ProgressViewDialog;
import com.niivo.com.niivo.Utils.AWS_Helper.S3UploadService;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.PickerHelper;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.joda.time.DateTime;
import org.joda.time.Years;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Parameter;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.STORAGE_STATS_SERVICE;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCIdentityDetails_2_Fragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    TextView marriedBtn, singleBtn, maleBtn, femaleBtn, indianBtn, otherBtn, countryLBL, continueBtn;
    LinearLayout ifMarriedLL, camImgBtn, nationalityLL;
    AppCompatSpinner countrySpinner, noOfChildrenSpinner, salaryDependentSpinner, pepSpinner;
    RightGravityTextInputLayout tpinRTL;
    ImageView profilePicImg;
    TextInputEditText tpinET;
    RelativeLayout profileImgRL;

    //Non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    ItemCountryList countryList;
    File profilePicFile;
    ProgressViewDialog progressViewDialog;
    PickerHelper helper;
    boolean single = true, indian = true, male = true, kycStatus = false;
    String userId = "", profileImgRef = "";
    ArrayList<ItemSalaryDependent> ageScore, noOfchildren, salaryDependent;
    ItemPepList pepList;
    String countryOfBirth = "", placeOfbirth = "", grossAnnualIncome = "",
            occupationCode = "", countryOfResidency = "", wealthSource = "",
            dob = "", ageProfileScore = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    int ageInYear = 0;
    boolean isIdentitySubmitted = false;
    ItemPanDetails panDetails;
    private static final int PICK_IMAGE_CAMERA = 111;
    private static final int PICK_IMAGE_GALLERY = 222;
    private static final int REQUEST_CODE_HIGH_QUALITY_IMAGE=333;
    private Uri mHighQualityImageUri = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kyc_identity_details_2, container, false);
        contextd = container.getContext();
        marriedBtn = (TextView) rootView.findViewById(R.id.marriedBtn);
        singleBtn = (TextView) rootView.findViewById(R.id.singleBtn);
        maleBtn = (TextView) rootView.findViewById(R.id.maleBtn);
        femaleBtn = (TextView) rootView.findViewById(R.id.femaleBtn);
        indianBtn = (TextView) rootView.findViewById(R.id.indianBtn);
        otherBtn = (TextView) rootView.findViewById(R.id.otherBtn);
        ifMarriedLL = (LinearLayout) rootView.findViewById(R.id.ifMarriedLL);
        countryLBL = (TextView) rootView.findViewById(R.id.countryLBL);
        countrySpinner = (AppCompatSpinner) rootView.findViewById(R.id.countrySpinner);
        tpinRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.tpinRTL);
        continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
        camImgBtn = (LinearLayout) rootView.findViewById(R.id.camImgBtn);
        profilePicImg = (ImageView) rootView.findViewById(R.id.profilePicImg);
        nationalityLL = (LinearLayout) rootView.findViewById(R.id.nationalityLL);
        noOfChildrenSpinner = (AppCompatSpinner) rootView.findViewById(R.id.noOfChildrenSpinner);
        salaryDependentSpinner = (AppCompatSpinner) rootView.findViewById(R.id.salaryDependentSpinner);
        tpinET = (TextInputEditText) rootView.findViewById(R.id.tpinET);
        pepSpinner = (AppCompatSpinner) rootView.findViewById(R.id.pepSpinner);
        profileImgRL = (RelativeLayout) rootView.findViewById(R.id.profileImgRL);
        marriedBtn.setOnClickListener(this);
        singleBtn.setOnClickListener(this);
        maleBtn.setOnClickListener(this);
        femaleBtn.setOnClickListener(this);
        indianBtn.setOnClickListener(this);
        otherBtn.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
        camImgBtn.setOnClickListener(this);
        profilePicImg.setOnClickListener(this);

        singleBtn.setSelected(true);
        maleBtn.setSelected(true);
        indianBtn.setSelected(true);
        countryLBL.setVisibility(View.GONE);
        countrySpinner.setVisibility(View.GONE);
        tpinRTL.setVisibility(View.GONE);
        doPermissionWork();
        if (countryList == null)
            getCountryList();
        if (pepList == null)
            getPEP_List();
        return rootView;
    }

    void doPermissionWork() {
        indian = getArguments().getBoolean("isIndian");
        kycStatus = getArguments().getBoolean("kycStatus");
        userId = Common.getPreferences(contextd, "userID");
        ageScore = new ArrayList<>();
        ageScore.add(new ItemSalaryDependent("Age Less than 30", "11", "4"));
        ageScore.add(new ItemSalaryDependent("Age 30 - 45", "12", "3"));
        ageScore.add(new ItemSalaryDependent("Age 46 - 55", "13", "2"));
        ageScore.add(new ItemSalaryDependent("Age more than 55", "14", "1"));
        noOfchildren = new ArrayList<>();
        noOfchildren.add(new ItemSalaryDependent("No Children", "21", "4"));
        noOfchildren.add(new ItemSalaryDependent("1 Child", "22", "3"));
        noOfchildren.add(new ItemSalaryDependent("2 Children", "23", "2"));
        noOfchildren.add(new ItemSalaryDependent("More than 2 children", "24", "1"));
        salaryDependent = new ArrayList<>();
        salaryDependent.add(new ItemSalaryDependent("1 person dependent", "31", "4"));
        salaryDependent.add(new ItemSalaryDependent("2 persons dependent", "32", "3"));
        salaryDependent.add(new ItemSalaryDependent("3 persons dependent", "33", "2"));
        salaryDependent.add(new ItemSalaryDependent("More than 3 persons dependent", "34", "1"));
        countryOfBirth = getArguments().getString("countryOfBirth");
        placeOfbirth = getArguments().getString("placeOfbirth");
        grossAnnualIncome = getArguments().getString("grossAnnualIncome");
        occupationCode = getArguments().getString("occupationCode");
        countryOfResidency = getArguments().getString("countryOfResidency");
        wealthSource = getArguments().getString("wealthSource");
        dob = getArguments().getString("dob");
        try {
            DateTime dobCal = new DateTime(sdf.parse(dob));
            DateTime today = new DateTime(Calendar.getInstance().getTime());
            ageInYear = Years.yearsBetween(dobCal, today).getYears();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //according to above age scores
        ageProfileScore = ageInYear < 30
                ? "11" : (ageInYear >= 30 || ageInYear <= 45)
                ? "12" : (ageInYear >= 46 || ageInYear <= 55)
                ? "13" : "14";
        noOfChildrenSpinner.setAdapter(new SalaryDependentAdapter(contextd, noOfchildren));
        salaryDependentSpinner.setAdapter(new SalaryDependentAdapter(contextd, salaryDependent));
        if (indian) {
            nationalityLL.setVisibility(View.GONE);
        } else
            nationalityLL.setVisibility(View.VISIBLE);
        progressViewDialog = ProgressViewDialog.with(contextd);
        progressViewDialog.setCancelable(false);
        helper = PickerHelper.with(contextd, KYCIdentityDetails_2_Fragment.this);
        if ((ContextCompat.checkSelfPermission(contextd, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 125);
        }
        if (kycStatus) {
            panDetails = (ItemPanDetails) getArguments().getSerializable("panDetail");
            profileImgRL.setVisibility(View.GONE);
            profileImgRef = "";
            if (panDetails.getData().getPan_data().getAPP_MAR_STATUS().equals("01")) {

            } else {
            }
        }
        if (getArguments().getBoolean("preRegistered"))
            setUpPreRegistration();
    }

    boolean preRegistered = false;
    ItemUserDetail userDetail;
    int step = 0;

    void setUpPreRegistration() {
        preRegistered = true;
        userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
        step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
        if (step > 2) {
            if (userDetail.getData().getMarital_status().equals("MARRIED")) {
                single = false;
                marriedBtn.setSelected(true);
                singleBtn.setSelected(false);
                ifMarriedLL.setVisibility(View.VISIBLE);
                for (int i = 0; i < noOfchildren.size(); i++)
                    if (noOfchildren.get(i).getNIIVO_CODE().equals(userDetail.getData().getNo_of_children()))
                        noOfChildrenSpinner.setSelection(i);
//                for(int i=0;i<salaryDependent.size();i++)
//                    if(salaryDependent.get(i).getNIIVO_CODE().equals(userDetail.getData().g))
            } else {
                single = true;
                singleBtn.setSelected(true);
                marriedBtn.setSelected(false);
                ifMarriedLL.setVisibility(View.GONE);
            }
            if (userDetail.getData().getGender().equalsIgnoreCase("M")) {
                maleBtn.setSelected(true);
                femaleBtn.setSelected(false);
            } else {
                femaleBtn.setSelected(true);
                maleBtn.setSelected(false);
            }
        }
        if (pepList == null)
            getPEP_List();
        else {
            for (int i = 0; i < pepList.getData().size(); i++) {
                if (pepList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getPolitically_exposed_person()))
                    pepSpinner.setSelection(i);
            }
        }
        if (userDetail.getData().getNationality() != null)
            if (!userDetail.getData().getNationality().equals("INDIAN")) {
                if (countryList == null)
                    getCountryList();
                else {
                    for (int i = 0; i < countryList.getData().size(); i++) {
                        if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getCountry()))
                            countrySpinner.setSelection(i);
                    }
                }
                tpinET.setText(userDetail.getData().getTpin());
            } else {

            }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.marriedBtn:
                single = false;
                marriedBtn.setSelected(true);
                singleBtn.setSelected(false);
                ifMarriedLL.setVisibility(View.VISIBLE);
                break;
            case R.id.singleBtn:
                single = true;
                singleBtn.setSelected(true);
                marriedBtn.setSelected(false);
                ifMarriedLL.setVisibility(View.VISIBLE);
                break;
            case R.id.maleBtn:
                male = true;
                maleBtn.setSelected(true);
                femaleBtn.setSelected(false);
                break;
            case R.id.femaleBtn:
                male = false;
                femaleBtn.setSelected(true);
                maleBtn.setSelected(false);
                break;
            case R.id.indianBtn:
                indian = true;
                indianBtn.setSelected(true);
                otherBtn.setSelected(false);
                countryLBL.setVisibility(View.GONE);
                countrySpinner.setVisibility(View.GONE);
                tpinRTL.setVisibility(View.GONE);
                break;
            case R.id.otherBtn:
                indian = false;
                indianBtn.setSelected(false);
                otherBtn.setSelected(true);
                countryLBL.setVisibility(View.VISIBLE);
                countrySpinner.setVisibility(View.VISIBLE);
                tpinRTL.setVisibility(View.VISIBLE);
                break;
            case R.id.continueBtn:
                if (validate()) {
                    if (kycStatus) {
                        setIdentityDetails();
                    } else {
                        progressViewDialog.show();
                        LocalBroadcastManager.getInstance(contextd).registerReceiver(getUploadCallBack, new IntentFilter("onUploadStatus"));
                        Intent i = new Intent(contextd, S3UploadService.class);
                        i.putExtra("userId", userId);
                        i.putExtra("fileType", "profile_picture");
                        i.putExtra("file", profilePicFile);
                        contextd.startService(i);
                    }
                }
                break;
            case R.id.camImgBtn:
            case R.id.profilePicImg:
                selectImage();
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.OFF)
//                        .setAllowFlipping(false)
//                        .setAutoZoomEnabled(true)
//                        .setBorderCornerColor(Color.parseColor("#42E598"))
//                        .setBorderLineColor(Color.parseColor("#3CB5B9"))
//                        .setAspectRatio(4, 5)
//                        .start(contextd, this);
                break;
        }
    }


    private Uri generateTimeStampPhotoFileUri() {

        Uri photoFileUri = null;
        File outputDir = getPhotoDirectory();
        if (outputDir != null) {
         //   Time t = new Time();
           // t.setToNow();
            File photoFile = new File(outputDir, System.currentTimeMillis()
                    + ".jpg");
           // photoFileUri = Uri.fromFile(photoFile);
            photoFileUri =  FileProvider.getUriForFile(contextd, BuildConfig.APPLICATION_ID + ".provider",photoFile);
        }
        return photoFileUri;
    }

    private File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageStagte = Environment.getExternalStorageState();
        if (externalStorageStagte.equals(Environment.MEDIA_MOUNTED)) {
            File photoDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            outputDir = new File(photoDir, getString(R.string.app_name));
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(contextd, "Failed to create directory " + outputDir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    outputDir = null;
                }
        }
        return outputDir;
    }

    private void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();
                        mHighQualityImageUri = generateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //intent.setClassName("com.android.camera", "com.android.camera.Camera");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean validate() {
        boolean b = false;
        if (!kycStatus && profilePicFile == null) {
            b = false;
            Common.showToast(contextd, "Please add your profile photo.");
        } else
            b = true;
        if (!indian) {
            if (tpinET.getText().toString().trim().equals("")) {
                b = false;
                tpinRTL.setError("Required Field..!");
            } else
                b = true;
        }
        return b;
    }

   /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK || requestCode == PICK_IMAGE_CAMERA) {
            Uri resultUri=  mHighQualityImageUri;
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                        bitmap = helper.getResizedBitmap(bitmap, 640);

                        camImgBtn.setVisibility(View.GONE);
                        profilePicFile = helper.saveBitmapToFile(bitmap, "profile_pic");
                        Picasso.with(contextd)
                                .load(profilePicFile)
                                .memoryPolicy(MemoryPolicy.NO_STORE)
                                .into(profilePicImg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

        }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && mHighQualityImageUri != null) {
            Uri resultUri=  mHighQualityImageUri;
          //  Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            // Uri resultUri = result.getUri();
              try {
             Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
            bitmap = helper.getResizedBitmap(bitmap, 1920);
            bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
            camImgBtn.setVisibility(View.GONE);
            profilePicFile = helper.saveBitmapToFile(bitmap, "profile_pic");
            Picasso.with(contextd)
                    .load(profilePicFile)
                    .memoryPolicy(MemoryPolicy.NO_STORE)
                    .into(profilePicImg);
                } catch (IOException e) {
                    e.printStackTrace();
              }
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
        } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri resultUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                bitmap = helper.getResizedBitmap(bitmap, 1920);
                bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                camImgBtn.setVisibility(View.GONE);
                profilePicFile = helper.saveBitmapToFile(bitmap, "profile_pic");
                Picasso.with(contextd)
                        .load(profilePicFile)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .into(profilePicImg);

            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isIdentitySubmitted", isIdentitySubmitted);
        getTargetFragment().onActivityResult(101,
                isIdentitySubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED, i);
    }

    void getCountryList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetCountry);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("COUNTRYCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getPEP_List() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetPEPlist);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("PEPCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void setIdentityDetails() {
        LocalBroadcastManager.getInstance(contextd).unregisterReceiver(getUploadCallBack);
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_IdentityDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("ADDIDENTITY");
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("birth_country", countryOfBirth);
        requestedServiceDataModel.putQurry("birth_place", placeOfbirth);
        requestedServiceDataModel.putQurry("address_type", "");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("gross_annual_income", grossAnnualIncome);
        requestedServiceDataModel.putQurry("occupation", occupationCode);
        requestedServiceDataModel.putQurry("politically_exposed_person", pepList.getData().get(pepSpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("tax_residency_country", countryOfResidency);
        requestedServiceDataModel.putQurry("wealth_source", wealthSource);
        requestedServiceDataModel.putQurry("marital_status", single ? "SINGLE" : "MARRIED");
        requestedServiceDataModel.putQurry("no_of_children", single ? "21" : noOfchildren.get(noOfChildrenSpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("salary_dependent", single ? "" : salaryDependent.get(salaryDependentSpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("gender", male ? "M" : "F");
        requestedServiceDataModel.putQurry("nationality", indian ? "INDIAN" : "OTHER");
        requestedServiceDataModel.putQurry("country", indian ? "101" : countryList.getData().get(countrySpinner.getSelectedItemPosition()).getNIIVO_CODE()); //INDIA
        requestedServiceDataModel.putQurry("photo", profileImgRef);
        requestedServiceDataModel.putQurry("profileAgeScore", ageProfileScore);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetCountry:
       //         Log.e("getCountry", json);
                countryList = new Gson().fromJson(json, ItemCountryList.class);
                countrySpinner.setAdapter(new CountryListAdapter(contextd, countryList));
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.GetPEPlist:
                Log.e("getPepList", json);
                pepList = new Gson().fromJson(json, ItemPepList.class);
                pepSpinner.setAdapter(new PepListAdapter(contextd, pepList));
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.KYC_IdentityDetails:
                Log.e("setIdentityDetails", json);
                Common.showToast(contextd, message);
                isIdentitySubmitted = true;
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetCountry:
                Log.e("getCountry", json);
                break;
            case ResponseType.GetPEPlist:
                Log.e("getPepList", json);
                break;
            case ResponseType.KYC_IdentityDetails:
                Log.e("setIdentityDetails", json);
                break;
        }
    }

    BroadcastReceiver getUploadCallBack = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressViewDialog.dismiss();
            if (intent.getStringExtra("state").equals("COMPLETED")) {
                profileImgRef = intent.getStringExtra("panRef");
                setIdentityDetails();
            } else {
                Common.showToast(contextd, "Upload failed! Try again.");
            }
        }
    };
}
