package com.niivo.com.niivo.Fragment.KYCnew;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.BuildConfig;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.LocationHelper.LocationAddress;
import com.niivo.com.niivo.Utilities.Sign_Helper.SignDrawActivity;
import com.niivo.com.niivo.Utils.AWS_Helper.ProgressViewDialog;
import com.niivo.com.niivo.Utils.AWS_Helper.S3UploadService;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.LocationHelper.EasyWayLocation;
import com.niivo.com.niivo.Utils.LocationHelper.Listener;
import com.niivo.com.niivo.Utils.MyApplication;
import com.niivo.com.niivo.Utils.PickerHelper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.niivo.com.niivo.Utils.LocationHelper.EasyWayLocation.LOCATION_SETTING_REQUEST_CODE;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCBankDetailsImageFragment extends Fragment implements View.OnClickListener, ResponseDelegate, Listener {
    TextView continueBtn, hintLbl;
    LinearLayout chequeImgBtn;
    RelativeLayout signatureImgBtn;
    ImageView chequeImg, signatureImg, retryBtn;
    View rootView;

    //Non ui vars
    Context contextd;
    boolean isChequeUploaded = false, isSignatureUploaded = false, isSubmitted = false;
    File imgFileCheque = null, imgFileSignature = null;
    PickerHelper helper;
    ProgressViewDialog progressViewDialog;
    int type = 0;
    String chequeImgRef = "", signatureImgRef = "", userId = "";
    RequestedServiceDataModel requestedServiceDataModel;
    String chequebase = "", signatureBase = "";
    Bitmap chequeBitmap, signBitmap;
    MainActivity activity;
    ItemUserDetail userDetail;
    EasyWayLocation easyWayLocation;
    private static final int PICK_IMAGE_CAMERA = 111;
    private static final int PICK_IMAGE_GALLERY = 222;
    Uri mHighQualityImageUri=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_bank_details_image, container, false);
            contextd = container.getContext();
            continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
            continueBtn.setOnClickListener(this);
            hintLbl = (TextView) rootView.findViewById(R.id.hintLbl);
            chequeImgBtn = (LinearLayout) rootView.findViewById(R.id.chequeImgBtn);
            signatureImgBtn = (RelativeLayout) rootView.findViewById(R.id.signatureImgBtn);
            chequeImg = (ImageView) rootView.findViewById(R.id.chequeImg);
            signatureImg = (ImageView) rootView.findViewById(R.id.signatureImg);
            retryBtn = (ImageView) rootView.findViewById(R.id.retryBtn);
            retryBtn.setOnClickListener(this);
            chequeImgBtn.setOnClickListener(this);
            signatureImgBtn.setOnClickListener(this);
            chequeImg.setOnClickListener(this);
            signatureImg.setOnClickListener(this);
            activity = (MainActivity) getActivity();
            userId = Common.getPreferences(activity, "userID");
            doPermissionWork();
        }
        return rootView;
    }

    void doPermissionWork() {
        try {
            bankDetail = new JSONObject(getArguments().getString("ifscDetail"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        retryBtn.setVisibility(View.GONE);
        progressViewDialog = ProgressViewDialog.with(contextd);
        progressViewDialog.setCancelable(false);
        helper = PickerHelper.with(contextd, KYCBankDetailsImageFragment.this);
        // settingCanWrite();
        if ((ContextCompat.checkSelfPermission(contextd, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, 125);
        }
    }

    boolean settingCanWrite() {
        if(true)
        {
            return true;
        }
        boolean settingsCanWrite = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            settingsCanWrite = Settings.System.canWrite(contextd.getApplicationContext());
        }
        if (!settingsCanWrite) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            startActivity(intent);
        }
        //return true;
        return settingsCanWrite;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 125) {
            easyWayLocation = new EasyWayLocation(contextd);
            easyWayLocation.setListener(this);
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBtn: {
                if (imgFileCheque == null) {
                    Common.showToast(contextd, "Please add a picture of Cheque leafe.");
                } else if (imgFileSignature == null) {
                    Common.showToast(contextd, "Please add your signature before proceed");
                } else {
                    progressViewDialog.show();
                    LocalBroadcastManager.getInstance(contextd).registerReceiver(uploadReciever, new IntentFilter("onUploadStatus"));
                    Intent i = new Intent(activity, S3UploadService.class);
                    i.putExtra("userId", userId);
                    i.putExtra("fileType", "bank_details");
                    i.putExtra("file", imgFileCheque);
                    contextd.startService(i);
                }
            }
            break;
            case R.id.chequeImgBtn:
            case R.id.chequeImg:
                if (settingCanWrite()) {
                    type = 1;
                    selectImage();
//                    CropImage.activity()
//                            .setGuidelines(CropImageView.Guidelines.OFF)
//                            .setAllowFlipping(false)
//                            .setAutoZoomEnabled(true)
//                            .setBorderCornerColor(Color.parseColor("#42E598"))
//                            .setBorderLineColor(Color.parseColor("#3CB5B9"))
//                            .setMinCropResultSize(640, 360)
//                            .setAspectRatio(16, 9)
//                            .start(contextd, this);
                }
                break;
            case R.id.signatureImgBtn:
            case R.id.signatureImg:
            case R.id.retryBtn:
                if (settingCanWrite()) {
                    startActivityForResult(new Intent(contextd, SignDrawActivity.class), 103);
                }
                break;
        }
    }
    private Uri generateTimeStampPhotoFileUri() {
        Uri photoFileUri = null;
        File outputDir = getPhotoDirectory();
        if (outputDir != null) {
            File photoFile = new File(outputDir, System.currentTimeMillis() + ".jpg");
            //photoFileUri = Uri.fromFile(photoFile);
            photoFileUri =  FileProvider.getUriForFile(contextd, BuildConfig.APPLICATION_ID + ".provider",photoFile);
        }
        return photoFileUri;
    }

    private File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageStagte = Environment.getExternalStorageState();
        if (externalStorageStagte.equals(Environment.MEDIA_MOUNTED)) {
            File photoDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            outputDir = new File(photoDir, getString(R.string.app_name));
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(contextd, "Failed to create directory " + outputDir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    outputDir = null;
                }
        }
        return outputDir;
    }


    private void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();
                        mHighQualityImageUri = generateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //intent.setClassName("com.android.camera", "com.android.camera.Camera");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isSubmitted", isSubmitted);
        getTargetFragment().onActivityResult(101,
                isSubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED, i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && mHighQualityImageUri != null) {
            Uri resultUri=  mHighQualityImageUri;
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                    bitmap = helper.getResizedBitmap(bitmap, 1920);
                    bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                    chequeBitmap = bitmap;
                    imgFileCheque = helper.saveBitmapToFile(bitmap, "cheque_leaf");
                    Picasso.with(contextd)
                            .load(imgFileCheque)
                            .memoryPolicy(MemoryPolicy.NO_STORE)
                            .into(chequeImg);
                    chequeImgBtn.setVisibility(View.GONE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
            }
         else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {
                Uri resultUri = data.getData();
               try {
                 Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
               bitmap = helper.getResizedBitmap(bitmap, 1920);
               bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
               chequeBitmap = bitmap;
               imgFileCheque = helper.saveBitmapToFile(bitmap, "cheque_leaf");
               Picasso.with(contextd)
                       .load(imgFileCheque)
                       .memoryPolicy(MemoryPolicy.NO_STORE)
                       .into(chequeImg);
               chequeImgBtn.setVisibility(View.GONE);
                } catch (IOException e) {
                    e.printStackTrace();
                }

        } else if (requestCode == 103) {
            if (data.getStringExtra("Sign").equals("done")) {
                imgFileSignature = (File) data.getSerializableExtra("signatureFile");
                signBitmap = helper.fileToBitmap(imgFileSignature);
                signBitmap = helper.getResizedBitmap(signBitmap, 1080);
                hintLbl.setVisibility(View.GONE);
                retryBtn.setVisibility(View.VISIBLE);
                Picasso.with(contextd)
                        .load(imgFileSignature)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .into(signatureImg);
            }
        } else if (requestCode == LOCATION_SETTING_REQUEST_CODE) {
            easyWayLocation.onActivityResult(resultCode);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    BroadcastReceiver uploadReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressViewDialog.dismiss();
            if (intent.getStringExtra("state").equals("COMPLETED")) {
                if (!isChequeUploaded) {
                    isChequeUploaded = true;
                    chequeImgRef = intent.getStringExtra("panRef");
                } else {
                    signatureImgRef = intent.getStringExtra("panRef");
                    isSignatureUploaded = true;
                }
                if (!isSignatureUploaded) {
                    isSignatureUploaded = false;
                    progressViewDialog.show();
                    Intent i = new Intent(contextd, S3UploadService.class);
                    i.putExtra("userId", userId);
                    i.putExtra("fileType", "bank_details");
                    i.putExtra("file", imgFileSignature);
                    contextd.startService(i);
                } else {
                    progressViewDialog.dismiss();
                    setBankDetails();
                }
            } else {
                Common.showToast(contextd, "Upload failed! Try again.");
            }
        }
    };
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en")),
            sdfAlt = new SimpleDateFormat("dd-MM-yyyy", new Locale("en"));
    String dob = "", dobAlt = "", nomniDob = "", nomniDobAlt = "", mobileNo = "", userid = "", cityPlace = "";
    JSONObject bankDetail = null;
    File AofJsonFile;
    double currentLatitude, currentLongitude;

    void createJson() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (chequebase.equals(""))
                    chequebase = Common.getBitmapBase64(chequeBitmap);
                if (signatureBase.equals(""))
                    signatureBase = Common.getBitmapBase64(signBitmap);


                if(userDetail.getData().getDob()!=null) {
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(sdf.parse(userDetail.getData().getDob()));
                        dob = sdf.format(calendar.getTime());
                        dobAlt = sdfAlt.format(calendar.getTime());
//                    calendar.setTime(sdf.parse(userDetail.getData().getNominee_dob()));
//                    nomniDob = sdf.format(calendar.getTime());
//                    nomniDobAlt = sdfAlt.format(calendar.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                mobileNo = Common.getPreferences(contextd, "mobileNo");
                userid = Common.getPreferences(contextd, "userID");
                org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();

                JSONObject obj = new JSONObject();
                try {
                    jsonObject.put("ARN", "ARN-122801");
                    jsonObject.put("EUIN", "E193918");
                    jsonObject.put("Applicant1Name", userDetail.getData().getName());
                    jsonObject.put("Applicant1Dob", dobAlt);

                    jsonObject.put("Applicant1OccupationDetails", userDetail.getData().getOccupation_string().getFRONTEND_OPTION_NAME());
                    jsonObject.put("Applicant1ContactAddress", userDetail.getData().getCa_address());
                    jsonObject.put("Applicant1State", userDetail.getData().getCa_state_string().getFRONTEND_OPTION_NAME());

                    jsonObject.put("Applicant1Country", "INDIA");
                    jsonObject.put("Applicant1City", userDetail.getData().getCa_city());
                    jsonObject.put("Applicant1Pincode", userDetail.getData().getCa_pincode());
                    jsonObject.put("Applicant1Email", userDetail.getData().getEmail());
                    jsonObject.put("Applicant1Mobile", mobileNo);
                    jsonObject.put("Applicant1PanNumber", userDetail.getData().getPencard().toUpperCase());
                    jsonObject.put("Applicant1FathersName", userDetail.getData().getFather_name());
                    jsonObject.put("Applicant1ModeOfHolding", "SINGLE");
                    jsonObject.put("BankMandateAcNo", userDetail.getData().getAccount_number());
                    jsonObject.put("BankMandateAcType", userDetail.getData().getAccount_type_string() + "");
                    jsonObject.put("BankMandateIfscCode", userDetail.getData().getIfsc_code());
                    jsonObject.put("BankMandateNameOfBank", bankDetail.getJSONObject("data").getString("name") + "");
                    jsonObject.put("BankMandateBranch", bankDetail.getJSONObject("data").getString("branch") + "");
                    jsonObject.put("BankMandateBankAddress", bankDetail.getJSONObject("data").getString("address") + "");
                    jsonObject.put("BankMandateBankCity", bankDetail.getJSONObject("data").getString("city") + "");
                    jsonObject.put("BankMandateBankState", bankDetail.getJSONObject("data").getString("state") + "");

                    jsonObject.put("NomineeName", userDetail.getData().getNominee_name());
                    jsonObject.put("NomineeRelationship", userDetail.getData().getNominee_relationship());
                    jsonObject.put("NomineeAddress", userDetail.getData().getNominee_address());
                    jsonObject.put("Date", sdfAlt.format(Calendar.getInstance().getTime()));
                    jsonObject.put("Place", cityPlace);
                    jsonObject.put("ChequeBase64", chequebase);
                    jsonObject.put("SignBase64", signatureBase);
                    AofJsonFile = writeToFile(jsonObject);
                    Log.i( "run: ",AofJsonFile.length()+"");
                    progressViewDialog.dismiss();
                    sendJsonFile();
                    AofJsonFile = writeToFile(jsonObject);
                    Log.i( "run: ",AofJsonFile.length()+"");
                    progressViewDialog.dismiss();
                    sendJsonFile();
                } catch (JSONException e) {
                    progressViewDialog.dismiss();
                    e.printStackTrace();
               }
            }
        }, 500);
    }

    File writeToFile(org.json.simple.JSONObject obj) {
        File filesDir = getActivity().getBaseContext().getFilesDir();
        File file = new File(filesDir, "AOFBse.json");
        if(file.exists())
        {
            file.delete();
        }
        FileWriter writer;
        try {
            /*file.createNewFile();
            writer = new FileWriter(file);
            writer.write(obj.toJSONString());
            writer.flush();
            writer.close();*/
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(obj.toJSONString());
            myOutWriter.close();
            fOut.flush();
            fOut.close();
//            FileOutputStream fOut = new FileOutputStream(file);
//            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
//            myOutWriter.append(data);
//            myOutWriter.close();
//            fOut.flush();
//            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            e.printStackTrace();
            file = null;
        }
        return file;
    }

    void setBankDetails() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_BankDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("ADDBANK");
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("account_type", getArguments().getString("account_type"));
        requestedServiceDataModel.putQurry("account_holder_name", getArguments().getString("account_holder_name"));
        requestedServiceDataModel.putQurry("account_number", getArguments().getString("account_number"));
        requestedServiceDataModel.putQurry("ifsc_code", getArguments().getString("ifsc_code"));
        requestedServiceDataModel.putQurry("cheque_leaf", chequeImgRef);
        requestedServiceDataModel.putQurry("digital_signature", signatureImgRef);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(MyApplication.getContext()));
        requestedServiceDataModel.putQurry("kyc_status", "PENDING");
        requestedServiceDataModel.putQurry("pan_verify", Common.getPreferences(getActivity() ,"kyc_status"));
        try {
            requestedServiceDataModel.putQurry("bank_id", bankDetail.getJSONObject("data").getString("bank_id") + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void sendJsonFile() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.KYCUploadJson);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putFiles("json", AofJsonFile);
        requestedServiceDataModel.setWebServiceType("json-upload");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_BankDetails:
                Common.showToast(contextd, message);
        //        Log.e("setBankDetails", json);
                LocalBroadcastManager.getInstance(contextd).unregisterReceiver(uploadReciever);
                userDetail = new Gson().fromJson(json, ItemUserDetail.class);
                createJson();
                break;
            case ResponseType.KYCUploadJson:
                Log.e("uploadJson", json);
                isSubmitted = true;
               getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_BankDetails:
                Common.showToast(contextd, message);
           //     Log.e("setBankDetails", json);
                break;
            case ResponseType.KYCUploadJson:
           //     Log.e("uploadJson", json);
                break;
        }
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
        currentLatitude = easyWayLocation.getLatitude();
        currentLongitude = easyWayLocation.getLongitude();
        getLocationAddress();
    }

    @Override
    public void onPositionChanged() {
        if (currentLatitude == 0.0f || currentLongitude == 0.0f) {
            currentLatitude = easyWayLocation.getLatitude();
            currentLongitude = easyWayLocation.getLongitude();
            getLocationAddress();
        }
    }

    @Override
    public void locationCancelled() {

    }

    @Override
    public void onResume() {
        super.onResume();
        // make the device update its location
        if (easyWayLocation != null)
            easyWayLocation.beginUpdates();
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        if (easyWayLocation != null)
            easyWayLocation.endUpdates();
        super.onPause();
    }

    void getLocationAddress() {
        LocationAddress locationAddress = new LocationAddress();
        locationAddress.getAddressFromLocation(currentLatitude, currentLongitude,
                contextd.getApplicationContext(), new GeocoderHandler());

    }

    class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            cityPlace = locationAddress;
            Log.e("cityPlace", cityPlace);
        }
    }
}
