package com.niivo.com.niivo.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentError extends AppCompatActivity implements ResponseDelegate {
    TextView thanks_text;
    Button tryAgainBtn,BackToHome,backexits;
    String amount,schemeName;
    private RequestedServiceDataModel requestedServiceDataModel;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_error);
        thanks_text = (TextView)findViewById(R.id.thanks_text);
        tryAgainBtn=(Button)findViewById(R.id.tryAgainBtn);
        BackToHome=(Button)findViewById(R.id.backToHome);

        backexits=(Button)findViewById(R.id.backexits);


        thankyou();
        tryAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryagain();
            }
        });
        BackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentError.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    void thankyou() {
        requestedServiceDataModel = new RequestedServiceDataModel(this,this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-payment.php");
        baseRequestData.setTag(ResponseType.ThankYou);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("orderid",getIntent().getStringExtra("orderid"));
        requestedServiceDataModel.setWebServiceType("ThankYou");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    void tryagain() {
        requestedServiceDataModel = new RequestedServiceDataModel(this,this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-payment.php");
        baseRequestData.setTag(ResponseType.TryAgain);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("orderid",getIntent().getStringExtra("orderid"));//"76453904"),78328589;//getArguments().getString("orderno"));
        requestedServiceDataModel.setWebServiceType("REORDER");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {

        switch (baseRequestData.getTag()) {
            case ResponseType.ThankYou:
                try {
         //           Log.d("Thankyou json PE", json);
                    JSONObject jsonObject = new JSONObject(json);
                    amount = jsonObject.getJSONObject("data").getString("amount");
                    schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
                    String str = "Oops! There seems to be problem in your payment of Rs."+amount+" in "+schemeName+".";
                    thanks_text.setText(str);
                //    Log.i("Url Response", "onSuccess: "+str);

                    }catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.TryAgain:
                Intent i = new Intent(PaymentError.this, PaymentHelperActivity.class);
                i.putExtra("orderid",getIntent().getStringExtra("orderid"));
                i.putExtra("rawHtml",message);
                startActivityForResult(i, 113);
                //  startActivityForResult(new Intent(contextd, PaymentHelperActivity.class).putExtra("rawHtml", message), 113);
                break;
//                    Log.d("TryAgain json", json);
//                    JSONObject jsonObject = new JSONObject(json);
//                    Intent i = new Intent(PaymentError.this,PaymentHelperActivity.class);
//                    i.putExtra("rawHtml",message);
//                    startActivityForResult(i, 113);
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ThankYou:
           //     Log.d("Failler ThankYou json", json);
            break;
            case ResponseType.TryAgain:
       //         Log.d("Failler  TryAgain json", json);
                break;
        }
    }
}
