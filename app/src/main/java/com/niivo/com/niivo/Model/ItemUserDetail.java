package com.niivo.com.niivo.Model;

import java.io.Serializable;

/**
 * Created by deepak on 2/10/18.
 */

public class ItemUserDetail implements Serializable {
    public ItemUserDetail() {
    }

    String status, msg;
    UserDetail data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserDetail getData() {
        return data;
    }

    public void setData(UserDetail data) {
        this.data = data;
    }

    public class UserDetail implements Serializable {
        public UserDetail() {
        }

        String userid, dtdate, mobile, password,
                pancard_type, pencard, name, dob,
                entity_type, father_name, pancard_image, otp,
                otp_verify, status, kyc_status, last_login,
                verification_step, image_upload_status, image_upload_msg, aof_json,
                advisor_id, default_lang, ekyc_steps, residency_type,
                ca_pincode, ca_address, ca_city, ca_state,
                ca_country, pa_pincode, pa_address, pa_city,
                pa_state, pa_country, email, address_proof_type,
                front_image, back_image, birth_country, birth_place,
                address_type, gross_annual_income, occupation, politically_exposed_person,
                tax_residency_country, wealth_source, photo, marital_status,
                no_of_children, gender, nationality, country,
                tpin, nominee_name, nominee_dob, nominee_relationship,
                nominee_address, account_type, account_holder_name, account_number,
                ifsc_code, cheque_leaf, digital_signature, wealthSourceCode,
                occupationType, account_type_string,is_mendate,mandate_limit,mandate_id;
        StateDetail ca_state_string;
        OccupationDetail occupation_string;

        public String getIs_mendate() {
            return is_mendate;
        }

        public void setIs_mendate(String is_mendate) {
            this.is_mendate = is_mendate;
        }

        public String getMandate_limit() {
            return mandate_limit;
        }

        public void setMandate_limit(String mandate_limit) {
            this.mandate_limit = mandate_limit;
        }

        public String getMandate_id() {
            return mandate_id;
        }

        public void setMandate_id(String mandate_id) {
            this.mandate_id = mandate_id;
        }

        public String getAccount_type_string() {
            return account_type_string;
        }

        public void setAccount_type_string(String account_type_string) {
            this.account_type_string = account_type_string;
        }

        public StateDetail getCa_state_string() {
            return ca_state_string;
        }

        public void setCa_state_string(StateDetail ca_state_string) {
            this.ca_state_string = ca_state_string;
        }

        public OccupationDetail getOccupation_string() {
            return occupation_string;
        }

        public void setOccupation_string(OccupationDetail occupation_string) {
            this.occupation_string = occupation_string;
        }



        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPancard_type() {
            return pancard_type;
        }

        public void setPancard_type(String pancard_type) {
            this.pancard_type = pancard_type;
        }

        public String getPencard() {
            return pencard;
        }

        public void setPencard(String pencard) {
            this.pencard = pencard;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getEntity_type() {
            return entity_type;
        }

        public void setEntity_type(String entity_type) {
            this.entity_type = entity_type;
        }

        public String getFather_name() {
            return father_name;
        }

        public void setFather_name(String father_name) {
            this.father_name = father_name;
        }

        public String getPancard_image() {
            return pancard_image;
        }

        public void setPancard_image(String pancard_image) {
            this.pancard_image = pancard_image;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getOtp_verify() {
            return otp_verify;
        }

        public void setOtp_verify(String otp_verify) {
            this.otp_verify = otp_verify;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getKyc_status() {
            return kyc_status;
        }

        public void setKyc_status(String kyc_status) {
            this.kyc_status = kyc_status;
        }

        public String getLast_login() {
            return last_login;
        }

        public void setLast_login(String last_login) {
            this.last_login = last_login;
        }

        public String getVerification_step() {
            return verification_step;
        }

        public void setVerification_step(String verification_step) {
            this.verification_step = verification_step;
        }

        public String getImage_upload_status() {
            return image_upload_status;
        }

        public void setImage_upload_status(String image_upload_status) {
            this.image_upload_status = image_upload_status;
        }

        public String getImage_upload_msg() {
            return image_upload_msg;
        }

        public void setImage_upload_msg(String image_upload_msg) {
            this.image_upload_msg = image_upload_msg;
        }

        public String getAof_json() {
            return aof_json;
        }

        public void setAof_json(String aof_json) {
            this.aof_json = aof_json;
        }

        public String getAdvisor_id() {
            return advisor_id;
        }

        public void setAdvisor_id(String advisor_id) {
            this.advisor_id = advisor_id;
        }

        public String getDefault_lang() {
            return default_lang;
        }

        public void setDefault_lang(String default_lang) {
            this.default_lang = default_lang;
        }

        public String getEkyc_steps() {
            return ekyc_steps;
        }

        public void setEkyc_steps(String ekyc_steps) {
            this.ekyc_steps = ekyc_steps;
        }

        public String getResidency_type() {
            return residency_type;
        }

        public void setResidency_type(String residency_type) {
            this.residency_type = residency_type;
        }

        public String getCa_pincode() {
            return ca_pincode;
        }

        public void setCa_pincode(String ca_pincode) {
            this.ca_pincode = ca_pincode;
        }

        public String getCa_address() {
            return ca_address;
        }

        public void setCa_address(String ca_address) {
            this.ca_address = ca_address;
        }

        public String getCa_city() {
            return ca_city;
        }

        public void setCa_city(String ca_city) {
            this.ca_city = ca_city;
        }

        public String getCa_state() {
            return ca_state;
        }

        public void setCa_state(String ca_state) {
            this.ca_state = ca_state;
        }

        public String getCa_country() {
            return ca_country;
        }

        public void setCa_country(String ca_country) {
            this.ca_country = ca_country;
        }

        public String getPa_pincode() {
            return pa_pincode;
        }

        public void setPa_pincode(String pa_pincode) {
            this.pa_pincode = pa_pincode;
        }

        public String getPa_address() {
            return pa_address;
        }

        public void setPa_address(String pa_address) {
            this.pa_address = pa_address;
        }

        public String getPa_city() {
            return pa_city;
        }

        public void setPa_city(String pa_city) {
            this.pa_city = pa_city;
        }

        public String getPa_state() {
            return pa_state;
        }

        public void setPa_state(String pa_state) {
            this.pa_state = pa_state;
        }

        public String getPa_country() {
            return pa_country;
        }

        public void setPa_country(String pa_country) {
            this.pa_country = pa_country;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress_proof_type() {
            return address_proof_type;
        }

        public void setAddress_proof_type(String address_proof_type) {
            this.address_proof_type = address_proof_type;
        }

        public String getFront_image() {
            return front_image;
        }

        public void setFront_image(String front_image) {
            this.front_image = front_image;
        }

        public String getBack_image() {
            return back_image;
        }

        public void setBack_image(String back_image) {
            this.back_image = back_image;
        }

        public String getBirth_country() {
            return birth_country;
        }

        public void setBirth_country(String birth_country) {
            this.birth_country = birth_country;
        }

        public String getBirth_place() {
            return birth_place;
        }

        public void setBirth_place(String birth_place) {
            this.birth_place = birth_place;
        }

        public String getAddress_type() {
            return address_type;
        }

        public void setAddress_type(String address_type) {
            this.address_type = address_type;
        }

        public String getGross_annual_income() {
            return gross_annual_income;
        }

        public void setGross_annual_income(String gross_annual_income) {
            this.gross_annual_income = gross_annual_income;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getPolitically_exposed_person() {
            return politically_exposed_person;
        }

        public void setPolitically_exposed_person(String politically_exposed_person) {
            this.politically_exposed_person = politically_exposed_person;
        }

        public String getTax_residency_country() {
            return tax_residency_country;
        }

        public void setTax_residency_country(String tax_residency_country) {
            this.tax_residency_country = tax_residency_country;
        }

        public String getWealth_source() {
            return wealth_source;
        }

        public void setWealth_source(String wealth_source) {
            this.wealth_source = wealth_source;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getMarital_status() {
            return marital_status;
        }

        public void setMarital_status(String marital_status) {
            this.marital_status = marital_status;
        }

        public String getNo_of_children() {
            return no_of_children;
        }

        public void setNo_of_children(String no_of_children) {
            this.no_of_children = no_of_children;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getTpin() {
            return tpin;
        }

        public void setTpin(String tpin) {
            this.tpin = tpin;
        }

        public String getNominee_name() {
            return nominee_name;
        }

        public void setNominee_name(String nominee_name) {
            this.nominee_name = nominee_name;
        }

        public String getNominee_dob() {
            return nominee_dob;
        }

        public void setNominee_dob(String nominee_dob) {
            this.nominee_dob = nominee_dob;
        }

        public String getNominee_relationship() {
            return nominee_relationship;
        }

        public void setNominee_relationship(String nominee_relationship) {
            this.nominee_relationship = nominee_relationship;
        }

        public String getNominee_address() {
            return nominee_address;
        }

        public void setNominee_address(String nominee_address) {
            this.nominee_address = nominee_address;
        }

        public String getAccount_type() {
            return account_type;
        }

        public void setAccount_type(String account_type) {
            this.account_type = account_type;
        }

        public String getAccount_holder_name() {
            return account_holder_name;
        }

        public void setAccount_holder_name(String account_holder_name) {
            this.account_holder_name = account_holder_name;
        }

        public String getAccount_number() {
            return account_number;
        }

        public void setAccount_number(String account_number) {
            this.account_number = account_number;
        }

        public String getIfsc_code() {
            return ifsc_code;
        }

        public void setIfsc_code(String ifsc_code) {
            this.ifsc_code = ifsc_code;
        }

        public String getCheque_leaf() {
            return cheque_leaf;
        }

        public void setCheque_leaf(String cheque_leaf) {
            this.cheque_leaf = cheque_leaf;
        }

        public String getDigital_signature() {
            return digital_signature;
        }

        public void setDigital_signature(String digital_signature) {
            this.digital_signature = digital_signature;
        }

        public String getWealthSourceCode() {
            return wealthSourceCode;
        }

        public void setWealthSourceCode(String wealthSourceCode) {
            this.wealthSourceCode = wealthSourceCode;
        }

        public String getOccupationType() {
            return occupationType;
        }

        public void setOccupationType(String occupationType) {
            this.occupationType = occupationType;
        }
    }

    public class OccupationDetail implements Serializable {
        public OccupationDetail() {
        }

        String FRONTEND_OPTION_NAME, NIIVO_CODE, UCC_CODE, FATCA_CODE, FATCA_TYPE, BSE_IMAGE_CODE, CVL_CODE, PROFILE_SCORE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getUCC_CODE() {
            return UCC_CODE;
        }

        public void setUCC_CODE(String UCC_CODE) {
            this.UCC_CODE = UCC_CODE;
        }

        public String getFATCA_CODE() {
            return FATCA_CODE;
        }

        public void setFATCA_CODE(String FATCA_CODE) {
            this.FATCA_CODE = FATCA_CODE;
        }

        public String getFATCA_TYPE() {
            return FATCA_TYPE;
        }

        public void setFATCA_TYPE(String FATCA_TYPE) {
            this.FATCA_TYPE = FATCA_TYPE;
        }

        public String getBSE_IMAGE_CODE() {
            return BSE_IMAGE_CODE;
        }

        public void setBSE_IMAGE_CODE(String BSE_IMAGE_CODE) {
            this.BSE_IMAGE_CODE = BSE_IMAGE_CODE;
        }

        public String getCVL_CODE() {
            return CVL_CODE;
        }

        public void setCVL_CODE(String CVL_CODE) {
            this.CVL_CODE = CVL_CODE;
        }

        public String getPROFILE_SCORE() {
            return PROFILE_SCORE;
        }

        public void setPROFILE_SCORE(String PROFILE_SCORE) {
            this.PROFILE_SCORE = PROFILE_SCORE;
        }
    }

    public class StateDetail implements Serializable {
        public StateDetail() {
        }

        String FRONTEND_OPTION_NAME, NIIVO_CODE, UCC_CODE, CVL_CODE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getUCC_CODE() {
            return UCC_CODE;
        }

        public void setUCC_CODE(String UCC_CODE) {
            this.UCC_CODE = UCC_CODE;
        }

        public String getCVL_CODE() {
            return CVL_CODE;
        }

        public void setCVL_CODE(String CVL_CODE) {
            this.CVL_CODE = CVL_CODE;
        }
    }
}
