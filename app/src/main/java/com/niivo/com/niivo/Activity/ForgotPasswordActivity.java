package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.ImageView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;
import com.niivo.com.niivo.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements ResponseDelegate {
    TextInputEditText phoneNumber;
    ImageView logo;
    RightGravityTextInputLayout phoneTIL;

    private RequestedServiceDataModel requestedServiceDataModel;
    String otp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(ForgotPasswordActivity.this, Common.getLanguage(ForgotPasswordActivity.this), false);
        setContentView(R.layout.activity_forgot_password);
        logo = (ImageView) findViewById(R.id.logo);
        Button signUp = (Button) findViewById(R.id.button_send);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                   // if (Utility.checkSMSPermission(ForgotPasswordActivity.this)) {
                        forgotPassword();
                     //else {
                       // Common.showToast(ForgotPasswordActivity.this, ForgotPasswordActivity.this.getResources().getString(R.string.permisionNotGranted));
                    //}
                }


            }
        });

        logo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        phoneNumber = (TextInputEditText) findViewById(R.id.signup_phone_number);
        phoneTIL = (RightGravityTextInputLayout) findViewById(R.id.phoneTIL);
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                phoneTIL.setErrorEnabled(false);
                phoneTIL.setError(null);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.READ_SMS:
               // if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    forgotPassword();
               // } else {
                    //code for deny
             //   }
                break;
        }
    }

    boolean validate() {
        boolean b = false;
        if (phoneNumber.getText().toString().trim().equals("")) {
            b = false;
            phoneTIL.setError(ForgotPasswordActivity.this.getResources().getString(R.string.requiredField));
        } else {
            if (phoneNumber.getText().toString().trim().length() != 10) {
                phoneTIL.setError(ForgotPasswordActivity.this.getResources().getString(R.string.invalidPhoneNumber));
                b = false;
            } else
                b = true;
        }
        return b;
    }

    void forgotPassword() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ForgotPass);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", phoneNumber.getText().toString().trim());
      //  requestedServiceDataModel.putQurry("mobile", phoneNumber.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("FORGOTPASS");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ForgotPass:
         //       Log.d("res", json);
                Common.SetPreferences(this, "mobile", phoneNumber.getText().toString().trim());
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    otp = jsonObject.getJSONObject("data").getString("otp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                SignUpActivity.otpWhere = "forgot";
                Intent intent = new Intent(ForgotPasswordActivity.this, ForgotPassOTPActivity.class).putExtra("otp", otp);
                startActivity(intent);
                Common.showToast(this, message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ForgotPass:
                Common.showToast(ForgotPasswordActivity.this,
                        ForgotPasswordActivity.this.getResources().getString(R.string.mobile_no_not_exist));
                break;
        }
    }
}
