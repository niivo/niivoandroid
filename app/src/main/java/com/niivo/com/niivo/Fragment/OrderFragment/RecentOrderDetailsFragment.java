package com.niivo.com.niivo.Fragment.OrderFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecentOrderDetailsFragment extends Fragment {

    Context contextd;
    TextView detailAmountInvested, detailInvestmentDate, tv_orderno, tv_ordertype, detailSchemeName;
    MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.recent_order_details, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.myInvestment));
        activity = (MainActivity) getActivity();
        detailAmountInvested = (TextView) rootView.findViewById(R.id.detailAmountInvested);
        tv_orderno = (TextView) rootView.findViewById(R.id.tv_orderno);
        detailInvestmentDate = (TextView) rootView.findViewById(R.id.detailInvestmentDate);
        tv_ordertype = (TextView) rootView.findViewById(R.id.tv_ordertype);
        detailSchemeName = (TextView) rootView.findViewById(R.id.detailSchemeName);

        detailAmountInvested.setText(Common.currencyString(getArguments().getString("detailAmountInvested"), false));
     //   detailInvestmentDate.setText(getArguments().getString("detailInvestmentDate"));

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr=getArguments().getString("detailInvestmentDate");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        detailInvestmentDate.setText(outputDateStr);

        tv_orderno.setText(getArguments().getString("tv_orderno"));
        tv_ordertype.setText(getArguments().getString("tv_ordertype"));
        detailSchemeName.setText(getArguments().getString("detailSchemeName"));
        return rootView;
    }
}
