package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 28/9/18.
 */

public class ItemSalaryDependent {
    String FRONTEND_OPTION_VALUE, NIIVO_CODE, PROFILE_SCORE;

    public ItemSalaryDependent(String FRONTEND_OPTION_VALUE, String NIIVO_CODE, String PROFILE_SCORE) {
        this.FRONTEND_OPTION_VALUE = FRONTEND_OPTION_VALUE;
        this.NIIVO_CODE = NIIVO_CODE;
        this.PROFILE_SCORE = PROFILE_SCORE;
    }

    public String getFRONTEND_OPTION_VALUE() {
        return FRONTEND_OPTION_VALUE;
    }

    public void setFRONTEND_OPTION_VALUE(String FRONTEND_OPTION_VALUE) {
        this.FRONTEND_OPTION_VALUE = FRONTEND_OPTION_VALUE;
    }

    public String getNIIVO_CODE() {
        return NIIVO_CODE;
    }

    public void setNIIVO_CODE(String NIIVO_CODE) {
        this.NIIVO_CODE = NIIVO_CODE;
    }

    public String getPROFILE_SCORE() {
        return PROFILE_SCORE;
    }

    public void setPROFILE_SCORE(String PROFILE_SCORE) {
        this.PROFILE_SCORE = PROFILE_SCORE;
    }
}
