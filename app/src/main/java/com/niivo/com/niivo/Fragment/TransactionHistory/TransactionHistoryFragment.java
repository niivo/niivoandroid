package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.niivo.com.niivo.R;

/**
 * Created by deepak on 7/2/18.
 */

public class TransactionHistoryFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    TextView title;
    //non ui vars
    Context contextd;
    String type = "";
    View rootView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_transaction, container, false);
            contextd = getActivity();
            tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
            viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
            viewPager.setOffscreenPageLimit(1);
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
            icon_navi.setVisibility(View.VISIBLE);
            title = (TextView) toolbar.findViewById(R.id.toolbar_title);
            title.setText(contextd.getResources().getString(R.string.transactions));
            setScreenInfo();
        }
        return rootView;
    }

    void setScreenInfo() {
        setTabs();
        viewPager.setAdapter(new PagerAdapter(this.getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        if (type.equals("sip")) {
            title.setText(contextd.getResources().getString(R.string.sipInstallments));
            tabLayout.getTabAt(1).select();
            //call SIP API here for upcoming
        } else if (type.equals("history")) {
            title.setText(contextd.getResources().getString(R.string.transactions));
            tabLayout.getTabAt(0).select();
            //call History API for past
        }
    }

    void setTabs() {
        for (int i = 0; i < 4; i++) {
            tabLayout.addTab(tabLayout.newTab(), i);
            View tabView = LayoutInflater.from(contextd).inflate(R.layout.item_tab, null);
            TextView tabText = (TextView) tabView.findViewById(R.id.tabText);
            if (i == 0) {
                tabLayout.getTabAt(i).setText(R.string.purchases);
            } else if (i == 1) {
                tabLayout.getTabAt(i).setText(R.string.pending_purchase);
            } else if (i == 2) {
                tabLayout.getTabAt(i).setText(R.string.pending_withdrawls);
            } else if (i == 3) {
                tabLayout.getTabAt(i).setText(R.string.withDrawals);
            }
//            tabLayout.getTabAt(i).setCustomView(tabView);
        }
    }


    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Bundle b = new Bundle();
                b.putAll(TransactionHistoryFragment.this.getArguments());
                b.putString("orderStatus", "CONFIRM");
                b.putString("paymentStatus", "1");
                return TransactionListFragment.getInstance(b);
            } else if (position == 1) {
                Bundle b2 = new Bundle();
                b2.putAll(TransactionHistoryFragment.this.getArguments());
                b2.putString("orderStatus", "CONFIRM");
                b2.putString("paymentStatus", "-1");
                return TransactionListFragment.getInstance(b2);
            } else if (position == 2) {
                Bundle b = new Bundle();
                b.putAll(TransactionHistoryFragment.this.getArguments());
                b.putString("orderStatus", "pendingWithdraw");
                return TransactionListFragment.getInstance(b);
            } else if (position == 3) {
                Bundle b = new Bundle();
                b.putAll(TransactionHistoryFragment.this.getArguments());
                b.putString("orderStatus", "withdraw");
                return TransactionListFragment.getInstance(b);
            } else return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            if (i == 0) {
                return contextd.getResources().getString(R.string.purchases);
            } else if (i == 1) {
                return contextd.getResources().getString(R.string.pending_purchase);
            } else if (i == 2) {
                return contextd.getResources().getString(R.string.pending_withdrawls);
            } else if (i == 3) {
                return contextd.getResources().getString(R.string.withDrawals);
            } else
                return "";
        }
    }

}
