package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

public class SignInActivity extends AppCompatActivity implements ResponseDelegate {
    EditText password;
    TextInputLayout phoneNumberIL, paswordIL;
    TextInputEditText phoneNumber;

    private RequestedServiceDataModel requestedServiceDataModel;
    String deviceToken = "";
    ImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(SignInActivity.this, Common.getLanguage(SignInActivity.this), false);
        setContentView(R.layout.activity_sign_in);
        TextView textView = (TextView) findViewById(R.id.text_forgot);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        textView.setText(Html.fromHtml(SignInActivity.this.getResources().getString(R.string.forgotPassword)));
        TextView signin = (TextView) findViewById(R.id.button_signin_signin);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
//                    if (Common.getConnectivityStatus(SignInActivity.this))
//                    Intent i = new Intent(SignInActivity.this , IPV_CameraActivity.class);
//                    startActivity(i);
                    signIn();
//                    else
//                        Common.showToast(SignInActivity.this, SignInActivity.this.getResources().getString(R.string.checkNetworkConnection));
                }
            }
        });

        TextView signup = (TextView) findViewById(R.id.text_signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        TextView forgotpass = (TextView) findViewById(R.id.text_forgot);
        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        phoneNumber = (TextInputEditText) findViewById(R.id.signin_phone_number);
        password = (EditText) findViewById(R.id.edit_password);
        phoneNumberIL = (TextInputLayout) findViewById(R.id.signup_inputlayout_number);
        paswordIL = (TextInputLayout) findViewById(R.id.signup_inputlayout_pass);
        deviceToken = "1234";
        imgLogo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    phoneNumberIL.setError(null);
                    phoneNumberIL.setErrorEnabled(false);
                }
            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    paswordIL.setError(null);
                    paswordIL.setErrorEnabled(false);
                }
            }
        });
    }


    boolean validate() {
        boolean b = false;
        if (phoneNumber.getText().toString().trim().equals("")) {
            b = false;
            phoneNumberIL.setError(SignInActivity.this.getResources().getString(R.string.requiredField));
        } else if (phoneNumber.getText().toString().trim().length() != 10) {
            phoneNumberIL.setError(SignInActivity.this.getResources().getString(R.string.invalidPhoneNumber));
            b = false;
        } else if (password.getText().toString().trim().equals("")) {
            paswordIL.setError(SignInActivity.this.getResources().getString(R.string.requiredField));
            b = false;
        } else if (password.getText().toString().trim().length() < 6) {
            b = false;
            paswordIL.setError(SignInActivity.this.getResources().getString(R.string.useAtLeast6Digits));
        } else if (password.getText().toString().trim().length() > 16) {
            b = false;
            paswordIL.setError(SignInActivity.this.getResources().getString(R.string.notMoreThan16Digits));
        } else {
            b = true;
        }
        return b;
    }

    void signIn() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.Signin);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", phoneNumber.getText().toString().trim());
        requestedServiceDataModel.putQurry("password", password.getText().toString().trim());
        requestedServiceDataModel.putQurry("device_token", Common.getToken(SignInActivity.this));
        requestedServiceDataModel.putQurry("default_lang", Common.sendLanguage(SignInActivity.this));
        requestedServiceDataModel.setWebServiceType("LOGIN");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.Signin:
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Common.SetPreferences(this, "userID", jsonObject.getJSONObject("data").getString("userid"));
                    Common.SetPreferences(this, "mobileNo", phoneNumber.getText().toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                startActivity(intent);
                finishAffinity();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.Signin:
                Common.showToast(SignInActivity.this,
                        SignInActivity.this.getResources().getString(R.string.mobile_no_possword_incorrect));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.startGetToken();

    }
}
