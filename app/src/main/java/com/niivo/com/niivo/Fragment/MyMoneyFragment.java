package com.niivo.com.niivo.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.InvestmentPlan.TabFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCRegistrationFragment;
import com.niivo.com.niivo.Fragment.TransactionHistory.InvestHistoryFragment;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

/**
 * Created by andro on 22/5/17.
 */

public class MyMoneyFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Activity _activity;
    View parentView;
    ImageButton back;
    LinearLayout upcoming, paidLL,veryTxt;
    TextView totalInvested, totalCurrent, totalEarnings, upcomingInstallment, totalPaid, totalEarningsLbl;
    TextView backTohome, toolbarTitle,investrquest,ragistration,kycstatustextview;
    ImageButton amntInvested;
    SwipeRefreshLayout swipeRefreshLayout;

    //Non ui Vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    boolean validFlag = false, isfirstTime = false;
    JSONObject sipRes = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    String userid;
    public ItemUserDetail userDetail;
    LinearLayout investlayout;

    private static final String SHOWCASE_ID = "Simple Showcase";
    boolean flag=false;
    private GuideView.Builder guidview = new GuideView.Builder(contextd);
    HomeFragment homeFragment;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        if (parentView == null) {
            Common.setLanguage(_activity, Common.getLanguage(_activity), false);
            parentView = inflater.inflate(R.layout.fragment_my_money, null, false);
            contextd = container.getContext();
            activity = (MainActivity) getActivity();
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
            back.setVisibility(View.GONE);
            toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
            toolbarTitle.setText(contextd.getResources().getString(R.string.myMoney));
            investrquest=(TextView) parentView.findViewById(R.id.investrequest);
            amntInvested = (ImageButton) parentView.findViewById(R.id.amntInvested);
            upcoming = (LinearLayout) parentView.findViewById(R.id.upcoming);
            investlayout= (LinearLayout) parentView.findViewById(R.id.investlayout);
            backTohome = (TextView) parentView.findViewById(R.id.backToHome);
            veryTxt = (LinearLayout) parentView.findViewById(R.id.veryTxt);
            totalInvested = (TextView) parentView.findViewById(R.id.totalInvested);
            totalCurrent = (TextView) parentView.findViewById(R.id.totalCurrent);
            totalEarnings = (TextView) parentView.findViewById(R.id.totalEarnings);
            upcomingInstallment = (TextView) parentView.findViewById(R.id.upcomingInstallment);
            totalPaid = (TextView) parentView.findViewById(R.id.totalPaid);
            paidLL = (LinearLayout) parentView.findViewById(R.id.paidBalanceLL);
            totalEarningsLbl = (TextView) parentView.findViewById(R.id.totalEarningsLbl);
            swipeRefreshLayout= (SwipeRefreshLayout) parentView.findViewById(R.id.pullToRefresh);
            swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.textGreen));
            ragistration= (TextView) parentView.findViewById(R.id.ragistration);
            kycstatustextview= (TextView) parentView.findViewById(R.id.kycstatustext);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getProfileStatus(userid);
                    getMonyStats_new(userid);
                    getSip();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
            getMonyStats_new(userid);
            paidLL.setVisibility(View.GONE);
            amntInvested.setOnClickListener(this);
            upcoming.setOnClickListener(this);
            backTohome.setOnClickListener(this);
            if (sipRes != null) {
                Log.d("sipRes....", String.valueOf(sipRes));
                Calendar ct = Calendar.getInstance();
                try {
                    try {
                        ct.setTime(sdf.parse(sipRes.getJSONArray("data").getJSONObject(0).getString("due_date")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                upcomingInstallment.setText(Common.getDateString(ct));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (activity.isMyMoneyFragment())
                getProfileStatus(Common.getPreferences(contextd, "userID"));
        }

        ragistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new KYCRegistrationFragment()).addToBackStack(null).
                        commit();
            }
        });
        investlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new TabFragment()).addToBackStack(null).
                        commit();
            }
        });
        SharedPreferences wmbPreference = PreferenceManager.getDefaultSharedPreferences(contextd);
        boolean isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun)
        {
            // Code to run once
            ShowWelcomestmt();
            flag=true;
            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putBoolean("FIRSTRUN", false);
            //editor.commit();
            editor.apply();
        }
        //ShowWelcomestmt();

        return parentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        back.setVisibility(View.GONE);
        toolbarTitle.setText(contextd.getResources().getString(R.string.myMoney));
        amntInvested.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getWidth());
            }
        });
        if (sipRes != null) {
            Calendar ct = Calendar.getInstance();
            try {
                try {
                    ct.setTime(sdf.parse(sipRes.getJSONArray("data").getJSONObject(0).getString("due_date")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                upcomingInstallment.setText(Common.getDateString(ct));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (activity.isMyMoneyFragment())
            getProfileStatus(Common.getPreferences(contextd, "userID"));
    }

    private void ShowWelcomestmt() {//int viewId, final int type

        guidview = new GuideView.Builder(contextd);
        guidview.setTitle(contextd.getResources().getString(R.string.welcome))
                .setContentText(contextd.getResources().getString(R.string.welcomee))
                .setTargetView(toolbarTitle)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                //.setIndicatorHeight(-5)
                .setDismissType(DismissType.anywhere)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        ShowLaylut();
                    }
                })
                .build()
                .show();
    }


    private void ShowLaylut() {//int viewId, final int type

       guidview = new GuideView.Builder(contextd);
        guidview.setTitle(contextd.getResources().getString(R.string.myMoney))
                .setContentText(contextd.getResources().getString(R.string.investlayout))
                .setTargetView(investlayout)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setIndicatorHeight(80)
                .setDismissType(DismissType.anywhere)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        ShowInvest();
                    }
                })
                .build()
                .show();
    }

    private void ShowInvest() {//int viewId, final int type

        new GuideView.Builder(contextd)
                .setTitle(contextd.getResources().getString(R.string.myInvestment))
                .setContentText(contextd.getResources().getString(R.string.myinvest))
                .setTargetView(amntInvested)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.anywhere)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        ShowInvestButton();
                    }
                })
                .build()
                .show();
    }
    private void ShowInvestButton() {//int viewId, final int type

        new GuideView.Builder(contextd)
                .setTitle(contextd.getResources().getString(R.string.invest))
                .setContentText(contextd.getResources().getString(R.string.investbutton))
                .setTargetView(backTohome)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.anywhere)
                .build()
                .show();
    }
    @Override
    public void onClick(View v) {
        Bundle b;
        Fragment frag;
        UiUtils.hideKeyboard(contextd);
        switch (v.getId()) {
            case R.id.amntInvested:
             //   if (validFlag) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
                prefs.edit().putString("flag", String.valueOf(flag)).commit();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, new TabFragment()).addToBackStack(null).//TabFragment
                            commit();


           //     }
                break;
            case R.id.upcoming:
                if (validFlag) {
                    if (sipRes != null) {

                        b = new Bundle();
                        try {
                            b.putString("type", sipRes.getJSONArray("data").getJSONObject(0).getString("type"));
                            Log.d("type--type",sipRes.getJSONArray("data").getJSONObject(0).getString("type"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        b.putString("sipInst", sipRes.toString());
                        Log.d("sipRes.toString()....",sipRes.toString());
                        frag = new InvestHistoryFragment();
                        frag.setArguments(b);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    } else {
                        Common.showToast(contextd, contextd.getResources().getString(R.string.noSipCreated));
                    }
                }
                else {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.noSipCreated));
                }
                break;
            case R.id.backToHome:
                    SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(contextd);
                    prefss.edit().putString("flag", String.valueOf(flag)).commit();
                    flag=false;
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new HomeFragment()).addToBackStack(null).
                        commit();
                break;
        }
    }

    void getProfileStatus(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        if (isfirstTime)
            requestedServiceDataModel.executeWithoutProgressbar();
        else
            requestedServiceDataModel.execute();
    }

    void getMonyStats(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.MyMoney);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("MYMONEY");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        if (isfirstTime)
            requestedServiceDataModel.execute();
        else
            requestedServiceDataModel.execute();
    }

    void getMonyStats_new(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
       requestedServiceDataModel.putQurry("userid", userid);
       // requestedServiceDataModel.putQurry("userid", "10000005");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("HOLDINGAMOUNT");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        if (isfirstTime)
            requestedServiceDataModel.execute();
        else
            requestedServiceDataModel.execute();
    }

    void getSip() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("SIP-DUE-REPORT");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
     //   Log.e("Resholdings2", json);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                activity.userDetail = new Gson().fromJson(json, ItemUserDetail.class);
                try {
                    JSONObject obj = new JSONObject(json);
                     String Kycstatus =obj.getJSONObject("data").getString("kyc_status");
                     prefs.edit().putString("Kycstatuss", Kycstatus).commit();
                    String account_number = obj.getJSONObject("data").getString("account_number");
                    Log.d("account_number...", account_number);
                    prefs.edit().putString("accountNumber", account_number).commit();
                    String kycstatustext = obj.getJSONObject("data").getString("kyc_status");
                    if (!obj.getJSONObject("data").getString("kyc_status").equalsIgnoreCase("NOT STARTED")) {
                        validFlag = true;
                        getMonyStats_new(Common.getPreferences(contextd, "userID"));
                        veryTxt.setVisibility(View.GONE);
                    }else {
                        validFlag = false;
                        kycstatustextview.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
                        veryTxt.setVisibility(View.VISIBLE);
                        ragistration.setVisibility(View.VISIBLE);
                        backTohome.setVisibility(View.VISIBLE);
                    }
                    isfirstTime = true;
                    if (obj.getJSONObject("data").getString("kyc_status").equalsIgnoreCase("SUBMITTED")) {
                        Log.e("actionView....", activity.navigationView.getMenu().getItem(1).getActionView() + "");
                        activity.navigationView.getMenu().getItem(1).setActionView(R.layout.submitted_kyc_status);
                    } else if (!obj.getJSONObject("data").getString("kyc_status").equalsIgnoreCase("COMPLETE")
                            &&
                            (!obj.getJSONObject("data").getString("kyc_status").equalsIgnoreCase("COMPLETE"))){
                        Log.e("actionView", activity.navigationView.getMenu().getItem(1).getActionView() + "");
                        activity.navigationView.getMenu().getItem(1).setActionView(R.layout.nav_kyc_status);
                    }else {
                        if (activity.navigationView.getMenu().getItem(1).getActionView() != null)
                            activity.navigationView.getMenu().getItem(1).getActionView().setVisibility(View.GONE);
                    }
                    if(kycstatustext.equals("SUBMITTED")){
                        veryTxt.setVisibility(View.VISIBLE);
                        ragistration.setVisibility(View.GONE);
                        ViewGroup.LayoutParams params = kycstatustextview.getLayoutParams();
                        params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        kycstatustextview.setLayoutParams(params);
                        kycstatustextview.setText(contextd.getResources().getString(R.string.kycSubmitfrontpage));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.HoldingList://MyMoney:
              //  Log.e("res", json);
                try {
                    investrquest.setVisibility(View.GONE);
                    JSONObject odt = new JSONObject(json);
                    totalInvested.setText(Common.currencyString(odt.getJSONObject("data").getString("invest"), false));
                    String investment=Common.currencyString(odt.getJSONObject("data").getString("invest"), false);
                    Log.d("investment====",investment);
                    totalCurrent.setText(Common.currencyString((odt.getJSONObject("data").getString("total_holdings").toString().equals("null")
                            ? "0" : odt.getJSONObject("data").getString("total_holdings")), false));

                    float totalEarn = 0;
                    totalEarn = (Float.parseFloat((odt.getJSONObject("data").getString("total_holdings").toString().equals("null") ? "0" : odt.getJSONObject("data").getString("total_holdings")))
                            - Float.parseFloat(odt.getJSONObject("data").getString("invest").trim()));
                    totalEarnings.setText(Common.currencyString(String.format(new Locale("en"), "%.2f", (float) Math.abs(totalEarn)), false));

                    isfirstTime = true;
//                    if (sipRes == null)
               /*     if (odt.getJSONObject("data").getString("amount_paid").equals("0")) {
                        paidLL.setVisibility(View.GONE);
                    } else {
                        paidLL.setVisibility(View.VISIBLE);
                        totalPaid.setText(Common.currencyString(odt.getJSONObject("data").getString("amount_paid"), false));
                    }*/

                    if (Double.parseDouble(totalEarn + "") >= 0) {
                        totalEarningsLbl.setText(R.string.profit);
                    } else {
                        totalEarningsLbl.setText(R.string.loss);
                    }

                    getSip();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.OrderList:
                sipRes = null;
                try {
                    sipRes = new JSONObject(json);
                    Calendar ct = Calendar.getInstance();
                    try {
                        ct.setTime(sdf.parse(sipRes.getJSONArray("data").getJSONObject(0).getString("due_date")));
                        upcomingInstallment.setText(Common.getDateString(ct));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
              //  Log.e("res", json);
           //     Common.showToast(contextd, message);
                break;
            case ResponseType.HoldingList://MyMoney:
               // investrquest.setVisibility(View.VISIBLE);
           //     Log.e("res", json);
//                if (sipRes == null)
                getSip();

         //       Common.showToast(contextd, message);
                break;
            case ResponseType.OrderList:
          //      Common.showToast(contextd, message);
                sipRes = null;
                break;
        }
    }
}

