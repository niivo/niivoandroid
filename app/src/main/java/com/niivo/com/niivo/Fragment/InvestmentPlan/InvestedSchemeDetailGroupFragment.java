package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.MyMoneyAdapter;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 9/6/17.
 */

public class InvestedSchemeDetailGroupFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Context contextd;
    //TextView ammountInvested, investmentValue, totalEarnings, nav, numOfUnit, fundClass, investmentType, paidInstallment, schemeName;
    TextView cancelSIP;
    //,tv_fund;
    //non uivars
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    RecyclerView mymoneyRecyclerview;
    MyMoneyAdapter myMoneyAdapter;
    TextView tv_fund, tv_regDate;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));
    private String order_type;
    // SimpleDateFormat getSDF = new SimpleDateFormat("dd MMM yyyy", new Locale("en"));

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_invested_scheme_detail_group, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mymoneyRecyclerview = (RecyclerView) rootView.findViewById(R.id.investedListRV);
        textView.setText(contextd.getResources().getString(R.string.myMoney));
        activity = (MainActivity) getActivity();
        tv_fund = (TextView) rootView.findViewById(R.id.tv_fund);
        tv_regDate = (TextView) rootView.findViewById(R.id.regDate);
    /*     cancelSIP = (TextView) rootView.findViewById(R.id.cancelSipBtn);

         cancelSIP.setVisibility((!getArguments().getString("orderType").equals("ONETIME") &&
                getArguments().getString("from").equals("direct")) ? View.VISIBLE : View.GONE);
        cancelSIP.setOnClickListener(this);
      */
        setView();
        return rootView;
    }

    private void setView() {
        String regDate = getArguments().getString("regDate");
        String fundName = getArguments().getString("fundName");
        order_type = getArguments().getString("order_type");
        tv_fund.setText(fundName);
        try {
            Calendar ct = Calendar.getInstance();
            ct.setTime(getSDF.parse(regDate));
            tv_regDate.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //  tv_regDate.setText(regDate);

        final OrderedList.OrderedItem orderedItemArrayList = (OrderedList.OrderedItem) getArguments().getSerializable("schemeDetail");
        mymoneyRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        myMoneyAdapter = new MyMoneyAdapter(getActivity(), orderedItemArrayList);
        mymoneyRecyclerview.setAdapter(myMoneyAdapter);
    }

    void setDetails(OrderedList.OrderedItem orderedItemArrayList) {
        // OrderedList.OrderedItem orderedItemArrayList = new Gson().fromJson(json,OrderedList.OrderedItem.class);
        Log.i("setDetails1: ", orderedItemArrayList.getOrderedItemArrayListSub().get(0).getDtdate());
        Log.i("setDetails2: ", orderedItemArrayList.getOrderedItemArrayListSub().get(1).getDtdate());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelSipBtn:
                if (!order_type.equals("ONETIME"))
                    cancelSIP();
                break;
        }
    }

    void cancelSIP(String userid, String orderid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("orderid", orderid.trim());
        requestedServiceDataModel.putQurry("order_type",order_type);
        requestedServiceDataModel.putQurry("is_goal", "0");
        requestedServiceDataModel.putQurry("goal_id", "");
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
           //     Log.e("cancelRes", json);
                whenSuccess(message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    if (jsonObject.has("retry")) {
                        if (jsonObject.getString("retry").trim().equals("1")) {
                            retryLastRequest(message);
                        }
                    } else {
                        Common.showToast(contextd, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void whenSuccess(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.home), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.getBackToPortfolio();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void retryLastRequest(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), getArguments().getString("orderId"));
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void cancelSIP() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(contextd.getResources().getString(R.string.cancelMonthlyBasedSIP));
        builder.setPositiveButton(contextd.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), getArguments().getString("orderId"));
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

}
