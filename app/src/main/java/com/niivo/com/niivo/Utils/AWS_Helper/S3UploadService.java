package com.niivo.com.niivo.Utils.AWS_Helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Region;
import com.niivo.com.niivo.R;

import java.io.File;

import static com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED;
import static com.amazonaws.mobileconnectors.s3.transferutility.TransferState.IN_PROGRESS;

/**
 * Created by deepak on 26/9/18.
 */

public class S3UploadService extends Service {
    AmazonS3Client s3Client;
    TransferUtility transferUtility;
    NotificationCompat.Builder notificationBuilder;
    NotificationManager notificationManager;
    String key = "";

    @Override
    public void onCreate() {
        super.onCreate();
        BasicAWSCredentials credentials = new BasicAWSCredentials(this.getResources().getString(R.string.AWS_accessKey),
                this.getResources().getString(R.string.AWS_secretKey));
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxErrorRetry(3);
        configuration.setConnectionTimeout(501000);
        configuration.setSocketTimeout(501000);
        configuration.setProtocol(Protocol.HTTP);

        s3Client = new AmazonS3Client(credentials, configuration);
        s3Client.setRegion(Region.AP_Mumbai.toAWSRegion());

        transferUtility =
                TransferUtility.builder()
                        .context(this)
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .defaultBucket(this.getResources().getString(R.string.AWS_bucket))
                        .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null&&intent.getStringExtra("userId")!=null) {
            String user_id = intent.getStringExtra("userId") + "";
            final String fileType = intent.getStringExtra("fileType");
            File file = (File) intent.getSerializableExtra("file");
            Log.d("file.....", String.valueOf(file));
            key = user_id + "/" + fileType.toUpperCase() + "/" + file.getName();
            Log.d("key.....", String.valueOf(key));
            key = key.toLowerCase();
            TransferObserver observer = transferUtility.upload(this.getResources().getString(R.string.AWS_bucket),
                    key,
                    file);
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.e("onStateChanged", "id: " + id + "\nTransferState: " + state.toString());
                    if (notificationBuilder == null)
                        setUploadNotification(fileType);
                    else {
                        if (state == IN_PROGRESS) {
                            notificationBuilder.setContentText("Upload " + state.toString())
                                    .setProgress(100, 0, true);
                            notificationManager.notify(121, notificationBuilder.build());
                        } else {
                            notificationManager.cancelAll();
                            notificationBuilder = null;
                            Intent i = new Intent();
                            i.putExtra("state", state == COMPLETED ? "COMPLETED" : "FAILED");
                            i.putExtra("panRef", key);
                            i.setAction("onUploadStatus");
                            LocalBroadcastManager.getInstance(S3UploadService.this).sendBroadcast(i);
                        }
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    int progress = (int) ((double) bytesCurrent * 100 / bytesTotal);
                    Log.e("onProgressChanged", "id: " + id
                            + "\nbytesCurrent: " + bytesCurrent
                            + "\nbytesTotal: " + bytesTotal
                            + "\nbytesCurrent(int): " + (int) bytesCurrent / 1000
                            + "\nbytesTotal(int): " + (int) bytesTotal / 1000
                            + "\npercentage: " + progress);
                    notificationBuilder.setProgress(100, progress, false);
                    notificationManager.notify(121, notificationBuilder.build());
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                }
            });
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    void setUploadNotification(String type) {
        String message = type.equals("PAN_CARD")
                ? "Uploading Pan card image."
                : type.equals("IPV")
                ? "Uploading IPV Video."
                : type.equals("ADDRESS_PROOF")
                ? "Uploading Address Proof image."
                : type.equalsIgnoreCase("profile_picture")
                ? "Uploading Profile picture."
                : "Uploading some file.";

        notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder = notificationBuilder.setAutoCancel(false)
                .setTicker("Niivo")
                .setSmallIcon(R.drawable.notify)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Niivo")
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentText(message);
        notificationBuilder.setOngoing(true);
        notificationBuilder.setProgress(100, 0, false);
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel("Niivo");
            if (mChannel == null) {
                mChannel = new NotificationChannel("Niivo", "Niivo", importance);
                mChannel.setDescription("Niivo");
                mChannel.setImportance(NotificationManager.IMPORTANCE_LOW);
                mChannel.setShowBadge(false);

                notificationManager.createNotificationChannel(mChannel);
            }
            notificationBuilder = notificationBuilder.setChannelId("Niivo");
        }
        notificationManager.notify(121, notificationBuilder.build());
    }
}
