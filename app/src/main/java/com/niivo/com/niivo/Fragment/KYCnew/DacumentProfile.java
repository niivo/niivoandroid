package com.niivo.com.niivo.Fragment.KYCnew;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class DacumentProfile extends Fragment {
    Activity _activity;
    View parentView;
    ImageButton back;
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    TextView toolbarTitle;


//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            _activity = getActivity();
            if (parentView == null) {
                Common.setLanguage(_activity, Common.getLanguage(_activity), false);
                parentView = inflater.inflate(R.layout.fragment_dacument_profile, null, false);
                contextd = container.getContext();
                activity = (MainActivity) getActivity();
                Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
                back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
                back.setVisibility(View.GONE);
                toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
                toolbarTitle.setText(contextd.getResources().getString(R.string.myMoney));
                //      return inflater.inflate(R.layout.fragment_dacument_profile, container, false);


            }
            return parentView;
        }
}
