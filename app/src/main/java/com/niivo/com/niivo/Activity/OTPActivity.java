package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

public class OTPActivity extends AppCompatActivity implements ResponseDelegate {
    public static EditText otp1, otp2, otp3, otp4;
    TextView resendOTP;

    private RequestedServiceDataModel requestedServiceDataModel;
    public static String otp = "";
    String deviceToken = "", mobile = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(OTPActivity.this, Common.getLanguage(OTPActivity.this), false);
        setContentView(R.layout.activity_otp);
        resendOTP = (TextView) findViewById(R.id.text_otp);
        resendOTP.setText(Html.fromHtml(OTPActivity.this.getResources().getString(R.string.resendotp)));
        TextView button_verify = (TextView) findViewById(R.id.button_verify);
        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    otp = otp1.getText().toString().trim() + "" + otp2.getText().toString().trim() + "" + otp3.getText().toString().trim() + "" + otp4.getText().toString().trim() + "";
                    deviceToken = "1234";
                    login();
                }
            }
        });

        otp1 = (EditText) findViewById(R.id.otp1);
        otp2 = (EditText) findViewById(R.id.otp2);
        otp3 = (EditText) findViewById(R.id.otp3);
        otp4 = (EditText) findViewById(R.id.otp4);
        mobile = Common.getPreferences(OTPActivity.this, "mobile");
        Common.SetPreferences(OTPActivity.this, "mobileNo", mobile);
        resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });
        setUpFocusWork();
        setOTP(getIntent().getStringExtra("otp"));
    }

    public static void setOTPsms(String message) {
        Log.e("message", message);
        otp = message.trim();
        otp1.setText(otp.charAt(0) + "");
        otp2.setText(otp.charAt(1) + "");
        otp3.setText(otp.charAt(2) + "");
        otp4.setText(otp.charAt(3) + "");
    }

    boolean validate() {
        boolean b = false;
        if (otp1.getText().toString().trim().equals("") || otp2.getText().toString().trim().equals("") || otp3.getText().toString().trim().equals("") || otp4.getText().toString().trim().equals("")) {
            otp4.setError(OTPActivity.this.getResources().getString(R.string.allFieldsRequired));
            b = false;
        } else {
            b = true;
        }
        return b;
    }

    void setOTP(String otpp) {

    }

    void login() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.VerifyOTP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", mobile);
        requestedServiceDataModel.putQurry("otp", otp);
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.putQurry("device_token",Common.getToken(OTPActivity.this));
        requestedServiceDataModel.putQurry("default_lang", Common.sendLanguage(OTPActivity.this));
        requestedServiceDataModel.setWebServiceType("OTPVERIFY");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void resendOtp() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ResendOTP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", mobile);
        requestedServiceDataModel.setWebServiceType("RESENDOTP");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.VerifyOTP:
           //     Log.d("res", json);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Common.SetPreferences(this, "userID", jsonObject.getJSONObject("data").getString("userid"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(OTPActivity.this, MainActivity.class);
                startActivity(intent);
                finishAffinity();
                break;
            case ResponseType.ResendOTP:
           //     Log.d("res", json);
                Common.showToast(this, message);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    otp = jsonObject.getJSONObject("data").getString("otp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setOTP(otp);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.VerifyOTP:
         //       Log.d("res", json);
                break;
            case ResponseType.ResendOTP:
          //      Log.d("res", json);
                break;
        }
        Common.showToast(this, message);
    }

    void setUpFocusWork() {
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp1.getText().toString().trim().length() > 0) {
                    otp1.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp2.getText().toString().trim().length() > 0) {
                    otp2.clearFocus();
                    otp3.requestFocus();
                }
                if (otp2.getText().toString().trim().equals("")) {
                    otp2.clearFocus();
                    otp1.requestFocus();
                }
            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp3.getText().toString().trim().length() > 0) {
                    otp3.clearFocus();
                    otp4.requestFocus();
                }
                if (otp3.getText().toString().trim().equals("")) {
                    otp3.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp4.getText().toString().trim().equals("")) {
                    otp4.clearFocus();
                    otp3.requestFocus();
                }
            }
        });
    }
}
