package com.niivo.com.niivo.Fragment.Advisor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 9/2/18.
 */

public class SetAdvisorFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    EditText advisorCode;
    TextView submitBtn;

    //Non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_input_advisor_code, container, false);
        contextd = getActivity();
        activity = (MainActivity) getActivity();
        advisorCode = (EditText) rootView.findViewById(R.id.advisorCode);
        advisorCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        submitBtn = (TextView) rootView.findViewById(R.id.submitBtn);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.advisor).toUpperCase());
        ImageButton back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        submitBtn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitBtn:
                if (advisorCode.getText().toString().trim().equals("")) {
                    advisorCode.setError(this.getResources().getString(R.string.requiredField));
                } else {
                    advisorCode.setError(null);
                    setAdvisor(Common.getPreferences(contextd, "userID"), advisorCode.getText().toString());
                }
                break;
        }
    }

    void setAdvisor(String userid, String advisorCode) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-advisor.php");
        baseRequestData.setTag(ResponseType.SetAdvisor);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("user_id", userid);
        requestedServiceDataModel.putQurry("advisor_code", advisorCode);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("SETADVISOR");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.SetAdvisor:
                if (activity.getSupportFragmentManager().getFragments().get(activity.getSupportFragmentManager().getBackStackEntryCount() - 1) instanceof GetAdvisorFragment)
                    activity.getSupportFragmentManager().popBackStack();
                activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new GetAdvisorFragment()).
                        commit();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.SetAdvisor:
                Common.showDialog(contextd,message);
            //    Log.e("setAdvisor", json);
                break;
        }
    }
}
