package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 12/10/17.
 */

public class ItemState {
    String stateName, stateCode;

    public ItemState(String stateName, String stateCode) {
        this.stateName = stateName;
        this.stateCode = stateCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
