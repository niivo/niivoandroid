package com.niivo.com.niivo.Fragment.KYCnew;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 3/10/18.
 */

public class KYCStatusFragment extends Fragment implements ResponseDelegate {
    TextView verify;
    TextView statusMessage, docVerTxt;
    ImageView statusPic;
    ImageButton back;

    //Non ui vars
    Context contextd;
    MainActivity activity;
    RequestedServiceDataModel requestedServiceDataModel;
    Fragment frag = null;
    LinearLayout kycstatuslayout;
    ProgressBar progress_bar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_document_status, container, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.app_name));
        verify = (TextView) rootView.findViewById(R.id.button_verify);
        activity = (MainActivity) getActivity();
        kycstatuslayout= (LinearLayout) rootView.findViewById(R.id.kycstatuslayout);
        progress_bar= (ProgressBar) rootView.findViewById(R.id.progress_bar);

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (frag != null)
                {
                    activity.getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, frag).addToBackStack(null)
                            .commit();
                }
                else
                {
                    activity.getSupportFragmentManager().beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null)
                            .commit();
                }

            }
        });
        statusMessage = (TextView) rootView.findViewById(R.id.statusMessage);
        statusPic = (ImageView) rootView.findViewById(R.id.statusImage);
        docVerTxt = (TextView) rootView.findViewById(R.id.docVerTxt);
        getProfile(Common.getPreferences(activity, "userID"));
//        if (activity.userDetail == null)
//            getProfile(Common.getPreferences(activity, "userID"));
//        else {
//            setUpStatus();
//        }
        return rootView;
    }

    void setUpStatus() {
        if (activity.userDetail.getData().getKyc_status().equals("COMPLETE")){//|| activity.userDetail.getData().getKyc_status().equals("SUBMITTED"))
            statusPic.setImageResource(R.drawable.approved_alert);
            statusPic.setBackground(contextd.getResources().getDrawable(R.drawable.green_oval_shape));
            statusMessage.setText(contextd.getResources().getString(R.string.verified));
            frag = null;
            docVerTxt.setText("");
            verify.setText(contextd.getResources().getString(R.string.backToHome));
        }
//        else if (activity.userDetail.getData().getKyc_status().equals("NOT STARTED")) {
////            statusMessage.setVisibility(View.GONE);
////            statusPic.setVisibility(View.GONE);
////            statusPic.setVisibility(View.GONE);
////            docVerTxt.setVisibility(View.GONE);
////            verify.setVisibility(View.GONE);
//            //frag = new KYCRegistrationFragment();
////            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
////                    .replace(R.id.frame_container, new KYCRegistrationFragment()).addToBackStack(null).
////                    commit();
//           /// getActivity().finish();
//            statusMessage.setText(contextd.getResources().getString(R.string.notVerified));
//            statusPic.setImageResource(R.drawable.pending_alert);
//            statusPic.setBackground(contextd.getResources().getDrawable(R.drawable.red_oval_shape));
//            frag = new KYCRegistrationFragment();
//            docVerTxt.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
//            verify.setText(contextd.getResources().getString(R.string.verifyNow));
//        }
        else if (activity.userDetail.getData().getKyc_status().equals("REJECTED")) {
            statusPic.setImageResource(R.drawable.pending_alert);
            statusPic.setBackground(contextd.getResources().getDrawable(R.drawable.red_oval_shape));
            statusMessage.setText(contextd.getResources().getString(R.string.lastDocumentVerificationNotSuccess));
            verify.setText(contextd.getResources().getString(R.string.verifyNow));
            frag =  null;
        } else if (activity.userDetail.getData().getKyc_status().equals("PENDING")) {
            statusPic.setImageResource(R.drawable.pending_alert);
            statusPic.setBackground(contextd.getResources().getDrawable(R.drawable.green_oval_shape));
            statusMessage.setText(R.string.kyc_verfification_is_submitted);
            docVerTxt.setText("");
            verify.setVisibility(View.GONE);
            frag = null;
        } else if (activity.userDetail.getData().getKyc_status().equals("SUBMITTED")) {//|| activity.userDetail.getData().getKyc_status().equals("SUBMITTED"))
//            statusPic.setImageResource(R.drawable.approved_alert);
//            statusPic.setBackground(contextd.getResources().getDrawable(R.drawable.green_oval_shape));
            statusPic.setVisibility(View.GONE);
            statusMessage.setText(contextd.getResources().getString(R.string.kycSubmit));
            frag = null;
//            docVerTxt.setText("");
            docVerTxt.setVisibility(View.GONE);
            verify.setText(contextd.getResources().getString(R.string.backToHome));
        }
    }
    void notStartKyc(){
        if (activity.userDetail.getData().getKyc_status().equals("NOT STARTED")){
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                   .replace(R.id.frame_container, new KYCRegistrationFragment()).addToBackStack(null).
                    commit();
        }
    }

    void getProfile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(activity, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
         //       Log.e("userProfile", json);
                progress_bar.setVisibility(View.GONE);
                kycstatuslayout.setVisibility(View.VISIBLE);
                activity.userDetail = new Gson().fromJson(json, ItemUserDetail.class);
                setUpStatus();
                //notStartKyc();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
          //      Log.e("userProfile", json);
                break;
        }
    }
}
