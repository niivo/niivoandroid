package com.niivo.com.niivo.Fragment.EMandate;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 10/10/18.
 */

public class EmandateSatatusFragment extends Fragment implements View.OnClickListener {
    View rootView;
    ImageView statusImg;
    TextView statusMessageTv;
    TextView tryAgainBtn;
    //Non ui vars
    Context contextd;
    ItemUserDetail userDetail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_e_mandet_status, container, false);
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            textView.setText(R.string.mandate);
            ImageView back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
            back.setVisibility(View.VISIBLE);
            contextd = container.getContext();
            userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
            statusImg = (ImageView) rootView.findViewById(R.id.statusImg);
            statusMessageTv = (TextView) rootView.findViewById(R.id.statusMessageTV);
            tryAgainBtn = (TextView) rootView.findViewById(R.id.tryAgainBtn);
            tryAgainBtn.setOnClickListener(EmandateSatatusFragment.this);
            setUpStatus();
        }
        return rootView;
    }

    void setUpStatus() {
        if (userDetail.getData().getIs_mendate().equals("VERIFIED")) { //verified and can be edit
            statusImg.setImageResource(R.drawable.check);
            statusMessageTv.setText(String.format(contextd.getResources().getString(R.string.mandate_message), Common.currencyString(userDetail.getData().getMandate_limit(),
                    false, false), Common.currencyString(userDetail.getData().getMandate_limit(),
                    false, false)));
            tryAgainBtn.setText(R.string.edit_e_mandate);
        } else if (userDetail.getData().getIs_mendate().equals("REJECTED")) { //rejected by the admin
            statusImg.setImageResource(R.drawable.error);
            statusMessageTv.setText(String.format(contextd.getResources().getString(R.string.mandate_rejected),  Common.currencyString(userDetail.getData().getMandate_limit(),
                    false, false)));
            tryAgainBtn.setText(R.string.tryAgain);
        } else if (userDetail.getData().getIs_mendate().equals("REGISTERED")) { //Not verified yet.
            statusImg.setImageResource(R.drawable.arrows);
            statusMessageTv.setText(String.format(contextd.getResources().getString(R.string.mandate_registered),  Common.currencyString(userDetail.getData().getMandate_limit(),
                    false, false)));
            tryAgainBtn.setVisibility(View.GONE);
        } else { //Not registered yet.
            statusImg.setVisibility(View.GONE);
            statusMessageTv.setText(R.string.mandate_no_registered);
            tryAgainBtn.setText(R.string.register_e_mandate);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tryAgainBtn:
                if (!userDetail.getData().getIs_mendate().equals("REGISTERED")) {
                    Fragment fragment = new EmandateSelectAmountFragment();
                    fragment.setArguments(getArguments());
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, fragment).addToBackStack(null).
                            commit();
                }
                break;
        }
    }
}
