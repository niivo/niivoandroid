package com.niivo.com.niivo.Requests;

/**
 * Created by user4 on 31/5/16.
 */
public interface ResponseType {

    public static final int VerifyOTP = 101;
    public static final int SIGNUP = 102;
    int ResendOTP = 103;
    int Logout = 104;
    int Signin = 105;
    int ForgotPass = 106;
    int ResetPass = 107;
    int ChangePass = 108;
    int UserProfile = 109;
    int PanCardVerfication = 110;
    int GetFundList = 111;
    int PurchaseFundMonthly = 112;

    int OrderList = 113;
    int AAdharApi = 114;
    int AadharApiResponse = 115;
    int PurchaseFundOneTime = 116;
    int UpdateUSer = 117;
    int GoalFundsList = 118;
    int DoPayment = 119;
    int MyMoney = 120;
    int WithDrawOTP = 121;
    int IFSCVerify = 122;
    int UploadIFSC = 123;
    int GetJson = 124;
    int AddToCart = 125;
    int RemoveFromCart = 126;
    int ChangeLang = 127;
    int GetCartList = 128;
    int EditCartItem = 129;
    int CancelSIP = 130;
    int SetAdvisor = 131;
    int GetAdvisor = 132;
    int RemoveAdvisor = 133;
    int WithDrawReq = 134;
    int GetStates = 135;
    int GetCountry = 136;
    int GetProofType = 137;
    int GetOccupation = 138;
    int GetAnnualIncome = 139;

    int KYC_BasicDetails = 140;
    int KYC_ContactDetails = 141;
    int KYC_IdentityDetails = 142;

    int GetTaxStatuscode = 143;
    int GetWealthSourceCode = 144;

    int KYC_NomineeDetails = 145;

    int GetPEPlist = 146;

    int KYC_BankDetails = 147;
    int KYC_IPV_Details = 148;
    int KYC_Get_PAN_Details = 149;
    int KYCUploadJson = 150;
    int MandateRequest = 151;
    int MandateAuthUrl=152;
    int OrderDetail = 153;
    int HoldingList=154;
    int WithdrawList=155;
    int RecentOrder = 156;
    int Profile = 157;
    int ThankYou = 158;
    int TryAgain = 159;
    int PastSip=160;
    int EquityList=161;
    int HybridList=162;
    int DebtList=163;
    int PancardService=165;
    int InvestmentListSort=166;
    int EquityAscSort=167;
    int HybridAscSort=168;
    int DebtAscSort=169;
    int EquityDscSort=170;
    int HybridDscSort=171;
    int DebtDscSort=172;
    int DscDateSort=173;
    int AllFundAsc=174;
    int AllFundDsc=175;
    int MandateStatus=176;

}
