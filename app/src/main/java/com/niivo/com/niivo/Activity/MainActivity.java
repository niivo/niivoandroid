package com.niivo.com.niivo.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.niivo.com.niivo.Fragment.Advisor.GetAdvisorFragment;
import com.niivo.com.niivo.Fragment.ChangeLanguageFragment;
import com.niivo.com.niivo.Fragment.ChangePasswordFragment;
import com.niivo.com.niivo.Fragment.EMandate.EmandateSatatusFragment;
import com.niivo.com.niivo.Fragment.FAQFragment;
import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCRegistrationFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCStatusFragment;
import com.niivo.com.niivo.Fragment.MyCartFragment;
import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.Fragment.TransactionHistory.ViewTransactionFragment;
import com.niivo.com.niivo.Fragment.UserProfileFragment;
import com.niivo.com.niivo.Fragment.WhyNiivoFragment;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import smartdevelop.ir.eram.showcaseviewlib.GuideView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ResponseDelegate {

    public static final String TAG = "MainActivity";
    private Toolbar toolbar;
    private DrawerLayout drawer;
    public NavigationView navigationView;
    Fragment fragment = null;
    //NonUi Vars
    private RequestedServiceDataModel requestedServiceDataModel;
    String userId = "", deviceToken = "";
    FragmentManager manager;
    public static ImageButton icon_navi,icon_navii;
    public static MainActivity activity;
    public ItemUserDetail userDetail;
  //  private RequestedServiceDataModel requestedServiceDataModel;
    private String orderIdValueForProcessing;
    private String url;
    boolean flag=false;
    String backflag ;
    HomeFragment homeFragment = new HomeFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(MainActivity.this, Common.getLanguage(MainActivity.this), false);
        activity = this;
        setContentView(R.layout.activity_mainactivity);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, new MyMoneyFragment()).
                commit();
        manager = getSupportFragmentManager();
        setToolBar();
        SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(this);
        prefss.edit().putString("flag", String.valueOf(flag)).commit();
        String tmp= prefss.getString("backflag", null);
        if(tmp == null) {
            prefss.edit().putString("backflag", String.valueOf("show")).commit();
        }
    }
    private void getOrderDetailsValues() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderDetail);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(getApplicationContext(), "userID"));
        requestedServiceDataModel.putQurry("order_id:", orderIdValueForProcessing);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETORDERDETAIL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
 //       Log.i("Request", "getOrderDetailsValues: ");
    }

 /*   @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {


        }
    }*/
    @Override
    public void onBackPressed() {
        UiUtils.hideKeyboard(this);
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragmentManager instanceof MyMoneyFragment) {
            if (drawer.isDrawerOpen(Gravity.RIGHT))
                drawer.closeDrawer(Gravity.RIGHT);
            else
                exitDialog(MainActivity.this.getResources().getString(R.string.areYouSureExit));
        }
        if(fragmentManager instanceof HomeFragment) {
            SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(this);
            backflag = prefss.getString("backflag", null);
            if(backflag.equals("hide")) {
                if (this.getSupportFragmentManager().getBackStackEntryCount() >= 1)
                    super.onBackPressed();
                else
                    getBackToHome();
            }
        }
        else {
            if (this.getSupportFragmentManager().getBackStackEntryCount() >= 1)
                super.onBackPressed();
            else
                getBackToHome();
        }
    }
    public void exitDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainActivity.this, R.style.myAlertDialogTheme);
        builder.setTitle(MainActivity.this.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(MainActivity.this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
                MainActivity.this.finish();
            }
        });
        builder.setNegativeButton(MainActivity.this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        navigationView.getMenu().getItem(6).setActionView()
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Menu menu =navigationView.getMenu();
        MenuItem profile = menu.findItem(R.id.profile);
//        if (activity.userDetail.getData().getKyc_status().equals("COMPLETE"))
//        {
//            profile.setVisible(true);
//        }
        // Handle navigation view item clicks here.
        int id = item.getItemId();

         if (id == R.id.profile) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new UserProfileFragment();
        }else if (id == R.id.nav_Home) {
            icon_navi.setVisibility(View.GONE);
            fragment = null;
            getBackToPortfolio();
        }
         else if (id == R.id.documentKYC) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = null;
//            startActivity(new Intent(MainActivity.this,IPV_CameraActivity.class));
//            fragment = new KYC_StatusFragment();
            fragment = new KYCRegistrationFragment();//KYCStatusFragment
        }
         else if (id == R.id.nav_my_cart) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new MyCartFragment();
        } else if (id == R.id.investHome) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new HomeFragment();
        }
//        else  if (id==R.id.order){
//            icon_navi.setVisibility(View.VISIBLE);
//            fragment = new OrderFragment();
//        }
//        else if (id == R.id.nav_withdraw) {
//            icon_navi.setVisibility(View.VISIBLE);
//            fragment = new WithDrawFundList();
//        }
        else if (id == R.id.nav_transaction) {
            icon_navi.setVisibility(View.VISIBLE);
//            Bundle b = new Bundle();
//            b.putString("type", "history");
//            fragment = new InvestHistoryFragment();
            fragment = new ViewTransactionFragment();//ViewAllTransactionFragment();//TransactionHistoryFragment();
//            fragment.setArguments(b);
        }
//        else if (id == R.id.nav_EditBank) {
//          //  if(userDetail.getData().getKyc_status())
//            if (userDetail.getData().getKyc_status().equalsIgnoreCase("complete")) {
//                icon_navi.setVisibility(View.VISIBLE);
//                fragment = new KYCBankDetailsFragment();
//                Bundle b = new Bundle();
//                b.putBoolean("preRegistered", true);
//                b.putSerializable("userDetail", userDetail);
//                fragment.setArguments(b);
//            } else {
//                Common.showDialog(MainActivity.this,
//                        MainActivity.this.getResources().getString(R.string.kyc_process_need_toget_complete));
//            }
//
//        }
        else if (id == R.id.nav_advisor) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new GetAdvisorFragment();
        } else if (id == R.id.nav_e_mandate) {
            if (userDetail.getData().getKyc_status().equalsIgnoreCase("complete")) {
                icon_navi.setVisibility(View.VISIBLE);
                fragment = new EmandateSatatusFragment();
                Bundle b = new Bundle();
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
            } else {
                Common.showDialog(MainActivity.this,
                        MainActivity.this.getResources().getString(R.string.kyc_process_need_toget_complete));
            }
        } else if (id == R.id.nav_chng_lang) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new ChangeLanguageFragment();
        } else if (id == R.id.nav_chng_pass) {
            icon_navi.setVisibility(View.VISIBLE);
            fragment = new ChangePasswordFragment();
        } else if (id == R.id.nav_why_nivo) {
            icon_navi.setVisibility(View.GONE);
            fragment = new WhyNiivoFragment();
        } else if (id == R.id.nav_faq) {
            icon_navi.setVisibility(View.GONE);
            fragment = new FAQFragment();
        } else if (id == R.id.nav_suport) {
            if (fragment instanceof MyMoneyFragment)
                icon_navi.setVisibility(View.GONE);
            fragment = null;
            startActivity(new Intent(MainActivity.this, HelpNSupportActivity.class));
        } else if (id == R.id.nav_logout) {
            fragment = null;
            logoutDialog(MainActivity.this.getResources().getString(R.string.areYouSureLogout));
        }
        if (fragment != null) {
            Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
            for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
                if (!(fragmentManager instanceof MyMoneyFragment)) {
                    getSupportFragmentManager().popBackStack();
                    Log.e("res", i + "");
                }
            }
            UiUtils.hideKeyboard(this);
        }
        Log.e("Info", "UserID: " + Common.getPreferences(MainActivity.this, "userID") + "\nMobileNo: " + Common.getPreferences(MainActivity.this, "mobileNo"));

        if (fragment != null) {
            UiUtils.hideKeyboard(this);
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, fragment).addToBackStack(null).
                    commit();
        }
        drawer.closeDrawer(Gravity.RIGHT);
        return true;
    }

    public void getBackToHome() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof HomeFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getBackToHome", i + "");
            }
        }
        UiUtils.hideKeyboard(this);
    }
    public void goToKyc() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof MyMoneyFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getBackToHome", i + "");
            }
        }
        Fragment frg = new KYCRegistrationFragment();
        Bundle b = new Bundle();
        b.putSerializable("userDetail", userDetail);
        frg.setArguments(b);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frg).addToBackStack(null).
                commit();
        UiUtils.hideKeyboard(this);
    }
    public void goToEmandate() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof MyMoneyFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getBackToHome", i + "");
            }
        }
        Fragment frg = new EmandateSatatusFragment();
        Bundle b = new Bundle();
        b.putSerializable("userDetail", userDetail);
        frg.setArguments(b);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frg).addToBackStack(null).
                commit();
        UiUtils.hideKeyboard(this);
    }

    public boolean isMyMoneyFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof MyMoneyFragment)
            return true;
        else
            return false;
    }

    public void getBackToPortfolio() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof MyMoneyFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getBackToPortfolio", i + "");
            }
        }
        UiUtils.hideKeyboard(this);
    }

    @SuppressLint("LongLogTag")
    public void getBackToHomeAndRemoveFromCart(String cartId) {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof HomeFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getBackToHomeAndRemoveFromCart", i + "");
            }
        }
        UiUtils.hideKeyboard(this);
        removeFromCart(Common.getPreferences(MainActivity.this, "userID"), cartId);
    }

    public void goForIvestMEnt() {
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof MyMoneyFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("goForIvestMEnt", i + "");
            }
        }
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, new HomeFragment()).addToBackStack(null).
                commit();
        UiUtils.hideKeyboard(this);
    }

    public void getDocBackHome(String msg, boolean b) {
        icon_navi.setVisibility(View.VISIBLE);
        Fragment fragmentManager = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!(fragmentManager instanceof MyMoneyFragment)) {
                getSupportFragmentManager().popBackStack();
                Log.e("getDocBackHome", i + "");
            }
        }
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        fragment = new KYCStatusFragment();
        bundle.putString("msg", msg);
        fragment.setArguments(bundle);
        if (fragment != null) {
            icon_navi.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, fragment).addToBackStack(null).
                    commit();
        }
        UiUtils.hideKeyboard(this);
    }

    public void setToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi1);
        ImageButton back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        View headerview = navigationView.getHeaderView(0);
        ImageButton close = (ImageButton) headerview.findViewById(R.id.nav_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
    }

    public void logoutDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainActivity.this, R.style.myAlertDialogTheme);
        builder.setTitle(MainActivity.this.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(MainActivity.this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
                userId = Common.getPreferences(MainActivity.this, "userID");
                deviceToken = "1234";
                logout();
            }
        });

        builder.setNegativeButton(MainActivity.this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    void logout() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.Logout);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(MainActivity.this));
        requestedServiceDataModel.setWebServiceType("LOGOUT");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void removeFromCart(String userid, String cartid) {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.RemoveFromCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("cart_id", cartid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(MainActivity.this));
        requestedServiceDataModel.setWebServiceType("REMOVECART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.Logout:
               // Log.d("res", json);
                getSharedPreferences("prefs_login", Context.MODE_PRIVATE).edit().clear().commit();
                Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(intent);
                finishAffinity();
                break;
            case ResponseType.RemoveFromCart:
              //  Log.e("Removed From Cart", json);
                break;
            case ResponseType.OrderDetail:
                try {
                    JSONObject jsonObject = new JSONObject(json);
                   String amount = jsonObject.getJSONObject("data").getString("amount");
                    String schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
                    String str = "Thank you for investing "+"Rs."+amount+" in "+schemeName;
                  //  Log.i("Url Response", "onSuccess: "+str);
                    //  thanks_text.setText(str);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.Logout:
             //   Log.d("res", json);
                break;
            case ResponseType.RemoveFromCart:
                Common.showToast(this, message);
                break;
        }
    }

}
