package com.niivo.com.niivo.Requests;


import android.util.Log;

import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.MyApplication;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Asus on 13-07-2015.
 */
public class ServiceGenerator {
    // private static RestAdapter.Builder builder = new RestAdapter.Builder().setClient(new OkClient());

    // No need to instantiate this class.
    private ServiceGenerator() {
    }


    public static <S> S createService(Class<S> serviceClass, String baseUrl) {

// set your desired log level
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(2000, TimeUnit.SECONDS);
        httpClient.readTimeout(2000, TimeUnit.SECONDS);
        httpClient.writeTimeout(1000, TimeUnit.SECONDS);
        final String token = MyApplication.getToken();

        if (token != null && !token.equalsIgnoreCase("") && !token.equalsIgnoreCase("0")) {
            Log.e("token", token.toString());

            httpClient.networkInterceptors().add(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("device_token", token)
                            .method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                }
            });
        }

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(baseUrl).build();
        return retrofit.create(serviceClass);
/*        okClientBuilder.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request newRequest;

                newRequest = request.newBuilder()
                        //.addHeader("Authorization", "Bearer " + Common.getPreferences(CribChumApp.getContext(), "token"))
                     //   .addHeader("Accept", "application/json")
                     //   .addHeader("lang", "en")
                        .build();

                return chain.proceed(newRequest);
            }
        });*/
    }


}
