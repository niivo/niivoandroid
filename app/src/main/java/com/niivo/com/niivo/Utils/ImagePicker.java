package com.niivo.com.niivo.Utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.niivo.com.niivo.R;

/**
 * Created by deepak on 17/4/18.
 */

public class ImagePicker {

    public  static  Uri imageUriG=null;
    public static void showPickerDio(final Context contextd, final Fragment frag, final int CAMERArequest, final int GALLERYrequest) {
        CharSequence[] items = {contextd.getResources().getString(R.string.takePhoto), contextd.getResources().getString(R.string.chooseFromLibrary)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
//        if (!message.equals(""))
//            builder.setMessage(message);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            Uri imageUri;

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, "NiivoImage");
                    values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                    try {
                        imageUri = contextd.getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (imageUri != null){
                        imageUriG=imageUri;
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);}
                    frag.startActivityForResult(intent, CAMERArequest);
                } else if (item == 1) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    frag.startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERYrequest);
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
