package com.niivo.com.niivo.Fragment.Advisor;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 9/2/18.
 */

public class GetAdvisorFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView advisorCode, advisorName;
    TextView edit, remove;
    LinearLayout parentView;


    //Non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    String adCode = "", adName = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_remove_advisor, container, false);
        contextd = getActivity();
        activity = (MainActivity) getActivity();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.advisor).toUpperCase());
        ImageButton back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        parentView = (LinearLayout) rootView.findViewById(R.id.parentView);
        back.setVisibility(View.VISIBLE);

        advisorCode = (TextView) rootView.findViewById(R.id.advisorCode);
        advisorName = (TextView) rootView.findViewById(R.id.advisorName);
        edit = (TextView) rootView.findViewById(R.id.editBtn);
        remove = (TextView) rootView.findViewById(R.id.removeBtn);
        edit.setOnClickListener(this);
        remove.setOnClickListener(this);
        getAdvisor(Common.getPreferences(contextd, "userID"));
        return rootView;
    }

    void setDetails() {
        if (!adCode.equals("")) {
            parentView.setVisibility(View.VISIBLE);
            advisorCode.setText(adCode);
            advisorName.setText(adName);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.removeBtn:
                removeDialog();
                break;
            case R.id.editBtn:
                activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new SetAdvisorFragment()).addToBackStack(null).
                        commit();
                break;
        }
    }

    private void removeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(contextd,R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(R.string.sure_to_remove_advisor);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeAdvisor(Common.getPreferences(contextd, "userID"));
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void getAdvisor(String userid) {
        parentView.setVisibility(View.INVISIBLE);
        setDetails();
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-advisor.php");
        baseRequestData.setTag(ResponseType.GetAdvisor);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("user_id", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETUSERADVISOR");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void removeAdvisor(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-advisor.php");
        baseRequestData.setTag(ResponseType.RemoveAdvisor);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("user_id", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("REMOVEADVISOR");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetAdvisor:
           //     Log.e("getAdvisor", json);
                parentView.setVisibility(View.VISIBLE);
                try {
                    JSONObject obj = new JSONObject(json);
                    adCode = obj.getJSONObject("data").getString("code");
                    adName = obj.getJSONObject("data").getString("name").toUpperCase();
                    advisorCode.setText(adCode);
                    advisorName.setText(adName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.RemoveAdvisor:
                activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new SetAdvisorFragment()).
                        commit();
                Common.showDialog(contextd, contextd.getResources().getString(R.string.advisor_removed));
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetAdvisor:
                activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new SetAdvisorFragment()).
                        commit();
                break;
            case ResponseType.RemoveAdvisor:
                break;
        }
    }
}
