package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 25/9/18.
 */

public class ItemOccuptaionList {
    String status, msg;
    ArrayList<Occupation> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<Occupation> getData() {
        return data;
    }

    public void setData(ArrayList<Occupation> data) {
        this.data = data;
    }

    public class Occupation {
        String FRONTEND_OPTION_NAME, NIIVO_CODE, UCC_CODE, FATCA_CODE, FATCA_TYPE, BSE_IMAGE_CODE, CVL_CODE, PROFILE_SCORE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getUCC_CODE() {
            return UCC_CODE;
        }

        public void setUCC_CODE(String UCC_CODE) {
            this.UCC_CODE = UCC_CODE;
        }

        public String getFATCA_CODE() {
            return FATCA_CODE;
        }

        public void setFATCA_CODE(String FATCA_CODE) {
            this.FATCA_CODE = FATCA_CODE;
        }

        public String getFATCA_TYPE() {
            return FATCA_TYPE;
        }

        public void setFATCA_TYPE(String FATCA_TYPE) {
            this.FATCA_TYPE = FATCA_TYPE;
        }

        public String getBSE_IMAGE_CODE() {
            return BSE_IMAGE_CODE;
        }

        public void setBSE_IMAGE_CODE(String BSE_IMAGE_CODE) {
            this.BSE_IMAGE_CODE = BSE_IMAGE_CODE;
        }

        public String getCVL_CODE() {
            return CVL_CODE;
        }

        public void setCVL_CODE(String CVL_CODE) {
            this.CVL_CODE = CVL_CODE;
        }

        public String getPROFILE_SCORE() {
            return PROFILE_SCORE;
        }

        public void setPROFILE_SCORE(String PROFILE_SCORE) {
            this.PROFILE_SCORE = PROFILE_SCORE;
        }
    }
}
