package com.niivo.com.niivo.Utils.NetWorkCheck;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.niivo.com.niivo.Requests.ServerRequest;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by deepak on 23/8/18.
 */

public class NetHelperReciever extends BroadcastReceiver {
    public NetHelperReciever() {
    }

    ServerRequest request;

    public NetHelperReciever(ServerRequest request) {
        this.request = request;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("tryAgainReq", "" + intent.getAction());
        try {
            request.executeRequest();
            context.unregisterReceiver(this);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
