package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 7/6/17.
 */

public class ItemReturns {
    String type, returnName;

    public ItemReturns(String type, String returnName) {
        this.type = type;
        this.returnName = returnName;
    }

    public String getReturnName() {
        return returnName;
    }

    public void setReturnName(String returnName) {
        this.returnName = returnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
