package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.InvestedRVAdapterNew;
import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Model.InvestmentList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import pl.droidsonroids.gif.GifImageView;


/**
 * Created by andro on 22/5/17.
 */
public class InvestedAmountFragment extends Fragment implements ResponseDelegate, InvestedRVAdapterNew.GetClick,PopupMenu.OnMenuItemClickListener {

    Activity _activity;
    View parentView;
    RecyclerView investedListRV;
    //Non UI vars
    private RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    InvestmentList investmentList;
    JSONObject response;
    MainActivity activity;
    String lang = "";
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout sortbutton;
    GifImageView filter;
    ImageView sortasc, sortdsc,changebutton;
    String Sorttemp = "desc";
    String Sorttype = "date";
    String JsonShear;
    LinearLayout button_money_invest;
    TextView changeText,emptymsg;
    Gson gson = new Gson();
    String JsonSort;
    String Kycstatuss;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.tablayoutrecycleview, null, false);
            contextd = container.getContext();
            activity = (MainActivity) getActivity();
            lang = Common.getLanguage(contextd);
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
            icon_navi.setVisibility(View.VISIBLE);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            filter = (GifImageView) parentView.findViewById(R.id.filter);
            textView.setText(contextd.getResources().getString(R.string.Investment));
            investedListRV = (RecyclerView) parentView.findViewById(R.id.investedListRV);
            investedListRV.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            sortbutton = (LinearLayout) parentView.findViewById(R.id.sortbutton);
            sortdsc = (ImageView) parentView.findViewById(R.id.sortdsc);
            changeText=(TextView) parentView.findViewById(R.id.changeText);
            button_money_invest = (LinearLayout) parentView.findViewById(R.id.button_money_invest);
            swipeRefreshLayout = (SwipeRefreshLayout) parentView.findViewById(R.id.pullToRefresh);
            swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.greenLight));
            emptymsg=(TextView) parentView.findViewById(R.id.emptymsg);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getDateSort();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
            button_money_invest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, new HomeFragment()).addToBackStack(null).
                            commit();
                }
            });
            sortdsc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Sorttemp.equals("desc")) {
                        Sorttemp = "asc";
                        sortdsc.setImageResource(R.drawable.up_arrow);
                        if (Sorttype.equals("date")) {
                           getDateAsc();
                        }
                        if (Sorttype.equals("fund")) {
                            getNameAsc();
                        }
                        if (Sorttype.equals("amount")) {
                            getAmountAsc();
                        }
                    } else {
                        Sorttemp = "desc";
                        sortdsc.setImageResource(R.drawable.down_arrow);
                        if (Sorttype.equals("date")) {
                            getDateDsc();
                        }if (Sorttype.equals("fund")) {
                            getNameDsc();
                        }if (Sorttype.equals("amount")) {
                            getAmountDsc();
                        }
                    }
                }
            });

            filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(v);
                }
            });
            sortbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup();
                }
            });
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
            Kycstatuss = prefs.getString("Kycstatuss", null);
            Log.d("Kycstatuss::::",Kycstatuss);

            //investedListRV.setVisibility(View.GONE);
            if(Kycstatuss.equals("NOT STARTED")){
                emptymsg.setVisibility(View.VISIBLE);
                investedListRV.setVisibility(View.GONE);
                emptymsg.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
            }else if(Kycstatuss.equals("SUBMITTED")){
                emptymsg.setVisibility(View.VISIBLE);
                investedListRV.setVisibility(View.GONE);
                emptymsg.setText(contextd.getResources().getString(R.string.kycSubmit));
            }else if(Kycstatuss.equals("COMPLETE")) {
                emptymsg.setVisibility(View.VISIBLE);
                emptymsg.setText(contextd.getResources().getString(R.string.noinvestment));
            }
            getDateSort();
        }

        return parentView;
    }

    public void getNameAsc(){
    try
    {
        sortdsc.setImageResource(R.drawable.up_arrow);
        Sorttemp = "asc";
        JSONObject obj = new JSONObject(JsonShear);
        Log.d("JsonShear", String.valueOf(obj));
        JSONArray array = obj.getJSONArray("data");
        List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            try {
                jsons.add(array.getJSONObject(i));
            }catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        Collections.sort(jsons, new Comparator<JSONObject>() {
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();
                try {
                    valA = String.valueOf(a.get("fundname"));
                    valB = String.valueOf(b.get("fundname"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return valA.compareTo(valB);
            }
        });
        String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
        InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
        investmentList = tempp;
        investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
    } catch(JSONException e)
    {
        e.printStackTrace();
    }
}
    public void getNameDsc(){
        try
        {
            sortdsc.setImageResource(R.drawable.down_arrow);
            Sorttemp = "desc";
            JSONObject obj = new JSONObject(JsonShear);
            JSONArray array = obj.getJSONArray("data");
            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    jsons.add(array.getJSONObject(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();
                    try {
                        valA = String.valueOf(a.get("fundname"));
                        valB = String.valueOf(b.get("fundname"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return valB.compareTo(valA);
                }
            });
            String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
            InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
            investmentList = tempp;
            investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
        } catch(JSONException e)
        { e.printStackTrace(); }

    }
    public void getAmountAsc(){
        try
        {
            sortdsc.setImageResource(R.drawable.up_arrow);
            Sorttemp = "asc";
            JSONObject obj = new JSONObject(JsonShear);
            JSONArray array = obj.getJSONArray("data");
            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    jsons.add(array.getJSONObject(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            Collections.sort(jsons, new Comparator<JSONObject>(){
                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    try {
                        return Double.compare(o1.getDouble("amount"), o2.getDouble("amount"));
                    } catch (JSONException e)
                    {e.printStackTrace();}
                    return 0;
                }
            });
            String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
            InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
            investmentList = tempp;
            investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
        } catch(JSONException e)
        {e.printStackTrace();}
    }
    public void getAmountDsc(){
        try
        {
            sortdsc.setImageResource(R.drawable.down_arrow);
            Sorttemp = "desc";
            JSONObject obj = new JSONObject(JsonShear);
            JSONArray array = obj.getJSONArray("data");
            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    jsons.add(array.getJSONObject(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            Collections.sort(jsons, new Comparator<JSONObject>(){
                @Override
                public int compare(JSONObject o1, JSONObject o2) {
                    try {
                        return Double.compare(o2.getDouble("amount"), o1.getDouble("amount"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            });
            String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
            InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
            investmentList = tempp;
            investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
        } catch(JSONException e)
        {e.printStackTrace();}
    }
    public void  getDateAsc(){
        try
        {
            sortdsc.setImageResource(R.drawable.up_arrow);
            Sorttemp = "asc";
            JSONObject obj = new JSONObject(JsonShear);
            JSONArray array = obj.getJSONArray("data");
            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    jsons.add(array.getJSONObject(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();
                    try {
                        valA = String.valueOf(a.get("dtdate"));
                        valB = String.valueOf(b.get("dtdate"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return valA.compareTo(valB);
                }
            });
            String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
            InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
            investmentList = tempp;
            investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
        } catch(JSONException e)
        {e.printStackTrace();}
    }
    public void  getDateDsc(){
        try
        {
            sortdsc.setImageResource(R.drawable.down_arrow);
            Sorttemp = "desc";
            JSONObject obj = new JSONObject(JsonShear);
            JSONArray array = obj.getJSONArray("data");
            List<JSONObject> jsons = new ArrayList<JSONObject>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    jsons.add(array.getJSONObject(i));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            Collections.sort(jsons, new Comparator<JSONObject>() {
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();
                    try {
                        valA = String.valueOf(a.get("dtdate"));
                        valB = String.valueOf(b.get("dtdate"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return valB.compareTo(valA);
                }
            });
            String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
            InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
            investmentList = tempp;
            investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
        } catch(JSONException e)
        { e.printStackTrace(); }
    }
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(contextd, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.filter_menu);
        popup.show();
    }
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(contextd, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.equity_item:
                getInvestmentEquityFundList();
                sortdsc.setImageResource(R.drawable.down_arrow);
                //Sorttemp = "desc";
                return true;
            case R.id.debt_item:
                getInvestmentDebtFundList();
                sortdsc.setImageResource(R.drawable.down_arrow);
                changeText.setText(getResources().getString(R.string.date));
                //Sorttemp = "desc";
                return true;
            case R.id.hybrid_item:
                getInvestmentHybridFundList();
                sortdsc.setImageResource(R.drawable.down_arrow);
                changeText.setText(getResources().getString(R.string.date));
                //Sorttemp = "desc";
                return true;
            case R.id.all_item:
                getInvestmentFundList();
                sortdsc.setImageResource(R.drawable.down_arrow);
                changeText.setText(getResources().getString(R.string.date));
                //Sorttemp = "desc";
                return true;
            default:
                return false;
        }
    }
    void popup() {
        final Dialog dialog = new Dialog(contextd);
        dialog.setContentView(R.layout.filterlayout);
        dialog.setTitle("Sorting");
        dialog.getWindow().getAttributes().windowAnimations = R.anim.slide_in_left;
        TextView name, amount, date;
        name = (TextView) dialog.findViewById(R.id.fundsort);
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sorttype ="fund";
                getNameAsc();
                changeText.setText(getResources().getString(R.string.fundname));
                dialog.dismiss();
            }
        });
        amount = (TextView) dialog.findViewById(R.id.amountsort);
        amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sorttype = "amount";
                getAmountAsc();
                changeText.setText(getResources().getString(R.string.amount));
                dialog.dismiss();
            }
        });
        date = (TextView) dialog.findViewById(R.id.datesort);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sorttype = "date";
                getDateAsc();
                changeText.setText(getResources().getString(R.string.date));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    void getInvestmentFundList(){
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
     //   requestedServiceDataModel.putQurry("category","all");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getInvestmentEquityFundList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.EquityList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("category","e");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    void getInvestmentDebtFundList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.DebtList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("category","d");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    void getInvestmentHybridFundList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HybridList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("category","h");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getDateSort() {
        sortdsc.setImageResource(R.drawable.up_arrow);
        Sorttemp = "asc";
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.DscDateSort);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
     //   requestedServiceDataModel.putQurry("category","all");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        InvestmentList temp = gson.fromJson(json, InvestmentList.class);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        emptymsg.setVisibility(View.GONE);
        investedListRV.setVisibility(View.VISIBLE);
        switch (baseRequestData.getTag()) {
            case ResponseType.DscDateSort:
                try
                {
                    sortdsc.setImageResource(R.drawable.down_arrow);
                    Sorttemp = "desc";
                    JSONObject obj = new JSONObject(json);
                    JSONArray array = obj.getJSONArray("data");
                    List<JSONObject> jsons = new ArrayList<JSONObject>();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            jsons.add(array.getJSONObject(i));
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    Collections.sort(jsons, new Comparator<JSONObject>() {
                        public int compare(JSONObject a, JSONObject b) {
                            String valA = new String();
                            String valB = new String();
                            try {
                                valA = String.valueOf(a.get("dtdate"));
                                valB = String.valueOf(b.get("dtdate"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return valB.compareTo(valA);
                        }
                    });
                    String JsonSort = "{\"status\":\"true\",\"msg\":\"Success\",\"data\":" + jsons + "}";
                    prefs.edit().putString("key", String.valueOf(JsonSort)).commit();
                    JsonShear = prefs.getString("key", null);
                    InvestmentList tempp = gson.fromJson(JsonSort, InvestmentList.class);
                    investmentList = tempp;
                    investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                } catch(JSONException e)
                { e.printStackTrace(); }
                investmentList = temp;
                investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                break;
            case ResponseType.HoldingList:

                prefs.edit().putString("key", String.valueOf(json)).commit();
                JsonShear = prefs.getString("key", null);
                InvestmentList HoldingList = gson.fromJson(JsonShear, InvestmentList.class);
                investmentList = HoldingList;
                investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                break;
            case ResponseType.EquityList:

                    prefs.edit().putString("key", String.valueOf(json)).commit();
                    JsonShear = prefs.getString("key", null);
                    InvestmentList EquityList = gson.fromJson(JsonShear, InvestmentList.class);
                    investmentList = EquityList;
                    investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                    break;
            case ResponseType.HybridList:
                prefs.edit().putString("key", String.valueOf(json)).commit();
                JsonShear = prefs.getString("key", null);
                InvestmentList HybridList = gson.fromJson(JsonShear, InvestmentList.class);
                investmentList = HybridList;
                investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                break;
            case ResponseType.DebtList:
                prefs.edit().putString("key", String.valueOf(json)).commit();
                JsonShear = prefs.getString("key", null);
                InvestmentList DebtList = gson.fromJson(JsonShear, InvestmentList.class);
                investmentList = DebtList;
                investedListRV.setAdapter(new InvestedRVAdapterNew(contextd, investmentList).setClick(this));
                break;
        }
    }

//    public static ArrayList<InvestmentList.InvestmentItem> removeDuplicates(ArrayList<InvestmentList.InvestmentItem> al) {
//        for(int i = 0; i < al.size(); i++) {
//            for(int j = i + 1; j < al.size(); j++) {
//                if(al.get(i).equals(al.get(j))){
//                    al.remove(j);
//                    j--;
//                }
//            }
//        }
//        return al;
//    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        Log.e("jsonRes OnFailure",json);

        emptymsg.setVisibility(View.VISIBLE);
        investedListRV.setVisibility(View.GONE);
     //   investedListRV.setVisibility(View.GONE);
      //  emptymsg.setText("You Don't have any Investment");
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                //emptymsg.setVisibility(View.VISIBLE);
               // Common.showToast(contextd, contextd.getResources().getString(R.string.noInvestMent));
                if(Kycstatuss.equals("NOT STARTED")){
                    emptymsg.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
                }else if(Kycstatuss.equals("SUBMITTED")){
                    emptymsg.setText(contextd.getResources().getString(R.string.kycSubmit));
                }else if(Kycstatuss.equals("COMPLETE")) {
                    emptymsg.setText(contextd.getResources().getString(R.string.noinvestment));
                }
                break;
            case ResponseType.DebtList:
             //   Common.showToast(contextd, contextd.getResources().getString(R.string.noInvestMent));
                emptymsg.setText(contextd.getResources().getString(R.string.notfound));
                break;
            case ResponseType.EquityList:
             //   Common.showToast(contextd, contextd.getResources().getString(R.string.noInvestMent));
                emptymsg.setText(contextd.getResources().getString(R.string.notfound));
                break;
            case ResponseType.HybridList:
             //   Common.showToast(contextd, contextd.getResources().getString(R.string.noInvestMent));
                emptymsg.setText(contextd.getResources().getString(R.string.notfound));
                break;

        }
    }

    @Override
    public void onItemClick(int position) {
//        if (investmentList.getData().get(position).getIs_goal().equals("1")) {
//            Log.e("Niivo", "Its a goal.");
//            Fragment frag = new InvestedGoalDetailFragment();
//            Bundle b = new Bundle();
//            try {
//                b.putString("goalDetail", response.getJSONArray("data").getJSONObject(position).toString());
//                frag.setArguments(b);
//                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                        .replace(R.id.frame_container, frag).addToBackStack(null).
//                        commit();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        } else if(!investmentList.getData().get(position).isGroupEntries() || investmentList.getData().get(position).isGroupEntries()) {
        //trnsact here
        Fragment frag = new InvestedSchemeDetailFragmentNew();//InvestedSchemeDetailFragment
        Bundle b = new Bundle();
        b.putString("currentamount", investmentList.getData().get(position).getAmount().trim());
        b.putString("detailInvestmentDate", investmentList.getData().get(position).getDtdate().trim());
        b.putString("detailFolio", investmentList.getData().get(position).getFolio_no().trim());
        b.putString("detailNumberOfUnit", investmentList.getData().get(position).getQuantity());
        b.putString("nav", investmentList.getData().get(position).getNav().trim());
        Log.d("navnav...",investmentList.getData().get(position).getNav().trim());
        b.putString("schemeName", investmentList.getData().get(position).getFundname().trim());
        b.putString("INWARD_TXN_NO", investmentList.getData().get(position).getINWARD_TXN_NO().trim());
        b.putString("Order_type", investmentList.getData().get(position).getOrder_type().trim());
        b.putString("BSE_SCHEME_CODE", investmentList.getData().get(position).getBSE_SCHEME_CODE().trim());
        b.putString("investment_amount",investmentList.getData().get(position).getInvestment_amount().trim());
        b.putString("order_status",investmentList.getData().get(position).getOrder_status().trim());

//            b.putString("schemeName", (lang.equals("en")
//                    ? investmentList.getData().get(position).getFundname()
//                    : lang.equals("hi")
//                    ? investmentList.getData().get(position).getFUNDS_HINDI()
//                    : lang.equals("gu")
//                    ? investmentList.getData().get(position).getFUNDS_GUJARATI()
//                    : lang.equals("mr")
//                    ? investmentList.getData().get(position).getFUNDS_MARATHI()
//                    : investmentList.getData().get(position).getFundname().trim()));

//            b.putString("from", "direct");
//            b.putString("orderId", investmentList.getData().get(position).getOrder_id());
//            b.putString("ammountInvested", investmentList.getData().get(position).getAmount());
//            b.putString("detailInvestmentDate", investmentList.getData().get(position).getDtdate());
//
//            b.putString("detailFolio",investmentList.getData().get(position).getFolio_no());
//          b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
//                    (Float.parseFloat(investmentList.getData().get(position).getCurrent_value()) - Float.parseFloat(investmentList.getData().get(position).getAmount()))));
//            b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(investmentList.getData().get(position).getNav())));
//            b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(investmentList.getData().get(position).getQuantity())));
//            b.putString("fundClass", "N/A");
//            b.putString("investmentOrderType", investmentList.getData().get(position).getOrder_type());
//
//            b.putString("investmentType", investmentList.getData().get(position).getOrder_type().equals("ONETIME")
//                    ? contextd.getResources().getString(R.string.oneTime)
//                    : investmentList.getData().get(position).getOrder_type().equalsIgnoreCase("SIP")?contextd.getResources().getString(R.string.monthly):contextd.getResources().getString(R.string.monthly_xsip));
//            b.putString("paidInstallment", "N/A");
//            b.putBoolean("isGroupEntries", investmentList.getData().get(position).isGroupEntries());
//            if(investmentList.getData().get(position).getOrder_type().equalsIgnoreCase("SIP") || investmentList.getData().get(position).getOrder_type().equalsIgnoreCase("XSIP"))
//                b.putString("paidInstallment", "1");
//            if(investmentList.getData().get(position).isGroupEntries())
//            {
//                if(investmentList.getData().get(position).getOrderedItemArrayListSub()!=null && investmentList.getData().get(position).getOrderedItemArrayListSub().size()!=0)
//                    b.putString("paidInstallment", investmentList.getData().get(position).getOrderedItemArrayListSub().size()+"");
//                b.putString("regDate", investmentList.getData().get(position).getSip_registration_date()+"");
//                b.putString("fundName", investmentList.getData().get(position).getFundname()+"");
//                if(investmentList.getData().get(position).isGroupEntries()) {
//                    try {
//                        b.putSerializable("schemeDetail", investmentList.getData().get(position));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            b.putString("detailFolio", investmentList.getData().get(position).getFolio_no());
//            b.putString("schemeName", (lang.equals("en")
//                    ? investmentList.getData().get(position).getFundname()
//                    : lang.equals("hi")
//                    ? investmentList.getData().get(position).getFUNDS_HINDI()
//                    : lang.equals("gu")
//                    ? investmentList.getData().get(position).getFUNDS_GUJARATI()
//                    : lang.equals("mr")
//                    ? investmentList.getData().get(position).getFUNDS_MARATHI()
//                    : investmentList.getData().get(position).getFundname()));

        frag.setArguments(b);

        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frag).addToBackStack(null).
                commit();
//        }
//        else if(investmentList.getData().get(position).isGroupEntries()) {
//            //trnsact here
//            Fragment frag = new InvestedSchemeDetailGroupFragment();
//            Bundle b = new Bundle();
//            b.putString("from", "direct");
//            b.putString("orderId", investmentList.getData().get(position).getOrder_id());
//            b.putString("ammountInvested", investmentList.getData().get(position).getAmount());
//            b.putString("investmentValue", investmentList.getData().get(position).getCurrent_value());
//            b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
//                    (Float.parseFloat(investmentList.getData().get(position).getCurrent_value()) - Float.parseFloat(investmentList.getData().get(position).getAmount()))));
//            b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(investmentList.getData().get(position).getNav())));
//            b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(investmentList.getData().get(position).getQuantity())));
//            b.putString("fundClass", "N/A");
//            b.putString("investmentType", investmentList.getData().get(position).getOrder_type().equals("ONETIME")
//                    ? contextd.getResources().getString(R.string.oneTime)
//                    : contextd.getResources().getString(R.string.monthly));
//            b.putString("paidInstallment", "N/A");
//            b.putString("orderType", investmentList.getData().get(position).getOrder_type());
//
//            b.putString("schemeName", (lang.equals("en")
//                    ? investmentList.getData().get(position).getFundname()
//                    : lang.equals("hi")
//                    ? investmentList.getData().get(position).getFUNDS_HINDI()
//                    : lang.equals("gu")
//                    ? investmentList.getData().get(position).getFUNDS_GUJARATI()
//                    : lang.equals("mr")
//                    ? investmentList.getData().get(position).getFUNDS_MARATHI()
//                    : investmentList.getData().get(position).getFundname()));
//            // frag.setArguments(b);
//            try {
//                b.putSerializable("schemeDetail",investmentList.getData().get(position));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            frag.setArguments(b);
//
//            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                    .replace(R.id.frame_container, frag).addToBackStack(null).
//                    commit();
    }

}
