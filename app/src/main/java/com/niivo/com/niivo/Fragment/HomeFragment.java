package com.niivo.com.niivo.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import android.content.SharedPreferences;

import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;


import android.widget.TextView;
import android.widget.Toast;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.InterMediateFragment.IntermidiateBaseFragment;
import com.niivo.com.niivo.Fragment.InvestHelper.SelectTimeFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.SquareRelativeLayout;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


/**
 * Created by andro on 22/5/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {
    Activity _activity;
    Toolbar toolbar;
    SquareRelativeLayout goalRetirement, goalForChild, goalSaveTax, goalVacation, goalBuyingCar, goalBuyingHouse, goalBusiness, goalEmergency, goalWealth;
    LinearLayout llFundsByCategory, llFundsByPerformance;
    //Non ui vars;
    Context contextd;
    String cateId = "";
    Fragment frag = null, fragAlt = null;
    Bundle b;
    LinearLayout layout;
    boolean flag=false;
    ImageButton back;
    private GuideView.Builder guidview;
    String backflag ;//= "show";
    private static final String TAG = "HomeFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        View parentView = inflater.inflate(R.layout.fragment_home, container, false);
        contextd = container.getContext();
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.invest_now);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        MainActivity.icon_navi.setVisibility(View.VISIBLE);
        goalRetirement = (SquareRelativeLayout) parentView.findViewById(R.id.rlRetirementFund);
        goalForChild = (SquareRelativeLayout) parentView.findViewById(R.id.rlForchild);
        goalSaveTax = (SquareRelativeLayout) parentView.findViewById(R.id.rlSaveTax);
        goalVacation = (SquareRelativeLayout) parentView.findViewById(R.id.rlVacation);
        goalBuyingCar = (SquareRelativeLayout) parentView.findViewById(R.id.rlBuyCar);
        goalBuyingHouse = (SquareRelativeLayout) parentView.findViewById(R.id.rlBuyingHouse);
        goalBusiness = (SquareRelativeLayout) parentView.findViewById(R.id.rlStartingBusiness);
        goalEmergency = (SquareRelativeLayout) parentView.findViewById(R.id.rlStartingEmergency);
        goalWealth = (SquareRelativeLayout) parentView.findViewById(R.id.rlWealthCreation);
        llFundsByCategory = (LinearLayout) parentView.findViewById(R.id.llFundsByCategory);
        //     llFundsByPerformance = (LinearLayout) parentView.findViewById(R.id.llFundsByPerformance);
        goalRetirement.setOnClickListener(this);
        goalForChild.setOnClickListener(this);
        goalSaveTax.setOnClickListener(this);
        goalVacation.setOnClickListener(this);
        goalBuyingCar.setOnClickListener(this);
        goalBuyingHouse.setOnClickListener(this);
        goalBusiness.setOnClickListener(this);
        goalEmergency.setOnClickListener(this);
        goalWealth.setOnClickListener(this);
        llFundsByCategory.setOnClickListener(this);
        //   llFundsByPerformance.setOnClickListener(this);
        layout = (LinearLayout) parentView.findViewById(R.id.layout);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
         flag = Boolean.parseBoolean(prefs.getString("flag", null));
            if(flag){
                ShowLayout();
            }
        return parentView;
    }

    public void ShowLayout() { //int viewId, final int type
        guidview = new GuideView.Builder(contextd);
        guidview.setTitle(contextd.getResources().getString(R.string.invest_now))
                .setContentText(contextd.getResources().getString(R.string.layout))
                .setTargetView(layout)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.anywhere)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        ShowCategoryLaylut();
                    }
                })
        .build()
        .show();
    }
    public void ShowCategoryLaylut() {
        guidview = new GuideView.Builder(contextd);
        guidview.setTitle(contextd.getResources().getString(R.string.invest_now))
                .setContentText(contextd.getResources().getString(R.string.layoutcategory))
                .setTargetView(llFundsByCategory)
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.anywhere)//optional - default dismissible by TargetView
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        showw();
                    }
                })
                .build()
                .show();

    }
    public void showw()
    {
        backflag="hide";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        prefs.edit().putString("backflag", backflag).commit();
    }

    @Override
    public void onClick(View v) {
        b = new Bundle();
        frag = new SelectTimeFragment();
        b.putString("from", "biginner");
        switch (v.getId()) {
            case R.id.rlRetirementFund:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.retairMentFundAlt));
                b.putString("goalNameExample", "My Retirement 2019");
                b.putInt("goalImgRes", R.drawable.retirementfund);
                cateId = "1";
                break;
            case R.id.rlForchild:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.forChildAlt));
                b.putString("goalNameExample", "Suraj's Education");
                b.putInt("goalImgRes", R.drawable.childswedding);
                cateId = "2";
                break;
            case R.id.rlSaveTax:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.saveTaxAlt));
                b.putString("goalNameExample", "Tax Saving Investment");
                b.putInt("goalImgRes", R.drawable.goal_tax);
                cateId = "3";
                break;
            case R.id.rlVacation:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.vacation));
                b.putString("goalNameExample", "Thailand 2022");
                b.putInt("goalImgRes", R.drawable.vacation);
                cateId = "4";
                break;
            case R.id.rlBuyCar:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.buyingACarAlt));
                b.putString("goalNameExample", "Buying Honda City");
                b.putInt("goalImgRes", R.drawable.buyingcar);
                cateId = "5";
                break;
            case R.id.rlBuyingHouse:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.buyingAHouseAlt));
                b.putString("goalNameExample", "3BHK Flat");
                b.putInt("goalImgRes", R.drawable.buyinghouse);
                cateId = "6";
                break;
            case R.id.rlStartingBusiness:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.startingABusinessAlt));
                b.putString("goalNameExample", "Pet Shop Business");
                b.putInt("goalImgRes", R.drawable.startingbusiness);
                cateId = "7";
                break;
            case R.id.rlStartingEmergency:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.emergencyFundAlt));
                b.putString("goalNameExample", "Emergency Saving");
                b.putInt("goalImgRes", R.drawable.startinganemergencyfund);
                cateId = "8";
                break;
            case R.id.rlWealthCreation:
                b.putString("goalNameAlt", contextd.getResources().getString(R.string.wealthCreationAlt));
                b.putString("goalNameExample", "For Future Saving");
                b.putInt("goalImgRes", R.drawable.wealthcreation);
                cateId = "9";
                break;
            case R.id.llFundsByCategory:
                frag = null;
                fragAlt = new IntermidiateBaseFragment();
                break;
//            case R.id.llFundsByPerformance:
//                frag = null;
//                fragAlt = new ExpertFragment();
//                break;
        }
        UiUtils.hideKeyboard(contextd);
        if (frag != null) {
            b.putString("cateId", cateId);
            frag.setArguments(b);
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, frag)
                    .addToBackStack(null).commit();
        } else if (fragAlt != null) {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, fragAlt).addToBackStack(null).
                    commit();
        }
    }
}
