package com.niivo.com.niivo.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.niivo.com.niivo.Activity.ForgotPassOTPActivity;
import com.niivo.com.niivo.Activity.OTPActivity;
import com.niivo.com.niivo.Activity.SignUpActivity;

/**
 * Created by deepak on 6/7/17.
 */

public class SMSReceiver extends BroadcastReceiver {
    public SMSReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    try {
                        if (senderNum.contains("tester")) {
                            if (SignUpActivity.otpWhere.equals("forgot"))
                                ForgotPassOTPActivity.setOTPsms(message);
                            if (SignUpActivity.otpWhere.equals("signup"))
                                OTPActivity.setOTPsms(message);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e) {
        }
    }

}