package com.niivo.com.niivo.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 27/3/18.
 */

public class ChangeLanguageFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView englishBtn, hindiBtn, gujratiBtn, marathiBtn;
    TextView nextBtn;
    ImageView back;

    //Non ui vars
    Context contextd;
    String selectedLang = "";
    RequestedServiceDataModel requestedServiceDataModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_choose_language, container, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.change_language).toUpperCase());

        englishBtn = (TextView) rootView.findViewById(R.id.englishBtn);
        hindiBtn = (TextView) rootView.findViewById(R.id.hindiBtn);
        gujratiBtn = (TextView) rootView.findViewById(R.id.gujratiBtn);
        marathiBtn = (TextView) rootView.findViewById(R.id.marathiBtn);
        nextBtn = (TextView) rootView.findViewById(R.id.btn_next);
        englishBtn.setOnClickListener(this);
        hindiBtn.setOnClickListener(this);
        gujratiBtn.setOnClickListener(this);
        marathiBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        nextBtn.setText(contextd.getResources().getString(R.string.submit));
        setLangs();
        return rootView;
    }

    void setLangs() {
        if (Common.getLanguage(getActivity()).equals("en"))
            englishBtn.setSelected(true);
        else if (Common.getLanguage(getActivity()).equals("hi"))
            hindiBtn.setSelected(true);
        else if (Common.getLanguage(getActivity()).equals("gu"))
            gujratiBtn.setSelected(true);
        else if (Common.getLanguage(getActivity()).equals("mr"))
            marathiBtn.setSelected(true);
        selectedLang = Common.sendLanguage(getActivity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.englishBtn:
                englishBtn.setSelected(true);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(false);
                selectedLang = "EN";
                break;
            case R.id.hindiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(true);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(false);
                selectedLang = "HI";
                break;
            case R.id.gujratiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(true);
                marathiBtn.setSelected(false);
                selectedLang = "GJ";
                break;
            case R.id.marathiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(true);
                selectedLang = "MH";
                break;
            case R.id.btn_next:
                chaneLanguage();
                break;
        }
    }

    void chaneLanguage() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ChangeLang);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("default_lang", selectedLang);
        requestedServiceDataModel.setWebServiceType("SETLANGUAGE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ChangeLang:
         //       Log.e("lang", json);
                Common.showToast(contextd, message);
                resetLanguage();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ChangeLang:
                Common.showToast(contextd, message);
                break;
        }
    }

    void resetLanguage() {
        String str = "";
        str = selectedLang.equals("EN") ?
                "en" : selectedLang.equals("HI") ?
                "hi" : selectedLang.equals("GJ") ?
                "gu" : selectedLang.equals("MH") ?
                "mr" : "en";
        Common.setLanguage(getActivity(), str, true);
    }
}
