package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.SipDueReports;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 8/12/17.
 */

public class SipDueReportAdapter extends BaseAdapter {
    Context contextd;
    SipDueReports list;
    SipDueHolder holder;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));

    public SipDueReportAdapter(Context context, SipDueReports res) {
        this.contextd = context;
        this.list = res;
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_sip_payment, parent, false);
            holder = new SipDueHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SipDueHolder) convertView.getTag();
        }
        holder.fundName.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
                list.getData().get(position).getGoal_detail().getGoal_name() : (list.getData().get(position).getOrder_detail().isEmpty() ?
                list.getData().get(position).getFund_name() : list.getData().get(position).getOrder_detail().get(0).getScheme_name()));
        holder.typeOfInvest.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
                contextd.getResources().getString(R.string.goal) :
                contextd.getResources().getString(R.string.fund));
        Calendar ct = Calendar.getInstance();
        try {
            ct.setTime(sdf.parse(list.getData().get(position).getDue_date()));
            holder.date.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (list.getData().get(position).getIs_goal().equals("1")) {
            if (list.getData().get(position).getGoal_detail().getFund_category() != null)
                if (list.getData().get(position).getGoal_detail().getFund_category().equals("1")) {
                    holder.ivLogo.setImageResource(R.drawable.retirementfund);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("2")) {
                    holder.ivLogo.setImageResource(R.drawable.childseducation);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("3")) {
                    holder.ivLogo.setImageResource(R.drawable.childswedding);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("4")) {
                    holder.ivLogo.setImageResource(R.drawable.vacation);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("5")) {
                    holder.ivLogo.setImageResource(R.drawable.buyingcar);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("6")) {
                    holder.ivLogo.setImageResource(R.drawable.buyinghouse);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("7")) {
                    holder.ivLogo.setImageResource(R.drawable.startingbusiness);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("8")) {
                    holder.ivLogo.setImageResource(R.drawable.startinganemergencyfund);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("9")) {
                    holder.ivLogo.setImageResource(R.drawable.wealthcreation);
                } else {
                    holder.ivLogo.setImageResource(R.drawable.rupee);
                }
        } else {
            holder.ivLogo.setImageResource(R.drawable.rupee);
        }
        holder.amount.setText(Common.currencyString(list.getData().get(position).getAmount(),false));
        return convertView;
    }


    public class SipDueHolder {
        TextView fundName, date, amount, typeOfInvest;
        ImageView ivLogo;

        public SipDueHolder(View item) {
            fundName = (TextView) item.findViewById(R.id.fundName);
            date = (TextView) item.findViewById(R.id.sipPaymentDate);
            amount = (TextView) item.findViewById(R.id.sipAmount);
            ivLogo = (ImageView) item.findViewById(R.id.cateGoryImage);
            typeOfInvest = (TextView) item.findViewById(R.id.typeOfInvest);
        }
    }

}
