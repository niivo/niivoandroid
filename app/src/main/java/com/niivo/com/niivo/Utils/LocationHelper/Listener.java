package com.niivo.com.niivo.Utils.LocationHelper;

public interface Listener {
    void locationOn();

    void onPositionChanged();

    void locationCancelled();
}