package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

public class CommonSignUpSignInActivity extends AppCompatActivity {
    String tok;
    ImageView logo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(CommonSignUpSignInActivity.this, Common.getLanguage(CommonSignUpSignInActivity.this), false);
        setContentView(R.layout.activity_sign_up_sign_in);
        TextView signup = (TextView) findViewById(R.id.button_signup);
        logo1 = (ImageView) findViewById(R.id.logo1);
        logo1.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getWidth());
            }
        });
        if (Common.getConnectivityStatus(CommonSignUpSignInActivity.this))
            tok = Common.getToken(CommonSignUpSignInActivity.this);
        if (tok != null)
            Log.e("Token", tok);
        else
            Log.e("Token", "not Generated");
        //FirebaseApp.initializeApp(this);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommonSignUpSignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
        TextView signin = (TextView) findViewById(R.id.button_signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommonSignUpSignInActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

    }
}
