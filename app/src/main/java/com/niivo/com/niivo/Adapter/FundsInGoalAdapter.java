package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemFundsInGoal;
import com.niivo.com.niivo.R;

import java.util.ArrayList;

/**
 * Created by deepak on 9/6/17.
 */

public class FundsInGoalAdapter extends ArrayAdapter<ItemFundsInGoal> {
    Context contextd;
    ArrayList<ItemFundsInGoal> list;

    public FundsInGoalAdapter(@NonNull Context context, ArrayList<ItemFundsInGoal> resource) {
        super(context, 0, resource);
        this.contextd = context;
        this.list = resource;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_fund_in_goal, parent, false);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            ImageView logo = (ImageView) convertView.findViewById(R.id.logo);
            title.setText(list.get(position).getTitle());
            logo.setImageResource(list.get(position).getLogo());
        }
        return convertView;
    }
}
