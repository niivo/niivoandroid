package com.niivo.com.niivo.Fragment.InterMediateFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.niivo.com.niivo.Adapter.EquityFundsAdapter;
import com.niivo.com.niivo.Fragment.GrowthDividendFragment;
import com.niivo.com.niivo.Model.ItemState;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;

import java.util.ArrayList;


/**
 * Created by deepak on 9/6/17.
 */

public class EquityFundsFragment extends Fragment {
    Context contextd;
    ListView fundsLsit;


    ArrayList<String> type;
    ArrayList<ItemState> funds;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_equity_debt, container, false);
        contextd = container.getContext();
        fundsLsit = (ListView) rootView.findViewById(R.id.fundTypes);

       Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
//        textView.setText(getArguments().getString("for").equals("equity") ? contextd.getResources().getString(R.string.equityFunds)
//                : contextd.getResources().getString(R.string.balanceFunds));

        if(getArguments().getString("for").equals("equity"))
        {
            textView.setText(getResources().getString(R.string.equityFundsAlt));
        }else if(getArguments().getString("for").equals("debt"))
        {
            textView.setText(getResources().getString(R.string.debtFundsAlt));
        }else if(getArguments().getString("for").equals("HYBRIDFUND"))
        {
            textView.setText(getResources().getString(R.string.hybridfund));
        }else if(getArguments().getString("for").equals("TAXSAVINGFUND"))
        {
            textView.setText(getResources().getString(R.string.saveTaxt));
        }else if(getArguments().getString("for").equals("OTHERFUND"))
        {
            textView.setText(getResources().getString(R.string.otherfund));
        }else {
            textView.setText(getResources().getString(R.string.newfund));
        }

        funds = new ArrayList<>();
        type = new ArrayList<>();
        if (getArguments().getString("for").equals("equity")) {
            funds.add(new ItemState(contextd.getResources().getString(R.string.largeCap),
                    contextd.getResources().getString(R.string.largeCapDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.midCap),
                    contextd.getResources().getString(R.string.midCapDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.thematic),
                    contextd.getResources().getString(R.string.thematicDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.broadMarket),
                    contextd.getResources().getString(R.string.broadMarketDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.smallCap),
                    contextd.getResources().getString(R.string.smallCapDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.contrafunds),
                    contextd.getResources().getString(R.string.valuecontrafund)));

            type.add("Large");
            type.add("Mid");
            type.add("Thematic");
            type.add("Broad");
            type.add("Small");
            type.add("Contra");
        } else if (getArguments().getString("for").equals("debt")) {
            funds.add(new ItemState(contextd.getResources().getString(R.string.Liquid),
                    contextd.getResources().getString(R.string.Liquiddsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Low),
                    contextd.getResources().getString(R.string.Lowdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Short),
                    contextd.getResources().getString(R.string.Shortdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Medium),
                    contextd.getResources().getString(R.string.Mediumdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Long),
                    contextd.getResources().getString(R.string.Longdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Banking),
                    contextd.getResources().getString(R.string.Balanceddsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Credit),
                    contextd.getResources().getString(R.string.Creditdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Corporate),
                    contextd.getResources().getString(R.string.Corporatedsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.other),
                    contextd.getResources().getString(R.string.otherdsc)));

            type.add("Liquid");
            type.add("Low");
            type.add("Short");
            type.add("Medium");
            type.add("Long");
            type.add("Banking");
            type.add("Credit");
            type.add("Corporate");
            type.add("Other");
        }else if(getArguments().getString("for").equals("HYBRIDFUND")){
            funds.add(new ItemState(contextd.getResources().getString(R.string.Arbitrage),
                    contextd.getResources().getString(R.string.Arbitragedsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.Balanced),
                    contextd.getResources().getString(R.string.Balanceddsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.EquityOriented),
                    contextd.getResources().getString(R.string.EquityOrienteddsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.debtOriented),
                    contextd.getResources().getString(R.string.debtOrienteddsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.MultiAsset),
                    contextd.getResources().getString(R.string.MultiAssetdsc)));

            type.add("Arbitrage");
            type.add("Balanced");
            type.add("EquityOriented");
            type.add("DebtOriented");
            type.add("MultiAsset");
        }else if(getArguments().getString("for").equals("OTHERFUND")) {
            funds.add(new ItemState(contextd.getResources().getString(R.string.childrens),
                    contextd.getResources().getString(R.string.childrensdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.retirement),
                    contextd.getResources().getString(R.string.retirementdsc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.index),
                    contextd.getResources().getString(R.string.indexdsc)));
            type.add("Childrens");
            type.add("Retirement");
            type.add("Index");
        } else {
            funds.add(new ItemState(contextd.getResources().getString(R.string.equityOriented),
                    contextd.getResources().getString(R.string.equityOrientedDesc)));
            funds.add(new ItemState(contextd.getResources().getString(R.string.debtOriented),
                    contextd.getResources().getString(R.string.debtOrientedDesc)));
            type.add("Equity");
            type.add("Debt");
        }
        EquityFundsAdapter adapter = new EquityFundsAdapter(contextd, funds);
        fundsLsit.setAdapter(adapter);
        fundsLsit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment frag = new GrowthDividendFragment();
                Bundle b = getArguments();
                if (getArguments().getString("for").equals("equity")) {
                    b.putString("sub_section", type.get(position));
                    b.putString("type", "EQUITYFUND");//EQUITYFUNDS
                } else if (getArguments().getString("for").equals("debt")) {
                    b.putString("sub_section", type.get(position));
                    b.putString("type", "DEBTFUND");//EQUITYFUNDS
                }else if (getArguments().getString("for").equals("OTHERFUND")) {
                    b.putString("sub_section", type.get(position));
                    b.putString("type", "OTHERFUND");
                }else  if (getArguments().getString("for").equals("HYBRIDFUND")) {
                    b.putString("sub_section", type.get(position));
                    b.putString("type", "HYBRIDFUND");//BALANCEDFUNDS
                }
                frag.setArguments(b);
                UiUtils.hideKeyboard(contextd);

                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, frag).addToBackStack(null).
                        commit();
            }
        });
        return rootView;
    }
}
