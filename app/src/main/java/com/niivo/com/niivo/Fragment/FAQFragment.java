package com.niivo.com.niivo.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.niivo.com.niivo.Adapter.FaqExpandAdapter;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepak on 6/11/17.
 */

public class FAQFragment extends Fragment {
    ExpandableListView faqList;
    ImageButton back;
    //Non ui Vars
    Context contextd;
    List<String> listQuestion, listAnswer;
    FaqExpandAdapter adapter;
    String lang = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_faq, container, false);
        faqList = (ExpandableListView) rootView.findViewById(R.id.faqQuestionList);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        title.setText(contextd.getResources().getString(R.string.faq).toUpperCase());
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        lang = Common.getLanguage(getActivity());
        back.setVisibility(View.VISIBLE);
        setListData();
        adapter = new FaqExpandAdapter(contextd, listQuestion, listAnswer);
        faqList.setAdapter(adapter);

        return rootView;
    }

    void setListData() {
        listQuestion = new ArrayList<>();
        listAnswer = new ArrayList<>();
        if (lang.equals("en")) {
            listQuestion.add("<p><strong>What is a mutual fund?</strong></p>");
            listQuestion.add("<p><strong>What is an Asset Management Company?</strong></p>");
            listQuestion.add("<p><strong>What is KYC?</strong></p>");
            listQuestion.add("<p><strong>How much money can I invest?</strong></p>");
            listQuestion.add("<p><strong>What are the benefits of investing in a mutual fund?</strong></p>");
            listQuestion.add("<p><strong>What are the types of risks?</strong></p>");
            listQuestion.add("<p><strong>Are returns from mutual funds guaranteed?</strong></p>");

            listAnswer.add("A mutual fund is an investment program funded by shareholders that trades in diversified holdings and is professionally managed.");
            listAnswer.add("An Asset Management Company (AMC) is a highly regulated organisation that pools money from investors and invests the same in a portfolio. They charge a small fee for fund management.");
            listAnswer.add("Know Your Customer, commonly known as KYC, enables a bank or a financial institution in authenticating the identity of its customers. This helps in prohibiting money-laundering activities and further ensures that the deposits/investments are made in the name of a real person and not fictitious one.");
            listAnswer.add("We currently use eKYC for non-KYC compliant users. According to Government of India regulations, an eKYC compliant investor can invest upto Rs. 50,000 per AMC per year.");
            listAnswer.add("The benefits of investing in mutual funds are as follows -\n" +
                    "Access to professional money managers - Your money is managed by experienced fund managers using advanced scientific and mathematical techniques.\n" +
                    "Diversification -Mutual funds aim to reduce the volatility of returns through diversification by investing in a number of companies across a broad section of industries and sectors. It prevents an investor from putting \"all eggs in one basket\". This inherently minimizes risk. Thus with a small investible surplus an investor can achieve diversification that would have otherwise not been possible.\n" +
                    "Liquidity - Open-ended mutual funds are priced daily and are always willing to buy back units from investors. This mean that investors can sell their holdings in mutual fund investments anytime without worrying about finding a buyer at the right price. In the case of other investment avenues such as stocks and bonds, buyers are not necessarily available and therefore these investment avenues are less liquid compared to open-ended schemes of mutual funds.\n" +
                    "Low transaction costs - Since mutual funds are a pool of money of many investors, the amount of investment made in securities is large. This therefore results in paying lower brokerage due to economies of scale.\n" +
                    "Transparency - Prices of open ended mutual funds are declared daily. Regular updates on the value of your investment are available. The portfolio is also disclosed regularly with the fund manager's investment strategy and outlook.\n" +
                    "Well-regulated industry - All the mutual funds are registered with SEBI and they function under strict regulations designed to protect the interests of investors.\n" +
                    "Convenience of small investments: Under normal circumstances, an individual investor would not be able to diversify his investments (and thus minimize risk) across a wide array of securities due to the small size of his investments and inherently higher transaction costs. A mutual fund on the other hand allows even individual investors to hold a diversified array of securities due to the fact that it invests in a portfolio of stocks. A mutual fund therefore permits risk diversification without an investor having to invest large amounts of money.");
            listAnswer.add("Risk is an inherent aspect of every form of investment. For mutual fund investments, risks would include variability, or period-by-period fluctuations in total return. The value of the scheme's investments may be affected by factors affecting capital markets such as price and volume volatility in the stock markets, interest rates, currency exchange rates, foreign investment, changes in government policy, political, economic or other developments.\n" +
                    "Market Risk: At times the prices or yields of all the securities in a particular market rise or fall due to broad outside influences. When this happens, the stock prices of both an outstanding, highly profitable company and a fledgling corporation may be affected. This change in price is due to \"market risk\".\n" +
                    "Inflation Risk: Sometimes referred to as \"loss of purchasing power.\" Whenever the rate of inflation exceeds the earnings on your investment, you run the risk that you'll actually be able to buy less, not more.\n" +
                    "Credit Risk: In short, how stable is the company or entity to which you lend your money when you invest? How certain are you that it will be able to pay the interest you are promised, or repay your principal when the investment matures?\n" +
                    "Interest Rate Risk: Changing interest rates affect both equities and bonds in many ways. Bond prices are influenced by movements in the interest rates in the financial system. Generally, when interest rates rise, prices of the securities fall and when interest rates drop, the prices increase. Interest rate movements in the Indian debt markets can be volatile leading to the possibility of large price movements up or down in debt and money market securities and thereby to possibly large movements in the NAV.\n" +
                    "Investment Risks: The sectoral fund schemes, investments will be predominantly in equities of select companies in the particular sectors. Accordingly, the NAV of the schemes are linked to the equity performance of such companies and may be more volatile than a more diversified portfolio of equities.\n" +
                    "Liquidity Risk: Thinly traded securities carry the danger of not being easily saleable at or near their real values. The fund manager may therefore be unable to quickly sell an illiquid bond and this might affect the price of the fund unfavorably. Liquidity risk is characteristic of the Indian fixed income market.\n" +
                    "Changes in the Government Policy: Changes in Government policy especially in regard to the tax benefits may impact the business prospects of the companies leading to an impact on the investments made by the fund.");
            listAnswer.add("Generally, Mutual Funds do not offer guaranteed returns to investors. Although, SEBI regulations allow Mutual Funds to offer guaranteed returns subject to the Fund meeting certain conditions, most Funds do not offer such guarantees. In case of a guaranteed return scheme, the sponsor or the AMC, guarantees a minimum level of return and makes good the difference if the actual returns are less than the guaranteed minimum. The name of the guarantor and the manner in which the guarantee shall be met must be disclosed in the offer document by the Mutual Fund. Investments in mutual funds are not guaranteed by the Government of India, the Reserve Bank of India or any other government body.");
        } else if (lang.equals("hi")) {
            listQuestion.add("<p><strong>म्युचुअल फ़ंड क्या है?</strong></p>");
            listQuestion.add("<p><strong>एसेट मैनेजमेंट कंपनी क्या है?</strong></p>");
            listQuestion.add("<p><strong>केवाईसी क्या है?</strong></p>");
            listQuestion.add("<p><strong>मैं कितनी राशि का निवेश कर सकता/सकती हूँ?</strong></p>");
            listQuestion.add("<p><strong>म्युचुअल फ़ंड में निवेश के क्या लाभ हैं?</strong></p>");
            listQuestion.add("<p><strong>जोखिमों के प्रकार क्या हैं?</strong></p>");
            listQuestion.add("<p><strong>क्या म्युचुअल फ़ंड से रिटर्न (प्रतिलाभ) मिलने की गारंटी है?</strong></p>");

            listAnswer.add("म्युचुअल फ़ंड, शेयरधारकों द्वारा वित्त पोषित एक निवेश कार्यक्रम है जो डायवर्सिफ़ाइड होल्डिंग्स में ट्रेड करता है और जिसे पेशेवर ढंग से प्रबंधित किया जाता है।");
            listAnswer.add("एसेट मैनेजमेंट कंपनी (एएमसी) एक अत्यधिक रेगुलेटेड संगठन है जो निवेशकों से पैसा इकट्ठा कर पोर्टफ़ोलियो में निवेश करता है। वे फ़ंड प्रबंधन के लिए एक छोटा सा शुल्क लेते हैं।");
            listAnswer.add("अपने ग्राहक को जानें, जिसे सामान्यतः केवाईसी के नाम से जाना जाता है, एक बैंक या वित्तीय संस्था को अपने ग्राहकों की पहचान को प्रमाणित करने में सक्षम बनाता है। यह धन-शोधन (मनी लॉन्डरिंग) संबंधी गतिविधियों को प्रतिबंधित करने में सहायता करता है और आगे यह सुनिश्चित करता है कि जमा/निवेश एक वास्तविक व्यक्ति के नाम से किया जाता है न कि किसी फर्जी नाम से।");
            listAnswer.add("वर्तमान में हम गैर-केवाईसी अनुरूप उपयोगकर्ताओं के लिए ई-केवाईसी का उपयोग करते हैं। भारत सरकार के विनियमों के अनुसार, एक ई-केवाईसी अनुरूप निवेशक रू. 50,000 प्रति एएमसी प्रति वर्ष तक निवेश कर सकता है।");
            listAnswer.add("म्युचुअल फ़ंड में निवेश के लाभ निम्नानुसार हैं -\n" +
                    "पेशेवर धन प्रबंधकों तक पहुँच - आपके पैसे को अनुभवी फ़ंड मैनेजरों द्वारा उन्नत वैज्ञानिक और गणितीय तकनीकों का उपयोग करके प्रबंधित किया जाता है।\n" +
                    "डायवर्सिफ़िकेशन (विविधीकरण) - म्युचुअल फ़ंड का लक्ष्य उद्योगों और क्षेत्रों के व्यापक खंड में कई कंपनियों में निवेश करके डायवर्सिफ़िकेशन के माध्यम से रिटर्न (प्रतिलाभ) की अस्थिरता को कम करना है। यह एक निवेशक को \"एक ही जगह सब कुछ निवेश\" करने से रोकता है। यह स्वाभाविक रूप से जोखिम को कम करता है। इस प्रकार एक छोटे से निवेश योग्य सरप्लस राशि के साथ एक निवेशक डायवर्सिफ़िकेशन प्राप्त कर सकता है जो अन्यथा संभव नहीं होगा।\n" +
                    "लिक्विडिटी (तरलता) - ओपन-एंडेड म्युचुअल फ़ंड की कीमत दैनिक आधार पर तय होती है और ये हमेशा निवेशकों से यूनिट्स को वापस खरीदने के लिए तैयार होती हैं। इसका मतलब यह है कि निवेशक किसी खरीदार की चिंता किए बिना किसी भी समय सही कीमत पर म्युचुअल फ़ंड निवेश में अपनी होल्डिंग्स बेच सकते हैं। स्टॉक और बांड जैसे अन्य निवेश विकल्पों के मामले में, यह जरूरी नहीं कि खरीदार उपलब्ध हो और इसलिए इन म्युचुअल फ़ंड्स की ओपन-एंडेड स्कीम की तुलना में ये निवेश विकल्प कम तरल हैं।\n" +
                    "कम लेनदेन लागत - चूँकि म्युचुअल फ़ंड कई निवेशकों के लिए पैसे का एक पूल है, इसलिए प्रतिभूतियों में किए गए निवेश की मात्रा बड़ी होती है। इसलिए, बड़े पैमाने की किफ़ायतों के कारण इनमें कम ब्रोकरेज़ (दलाली) का भुगतान करना होता है।\n" +
                    "पारदर्शिता - ओपन-एंडेड म्युचुअल फ़ंड की कीमतों को दैनिक आधार पर घोषित किया जाता है। आपके निवेश के मूल्य पर नियमित अपडेट उपलब्ध हैं। पोर्टफ़ोलियो को फ़ंड मैनेजर की निवेश रणनीति और दृष्टिकोण के साथ भी नियमित रूप से प्रकट किया जाता है।\n" +
                    "अच्छी तरह से विनियमित उद्योग - सभी म्युचुअल फ़ंड सेबी के साथ पंजीकृत हैं और वे निवेशकों के हितों की रक्षा के लिए बनाए गए सख्त विनियमों के अंतर्गत काम करते हैं।\n" +
                    "छोटे निवेश की सुविधा: सामान्य परिस्थितियों में, एक व्यक्तिगत निवेशक अपने निवेश के छोटे आकार और स्वाभाविक रूप से उच्च लेनदेन लागतों के कारण प्रतिभूतियों की व्यापक श्रेणी में अपने निवेश को डायवर्सिफ़ाई करने में (और इस प्रकार जोखिम को कम करने में) सक्षम नहीं होगा। दूसरी तरफ, एक म्युचुअल फ़ंड इस बात की वजह से व्यक्तिगत निवेशकों को विभिन्न प्रकार की प्रतिभूतियाँ रखने की अनुमति देता है क्योंकि यह स्टॉक के पोर्टफ़ोलियो में निवेश करता है। इसलिए, म्युचुअल फ़ंड एक निवेशक को बड़ी धनराशि का निवेश किए बिना जोखिम डायवर्सिफ़िकेशन (विविधीकरण) की अनुमति देता है।");
            listAnswer.add("जोखिम हर प्रकार के निवेश का एक अंतर्निहित पहलू है। म्युचुअल फ़ंड निवेश के लिए, जोखिम में परिवर्तनशीलता या कुल रिटर्न (प्रतिलाभ) में अवधि-दर-अवधि के उतार-चढ़ाव शामिल होंगे। स्कीम के निवेश का मूल्य शेयर बाजारों में कीमत और मात्रा की अस्थिरता, ब्याज दरों, मुद्रा विनिमय दरों, विदेशी निवेश, सरकारी नीति में बदलाव, राजनीतिक, आर्थिक या अन्य विकास जैसे पूंजी बाजार को प्रभावित करने वाले कारकों से प्रभावित हो सकता है।\n" +
                    "बाजार जोखिम: कभी-कभी व्यापक बाहरी प्रभावों के कारण किसी विशेष बाजार में सभी प्रतिभूतियों की कीमतों या मुनाफ़े में वृद्धि या गिरावट देखी जा सकती है। जब ऐसा होता है, तो उत्कृष्ट प्रदर्शन करने वाली, अत्यंत लाभदायक कंपनी तथा एक नए-नवेले कॉर्पोरेशन दोनों के शेयर की कीमतें प्रभावित हो सकती हैं। मूल्य में यह बदलाव \"बाजार जोखिम\" के कारण होता है।\n" +
                    "मुद्रास्फीति (इंफ़्लेशन) जोखिम: कभी-कभी इसे \"क्रय शक्ति का नुकसान\" के रूप में संदर्भित किया जाता है। जब भी मुद्रास्फीति की दर आपके निवेश पर होने वाली आय से अधिक होती है, तो आप जोखिम का वहन करते हैं अर्थात आप वास्तव में कम खरीद सकेंगे, अधिक नहीं।\n" +
                    "क्रेडिट (साख) जोखिम: संक्षेप में, जब आप निवेश के लिए अपना पैसा लगाते हैं तो वह कंपनी या संस्था कितनी स्थिर है? आप इस बात को लेकर कितने निश्चित हैं कि वायदे के अनुसार निवेश का ब्याज भुगतान करने में या निवेश के परिपक्व हो जाने पर आपके मूलधन को चुकाना में सक्षम होगी?\n" +
                    "ब्याज दर जोखिम: ब्याज दरों में परिवर्तन से कई मायनों में इक्विटी और बॉन्ड दोनों प्रभावित होते हैं। वित्तीय प्रणाली में बॉन्ड की कीमतें ब्याज दरों में हलचल से प्रभावित होती हैं। आम तौर पर, जब ब्याज दरें बढ़ती हैं तो प्रतिभूतियों की कीमतें गिरती हैं और जब ब्याज दरों में गिरावट होती है तो कीमतें बढ़ जाती हैं। भारतीय डेब्ट (ऋण) बाजारों में ब्याज दरों में हलचल से उतार-चढ़ाव हो सकता है जिससे डेब्ट और मुद्रा बाजार की प्रतिभूतियों में कीमतों में बहुत ज्यादा वृद्धि या गिरावट की संभावना बन सकती है, और इस प्रकार एनएवी में संभवतः बड़ी हलचल दिखाई दे सकती है।\n" +
                    "निवेश जोखिम: सेक्टोरल फ़ंड स्कीम के निवेश मुख्यतः विशेष सेक्टर्स में चुनिंदा कंपनियों की इक्विटी में होंगे। तदनुसार, स्कीम की एनएवी ऐसी कंपनियों के इक्विटी प्रदर्शन से जुड़ी होती है, और वह इक्विटी के अधिक डायवर्सिफ़ाइड पोर्टफ़ोलियो की तुलना में अधिक अस्थिर हो सकती है।\n" +
                    "लिक्विडिटी (तरलता) जोखिम: कम कारोबार वाली प्रतिभूतियों में अपने वास्तविक मूल्यों पर और आसपास आसानी से बिक्री योग्य नहीं होने का खतरा होता है। इसलिए फ़ंड मैनेजर एक अतरल बॉन्ड को त्वरित रूप से बेचने में असमर्थ हो सकता है, और इससे फ़ंड की कीमत पर प्रतिकूल असर पड़ सकता है। लिक्विडिटी (तरलता) जोखिम, भारतीय निश्चित आय बाज़ार की विशेषता है।\n" +
                    "सरकारी नीति में परिवर्तन: सरकारी नीति में परिवर्तन से, विशेष रूप से कर लाभ के संबंध में परिवर्तन से, कंपनियों की व्यवसाय संभावनाओं पर असर पड़ सकता है जिससे फ़ंड द्वारा किए गए निवेश पर असर पड़ता है।");
            listAnswer.add("आम तौर पर म्युचुअल फ़ंड, निवेशकों को गारंटीशुदा रिटर्न (प्रतिलाभ) की पेशकश नहीं करते हैं। हालाँकि, सेबी विनियमों से म्युचुअल फ़ंड्स को गारंटीशुदा रिटर्न की पेशकश करने की अनुमति मिलती है बशर्ते कि वह फ़ंड कुछ निश्चित शर्तों को पूरा करता हो, वैसे ज्यादातर फ़ंड इस तरह की गारंटी प्रदान नहीं करते हैं। एक गारंटीशुदा रिटर्न स्कीम के मामले में, प्रायोजक या एएमसी न्यूनतम स्तर के रिटर्न की गारंटी देता है और यदि वास्तविक रिटर्न गारंटीशुदा न्यूनतम रिटर्न से कम है तो यह अच्छा अंतर बनाता है। गारंटर का नाम और जिस तरीके से गारंटी पूरी की जाएगी, उसे म्युचुअल फ़ंड के प्रस्ताव दस्तावेज़ में प्रकट किया जाना चाहिए। भारत सरकार, भारतीय रिज़र्व बैंक या किसी अन्य सरकारी निकाय द्वारा म्युचुअल फ़ंड में निवेश की गारंटी नहीं दी जाती है।");
        } else if (lang.equals("gu")) {
            listQuestion.add("<p><strong>મ્યુચ્યુઅલ ફંડ શું હોય છે?</strong></p>");
            listQuestion.add("<p><strong>એસેટ મેનેજમેન્ટ કંપની એટલે શું?</strong></p>");
            listQuestion.add("<p><strong>કેવાયસી એટલે શું?</strong></p>");
            listQuestion.add("<p><strong>હું કેટલા નાણાં રોકી શકુ?</strong></p>");
            listQuestion.add("<p><strong>મ્યુચ્યુઅલ ફંડમાં રોકાણ કરવાથી શું લાભ થાય?</strong></p>");
            listQuestion.add("<p><strong>જોખમો કેવા પ્રકારના હોય છે?</strong></p>");
            listQuestion.add("<p><strong>શું મ્યુચ્યુઅલ ફંડ્સમાં વળતરની કોઈ ગેરેન્ટી હોય છે?</strong></p>");

            listAnswer.add("મ્યુચ્યુઅલ ફંડ રોકાણનો એક પ્રોગ્રામ છે જેમાં વિવિધ હોલ્ડિંગ્સમાં ટ્રેડિંગ કરતા શેરધારકો દ્વારા ફંડિંગ કરવામાં આવે છે અને પ્રોફેશનલ ધોરણે તેનું સંચાલન કરવામાં આવે છે.");
            listAnswer.add("એસેટ મેનજમેન્ટ કંપની (એએમસી) ઉચ્ચ સ્તરે નિયમન થતી સંસ્થા (હાઈલી રેગ્યુલેટેડ ઓર્ગેનાઈઝેશન) છે જે રોકાણકારો પાસેથી નાણાં લે છે અને ચોક્કસ પોર્ટફોલિયોમાં તેનું રોકાણ કરે છે. ફંડ મેનેજમેન્ટ માટે તેઓ ખૂબ ઓછી ફી વસુલે છે.");
            listAnswer.add("નો યોર કસ્ટમરને સામાન્યપણે કેવાયસી તરીકે ઓળખવામાં આવે છે, જે બેંક અથવા ફાઈનાન્સિઅલ ઈન્સ્ટિટ્યુટને પોતાના ગ્રાહકોની ઓળખનું પ્રમાણીકરણ કરવા સમર્થ બનાવે છે. તે નાણાં-ઉચાપત પ્રવૃત્તિઓને અવરોધવામાં મદદ કરે છે અને રોકાણકારોએ કરેલી ડિપોઝીટ/રોકાણ વાસ્તવિક વ્યક્તિના નામે જ છે અને તેમાં કશું જ બોગસ નથી તેની ખાતરી કરાવે છે. ");
            listAnswer.add("અમે હાલમાં નોન-કેવાયસી સુસંગત યુઝર્સ માટે ઈકેવાયસીનો ઉપયોગ કરીએ છીએ. ભારત સરકારના નિયમનો અનુસાર, ઈકેવાયસી સુસંગત રોકાણકાર પ્રત્યેક વર્ષે એએમસી દીઠ રૂપિયા 50,000 સુધીનું રોકાણ કરી શકે છે.");
            listAnswer.add("મ્યુચ્યુઅલ ફંડમાં રોકાણ કરવાના લાભો નીચે દર્શાવ્યા અનુસાર છે -\n" +
                    "પ્રોફેશનલ નાણાં સંચાલકો સુધી પહોંચી શકાય છે – અનુભવી ફંડ મેનેજર્સ દ્વારા અદ્યતન વૈજ્ઞાનિક અને ગાણિતિક ટેકનિક્સનો ઉપયોગ કરીને તમારા નાણાંનું સંચાલન કરવામા આવે છે.\n" +
                    "વૈવિદ્યતા – મ્યુચ્યુઅલ ફંડ્સ વિવિધ ઉદ્યોગો અને ક્ષેત્રોના બોર્ડ સેક્શનમાં સંખ્યાબંધ કંપનીઓમાં રોકાણ કરીને વૈવિદ્યતા દ્વારા વળતરના અત્યંત અને ઝડપી ચઢાવઉતારને ઘટાડે છે. તેઓ રોકાણકારને “બધા જ ઈંડા એક બાસ્કેટમાં” મુકવાથી રોકે છે. તેના કારણે જોખમની સંભાવનાઓ ઘટી જાય છે. આમ, નાની રોકાણપાત્ર સિલકથી રોકાણકાર વૈવિદ્યતા હાંસલ કરી શખે છે જે અન્ય કોઈ રીતે શક્ય નથી.\n" +
                    "લિક્વિડિટી – ઓપન એન્ડેડ મ્યુચ્યુઅલ ફંડ્સની કિંમત દરરોજ બદલાય છે અને હંમેશા રોકાણકારો પાસેથી યુનિટ્સ બાયબેકની તૈયારી ધરાવે છે. આનો અર્થ એવો થાય કે, રોકાણકારો મ્યુચ્યુઅલ ફંડ્સમાં તેમનું રોકાણ કોઈપણ સમયે યોગ્ય કિંમતે ખરીદદાર શોધવાની ચિંતામાં પડ્યા વગર વેચી શકે છે. અન્ય સ્ટોક્સ (શેર) અને બોન્ડ્સ જેવા રોકાણના સાધનોમાં ખરીદદારો મળી રહે તેવું જરૂરી નથી જેથી રોકાણના આ સાધનોમાં મ્યુચ્યુઅલ ફંડ્સની ઓપન એન્ડેડ સ્કીમની તુલનાએ ઓછી લિક્વિડિટી (તરલતા) જોવા મળે છે.\n" +
                    "ટ્રાન્ઝેક્શનનો ઓછો ખર્ચ – મ્યુચ્યુઅલ ફંડ્સમાં ખૂબ જ મોટી સંખ્યામાં રોકાણકારોનો જમાવડો હોવાથી, શેરમાં રોકવામાં આવેલી રકમ ઘણી વિપુલ હોય છે. પરિણામે માપદંડોની અર્થવ્યવસ્થાના કારણે તેમાં ઘણી ઓછી બ્રોકરેજ ચુકવવી પડે છે.\n" +
                    "પારદર્શકતા – ઓપન એન્ડેડ મ્યુચ્યુઅલ ફંડ્સની કિંમત દરરોજ જાહેર કરવામાં આવે છે. તમારા રોકાણના મુલ્યના નિયમિત અપડેટ્સ ઉપલબ્ધ હોય છે. નિયમિત ધોરણે ફંડ મેનેજર્સની રોકાણ વ્યૂહરચના અને દૃષ્ટીકોણ સાથે પોર્ટફોલિયો જાહેર કરવામાં આવે છે.\n" +
                    "સુસંચાલિત ઈન્ડસ્ટ્રી – તમામ મ્યુચ્યુઅલ ફંડ્સ સેબીમાં નોંધણીકૃત હોય છે અને રોકાણકારોના હિતોના રક્ષણ માટે ઘડવામાં આવેલા ચુસ્ત નિયમો હેઠળ તે કામ કરે છે.\n" +
                    "નાના રોકાણકારોની અનુકૂળતાઃ સામાન્ય સંજોગોમાં, કોઈપણ વ્યક્તિગત રોકાણકાર રોકાણની ઓછી મૂડી અને ઊંચી ટ્રાન્ઝેક્શન કિંમતના કારણે સંખ્યાબંધ શેરોની શ્રેણીમાં પોતાનું રોકાણ વૈવિદ્યસભર રીતે ન કરી શકે (અને તેનાથી જોખમ ઘટે છે). બીજી બાજુ, મ્યુચ્યુઅલ ફંડ્સ વ્યક્તિગત રોકાણકારોને પણ સંખ્યાબંધ શેરોમાં રોકાણ દ્વારા વૈવિદ્યસભર રોકાણની તક આપે છે કારણ કે તેઓ સ્ટોક્સના પોર્ટફોલિયોમાં જ રોકાણ કરે છે. આથી મ્યુચ્યુઅલ ફંડ્સ રોકાણકારોને મોટા પ્રમાણમાં રોકાણ કર્યા વગર જોખમ વૈવિદ્યતાની પરવાનગી આપે છે.");
            listAnswer.add("રોકાણના કોઈપણ સ્વરૂપમાં જોખમનું પરિબળ સાથે આવે જ છે. મ્યુચ્યુઅલ ફંડ્સ રોકાણના કિસ્સામાં, જોખમમાં પરિવર્તનશીલતા, સમયાંતરે કુલ વળતરમાં વધઘટ સમાવી લેવામાં આવે છે. સ્કીમના રોકાણોનું મુલ્ય શેરબજારમાં કિંમત અને વોલ્યૂમની વધઘટ, વ્યાજદરો, કરન્સી વિનિમય દરો, વિદેશી રોકાણ, સરકારની નીતિઓમાં ફેરફાર, રાજકીય, આર્થિક અથવા અન્ય ડેવલપમેન્ટ્સ જેવા મૂડી બજારને અસર કરતા પરિબળોથી પ્રભાવિત હોઈ શકે છે.\n" +
                    "બજારનું જોખમઃ બાહ્ય પ્રભાવોના કારણે કોઈપણ સમયે ચોક્કસ બજારમાં તમામ સિક્યુરિટી (શેરો)માં કિંમત અથવા વળતરમાં વધારો અથવા ઘટાડો થઈ શકે છે. આવું થાય ત્યારે, આઉટ સ્ટેન્ડિંગ, અતિ લાભદાયી કંપની અને નવા નવા કોર્પોરેશન (કંપનીઓ) બંનેના શેરની કિંમતોને અસર પડી શકે છે. કિંમતોમાં થતું આ પરિવર્તન “બજારના જોખમો”ના કારણે હોય છે.\n" +
                    "ફુગાવાનું જોખમઃ કેટલીક વખત આને “ખરીદશક્તિમાં ઘટાડો” એમ પણ કહેવામાં આવે છે. જ્યારે પણ ફુગાવાનો દર તમારા રોકાણની કમાણી કરતા વધી જાય, ત્યારે તમે એવા જોખમ તરફ આગળ વધો છો જેમાં તમે વાસ્તવમાં ઓછું ખરીદી શકો છો, વધુ નહીં.\n" +
                    "ધિરાણ જોખમઃ ટૂંકમાં, તમે જ્યારે રોકાણ કરો ત્યારે તે કંપની અથવા સંસ્થા કેટલી સ્થિર હોય છે? તમે કેટલી હદે નિશ્ચિંત હોવ છો કે, તેમણે આપેલા વચન પ્રમાણે તમારા નાણાં પર તેઓ વ્યાજ ચુકવશે અથવા રોકાણ પાકી જાય ત્યારે તમારી મુદ્દલ પાછી આપશે?\n" +
                    "વ્યાજ દરનું જોખમઃ બદલાતા વ્યાજ દરો શેર અને બોન્ડ્સ બંનેને અનેક પ્રકારે અસર કરે છે. આર્થિક તંત્રમાં બદલાતા વ્યાજ દરોની હિલચાલની અસર બોન્ડની કિંમત પર પડે છે. સામાન્યપણે, જ્યારે વ્યાજ દર વધે ત્યારે, શેરોના ભાવ ઘટે છે અને જ્યારે વ્યાજ દર ઘટે ત્યારે, શેરોના ભાવ વધે છે. ભારતીય ડેબ્ટ માર્કેટમાં વ્યાજ દરોની હિલચાલ ભારે વધઘટ વાળી હોઈ શકે છે અને તેના કારણે ડેબ્ટ અને નાણાં બજાર શેરોમાં ભાવોમાં મોટી હિલચાલની શક્યતા થાય છે અને તેના પરિણામે એનએવીમાં ઘણી મોટી વધઘટ જોવા મળે છે.\n" +
                    "રોકાણના જોખમોઃ સેક્ટોરલ ફંડ સ્કીમ્સમાં, મુખ્યત્વે કોઈ ચોક્કસ ક્ષેત્રમાં પસંદગીની કંપનીના શેરોમાં રોકાણ કરવામાં આવે છે. તદ અનુસાર, સ્કીમની એનએવી તે કંપનીઓના પરફોર્મન્સ સાથે સંકળાયેલી હોય છે અને જ્યારે વધુ શેરોના વધુ વૈવિદ્યતાપૂર્ણ પોર્ટફોલિયોમાં રોકાણ કર્યું હોય ત્યારે તેમાં વધારે વધઘટની સંભાવના રહે છે.\n" +
                    "લિક્વિડિટી જોખમઃ ખૂબ જ ઓછુ ટ્રેડિંગ થતા શેરોમાં તેના વાસ્તિવક મુલ્ય પર અથવા તેની નજીકના મુલ્ય પર સરળતાથી વેચાણની શક્યતા ન હોવાનું જોખમ આવે છે. આથી ફંડ મેનેજર ઈલલિક્વિડ (તરલતા વગરના) બોન્ડ્સ સરળતાથી વેચી શકતા નથી અને તેનાથી પ્રતિકૂળરૂપે ફંડની કિંમત પર અસર પડી શકે છે. લિક્વિડિટી જોખમ ભારતીય ફિક્સ્ડ ઈન્કમ માર્કેટની એક લાક્ષાણિકતા છે.\n" +
                    "સરકારની નીતિઓમાં ફેરફારઃ ખાસ કરીને ટેક્સ લાભો સહિત સરકારની વિવિધ નીતિઓમાં આવતા ફેરફારો કંપનીઓના વ્યવસાયની સંભાવનાઓને અસર કરે છે અને તેના કારણે ફંડ દ્વારા કરાતા રોકાણ પર અસર જોવા \n" +
                    "મળે છે.");
            listAnswer.add("સામાન્યપણે, મ્યુચ્યુઅલ ફંડ્સ રોકાણકારોને કોઈ ચોક્કસ વળતરની ગેરેન્ટી નથી આપતા. છતાં પણ, સેબી નિયમનોના કારણે મ્યુચ્યુઅલ ફંડ્સ ગેરેન્ટીડ વળતર ઓફર કરે છે જે કોઈપણ ફંડ નિશ્ચિત પરિસ્થિતિને પહોંચી વળે તેને આધીન હોય છે, મોટાભાગના ફંડ્સ આવી ગેરેન્ટી નથી આપતા. ગેરેન્ટીડ વળતરની સ્કીમ હોય તેવા કિસ્સામાં, સ્પોન્સર અથવા એએમસી, લઘુતમ સ્તરના વળતરની ગેરેન્ટી આપે છે અને જો વાસ્તવિક વળતર ગેરેન્ટીડ વળતર કરતા ઓછું હોય તો તફાવત પુરો કરવા ઘટતું કરે છે. ગેરેન્ટર અને કયા પ્રકારની ગેરેન્ટી આપવામાં આવે છે તે બંને મ્યુચ્યુઅલ ફંડ દ્વારા આપવામાં આવતા દસ્તાવેજમાં જાહેર કરવું આવશ્યક છે. મ્યુચ્યુઅલ ફંડ્સમાં રોકાણની ભારત સરકાર, ભારતીય રિઝર્વ બેંક અથવા અન્ય કોઈપણ સરકારી વિભાગ દ્વારા કોઈ ગેરેન્ટી લેવામાં આવતી નથી.");
        } else {
            listQuestion.add("<p><strong>म्युच्युअल फंड म्हणजे काय?</strong></p>");
            listQuestion.add("<p><strong>असेट मॅनेजमेंट कंपनी म्हणजे काय?</strong></p>");
            listQuestion.add("<p><strong>केवायसी म्हणजे काय?</strong></p>");
            listQuestion.add("<p><strong>मी किती पैसे गुंतवू शकतो?</strong></p>");
            listQuestion.add("<p><strong>म्युच्युअल फंडात गुंतवणूक करण्याचे फायदे कोणते आहेत?</strong></p>");
            listQuestion.add("<p><strong>जोखमीचे प्रकार कोणते आहेत?</strong></p>");
            listQuestion.add("<p><strong>म्युच्युअल फंडांपासून परतावा मिळण्याची हमी असते का?</strong></p>");

            listAnswer.add("म्युच्युअल फंड म्हणजे वैविध्यपूर्ण होल्डिंग्जमध्ये ट्रेड करणाऱ्या भागधारकांकडून निधी पुरवठा केलेला कार्यक्रम असतो आणि त्याचे व्यवस्थापन व्यावसायिकदृष्ट्या केले जाते. ");
            listAnswer.add("असेट मॅनेजमेंट कंपनी (एएमसी) म्हणजे उच्च नियमन असलेली संस्था असते, जी गुंतवणूकदारांकडून पैसे एकत्र करून ते पोर्टफोलिओमध्ये गुंतवते. ते निधीचे व्यवस्थापन करण्यासाठी छोटेसे शुल्क आकारतात. ");
            listAnswer.add("नो युवर कस्टमर - म्हणजेच तुमच्या ग्राहकाबद्दल जाणून घ्या - ज्याला सामान्यपणे केवायसी असे म्हणतात, त्यामुळे बँकेला किंवा वित्तीय संस्थेला तिच्या ग्राहकांची अधिकृतता निश्चित करणे शक्य होते. यामुळे काळ्या पैशाचे रूपांतर पांढऱ्या पैशात करण्यास आळा घातला जातो आणि त्याहीपुढे ठेवी/गुंतवणुका खोट्या व्यक्तींच्या नावे न करता खऱ्या व्यक्तींच्या नावे केल्या जात असल्याची काळजी घेतली जाते. ");
            listAnswer.add("ज्या वापरकर्त्यांनी केवायसी केलेले नाही त्यांच्यासाठी आम्ही हल्ली ई-केवायसीचा वापर करतो. भारत सरकारच्या नियमांनुसार ई-केवायसी पूर्ण केलेले गुंतवणूकदार दर एएमसीमागे दरवर्षी ५०,००० रुपयांपर्यंत गुंतवणूक करू शकतात. ");
            listAnswer.add("म्युच्युअल फंडात गुंतवणूक करण्याचे फायदे पुढीलप्रमाणे आहेत -\n" +
                    "व्यावसायिक निधी व्यवस्थापकांशी संपर्क - तुमच्या पैशाचे व्यवस्थापन प्रगत शास्त्रशुद्ध आणि गणितीय तंत्रांचा वापर करून अनुभवी फंड मॅनेजर्सकडून केले जाते. \n" +
                    "वैविध्यपूर्णता - उद्योग आणि उद्योग-क्षेत्रांच्या विस्तृत विभागातील अनेक कंपन्यांमध्ये गुंतवणूक करून वैविध्यपूर्णतेच्या माध्यमातून परताव्याची अस्थिरता कमी करणे हा म्युच्युअल फंडांचा उद्देश असतो. यामुळे गुंतवणूकदाराला ‘‘एकाच ठिकाणी सर्व गुंतवणूक’’ करण्यापासून आळा घातला जातो. यामुळे अर्थातच जोखीम कमी होते. अशा प्रकारे गुंतवणूक करण्यासाठी थोडीशी जास्तीची रक्कम हाताशी असलेला गुंतवणूकदार वैविध्यपूर्णता प्राप्त करू शकतो, जे अन्यथा शक्य होणार नाही.\n" +
                    "तरलता (रोखीकरणाची क्षमता) - ओपन एंडेड म्युच्युअल फंड्\u200Cसची किंमत दररोज ठरवली जाते आणि ते गुंतवणूकदारांकडून नेहमीच युनिट्\u200Cस खरेदी करण्यास तयार असतात. ह्याचाच अर्थ असा आहे की, गुंतवणूकदार योग्य किमतीला खरेदीदार शोधण्याची चिंता न करता त्यांची म्युच्युअल फंडातील गुंतवणूक कोणत्याही वेळी विकू शकतो. स्टॉक्स आणि बाँड्\u200Cस यांसारख्या गुंतवणुकीच्या इतर क्षेत्रांच्या बाबतीत खरेदीदार उपलब्ध असतीलच असे नाही आणि म्हणून गुंतवणुकीची ही क्षेत्रे म्युच्युअल फंडाच्या ओपन-एंडेड योजनांच्या तुलनेत कमी तरलताक्षम असतात. \n" +
                    "व्यवहारांचा कमी खर्च - म्युच्युअल फंडांमध्ये अनेक गुंतवणूकदारांचे पैसे एकत्र केलेले असतात त्यामुळे सेक्युरिटिज्\u200Cमध्ये केल्या गेलेल्या गुंतवणुकीचे प्रमाण मोठे असते. ह्याचा परिणाम, ‘इकॉनॉमी ऑफ स्केल’मुळे कमी दलाली देण्यात होतो. \n" +
                    "पारदर्शकता - ओपन एंडेड म्युच्युअल फंडांच्या किमती दररोज जाहीर केल्या जातात. तुमच्या गुंतवणुकीच्या मूल्यांसंबंधीचे नियमित अपडेट्\u200Cस उपलब्ध असतात. फंड मॅनेजरच्या गुंतवणूक धोरणांसाठी आणि दूरदृष्टीसाठी पोर्टफोलिओ नियमितपणे उघड केला जातो.\n" +
                    "सुनियमित उद्योग - सर्व म्युच्युअल फंडांची नोंदणी सेबीकडे केलेली असते आणि ते गुंतवणूकदारांच्या हिताचे रक्षण करण्यासाठी कठोर नियमांच्या अंतर्गत कार्य करीत असतात. \n" +
                    "लहान गुंतवणुकांसाठीची सोय - सामान्य स्थितीत वैयक्तिक गुंतवणूकदार छोट्या गुंतवणुकीमुळे आणि या व्यवहारांवर कराव्या लागणाऱ्या जास्तीच्या खर्चामुळे त्याच्या गुंतवणुकीत वैविध्यपूर्णता आणू शकत नाही (आणि म्हणून जोखीमही कमी करू शकत नाही). दुसरीकडे, म्युच्युअल फंडात वैयक्तिक गुंतवणूकदारालाही वैविध्यपूर्ण सेक्युरिटिज्\u200C धारण करता येतात, कारण म्युच्युअल फंड स्टॉक्सच्या पोर्टफोलिओमध्ये गुंतवणूक करीत असतात. म्हणून म्युच्युअल फंडात गुंतवणूकदाराला मोठी रक्कम न गुंतवताही जोखमीत वैविध्यपूर्णता आणता येते. ");
            listAnswer.add("जोखीम ही प्रत्येक प्रकारच्या गुंतवणुकीचा मूळ भाग आहे. म्युच्युअल फंडांतील गुंतवणुकांसाठीच्या जोखमींमध्ये वारंवार होणारे बदल, किंवा एकूण परताव्यात प्रत्येक कालागणिक होणारे चढउतार यांचा समावेश असेल. योजनेच्या गुंतवणुकीचे मूल्य हे स्टॉक मार्केट्\u200Cसमधील किंमत आणि प्रमाण यातील अस्थिरता, व्याजदर, चलन विनिमय दर, परकीय गुंतवणूक, सरकारच्या धोरणांमधील बदल, राजकीय, आर्थिक किंवा इतर घडामोडी यांसारख्या भांडवली बाजारातील घटकांमुळे प्रभावीत होऊ शकते. \n" +
                    "बाजारपेठेतील जोखीम: कधीकधी एखाद्या विशिष्ट बाजारपेठेतील सेक्युरिटिज्\u200Cच्या किमती किंवा परतावे बाहेरील घटकांच्या प्रभावामुळे वाढतात किंवा कमी होतात. जेव्हा असे घडते, तेव्हा उत्तम कामगिरी करणाऱ्या आणि मोठ्या प्रमाणावर नफा कमावणाऱ्या कंपन्या आणि फ्लेजिंग कॉर्पोरेशन यांच्यावर परिणाम होऊ शकतो. किमतीतील हा बदल ‘‘बाजारपेठेतील जोखमीमुळे’’ होतो. \n" +
                    "तेजीची जोखीम: ह्याला कधीकधी ‘‘क्रयशक्तीची हानी’’ असे म्हणतात. जेव्हा तुमच्या गुंतवणुकीवरील परताव्यापेक्षा तेजीचा दर अधिक असतो, तेव्हा तुम्ही प्रत्यक्षात कमी खरेदी करू शकता, अधिक नाही, अशी जोखीम असेल.\n" +
                    "पतविषयक जोखीम: थोडक्यात, तुम्ही जेव्हा गुंतवणूक करता तेव्हा तुम्ही ज्या कंपनीला किंवा संस्थेला तुमचे पैसे उसने देता ती किती स्थिर आहे? तुम्हाला जेवढे व्याज मिळेल असे सांगितले गेले आहे किंवा गुंतवणुकीची मुदतपूर्ती झाल्यानंतर मुद्दल रक्कम परत मिळण्याविषयी तुम्हाला किती खात्री आहे?\n" +
                    "व्याजदरासंबंधीची जोखीम: बदलत्या व्याजदरांचा परिणाम इक्विटीज्\u200C आणि बाँड्\u200Cस अशा दोहोंवरही अनेक मार्गांनी होत असतो. बाँड्\u200Cसच्या किमतीवर वित्तीय प्रणालीमधील व्याजदरांतील बदलांचा परिणाम होत असतो. सामान्यत:, जेव्हा व्याजदर वाढतात, तेव्हा सेक्युरिटिज्\u200Cच्या किमती कमी होतात आणि जेव्हा व्याजदर कमी होतात, तेव्हा किमती वाढतात. भारतीय डेट मार्केटमधील व्याजदरातील बदल अस्थिर असू शकतात, ज्यामुळे डेट आणि मनी मार्केट सेक्युरिटिज्\u200Cच्या किमतीत मोठ्या प्रमाणावर चढउतार होतात, ज्यामुळे एनएव्हीमध्ये मोठ्या प्रमाणात बदल होण्याची शक्यता असते.\n" +
                    "गुंतवणुकीतील जोखीम: सेक्टोरल फंड स्कीम्समध्ये गुंतवणूक मुख्यत: विशिष्ट क्षेत्रातील निवडक कंपन्यांच्या सेक्युरिटिज्\u200Cमध्ये असेल. त्यानुसार, स्कीम्सचे एनएव्ही अशा कंपन्यांच्या इक्विटी परफॉर्मन्सशी जोडलेल्या असतात आणि इक्विटीज्\u200Cच्या अधिक वैविध्यपूर्ण पोर्टफोलिओपेक्षा अधिक अस्थिर असू शकतात. \n" +
                    "तरलतेची जोखीम: ज्यांचे क्वचितच ट्रेडिंग केले जाते, अशा सेक्युरिटिज्\u200Cला त्या त्यांच्या जवळपासच्या किंवा खऱ्या किमतीला सहजगत्या विकल्या न जाण्याचा धोका असतो. म्हणून फंड मॅनेजरला तरलता नसलेला बाँड जलदगतीने विकणे शक्य होणार नाही आणि ह्याचा विपरीत परिणाम फंडाच्या किमतीवर होऊ शकतो. तरलतेसंबंधीची (रोखीकरण) जोखीम हे भारतीय स्थिर उत्पन्न बाजारपेठेचे वैशिष्ट्य आहे.\n" +
                    "सरकारच्या धोरणांमधील बदल: सरकारच्या धोरणांमधील बदल, विशेषत: करांमधील लाभांविषयीचे बदल कंपन्यांच्या व्यवसायावर परिणाम करू शकतात, ज्याचा परिणाम फंडांनी केलेल्या गुंतवणुकीवर होतो. ");
            listAnswer.add("सामान्यत: म्युच्युअल फंडांमधून गुंतवणूकदारांना परताव्याची हमी दिलेली नसते. फंडांनी विशिष्ट अटी पूर्ण केल्यास, सेबीच्या नियमांनी जरी म्युच्युअल फंडांना परताव्याची हमी देण्याची परवानगी दिलेली असली, तरी बहुतेक फंड अशी हमी देत नाहीत. परताव्याची हमी दिलेल्या योजनेच्या बाबतीत, प्रायोजक किंवा एएमसी परताव्याच्या विशिष्ट पातळीची हमी देतात आणि जर वास्तव परतावा किमान हमी दिलेल्या परताव्यापेक्षा कमी असेल, तर तो भरून काढतात. हमी देणाऱ्याचे नाव आणि हमी कोणत्या प्रकारे पूर्ण केली जाईल हे म्युच्युअल फंडांनी ऑफरच्या दस्तऐवजात स्पष्ट केलेले असायला हवे. म्युच्युअल फंडांतील गुंतवणुकांची हमी भारत सरकार, भारतीय रिझर्व्ह बँक किंवा अन्य सरकारी संस्थेने दिलेली नसते. ");
        }

    }
}
