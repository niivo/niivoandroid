package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 9/6/17.
 */

public class InvestedSchemeDetailFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Context contextd;
    TextView ammountInvested, investmentValue, totalEarnings, nav, numOfUnit, fundClass, investmentType, paidInstallment, schemeName;
    TextView cancelSIP;
    //non uivars
    RequestedServiceDataModel requestedServiceDataModel;

    MainActivity activity;
    private String order_type;
    private TextView tv_fund_class;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_invested_scheme_detail, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.myMoney));
        activity = (MainActivity) getActivity();
        ammountInvested = (TextView) rootView.findViewById(R.id.detailAmountInvested);
        tv_fund_class = (TextView) rootView.findViewById(R.id.tv_fund_class);
        investmentValue = (TextView) rootView.findViewById(R.id.detailInvestmentValue);
        totalEarnings = (TextView) rootView.findViewById(R.id.detailTotalEarnings);
        nav = (TextView) rootView.findViewById(R.id.detailNav);
        numOfUnit = (TextView) rootView.findViewById(R.id.detailNumberOfUnit);
        fundClass = (TextView) rootView.findViewById(R.id.detailFundClass);
        investmentType = (TextView) rootView.findViewById(R.id.detailInvestmentType);
        paidInstallment = (TextView) rootView.findViewById(R.id.detailPaidInstallment);
        schemeName = (TextView) rootView.findViewById(R.id.detailSchemeName);
        cancelSIP = (TextView) rootView.findViewById(R.id.cancelSipBtn);
        View line1 = (View) rootView.findViewById(R.id.line1);
        View line2 = (View) rootView.findViewById(R.id.line2);
        TextView view_paid_installments = (TextView)rootView.findViewById(R.id.view_paid_installments);
        ImageView arrow_view_paid_installments = (ImageView)rootView.findViewById(R.id.arrow_view_paid_installments);

        schemeName = (TextView) rootView.findViewById(R.id.detailSchemeName);
        cancelSIP = (TextView) rootView.findViewById(R.id.cancelSipBtn);

        ammountInvested.setText(Common.currencyString(getArguments().getString("ammountInvested"),false));
        investmentValue.setText(Common.currencyString(getArguments().getString("investmentValue"),false));
        totalEarnings.setText(Common.currencyString(getArguments().getString("totalEarnings"),false));
        nav.setText(Common.currencyString(getArguments().getString("nav"),true));
        numOfUnit.setText(getArguments().getString("numOfUnit"));
        fundClass.setText(getArguments().getString("fundClass"));
        investmentType.setText(getArguments().getString("investmentType"));
       // if(getArguments().get()
        order_type = getArguments().getString("investmentOrderType");
        tv_fund_class.setText(getString(R.string.payment_mode));
        if(order_type.equalsIgnoreCase("SIP"))
        {
            tv_fund_class.setText(getString(R.string.payment_mode));
            fundClass.setText(getString(R.string.manual));
        }
        else if(order_type.equalsIgnoreCase("XSIP"))
        {
            tv_fund_class.setText(getString(R.string.payment_mode));
            fundClass.setText(getString(R.string.automatic_deduction));
        }
        paidInstallment.setText(getArguments().getString("paidInstallment"));
        schemeName.setText(getArguments().getString("schemeName"));
        cancelSIP.setVisibility((!getArguments().getString("orderType").equals("ONETIME") &&
                getArguments().getString("from").equals("direct")) ? View.VISIBLE : View.GONE);
        cancelSIP.setOnClickListener(this);
        if(  getArguments().getBoolean("isGroupEntries")) {
           final OrderedList.OrderedItem orderedItemArrayList = (OrderedList.OrderedItem)getArguments().getSerializable("schemeDetail");
            view_paid_installments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment frag = new InvestedSchemeDetailGroupFragment();
                    Bundle b = new Bundle();
                    b.putString("regDate", getArguments().getString("regDate"));
                    b.putString("fundName", getArguments().getString("fundName"));
                    b.putString("schemeDetail", getArguments().getString("fundName"));
                    b.putString("order_type", order_type);
                    try {
                            b.putSerializable("schemeDetail", orderedItemArrayList);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    frag.setArguments(b);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, frag).addToBackStack(null).
                            commit();
                }
            });
            arrow_view_paid_installments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment frag = new InvestedSchemeDetailGroupFragment();
                    Bundle b = new Bundle();
                    b.putString("regDate", getArguments().getString("regDate"));
                    b.putString("fundName", getArguments().getString("fundName"));
                    b.putString("schemeDetail", getArguments().getString("fundName"));
                    b.putString("order_type", order_type);
                    try {
                        b.putSerializable("schemeDetail", orderedItemArrayList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    frag.setArguments(b);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, frag).addToBackStack(null).
                            commit();
                }
            });
         //   setDetails((OrderedList.OrderedItem) getArguments().getSerializable("schemeDetail"));
        }
        else
        {
            line1.setVisibility(View.INVISIBLE);
            line2.setVisibility(View.INVISIBLE);
            view_paid_installments.setVisibility(View.INVISIBLE);
            arrow_view_paid_installments.setVisibility(View.INVISIBLE);
            // View line1 = (View) rootView.findViewById(R.id.line1);
           /* View line2 = (View) rootView.findViewById(R.id.line2);
            TextView view_paid_installments = (TextView)rootView.findViewById(R.id.view_paid_installments);
            ImageView arrow_view_paid_installments = (ImageView)rootView.findViewById(R.id.arrow_view_paid_installments);
*/

        }
        return rootView;
    }
    void setDetails(OrderedList.OrderedItem orderedItemArrayList) {
        // OrderedList.OrderedItem orderedItemArrayList = new Gson().fromJson(json,OrderedList.OrderedItem.class);
        Log.i("setDetails1: ",orderedItemArrayList.getOrderedItemArrayListSub().get(0).getDtdate());
        Log.i("setDetails2: ",orderedItemArrayList.getOrderedItemArrayListSub().get(1).getDtdate());

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelSipBtn:
                if (!getArguments().getString("orderType").equals("ONETIME"))
                    cancelSIP();
                break;
        }
    }

    void cancelSIP(String userid, String orderid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("order_type",order_type);
        requestedServiceDataModel.putQurry("orderid", orderid.trim());
        requestedServiceDataModel.putQurry("is_goal", "0");
        requestedServiceDataModel.putQurry("goal_id", "");
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
         //       Log.e("cancelRes", json);
                whenSuccess(message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    if (jsonObject.has("retry")) {
                        if (jsonObject.getString("retry").trim().equals("1")) {
                            retryLastRequest(message);
                        }
                    } else {
                        Common.showToast(contextd, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void whenSuccess(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.home), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.getBackToPortfolio();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void retryLastRequest(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), getArguments().getString("orderId"));
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void cancelSIP() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(contextd.getResources().getString(R.string.cancelMonthlyBasedSIP));
        builder.setPositiveButton(contextd.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), getArguments().getString("orderId"));
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

}
