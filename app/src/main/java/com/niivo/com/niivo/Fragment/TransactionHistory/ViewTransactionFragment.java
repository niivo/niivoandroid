package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;

import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.AllTransactionAdapter;
import com.niivo.com.niivo.Model.ViewTransaction;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ViewTransactionFragment extends Fragment implements ResponseDelegate{
    Context contextd;
    MainActivity activity;
    RequestedServiceDataModel requestedServiceDataModel;
    ListView transactionlist;
    ViewTransaction viewTransaction;
    AllTransactionAdapter transactionAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView emptymsg;


    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_transaction_list, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        transactionlist = (ListView) rootView.findViewById(R.id.transactionlist);
        textView.setText(contextd.getResources().getString(R.string.myTransaction));
        activity = (MainActivity) getActivity();
        emptymsg= (TextView) rootView.findViewById(R.id.emptymsg);
        //filter=(ImageView) rootView.findViewById(R.id.filter);
//
////        swipeRefreshLayout =(SwipeRefreshLayout)rootView.findViewById(R.id.pullToRefresh);
////        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.textGreen));
////        swipeRefreshLayout.setDistanceToTriggerSync(250);
////        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
////            @Override
////            public void onRefresh() {
////                getViewTransaction(); // your code
////                swipeRefreshLayout.setRefreshing(false);
////            }
////        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        final String Kycstatuss = prefs.getString("Kycstatuss", null);
        Log.d("Kycstatuss::::",Kycstatuss);

        //investedListRV.setVisibility(View.GONE);
        if(Kycstatuss.equals("NOT STARTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
        }else if(Kycstatuss.equals("SUBMITTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.kycSubmit));
        }else if(Kycstatuss.equals("COMPLETE")) {
            //emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.noinvestment));
        }

        getViewTransaction();
        return rootView;
    }

    private void getViewTransaction() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("ALLINVEST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeRequestTransaction();
    }

    private void readJson(JSONObject jobj) {

        ArrayList<String> list=new ArrayList<String>();
        JSONArray jsonArray = null;
        try {
            jsonArray = jobj.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String year=obj.getString("fundname");

                if (!list.contains(year)) {
                    list.add(year);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("DistictreadJson", String.valueOf(list));
    }
    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData)  {
        switch (baseRequestData.getTag()) {

            case ResponseType.HoldingList:
              // emptymsg.setVisibility(View.GONE);
                    Gson gson = new Gson();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    readJson(jsonObject);
                    viewTransaction = gson.fromJson(json, ViewTransaction.class);
                    transactionAdapter = new AllTransactionAdapter(contextd, viewTransaction);
                    transactionlist.setAdapter(transactionAdapter);
                    break;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                emptymsg.setVisibility(View.VISIBLE);
               // Common.showToast(contextd, contextd.getResources().getString(R.string.noTransaction));
                break;
        }
    }

}
