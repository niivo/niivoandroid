package com.niivo.com.niivo.Model;

import java.io.Serializable;

/**
 * Created by deepak on 1/10/18.
 */

public class ItemPanDetails implements Serializable {
    String status, msg;
    PanDetail data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PanDetail getData() {
        return data;
    }

    public void setData(PanDetail data) {
        this.data = data;
    }

    public class PanDetail implements Serializable {
        String pancard_no, name, kyc_status;
        PAN_Card pan_data;

        public String getPancard_no() {
            return pancard_no;
        }

        public void setPancard_no(String pancard_no) {
            this.pancard_no = pancard_no;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getKyc_status() {
            return kyc_status;
        }

        public void setKyc_status(String kyc_status) {
            this.kyc_status = kyc_status;
        }

        public PAN_Card getPan_data() {
            return pan_data;
        }

        public void setPan_data(PAN_Card pan_data) {
            this.pan_data = pan_data;
        }
    }

    public class PAN_Card implements Serializable {
        String APP_TYPE, APP_IPV_FLAG, APP_GEN, APP_NAME, APP_F_NAME, APP_DOB_DT, APP_NATIONALITY,APP_RES_STATUS, APP_OTH_NATIONALITY,
                APP_COR_ADD1, APP_COR_ADD2, APP_COR_ADD3, APP_COR_CITY, APP_COR_PINCD, APP_COR_STATE, APP_COR_CTRY,
                APP_EMAIL, APP_COR_ADD_PROOF, APP_COR_ADD_REF, APP_PER_ADD1, APP_PER_ADD2, APP_PER_ADD3, APP_PER_CITY,
                APP_PER_PINCD, APP_PER_STATE, APP_PER_CTRY, APP_PER_ADD_PROOF, APP_INCOME, APP_OCC, APP_OTH_OCC, APP_DOC_PROOF,
                APP_MAR_STATUS;

        public String getAPP_RES_STATUS() {
            return APP_RES_STATUS;
        }

        public void setAPP_RES_STATUS(String APP_RES_STATUS) {
            this.APP_RES_STATUS = APP_RES_STATUS;
        }

        public String getAPP_TYPE() {
            return APP_TYPE;
        }

        public void setAPP_TYPE(String APP_TYPE) {
            this.APP_TYPE = APP_TYPE;
        }

        public String getAPP_IPV_FLAG() {
            return APP_IPV_FLAG;
        }

        public void setAPP_IPV_FLAG(String APP_IPV_FLAG) {
            this.APP_IPV_FLAG = APP_IPV_FLAG;
        }

        public String getAPP_GEN() {
            return APP_GEN;
        }

        public void setAPP_GEN(String APP_GEN) {
            this.APP_GEN = APP_GEN;
        }

        public String getAPP_NAME() {
            return APP_NAME;
        }

        public void setAPP_NAME(String APP_NAME) {
            this.APP_NAME = APP_NAME;
        }

        public String getAPP_F_NAME() {
            return APP_F_NAME;
        }

        public void setAPP_F_NAME(String APP_F_NAME) {
            this.APP_F_NAME = APP_F_NAME;
        }

        public String getAPP_DOB_DT() {
            return APP_DOB_DT;
        }

        public void setAPP_DOB_DT(String APP_DOB_DT) {
            this.APP_DOB_DT = APP_DOB_DT;
        }

        public String getAPP_NATIONALITY() {
            return APP_NATIONALITY;
        }

        public void setAPP_NATIONALITY(String APP_NATIONALITY) {
            this.APP_NATIONALITY = APP_NATIONALITY;
        }

        public String getAPP_OTH_NATIONALITY() {
            return APP_OTH_NATIONALITY;
        }

        public void setAPP_OTH_NATIONALITY(String APP_OTH_NATIONALITY) {
            this.APP_OTH_NATIONALITY = APP_OTH_NATIONALITY;
        }

        public String getAPP_COR_ADD1() {
            return APP_COR_ADD1;
        }

        public void setAPP_COR_ADD1(String APP_COR_ADD1) {
            this.APP_COR_ADD1 = APP_COR_ADD1;
        }

        public String getAPP_COR_ADD2() {
            return APP_COR_ADD2;
        }

        public void setAPP_COR_ADD2(String APP_COR_ADD2) {
            this.APP_COR_ADD2 = APP_COR_ADD2;
        }

        public String getAPP_COR_ADD3() {
            return APP_COR_ADD3;
        }

        public void setAPP_COR_ADD3(String APP_COR_ADD3) {
            this.APP_COR_ADD3 = APP_COR_ADD3;
        }

        public String getAPP_COR_CITY() {
            return APP_COR_CITY;
        }

        public void setAPP_COR_CITY(String APP_COR_CITY) {
            this.APP_COR_CITY = APP_COR_CITY;
        }

        public String getAPP_COR_PINCD() {
            return APP_COR_PINCD;
        }

        public void setAPP_COR_PINCD(String APP_COR_PINCD) {
            this.APP_COR_PINCD = APP_COR_PINCD;
        }

        public String getAPP_COR_STATE() {
            return APP_COR_STATE;
        }

        public void setAPP_COR_STATE(String APP_COR_STATE) {
            this.APP_COR_STATE = APP_COR_STATE;
        }

        public String getAPP_COR_CTRY() {
            return APP_COR_CTRY;
        }

        public void setAPP_COR_CTRY(String APP_COR_CTRY) {
            this.APP_COR_CTRY = APP_COR_CTRY;
        }

        public String getAPP_EMAIL() {
            return APP_EMAIL;
        }

        public void setAPP_EMAIL(String APP_EMAIL) {
            this.APP_EMAIL = APP_EMAIL;
        }

        public String getAPP_COR_ADD_PROOF() {
            return APP_COR_ADD_PROOF;
        }

        public void setAPP_COR_ADD_PROOF(String APP_COR_ADD_PROOF) {
            this.APP_COR_ADD_PROOF = APP_COR_ADD_PROOF;
        }

        public String getAPP_COR_ADD_REF() {
            return APP_COR_ADD_REF;
        }

        public void setAPP_COR_ADD_REF(String APP_COR_ADD_REF) {
            this.APP_COR_ADD_REF = APP_COR_ADD_REF;
        }

        public String getAPP_PER_ADD1() {
            return APP_PER_ADD1;
        }

        public void setAPP_PER_ADD1(String APP_PER_ADD1) {
            this.APP_PER_ADD1 = APP_PER_ADD1;
        }

        public String getAPP_PER_ADD2() {
            return APP_PER_ADD2;
        }

        public void setAPP_PER_ADD2(String APP_PER_ADD2) {
            this.APP_PER_ADD2 = APP_PER_ADD2;
        }

        public String getAPP_PER_ADD3() {
            return APP_PER_ADD3;
        }

        public void setAPP_PER_ADD3(String APP_PER_ADD3) {
            this.APP_PER_ADD3 = APP_PER_ADD3;
        }

        public String getAPP_PER_CITY() {
            return APP_PER_CITY;
        }

        public void setAPP_PER_CITY(String APP_PER_CITY) {
            this.APP_PER_CITY = APP_PER_CITY;
        }

        public String getAPP_PER_PINCD() {
            return APP_PER_PINCD;
        }

        public void setAPP_PER_PINCD(String APP_PER_PINCD) {
            this.APP_PER_PINCD = APP_PER_PINCD;
        }

        public String getAPP_PER_STATE() {
            return APP_PER_STATE;
        }

        public void setAPP_PER_STATE(String APP_PER_STATE) {
            this.APP_PER_STATE = APP_PER_STATE;
        }

        public String getAPP_PER_CTRY() {
            return APP_PER_CTRY;
        }

        public void setAPP_PER_CTRY(String APP_PER_CTRY) {
            this.APP_PER_CTRY = APP_PER_CTRY;
        }

        public String getAPP_PER_ADD_PROOF() {
            return APP_PER_ADD_PROOF;
        }

        public void setAPP_PER_ADD_PROOF(String APP_PER_ADD_PROOF) {
            this.APP_PER_ADD_PROOF = APP_PER_ADD_PROOF;
        }

        public String getAPP_INCOME() {
            return APP_INCOME;
        }

        public void setAPP_INCOME(String APP_INCOME) {
            this.APP_INCOME = APP_INCOME;
        }

        public String getAPP_OCC() {
            return APP_OCC;
        }

        public void setAPP_OCC(String APP_OCC) {
            this.APP_OCC = APP_OCC;
        }

        public String getAPP_OTH_OCC() {
            return APP_OTH_OCC;
        }

        public void setAPP_OTH_OCC(String APP_OTH_OCC) {
            this.APP_OTH_OCC = APP_OTH_OCC;
        }

        public String getAPP_DOC_PROOF() {
            return APP_DOC_PROOF;
        }

        public void setAPP_DOC_PROOF(String APP_DOC_PROOF) {
            this.APP_DOC_PROOF = APP_DOC_PROOF;
        }

        public String getAPP_MAR_STATUS() {
            return APP_MAR_STATUS;
        }

        public void setAPP_MAR_STATUS(String APP_MAR_STATUS) {
            this.APP_MAR_STATUS = APP_MAR_STATUS;
        }
    }
}
