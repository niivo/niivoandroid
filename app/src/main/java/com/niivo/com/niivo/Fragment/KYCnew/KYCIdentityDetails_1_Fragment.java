package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.AnnualIncomeListAdapter;
import com.niivo.com.niivo.Adapter.CountryListAdapter;
import com.niivo.com.niivo.Adapter.OccupationListAdapter;
import com.niivo.com.niivo.Adapter.WealthSourceAdapter;
import com.niivo.com.niivo.Model.ItemAnnualIncomeList;
import com.niivo.com.niivo.Model.ItemCountryList;
import com.niivo.com.niivo.Model.ItemOccuptaionList;
import com.niivo.com.niivo.Model.ItemPanDetails;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.Model.ItemWealthSourceList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCIdentityDetails_1_Fragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView continueBtn;
    AppCompatSpinner countryOfBirthSPinner, placeOfBirthSpinner,
            occupationTypeSpinner, countryOfTaxResidency, annualIncomeSpinner, wealthSourceSpinner;
    View rootView;

    //Non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    ItemCountryList countryList;
    ItemOccuptaionList occuptaionList;
    ItemAnnualIncomeList annualIncomeList;
    ItemWealthSourceList wealthSource;
    boolean isIdentitySubmitted = false, kycStatus = false, preRegistered = false;
    ItemPanDetails panDetails;
    ItemUserDetail userDetail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_identity_1, container, false);
            contextd = container.getContext();
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            textView.setText(R.string.identity_details);
            continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
            continueBtn.setOnClickListener(this);
            countryOfBirthSPinner = (AppCompatSpinner) rootView.findViewById(R.id.countryOfBirthSPinner);
            placeOfBirthSpinner = (AppCompatSpinner) rootView.findViewById(R.id.placeOfBirthSpinner);
            occupationTypeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.occupationTypeSpinner);
            countryOfTaxResidency = (AppCompatSpinner) rootView.findViewById(R.id.countryOfTaxResidency);
            annualIncomeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.annualIncomeSpinner);
            wealthSourceSpinner = (AppCompatSpinner) rootView.findViewById(R.id.wealthSourceSpinner);
            kycStatus = getArguments().getBoolean("kycStatus");
            panDetails = (ItemPanDetails) getArguments().getSerializable("panDetail");
            if (countryList == null)
                getCountryList();
            if (occuptaionList == null)
                getOccupationType();
            if (annualIncomeList == null)
                getAnnualIncome();
            if (wealthSource == null)
                getWealthSourceList();
            if (getArguments().getBoolean("preRegistered"))
                setUpPreRegistration();
        }
        return rootView;
    }

    int step = 0;

    void setUpPreRegistration() {
        preRegistered = true;
        userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
        step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
        if (step > 2) {
            if (countryList == null) {
                getCountryList();
            } else {
                for (int i = 0; i < countryList.getData().size(); i++) {
                    if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getBirth_place()))
                        placeOfBirthSpinner.setSelection(i);
                    if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getBirth_country()))
                        countryOfBirthSPinner.setSelection(i);
                    if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getTax_residency_country()))
                        countryOfTaxResidency.setSelection(i);
                }
            }
            if (annualIncomeList == null)
                getAnnualIncome();
            else {
                for (int i = 0; i < annualIncomeList.getData().size(); i++)
                    if (annualIncomeList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getGross_annual_income()))
                        annualIncomeSpinner.setSelection(i);
            }
            if (occuptaionList == null)
                getOccupationType();
            else {
                for (int i = 0; i < occuptaionList.getData().size(); i++)
                    if (occuptaionList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getOccupation()))
                        occupationTypeSpinner.setSelection(i);
            }
            if (wealthSource == null)
                getWealthSourceList();
            else
                for (int i = 0; i < wealthSource.getData().size(); i++)
                    if (wealthSource.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getWealth_source()))
                        wealthSourceSpinner.setSelection(i);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBtn:
                Fragment fragment = new KYCIdentityDetails_2_Fragment();
                Bundle b = getArguments();
                b.putString("countryOfBirth", countryList.getData().get(countryOfBirthSPinner.getSelectedItemPosition()).getNIIVO_CODE());
                b.putString("placeOfbirth", countryList.getData().get(countryOfBirthSPinner.getSelectedItemPosition()).getNIIVO_CODE());
                b.putString("grossAnnualIncome", annualIncomeList.getData().get(annualIncomeSpinner.getSelectedItemPosition()).getNIIVO_CODE());
                b.putString("occupationCode", occuptaionList.getData().get(occupationTypeSpinner.getSelectedItemPosition()).getNIIVO_CODE());
                b.putString("countryOfResidency", countryList.getData().get(countryOfTaxResidency.getSelectedItemPosition()).getNIIVO_CODE());
                b.putString("wealthSource", wealthSource.getData().get(wealthSourceSpinner.getSelectedItemPosition()).getNIIVO_CODE());
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCIdentityDetails_1_Fragment.this, 101);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, fragment).addToBackStack(null)
                        .commit();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isIdentitySubmitted", isIdentitySubmitted);
        getTargetFragment().onActivityResult(ResponseType.KYC_IdentityDetails,
                isIdentitySubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED
                , i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                isIdentitySubmitted = true;
                getActivity().onBackPressed();
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    void getOccupationType() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetOccupation);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("OCCUPATIONCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getAnnualIncome() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetAnnualIncome);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INCOMECODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getCountryList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetCountry);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("COUNTRYCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getWealthSourceList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetWealthSourceCode);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("WEALTHSOURCECODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetCountry:
         //       Log.e("getCountry", json);
                countryList = new Gson().fromJson(json, ItemCountryList.class);
                countryOfBirthSPinner.setAdapter(new CountryListAdapter(contextd, countryList));
                placeOfBirthSpinner.setAdapter(new CountryListAdapter(contextd, countryList));
                countryOfTaxResidency.setAdapter(new CountryListAdapter(contextd, countryList));
                setIndian();
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.GetOccupation:
        //        Log.e("getOccupation", json);
                occuptaionList = new Gson().fromJson(json, ItemOccuptaionList.class);
                occupationTypeSpinner.setAdapter(new OccupationListAdapter(contextd, occuptaionList));
                // change this if occupation is to be pre-filled and to disable
//                if (kycStatus) {
//                    if ((panDetails.getData().getPan_data().getAPP_OCC() + "").equals("")) {
//                        for (int i = 0; i < occuptaionList.getData().size(); i++) {
//                            if (occuptaionList.getData().get(i).getCVL_CODE().equals(panDetails.getData().getPan_data().getAPP_OCC())) {
//                                occupationTypeSpinner.setSelection(i);
//                                occupationTypeSpinner.setEnabled(false);
//                            }
//                        }
//                    }
//                }
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.GetAnnualIncome:
           //     Log.e("getAnnualIncome", json);
                annualIncomeList = new Gson().fromJson(json, ItemAnnualIncomeList.class);
                annualIncomeSpinner.setAdapter(new AnnualIncomeListAdapter(contextd, annualIncomeList));
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.GetWealthSourceCode:
        //        Log.e("getWealthSource", json);
                wealthSource = new Gson().fromJson(json, ItemWealthSourceList.class);
                wealthSourceSpinner.setAdapter(new WealthSourceAdapter(contextd, wealthSource));
                if (preRegistered)
                    setUpPreRegistration();
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetCountry:
        //        Log.e("getCountry", json);
                break;
            case ResponseType.GetOccupation:
        //        Log.e("getOccupation", json);
                break;
            case ResponseType.GetAnnualIncome:
       //         Log.e("getAnnualIncome", json);
                break;
            case ResponseType.GetWealthSourceCode:
        //        Log.e("getWealthSource", json);
                break;
        }
    }

    void setIndian() {
        for (int i = 0; i < countryList.getData().size(); i++)
            if (countryList.getData().get(i).getNIIVO_CODE().equals("101")) {
                countryOfBirthSPinner.setSelection(i);
                placeOfBirthSpinner.setSelection(i);
                countryOfTaxResidency.setSelection(i);
            }
    }
}
