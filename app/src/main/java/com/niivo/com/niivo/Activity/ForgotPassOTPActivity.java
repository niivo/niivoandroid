package com.niivo.com.niivo.Activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.SMSReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 28/6/17.
 */

public class ForgotPassOTPActivity extends AppCompatActivity implements ResponseDelegate {
    public static EditText otp1, otp2, otp3, otp4;
    TextView resendOTP;

    private RequestedServiceDataModel requestedServiceDataModel;
    public static String otp = "";
    String mobile = "", strOTP = "";
    SMSReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(ForgotPassOTPActivity.this, Common.getLanguage(ForgotPassOTPActivity.this), false);
        setContentView(R.layout.activity_otp);
        resendOTP = (TextView) findViewById(R.id.text_otp);

        otp1 = (EditText) findViewById(R.id.otp1);
        otp2 = (EditText) findViewById(R.id.otp2);
        otp3 = (EditText) findViewById(R.id.otp3);
        otp4 = (EditText) findViewById(R.id.otp4);
        setUpFocusWork();
        resendOTP.setText(Html.fromHtml(ForgotPassOTPActivity.this.getResources().getString(R.string.resendotp)));
        TextView button_verify = (TextView) findViewById(R.id.button_verify);
        button_verify.setText(ForgotPassOTPActivity.this.getResources().getString(R.string.next));
        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    otp = otp1.getText().toString().trim() + "" + otp2.getText().toString().trim() + "" + otp3.getText().toString().trim() + "" + otp4.getText().toString().trim() + "";
                    startActivity(new Intent(ForgotPassOTPActivity.this, ResetPasswordActivity.class).putExtra("otp", otp).putExtra("mobile", mobile));
                }
            }
        });

        mobile = Common.getPreferences(ForgotPassOTPActivity.this, "mobile");
        resendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });
        strOTP = getIntent().getStringExtra("otp").trim();
    }

    public static void setOTPsms(String message) {
        try {
            Log.e("message", message);

            otp = message.trim();
            otp1.setText(otp.charAt(0) + "");
            otp2.setText(otp.charAt(1) + "");
            otp3.setText(otp.charAt(2) + "");
            otp4.setText(otp.charAt(3) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    boolean validate() {
        boolean b = false;
        otp = otp1.getText().toString().trim() + "" + otp2.getText().toString().trim() + "" + otp3.getText().toString().trim() + "" + otp4.getText().toString().trim() + "";
        if (otp1.getText().toString().trim().equals("") || otp2.getText().toString().trim().equals("") || otp3.getText().toString().trim().equals("") || otp4.getText().toString().trim().equals("")) {
            otp4.setError(ForgotPassOTPActivity.this.getResources().getString(R.string.allFieldsRequired));
            b = false;
        } else if (!strOTP.equals(otp)) {
            b = false;
            otp4.setError(ForgotPassOTPActivity.this.getResources().getString(R.string.invalidOtp));
        } else {
            b = true;
        }
        return b;
    }

    void resendOtp() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ResendOTP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", mobile);
        requestedServiceDataModel.setWebServiceType("RESENDOTP");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ResendOTP:
             //   Log.d("res", json);
                Common.showToast(this, message);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    strOTP = jsonObject.getJSONObject("data").getString("otp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ResendOTP:
             //   Log.d("res", json);
                Common.showToast(this, message);
                break;
        }
        Common.showToast(this, message);
    }


    void setUpFocusWork() {
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp1.getText().toString().trim().length() > 0) {
                    otp1.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp2.getText().toString().trim().length() > 0) {
                    otp2.clearFocus();
                    otp3.requestFocus();
                }
                if (otp2.getText().toString().trim().equals("")) {
                    otp2.clearFocus();
                    otp1.requestFocus();
                }
            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp3.getText().toString().trim().length() > 0) {
                    otp3.clearFocus();
                    otp4.requestFocus();
                }
                if (otp3.getText().toString().trim().equals("")) {
                    otp3.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp4.getText().toString().trim().equals("")) {
                    otp4.clearFocus();
                    otp3.requestFocus();
                }
            }
        });
    }
}
