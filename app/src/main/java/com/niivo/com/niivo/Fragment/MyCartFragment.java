package com.niivo.com.niivo.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.MyCartAdapter;
import com.niivo.com.niivo.Fragment.InvestHelper.RedirectringFragment;
import com.niivo.com.niivo.Model.CartItemList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 12/6/17.
 */

public class MyCartFragment extends Fragment implements ResponseDelegate {
    Context contextd;
    ListView myCartList;
    TextView totalInvested,emptymsg;
    ImageButton back;

    //Non ui Vars
    int position_ = 0;
    MyCartAdapter adapter;
    RequestedServiceDataModel requestedServiceDataModel;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy", new Locale("en"));
    int investingPos = -1, deletPos = -1, editPos = -1;
    MainActivity activity;
    String remarkSTR = "";
    CartItemList cartList;
    CartItemList.CartItem cartEdit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_my_cart, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.myCart).toUpperCase());

        activity = (MainActivity) getActivity();
        myCartList = (ListView) rootView.findViewById(R.id.my_cart_list);
        totalInvested = (TextView) rootView.findViewById(R.id.totalInvested);
        getCartListing(Common.getPreferences(contextd, "userID"));
        emptymsg= (TextView) rootView.findViewById(R.id.emptymsg);
//        setTotal();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        final String Kycstatuss = prefs.getString("Kycstatuss", null);
        Log.d("Kycstatuss::::",Kycstatuss);

        //investedListRV.setVisibility(View.GONE);
        if(Kycstatuss.equals("NOT STARTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
        }else if(Kycstatuss.equals("SUBMITTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.kycSubmit));
        }else if(Kycstatuss.equals("COMPLETE")) {
            //emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.mycartmsg));
        }

        return rootView;
    }


    void setTotal() {
        if (cartList != null && cartList.getData().size() > 0) {
            double total = 0;
            for (int i = 0; i < cartList.getData().size(); i++) {
                if (cartList.getData().get(i).getIs_goal().equals("0")) {
                    total += Double.parseDouble(cartList.getData().get(i).getCart_detail().get(0).getScheme_amount().trim());
                } else {
                    for (int j = 0; j < cartList.getData().get(i).getCart_detail().size(); j++)
                        total += Double.parseDouble(cartList.getData().get(i).getCart_detail().get(j).getScheme_amount());
                }
            }
            totalInvested.setText(Common.currencyString(total + "", false));
        } else {
            totalInvested.setText("\u20B9 " + 0);
        }
        adapter.notifyDataSetChanged();
    }

    String sipDate = "";
    boolean isCams = false;
    Bundle b;

    void investInThisFund(int pos) {
        String amnt = "";
        double total = 0;
        if (cartList.getData().get(pos).getIs_goal().equals("1"))

            for (int j = 0; j < cartList.getData().get(pos).getCart_detail().size(); j++)
                total += Double.parseDouble(cartList.getData().get(pos).getCart_detail().get(j).getScheme_amount());

        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(Html.fromHtml("<p>" + contextd.getResources().getString(R.string.amount) + "<strong><span style=\"color: #3fd37e;\">"
                + "\t\t" + (cartList.getData().get(pos).getIs_goal().equals("0")
                ? Common.currencyString(cartList.getData().get(pos).getCart_detail().get(0).getScheme_amount(), false)
                : Common.currencyString(total + "", false)) + "</span></strong></p>" +
                "<p>" + contextd.getResources().getString(R.string.investFor) + " <strong>" + (cartList.getData().get(pos).getIs_goal().equals("0") ?
                cartList.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME() :
                cartList.getData().get(pos).getCart_detail().get(0).getGoal_name()) + "</strong>.</p>"));
        investingPos = pos;
        builder.setPositiveButton(contextd.getResources().getString(R.string.pay), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                if (investingPos != -1) {
                    remarkSTR = (cartList.getData().get(investingPos).getOrder_type().equals("ONETIME")
                            ? "Purchase Order for "
                            : "SIP Registration for ") + (cartList.getData().get(investingPos).getIs_goal().equals("0") ?
                            cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSTARMF_SCHEME_CODE()
                            : "Goal") + " of " + Common.getPreferences(contextd, "userID") + " at " +
                            timeF.format(Calendar.getInstance().getTime());
                    b = new Bundle();
                    b.putString("isInCart", cartList.getData().get(investingPos).getCart_detail().get(0).getCart_id());
                    if (cartList.getData().get(investingPos).getIs_goal().equals("1")) {
                        b.putString("from", "biginner");
                        b.putString("schemeCode", "");
                        b.putString("cateId", cartList.getData().get(investingPos).getCart_detail().get(0).getGoal_cat_id());
                        b.putString("goalName", cartList.getData().get(investingPos).getCart_detail().get(0).getGoal_name());
                        b.putString("orderType", cartList.getData().get(investingPos).getOrder_type());
                        b.putString("targetYear", cartList.getData().get(investingPos).getTarget_year());
                        b.putString("amountAmount", cartList.getData().get(investingPos).getCart_detail().get(0).getGoal_amount());
                        b.putString("remark", remarkSTR);

                        JSONArray arr = new JSONArray();
                        for (int j = 0; j < cartList.getData().get(investingPos).getCart_detail().size(); j++) {
                            JSONObject obj = new JSONObject();

                            try {
                                obj.put("scheme_name", cartList.getData().get(investingPos).getCart_detail().get(j).getScheme_detail().getSCHEME_NAME());
                                obj.put("scheme_code", cartList.getData().get(investingPos).getCart_detail().get(j).getScheme_detail().getSTARMF_SCHEME_CODE());
                                obj.put("scheme_amount", cartList.getData().get(investingPos).getCart_detail().get(j).getScheme_amount());
                                obj.put("scheme_qty", "0");
                                obj.put("nav", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_nav());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            arr.put(obj);
                        }

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DATE, 1);
                        Log.e("before", std.format(calendar.getTime()));
                        String sipFrom = monthY.format(calendar.getTime());
                        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(new Locale("en"));
                        DateTime from = dtf.parseDateTime(std.format(calendar.getTime()).toString()), to;
                        calendar.set(Calendar.YEAR, Integer.parseInt(cartList.getData().get(investingPos).getTarget_year().trim()));
                        calendar.add(Calendar.MONTH, -1);
                        Log.e("after", std.format(calendar.getTime()));
                        String sipTo = monthY.format(calendar.getTime());
                        to = dtf.parseDateTime(std.format(calendar.getTime()).toString());
                        int noOfMonth = (Months.monthsBetween(from, to).getMonths());
                        Log.e("NoOfMonth", noOfMonth + "");
                        b.putString("months", noOfMonth + "");
                        b.putString("schemeJson", arr.toString());
                        if (!cartList.getData().get(investingPos).getOrder_type().equals("ONETIME")) {
                            sipDate = getSipDate(cartList, investingPos);
                            b.putString("sipDate", sipDate);
                            b.putString("sipFrom", sipFrom);
                            b.putString("sipTo", sipTo);
                        }

                    } else {
                        b.putString("from", "notBiginner");
                        b.putString("schemeCode", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSTARMF_SCHEME_CODE());
                        b.putString("schemeName", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME());
                        b.putString("ammount", (cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_amount().equals("N.A.")
                                || cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_amount().equals("")) ?
                                (cartList.getData().get(investingPos).getOrder_type().equals("ONETIME") ? "1000" : "500") : cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_amount());
                        b.putString("quantity", "0");
                        b.putString("months", cartList.getData().get(investingPos).getNo_of_months());
                        b.putString("remark", remarkSTR);
                        b.putString("orderType", cartList.getData().get(investingPos).getOrder_type());
                        if (!cartList.getData().get(investingPos).getOrder_type().equals("ONETIME")) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DATE, 1);
                            Log.e("before", std.format(calendar.getTime()));
                            String sipFrom = monthY.format(calendar.getTime());
                            DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(new Locale("en"));
                            DateTime from = dtf.parseDateTime(std.format(calendar.getTime()).toString()), to;
                            calendar.add(Calendar.MONTH, Integer.parseInt(cartList.getData().get(investingPos).getNo_of_months().trim()));
                            Log.e("after", std.format(calendar.getTime()));
                            String sipTo = monthY.format(calendar.getTime());
                            to = dtf.parseDateTime(std.format(calendar.getTime()).toString());
                            int noOfMonth = (Months.monthsBetween(from, to).getMonths());
                            Log.e("NoOfMonth", noOfMonth + "");
                            b.putString("months", noOfMonth + "");
                            b.putString("SIPDates", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSIP_DATES());
                            b.putString("sipFrom", sipFrom);
                            b.putString("sipTo", sipTo);
                        }

                        JSONArray arr = new JSONArray();
                        JSONObject obj = new JSONObject();

                        try {
                            obj.put("scheme_name", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME());
                            obj.put("scheme_code", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSTARMF_SCHEME_CODE());
                            obj.put("scheme_amount", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_amount());
                            obj.put("scheme_qty", "0");
                            obj.put("nav", cartList.getData().get(investingPos).getCart_detail().get(0).getScheme_nav());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        arr.put(obj);
                        b.putString("schemeJson", arr.toString());


                    }
                    frag = new RedirectringFragment();
                    frag.setArguments(b);
                    UiUtils.hideKeyboard(contextd);
                    for (int i = 0; i < cartList.getData().get(investingPos).getCart_detail().size(); i++)
                        if (cartList.getData().get(investingPos).getCart_detail().get(i).getScheme_detail().getSTARMF_RTA_CODE().equals("CAMS"))
                            isCams = true;
                    getProfileStatus(Common.getPreferences(contextd, "userID"));
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                investingPos = -1;
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    String getSipDate(CartItemList arr, int investingPos) {
        String date = "";
        String tmp = "", tmp2 = "", tmp3 = "";
        if (arr.getData().get(investingPos).getCart_detail().size() > 0) {
            tmp = arr.getData().get(investingPos).getCart_detail().get(0).getScheme_detail().getSIP_DATES();
            tmp = tmp.startsWith(",") ? tmp.substring(1, tmp.length()) : tmp;
            tmp = tmp.endsWith(",") ? tmp.substring(0, (tmp.length() - 1)) : tmp;
        }
        if (arr.getData().get(investingPos).getCart_detail().size() > 1) {
            tmp2 = arr.getData().get(investingPos).getCart_detail().get(1).getScheme_detail().getSIP_DATES();
            tmp2 = tmp2.startsWith(",") ? tmp2.substring(1, tmp2.length()) : tmp2;
            tmp2 = tmp2.endsWith(",") ? tmp2.substring(0, (tmp2.length() - 1)) : tmp2;
        }
        if (arr.getData().get(investingPos).getCart_detail().size() > 2) {
            tmp3 = arr.getData().get(investingPos).getCart_detail().get(2).getScheme_detail().getSIP_DATES();
            tmp3 = tmp3.startsWith(",") ? tmp3.substring(1, tmp3.length()) : tmp3;
            tmp3 = tmp3.endsWith(",") ? tmp3.substring(0, (tmp3.length() - 1)) : tmp3;
        }

        String[] dt = tmp.split(","), dt2 = tmp2.split(","), dt3 = tmp3.split(",");

        if (arr.getData().get(investingPos).getCart_detail().size() > 2) {
            Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length + "\n3. " + dt3.length);
            int lenth = 0;
            lenth = dt.length < (dt2.length < dt3.length ? dt2.length : dt3.length)
                    ? dt.length :
                    (dt2.length < dt3.length ? dt2.length : dt3.length);
            Log.e("MinimumLength", lenth + "");
            for (int i = 0; i < lenth; i++) {
                if (dt[i].equals(dt2[i]) && dt[i].equals(dt3[i]))
                    if (date.equals(""))
                        date = dt[i];
            }
        } else if (arr.getData().get(investingPos).getCart_detail().size() > 1) {
            Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length);
            int lenth = 0;
            lenth = dt.length < dt2.length ? dt.length : dt2.length;
            Log.e("MinimumLength", lenth + "");
            for (int i = 0; i < lenth; i++) {
                if (dt[i].equals(dt2[i]))
                    if (date.equals(""))
                        date = dt[i];
            }
        } else if (arr.getData().get(investingPos).getCart_detail().size() > 0) {
            date = dt[0];
        } else {
            date = "";
        }
        return date;
    }


    SimpleDateFormat std = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en")),
            timeF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("en")),
            monthY = new SimpleDateFormat("MMM yyyy", new Locale("en"));

    Fragment frag;

    void getProfileStatus(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getCartListing(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.GetCartList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("CARTLIST");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void removeFromCart(String userid, String cartid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.RemoveFromCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("cart_id", cartid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("REMOVECART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void editCartAmount(String userid, String cartid, String amount) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.EditCartItem);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("cart_id", cartid);
        requestedServiceDataModel.putQurry("amount", amount);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("EDITCARTAMOUNT");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        emptymsg.setVisibility(View.GONE);
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                try {
                    JSONObject obj = new JSONObject(json);
                    if (isCams && (obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("PENDING") ||
                            obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("REJECTED"))) {
                        Common.showDialog(contextd, contextd.getResources().getString(R.string.checqueImgeVerification));
                    } else if (obj.getJSONObject("data").getString("kyc_status").equals("COMPLETE")){
                        if (obj.getJSONObject("data").getString("is_mendate").equalsIgnoreCase("VERIFIED")) {
                            b.putBoolean("isXSIP", true);
                            b.putString("mandate_id", obj
                                    .getJSONObject("data")
                                    .getString("mandate_id"));
                        } else {
                            b.putBoolean("isXSIP", false);
                        }
                        frag.setArguments(b);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
                        builder.setTitle(contextd.getResources().getString(R.string.app_name));
                        builder.setMessage(contextd.getResources().getString(R.string.kycPending));
                        String positiveText = contextd.getResources().getString(R.string.doItNow);
                        builder.setPositiveButton(positiveText,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        activity.getDocBackHome(contextd.getResources().getString(R.string.documentVerification), false);
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.setCancelable(false);
                        dialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.GetCartList:
                Gson gson = new Gson();
                cartList = gson.fromJson(json, CartItemList.class);
                adapter = new MyCartAdapter(contextd, cartList, new MyCartAdapter.OnViewHelper() {
                    @Override
                    public void deduct(int pos) {
                        deletPos = pos;
                        removeFromCart(Common.getPreferences(contextd, "userID"), cartList.getData().get(deletPos).getCart_detail().get(0).getCart_id());
                    }

                    @Override
                    public void editedAmnt(CartItemList.CartItem cart, int pos) {
                        editPos = pos;
                        cartEdit = cart;
                        editCartAmount(Common.getPreferences(contextd, "userID"),
                                cartList.getData().get(editPos).getCart_detail().get(0).getCart_id(),
                                cart.getCart_detail().get(0).getScheme_amount());
                    }

                    @Override
                    public void investFund(int pos) {
                        investInThisFund(pos);
                    }
                });
                myCartList.setAdapter(adapter);
                setTotal();
                break;
            case ResponseType.RemoveFromCart:
                cartList.getData().remove(deletPos);
                setTotal();
                break;
            case ResponseType.EditCartItem:
                cartList.getData().set(editPos, cartEdit);
                setTotal();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                Log.e("res", json);
                Common.showToast(contextd, message);
                break;
            case ResponseType.GetCartList:
             //   myCartList.setVisibility(View.GONE);
                emptymsg.setVisibility(View.VISIBLE);
                Log.e("res", json);
              //  Common.showToast(contextd, message);
                break;
            default:
                Common.showToast(contextd, message);
                break;
        }
    }

}
