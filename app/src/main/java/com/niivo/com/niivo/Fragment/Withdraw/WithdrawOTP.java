package com.niivo.com.niivo.Fragment.Withdraw;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;

import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.EditText;

import android.widget.ImageButton;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Activity.WithDrawOTPActivity;
import com.niivo.com.niivo.Activity.WithDrawProfileActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import static org.acra.ACRA.log;

public class WithdrawOTP extends Fragment implements ResponseDelegate {

    Activity _activity;
    EditText otp1, otp2, otp3, otp4;
    ImageView logo;
    ImageButton icon_navi;
    TextView button_verify;
    //Non ui vars
    RequestedServiceDataModel requestedServiceDataModel;
    String otpRes = "", otp = "";
    Context context;
    MainActivity activity;
    String amount,fullamount,fullunits,full_withdraw,order_id,schemeCode,INWARD_TXN_NO,id;

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = container.getContext();
        View rootView = inflater.inflate(R.layout.withdrawotp, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView tool = (TextView) toolbar.findViewById(R.id.toolbar_title);
        tool.setText(context.getResources().getString(R.string.Withdrawotp));
        icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setVisibility(View.VISIBLE);
        activity = (MainActivity) getActivity();
        logo= (ImageView)rootView.findViewById(R.id.logo);
        TextView resent = (TextView)rootView.findViewById(R.id.text_otp);
        logo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0,0,view.getWidth(),view.getHeight());
            }
        });
        resent.setText(Html.fromHtml(context.getResources().getString(R.string.resendotp)));
        button_verify =(TextView)rootView.findViewById(R.id.button_verify) ;
        Bundle bundle = getArguments();
          amount = bundle.getString("amount");
          order_id = bundle.getString("order_id");
          schemeCode = bundle.getString("schemeCode");
          fullamount = bundle.getString("fullamount");
          fullunits = bundle.getString("fullunits");
          INWARD_TXN_NO = bundle.getString("INWARD_TXN_NO");
          full_withdraw = bundle.getString("full_withdraw");
          id = bundle.getString("id");

        TextView textView = (TextView)rootView. findViewById(R.id.text_otp);
        textView.setText(Html.fromHtml(context.getResources().getString(R.string.resendotp)));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWithDrawOTP();
            }
        });
        TextView button_verify = (TextView)rootView.findViewById(R.id.button_verify);
        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    withDrawThis(amount,order_id,schemeCode,fullamount,fullunits,INWARD_TXN_NO,full_withdraw,id);
                }
            }
        });
        otp1 = (EditText)rootView. findViewById(R.id.otp1);
        otp2 = (EditText)rootView. findViewById(R.id.otp2);
        otp3 = (EditText)rootView.findViewById(R.id.otp3);
        otp4 = (EditText)rootView.findViewById(R.id.otp4);
        setUpFocusWork();
        getWithDrawOTP();
        return rootView;
    }

    boolean validate() {
        boolean b = false;
        otp = otp1.getText().toString().trim() + otp2.getText().toString().trim() + otp3.getText().toString().trim() + otp4.getText().toString().trim();
        if (otp1.getText().toString().trim().equals("")) {
            otp4.setError(getActivity().getResources().getString(R.string.requiredField));
            b = false;
        } else if (otp2.getText().toString().trim().equals("")) {
            otp4.setError(getActivity().getResources().getString(R.string.requiredField));
            b = false;
        } else if (otp3.getText().toString().trim().equals("")) {
            otp4.setError(getActivity().getResources().getString(R.string.requiredField));
            b = false;
        } else if (otp4.getText().toString().trim().equals("")) {
            otp4.setError(getActivity().getResources().getString(R.string.requiredField));
            b = false;
        } else if (!otp.equals(otpRes)) {
            b = false;
            Common.showToast(context, getActivity().getResources().getString(R.string.invalidOtp));
        } else {
            b = true;
        }
        return b;
    }

    void getWithDrawOTP() {
        requestedServiceDataModel = new RequestedServiceDataModel(context, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.WithDrawOTP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("WTHDRAWOTP");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(context, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getContext()));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void withDrawThis(String amount,String order_id,String schemeCode,String fullamount,String fullunits,String INWARD_TXN_NO,String full_withdraw,String id)
    {
        requestedServiceDataModel = new RequestedServiceDataModel(context, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.MyMoney);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("WITHDRAW");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(context, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(context));
        requestedServiceDataModel.putQurry("scheme_code", schemeCode);
        requestedServiceDataModel.putQurry("amount", amount);
        requestedServiceDataModel.putQurry("order_id", order_id);
        requestedServiceDataModel.putQurry("fullamount", fullamount);
        requestedServiceDataModel.putQurry("fullunits", fullunits);
        requestedServiceDataModel.putQurry("id", id);
        requestedServiceDataModel.putQurry("INWARD_TXN_NO", INWARD_TXN_NO);
        requestedServiceDataModel.putQurry("full_withdraw", full_withdraw);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }



    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {

        switch (baseRequestData.getTag()) {
            case ResponseType.WithDrawOTP:
        //        Log.e("res", json);
       //         Log.e("withdraw otp response", json);
                try {
                    JSONObject obj = new JSONObject(json);
                    otpRes = obj.getString("otp").trim();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.MyMoney:
                try {
                    JSONObject object = new JSONObject(json);
           //         Log.d("Mymoney json",json);
                    Toast.makeText(getContext(), "Thank You..! Your Withdraw is successfull...!",Toast.LENGTH_SHORT).show();

                    String order_id=String.valueOf(((object.getString("order_id"))));
                    String folioNo=String.valueOf(((object.getString("folioNo"))));
                    String msg=String.valueOf(((object.getString("msg"))));
                    String order_status=String.valueOf(((object.getString("order_status"))));
                    String fundname=String.valueOf(((object.getString("fundname"))));
                    String amount=String.valueOf(((object.getString("amount"))));
                    String units=String.valueOf(((object.getString("units"))));
                    String fullamount=String.valueOf(((object.getString("fullamount"))));
                    String fullunits=String.valueOf(((object.getString("fullunits"))));
                    String fullWithdraw=String.valueOf(((object.getString("fullWithdraw"))));

                    Intent i = new Intent(getActivity(), WithDrawProfileActivity.class);
                    i.putExtra("order_id",order_id);
                    i.putExtra("folioNo",folioNo);
                    i.putExtra("msg",msg);
                    i.putExtra("order_status",order_status);
                    i.putExtra("fundname",fundname);
                    i.putExtra("amount",amount);
                    i.putExtra("units",units);
                    i.putExtra("fullamount",fullamount);
                    i.putExtra("fullunits",fullunits);
                    i.putExtra("fullWithdraw",fullWithdraw);
                    startActivity(i);
                    getActivity().finish();

                }catch (JSONException e) {
                    e.printStackTrace();
                }

//                i.putExtra("done", "done");

                break;
        }

    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

        switch (baseRequestData.getTag()) {
            case ResponseType.WithDrawOTP:
                Common.showToast(context, message);
       //         Log.d("res", json);
      //          Log.d("Failled of OTP", json);
                break;
            case ResponseType.MyMoney:
                try {
                    JSONObject jsonObject = new JSONObject(json);
          //          Log.d("mymoney response json", json);
                    if (jsonObject.getString("retry").trim().equals("1"))
                        retryOnWebError(message, false);
                    else {
                        withJustError(message, false);
                    }
          //          Log.e("Failled of withdraw", message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Common.showToast(context, message);
                break;
        }

    }
    private void retryOnWebError(final String msg, final boolean oneTime) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.myDialogTheme);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
    //    Log.d("Response aftr builder", String.valueOf(builder));
        builder.setPositiveButton(context.getResources().getString(R.string.retry),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        withDrawThis(amount,order_id,schemeCode,fullamount,fullunits,INWARD_TXN_NO,full_withdraw,id);
                        dialog.dismiss();
          //              Log.d("Response positive", String.valueOf(dialog));
                    }
                });
        builder.setNegativeButton(context.getResources().getString(R.string.dismiss),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
          //              Log.d("Response Negative", String.valueOf(dialog));
                    }
                });

        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
    private void withJustError(final String msg, final boolean oneTime) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context, R.style.myDialogTheme);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(msg.replaceFirst("Failed ", ""));
     //   Log.e("Response builder", String.valueOf(builder));
        builder.setPositiveButton(context.getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
          //              Log.e("Response builder", String.valueOf(dialog));
                    }
                });
        builder.setNegativeButton(context.getResources().getString(R.string.backAndValidate), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
               // WithDrawOTPActivity.this.onBackPressed();
            }
        });


        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    void setUpFocusWork() {
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp1.getText().toString().trim().length() > 0) {
                    otp1.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp2.getText().toString().trim().length() > 0) {
                    otp2.clearFocus();
                    otp3.requestFocus();
                }
                if (otp2.getText().toString().trim().equals("")) {
                    otp2.clearFocus();
                    otp1.requestFocus();
                }
            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp3.getText().toString().trim().length() > 0) {
                    otp3.clearFocus();
                    otp4.requestFocus();
                }
                if (otp3.getText().toString().trim().equals("")) {
                    otp3.clearFocus();
                    otp2.requestFocus();
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otp4.getText().toString().trim().equals("")) {
                    otp4.clearFocus();
                    otp3.requestFocus();
                }
            }
        });
    }
}
