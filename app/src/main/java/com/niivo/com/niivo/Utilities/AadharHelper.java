package com.niivo.com.niivo.Utilities;

import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by deepak on 6/10/17.
 */

public class AadharHelper {

    public String getEncryptedBody(String uid, String aadharNumber, String pinCode) throws Exception {
        String token = "ZKJD-LJ4S-KJP1"; //Provided by AuthBridge

        try {


//            JSONObject dataObj = new JSONObject()
//                    .put("uid", aadharNumber)
//                    .put("uniqueid", uid)
//                    .put("actiontype", "otp")
//                    .put("pincode", pinCode);

            String dataObj = " {\n" +
                    "                        \"uid\" :  \"734338899353\" ,\n" +
                    "                        \"uniqueid\" : \"1507546896645\" ,\n" +
                    "                        \"actiontype\" : \"otp\" ,\n" +
                    "                        \"pincode\"  : \"302029\"\n" +
                    "                    }";


//                    {
//                        "uid" :  "786" ,
//                        "uniqueid" : "554087881649" ,
//                        "actiontype" : "otp" ,
//                        "pincode"  : "302029"
//                    }


            // getting cipher object of AES Tranforamation
            Cipher cipher = Cipher.getInstance("AES");
            byte[] key = token.getBytes("UTF-8");
            //System.out.println(Arrays.toString(key));

            MessageDigest sha256hash = MessageDigest.getInstance("SHA-512");

            // this is important to copy only first 128 bit of key, this must be
            // done by decryption process also.
            key = sha256hash.digest((token).getBytes());

            key = Arrays.copyOf(key, 16);


            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

            byte[] inputBytes = dataObj.toString().getBytes();
            byte[] outputBytes = cipher.doFinal(inputBytes);
            System.out.println(Arrays.toString(outputBytes));

            String outputString = new String(com.niivo.com.niivo.Utilities.Base64.getEncoder().encode(outputBytes));

            System.out.println("output string: " + outputString);
            String encodedURL = URLEncoder.encode(outputString, "UTF-8");

            return encodedURL;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return "";
    }


    public String getEncryptedBodyAlt(String aadharNumber, String pinCode, String uid) throws Exception {
        String token = "ZKJD-LJ4S-KJP1"; //Provided by AuthBridge

        if (uid.trim().equals("")) {
            SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmmssMs",new Locale("en"));
            uid = ft.format(new Date());
            System.out.println("uid is: " + uid);
        }


        JSONObject dataObj = new JSONObject()
                .put("uid", aadharNumber)
                .put("uniqueid", uid)
                .put("actiontype", "otp")
                .put("pincode", pinCode);


        System.out.println("Constructed JSON is: " + dataObj.toString());
        // getting cipher object of AES Tranforamation
        Cipher cipher = Cipher.getInstance("AES");
        byte[] key = token.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-512");

        // this is important to copy only first 256 bit of key, this must be
        // done by decryption process also.
        key = md.digest(key);
        key = Arrays.copyOf(key, 16);

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] inputBytes = dataObj.toString().getBytes();
        byte[] outputBytes = cipher.doFinal(inputBytes);

        String outputString = new String(com.niivo.com.niivo.Utilities.Base64.getEncoder().encode(outputBytes));

        System.out.println("output string: " + outputString);
        String encodedURL = URLEncoder.encode(outputString, "UTF-8");


        Log.e("EncryptString", "JAVA8:   " + com.niivo.com.niivo.Utilities.Base64.getEncoder().encodeToString(outputBytes) + "\n\n\nAndroid(Default):  "
                + Base64.encodeToString(outputBytes, Base64.DEFAULT)
                + "\n\n\nAndroid(NoWrap):  " + Base64.encodeToString(outputBytes, Base64.NO_WRAP)
                + "\n\n\nAndroid(URL Safe):  " + Base64.encodeToString(outputBytes, Base64.URL_SAFE)
                + "\n\n\nAndroid(CRLF):  " + Base64.encodeToString(outputBytes, Base64.CRLF)
                + "\n\n\nAndroid(No Close):  " + Base64.encodeToString(outputBytes, Base64.NO_CLOSE)
                + "\n\n\nAndroid(No Padding):  " + Base64.encodeToString(outputBytes, Base64.NO_PADDING));
        return encodedURL;
    }

    public String doDecrypt(String encrptedStr) {
        String encodekey = "ZKJD-LJ4S-KJP1";
        try {

            Cipher dcipher = Cipher.getInstance("AES");

            byte[] key = encodekey.getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-512");

            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

            dcipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            // decode with base64 to get bytes
            byte[] dec = com.niivo.com.niivo.Utilities.Base64.getDecoder().decode(encrptedStr.getBytes());
//                    Base64.getDecoder().decode(encrptedStr.getBytes());
            byte[] utf8 = dcipher.doFinal(dec);


            // create new string based on the specified charset
            return new String(utf8, "UTF8");

        } catch (Exception e) {

            e.printStackTrace();
        }
        return "";
    }

}
