package com.niivo.com.niivo.Adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ViewTransaction;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AllTransactionAdapter extends BaseAdapter {


    Context contextd;
    ViewTransaction list;
    TransactionHolder holder;
    String lang="";

    private ArrayList<ViewTransaction> mExampleList;
    public AllTransactionAdapter(Context contextd, ViewTransaction list) {
        this.contextd = contextd;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.getData().size();

    }

    @Override
    public Object getItem(int position) {
       return list.getData().get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.view_transaction, parent, false);
            holder = new TransactionHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (TransactionHolder) convertView.getTag();
        }
        if (list.getData().get(position).getTxn_mode().equals("R")) {
            holder.revesel.setVisibility(View.VISIBLE);
        }else{
            holder.revesel.setVisibility(View.GONE);
        }

        holder.detailSchemeName.setText(list.getData().get(position).getFundname());
        holder.detailAmountInvested.setText(Common.currencyString(list.getData().get(position).getAmount(),true));
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr=list.getData().get(position).getTrade_date().trim();
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        holder.detailInvestmentDate.setText(outputDateStr);
        holder.detailFolio.setText(list.getData().get(position).getFolio_no().trim());

        holder.tv_nav.setText("₹ "+list.getData().get(position).getNav().trim());
        holder.detailNumberOfUnit.setText(list.getData().get(position).getUnits().trim());
        holder.tv_orderstatus.setText(list.getData().get(position).getOrder_status().trim());
        holder.tv_orderid.setText(list.getData().get(position).getOrder_no().trim());
        return convertView;
    }

    class TransactionHolder  {
        TextView detailSchemeName;
        TextView detailAmountInvested;
        TextView detailInvestmentDate;
        TextView detailFolio;
        TextView detailNumberOfUnit;
        TextView tv_nav;
        TextView tv_orderstatus;
        TextView tv_orderid;
        ImageView revesel;

        public TransactionHolder(View v) {
            detailSchemeName = (TextView)v.findViewById(R.id.detailSchemeName);
            detailAmountInvested = (TextView)v.findViewById(R.id.detailAmountInvested);
            detailInvestmentDate = (TextView)v.findViewById(R.id.tv_date);
            detailFolio = (TextView)v.findViewById(R.id.detailFolio);
            tv_nav = (TextView)v.findViewById(R.id.tv_nav);
            detailNumberOfUnit = (TextView)v.findViewById(R.id.detailNumberOfUnit);
            tv_orderstatus = (TextView)v.findViewById(R.id.tv_orderstatus);
            tv_orderid = (TextView)v.findViewById(R.id.tv_orderid);
            revesel= (ImageView)v.findViewById(R.id.revesel);

        }
    }

}
