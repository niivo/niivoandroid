package com.niivo.com.niivo.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gopi on 10/4/17.
 */

public class CommonMethods {
    Context context;

    public CommonMethods(Context context) {
        this.context = context;
    }

    public Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }


    public void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public static String convertDateFormat(String selectedDate, String read,
                                           String write) {
        DateFormat readFormat = new SimpleDateFormat(read, new Locale("en"));
        DateFormat writeFormat = new SimpleDateFormat(write, new Locale("en"));
        Date date = null;
        String formattedDate = "";
        try {
            date = readFormat.parse(selectedDate);
            if (date != null) {
                formattedDate = writeFormat.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static void showAlert(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static File getTempFile(Context context) {
        final File path = new File(Environment.getExternalStorageDirectory(),
                "Niivo");
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, "image.jpg");
    }


    public static void testingDevelop(Context context, String message) {


        System.out.println("msg-- " + message);
        Log.e("msg--", message);
        Log.v("msg--", message);
        Log.d("msg--", message);


    }


    public static Boolean isInternetOn(Context con) {
        ConnectivityManager connectivityManager;

        NetworkInfo wifiInfo, mobileInfo;
        boolean connected = false;
        try {
            connectivityManager = (ConnectivityManager) con
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            connected = true;
                        }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return connected;
    }


    public void setPrefrences(String key, String Value) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("mcloud_pref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, Value);
        editor.commit();


    }

    public String getPrefrenceses(String key) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("mcloud_pref", 0);

        return sharedPreferences.getString(key, "");

    }


    public void cleatPrefrenceses() {


        SharedPreferences sharedPreferences = context.getSharedPreferences("mcloud_pref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear().commit();


    }


}
