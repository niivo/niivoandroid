package com.niivo.com.niivo.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;

public class MandateSuccessActivity extends AppCompatActivity {

    TextView mandateid,mandateamount,status,bankaccount,gotoHome;
    String mendate_id,mandatePrice,accountNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mandate_success);

        mandateid = (TextView) findViewById(R.id.mandateid);
        mandateamount = (TextView)findViewById(R.id.mandateamount);
        status = (TextView)findViewById(R.id.status);
        bankaccount = (TextView)findViewById(R.id.backaccount);
        gotoHome = (TextView)findViewById(R.id.gotoHome);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mendate_id = prefs.getString("mendate_id", null);
        mandatePrice=prefs.getString("mandatePrice", null);
        accountNumber=prefs.getString("accountNumber", null);
        mandateid.setText(mendate_id);
        mandateamount.setText(mandatePrice);
        status.setText(getResources().getString(R.string.mandatestatus));
        bankaccount.setText(accountNumber);
        gotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MandateSuccessActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
