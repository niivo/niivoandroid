package com.niivo.com.niivo.Fragment.OrderFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.InvestedRVAdapter;
import com.niivo.com.niivo.Adapter.OrderAdapter;
import com.niivo.com.niivo.Fragment.InvestmentPlan.InvestedSchemeDetailFragmentNew;
import com.niivo.com.niivo.Model.RecentOrder;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class OrderFragment extends Fragment implements ResponseDelegate,OrderAdapter.GetClick  {

    Context contextd;
    //RecyclerView myRecentList;
    ListView myRecentList;
    ImageButton back;
    RecentOrder recentOrder;
    JSONObject response;
    String lang = "";
    OrderAdapter adapter;
    RequestedServiceDataModel requestedServiceDataModel;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy", new Locale("en"));
    MainActivity activity;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView emptymsg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.order_fragment, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.Investment).toUpperCase());
        activity = (MainActivity) getActivity();
        myRecentList =(ListView)rootView.findViewById(R.id.my_Recent_list);
       // myRecentList =(RecyclerView) rootView.findViewById(R.id.my_Recent_list);
       // myRecentList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        emptymsg=(TextView) rootView.findViewById(R.id.emptymsg);
        swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.pullToRefresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.greenLight));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRecentOrderList();// your code
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        final String Kycstatuss = prefs.getString("Kycstatuss", null);
        Log.d("Kycstatuss::::",Kycstatuss);

        //investedListRV.setVisibility(View.GONE);
        if(Kycstatuss.equals("NOT STARTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.youHaventComeletDoc));
        }else if(Kycstatuss.equals("SUBMITTED")){
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.kycSubmit));
        }else if(Kycstatuss.equals("COMPLETE")) {
            emptymsg.setVisibility(View.VISIBLE);
            emptymsg.setText(contextd.getResources().getString(R.string.recentordermsg));
        }
        getRecentOrderList();
        return rootView;
    }

    private void getRecentOrderList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("RECENTORDERS");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData)  {
      //  Log.e("OrderFragment onSuccess", json);
        emptymsg.setVisibility(View.GONE);
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                Gson gson = new Gson();
                recentOrder = gson.fromJson(json, RecentOrder.class);
               // myRecentList.setAdapter(new OrderAdapter(contextd, recentOrder));
                adapter = new OrderAdapter(contextd, recentOrder);
                myRecentList.setAdapter(adapter);
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
    //    Log.e("OrderFragment onFailure" , json);
        emptymsg.setVisibility(View.VISIBLE);
        myRecentList.setVisibility(View.GONE);
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
             // Common.showToast(contextd, contextd.getResources().getString(R.string.noRecentOrder));
                break;
        }
    }
    @Override
    public void onItemClick(int position) {

        Fragment frag = new RecentOrderDetailsFragment();
        Bundle b = new Bundle();
        b.putString("detailAmountInvested", recentOrder.getData().get(position).getInvestamount().trim());
        b.putString("detailInvestmentDate", recentOrder.getData().get(position).getInvestdate().trim());
        b.putString("tv_orderno", recentOrder.getData().get(position).getOrderno().trim());
        b.putString("tv_ordertype", recentOrder.getData().get(position).getOrdertype().trim());
        b.putString("detailSchemeName", (lang.equals("en")
                ? recentOrder.getData().get(position).getFundname()
                : lang.equals("hi")
                ? recentOrder.getData().get(position).getFUNDS_HINDI()
                : lang.equals("gu")
                ? recentOrder.getData().get(position).getFUNDS_GUJARATI()
                : lang.equals("mr")
                ? recentOrder.getData().get(position).getFUNDS_MARATHI()
                : recentOrder.getData().get(position).getFundname().trim()));
        frag.setArguments(b);
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frag).addToBackStack(null).
                commit();

    }
}



