package com.niivo.com.niivo.web_view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.AppConstants;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.Utility;


/**
 * Created by hari on 10/7/18.
 */

public class WebViewActivity extends AppCompatActivity {


  //  @Bind(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
   // @Bind(R.id.tv_save)
    TextView tvSave;
    private Dialog progressdialog;
    WebView webView;
    private LinearLayout layoutNoNetwork;
    private TextView tvRetry;
    private Activity activity;
    private String webViewUrl;
    private ImageButton back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        tvToolbarTitle = findViewById(R.id.toolbar_title);
       // tvSave = findViewById(R.id.tvToolbarTitle);
        getIntentValues();
        activity = this;
       // tvToolbarTitle.setText(getString(R.string.fee));
      //  tvSave.setVisibility(View.GONE);
        initForWebView();
    }

    private void getIntentValues() {
        int pageType = getIntent().getIntExtra(AppConstants.WEB_PAGE_TYPE, 2);
        if (pageType == 1) {
            webViewUrl = AppConstants.PRIVACY_POLICY;
            tvToolbarTitle.setText(getString(R.string.privacy));
        } else if (pageType == 2) {
            webViewUrl = AppConstants.TERMS_CONDITIONS;
            tvToolbarTitle.setText(getString(R.string.terms));
        }

    }

    private void initForWebView() {
        webView = (WebView) findViewById(R.id.webView);
        back = (ImageButton)findViewById(R.id.icon_navi);
        tvRetry = (TextView) findViewById(R.id.tv_retry);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        layoutNoNetwork = (LinearLayout) findViewById(R.id.layout_no_network);
        if(Utility.isOnline(activity))
        {


            loadUrlIntowebView();
        }
        else
        {
            layoutNoNetwork.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
            tvRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Utility.isOnline(activity))
                        loadUrlIntowebView();

                }
            });

        }
    }

    private void loadUrlIntowebView() {
        layoutNoNetwork.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);

        //= AppConstants.TERMS_OF_USE_URL;
        String langCode = Common.getPreferences(getApplicationContext(),"lang_code");
        if(langCode==null || langCode.equals("") || langCode.equals("en"))
        {
            webViewUrl = webViewUrl;
        }

        Log.i("URL_PAGE", "loadUrlIntowebView: "+ webViewUrl);
        webView.loadUrl(webViewUrl);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setSupportZoom(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        showProgress(activity);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                if(progress == 100)
                    hideProgress();
            }
        });
    }

    private void showProgress(Context activityContext)
    {
        try {
            progressdialog = new Dialog(activityContext);
            progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressdialog.setContentView(R.layout.view_progress);
//        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            // Set the progress dialog background color transparent
            progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            // pDialog = new ProgressDialog(context);
            // pDialog.setMessage("Loading..");
            progressdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            progressdialog.setCancelable(true);
            progressdialog.show();
        }
        catch (Exception e)
        {

        }
    }
    private void hideProgress()
    {
        if (progressdialog.isShowing()) {
            try {
                progressdialog.dismiss();
            } catch (Exception e) {

            }
        }
    }


   }
