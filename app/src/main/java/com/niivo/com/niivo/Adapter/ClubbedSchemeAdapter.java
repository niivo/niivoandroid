package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.niivo.com.niivo.R;

/**
 * Created by deepak on 28/8/18.
 */

public class ClubbedSchemeAdapter extends RecyclerView.Adapter {
    Context contextd;

    public ClubbedSchemeAdapter(Context contextd) {
        this.contextd = contextd;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(contextd).inflate(R.layout.item_single_scheme, parent, false);
        return new ClubbedSchemeHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class ClubbedSchemeHolder extends RecyclerView.ViewHolder {
        public ClubbedSchemeHolder(View itemView) {
            super(itemView);
        }
    }
}
