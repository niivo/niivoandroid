package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 11/9/17.
 */

public class InvestmentFunds {
    String status, msg;
    ArrayList<FundItem> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<FundItem> getData() {
        return data;
    }

    public void setData(ArrayList<FundItem> data) {
        this.data = data;
    }

    public class FundItem {
        String AMFI_SCHEME_CODE;
        String ISIN;
        String ISIN_DIV;
        String SCHEME_NAME;
        String NAV;
        String ONE_M_PERF;
        String THREE_M_PERF;
        String SIX_M_PERF;
        String ONE_Y_PERF;
        String TWO_Y_PERF;
        String THREE_Y_PERF;
        String FIVE_Y_PERF;
        String SEVEN_Y_PERF;
        String TEN_Y_PERF;
        String PERSISTENCE_SCORE;
        String RISK_AVERSION_SCORE;
        String RISK_TAKING_SCORE;
        String LONG_TERM_SCORE;
        String TICKER;
        String DS192;
        String SHARE_CLASS;
        String FUND_NAME;
        String FUND_BENCHMARK;
        String FUND_CLASS;
        String FUND_CATEGORY;
        String FUND_CATEGORIZATION;
        String EXPENSE_RATIO;
        String STARMF_SCHEME_CODE;
        String STARMF_SCHEME_NAME;
        String STARMF_RTA_SCHEME_CODE;
        String STARMF_DIV_REINVESTFLAG;
        String DESCRIPTION;
        String STARMF_RTA_CODE;
        String NAV_DATE;
        String STARMF_SIP_FLAG;
        String MIN_INIT;
        String STARMF_MIN_PUR_AMT;
        String SIP_MIN_INSTALLMENT_AMT;
        String SIP_MULTPL_AMT;
        String SIP_MIN_INSTALLMENT_NUM;
        String SIP_MAX_INSTALLMENT_NUM;
        String SIP_DATES;
        String L1_FLAG;
        String FUNDS_ENGLISH;
        String FUNDS_GUJARATI;
        String FUNDS_HINDI;
        String FUNDS_MARATHI;
        String MAIN_CATEGORY;
        String SUB_CATEGORY;

        public String getMAIN_CATEGORY() {
            return MAIN_CATEGORY;
        }

        public void setMAIN_CATEGORY(String MAIN_CATEGORY) {
            this.MAIN_CATEGORY = MAIN_CATEGORY;
        }

        public String getSUB_CATEGORY() {
            return SUB_CATEGORY;
        }

        public void setSUB_CATEGORY(String SUB_CATEGORY) {
            this.SUB_CATEGORY = SUB_CATEGORY;
        }



        public String getFUNDS_ENGLISH() {
            return FUNDS_ENGLISH;
        }

        public void setFUNDS_ENGLISH(String FUNDS_ENGLISH) {
            this.FUNDS_ENGLISH = FUNDS_ENGLISH;
        }

        public String getFUNDS_GUJARATI() {
            return FUNDS_GUJARATI;
        }

        public void setFUNDS_GUJARATI(String FUNDS_GUJARATI) {
            this.FUNDS_GUJARATI = FUNDS_GUJARATI;
        }

        public String getFUNDS_HINDI() {
            return FUNDS_HINDI;
        }

        public void setFUNDS_HINDI(String FUNDS_HINDI) {
            this.FUNDS_HINDI = FUNDS_HINDI;
        }

        public String getFUNDS_MARATHI() {
            return FUNDS_MARATHI;
        }

        public void setFUNDS_MARATHI(String FUNDS_MARATHI) {
            this.FUNDS_MARATHI = FUNDS_MARATHI;
        }

        public String getL1_FLAG() {
            return L1_FLAG;
        }

        public void setL1_FLAG(String l1_FLAG) {
            L1_FLAG = l1_FLAG;
        }

        public String getSIP_DATES() {
            return SIP_DATES;
        }

        public void setSIP_DATES(String SIP_DATES) {
            this.SIP_DATES = SIP_DATES;
        }

        public String getSTARMF_MIN_PUR_AMT() {
            return STARMF_MIN_PUR_AMT;
        }

        public void setSTARMF_MIN_PUR_AMT(String STARMF_MIN_PUR_AMT) {
            this.STARMF_MIN_PUR_AMT = STARMF_MIN_PUR_AMT;
        }

        public String getSIP_MIN_INSTALLMENT_AMT() {
            return SIP_MIN_INSTALLMENT_AMT;
        }

        public void setSIP_MIN_INSTALLMENT_AMT(String SIP_MIN_INSTALLMENT_AMT) {
            this.SIP_MIN_INSTALLMENT_AMT = SIP_MIN_INSTALLMENT_AMT;
        }

        public String getSIP_MULTPL_AMT() {
            return SIP_MULTPL_AMT;
        }

        public void setSIP_MULTPL_AMT(String SIP_MULTPL_AMT) {
            this.SIP_MULTPL_AMT = SIP_MULTPL_AMT;
        }

        public String getSIP_MIN_INSTALLMENT_NUM() {
            return SIP_MIN_INSTALLMENT_NUM;
        }

        public void setSIP_MIN_INSTALLMENT_NUM(String SIP_MIN_INSTALLMENT_NUM) {
            this.SIP_MIN_INSTALLMENT_NUM = SIP_MIN_INSTALLMENT_NUM;
        }

        public String getSIP_MAX_INSTALLMENT_NUM() {
            return SIP_MAX_INSTALLMENT_NUM;
        }

        public void setSIP_MAX_INSTALLMENT_NUM(String SIP_MAX_INSTALLMENT_NUM) {
            this.SIP_MAX_INSTALLMENT_NUM = SIP_MAX_INSTALLMENT_NUM;
        }

        public String getFUND_CATEGORIZATION() {
            return FUND_CATEGORIZATION;
        }

        public void setFUND_CATEGORIZATION(String FUND_CATEGORIZATION) {
            this.FUND_CATEGORIZATION = FUND_CATEGORIZATION;
        }

        public String getEXPENSE_RATIO() {
            return EXPENSE_RATIO;
        }

        public void setEXPENSE_RATIO(String EXPENSE_RATIO) {
            this.EXPENSE_RATIO = EXPENSE_RATIO;
        }

        public String getDESCRIPTION() {
            return DESCRIPTION;
        }

        public void setDESCRIPTION(String DESCRIPTION) {
            this.DESCRIPTION = DESCRIPTION;
        }

        public String getSTARMF_SIP_FLAG() {
            return STARMF_SIP_FLAG;
        }

        public void setSTARMF_SIP_FLAG(String STARMF_SIP_FLAG) {
            this.STARMF_SIP_FLAG = STARMF_SIP_FLAG;
        }

        public String getMIN_INIT() {
            return MIN_INIT;
        }

        public void setMIN_INIT(String MIN_INIT) {
            this.MIN_INIT = MIN_INIT;
        }

        public String getAMFI_SCHEME_CODE() {
            return AMFI_SCHEME_CODE;
        }

        public void setAMFI_SCHEME_CODE(String AMFI_SCHEME_CODE) {
            this.AMFI_SCHEME_CODE = AMFI_SCHEME_CODE;
        }

        public String getISIN() {
            return ISIN;
        }

        public void setISIN(String ISIN) {
            this.ISIN = ISIN;
        }

        public String getISIN_DIV() {
            return ISIN_DIV;
        }

        public void setISIN_DIV(String ISIN_DIV) {
            this.ISIN_DIV = ISIN_DIV;
        }

        public String getSCHEME_NAME() {
            return SCHEME_NAME;
        }

        public void setSCHEME_NAME(String SCHEME_NAME) {
            this.SCHEME_NAME = SCHEME_NAME;
        }

        public String getNAV() {
            return NAV;
        }

        public void setNAV(String NAV) {
            this.NAV = NAV;
        }

        public String getONE_M_PERF() {
            return ONE_M_PERF;
        }

        public void setONE_M_PERF(String ONE_M_PERF) {
            this.ONE_M_PERF = ONE_M_PERF;
        }

        public String getTHREE_M_PERF() {
            return THREE_M_PERF;
        }

        public void setTHREE_M_PERF(String THREE_M_PERF) {
            this.THREE_M_PERF = THREE_M_PERF;
        }

        public String getSIX_M_PERF() {
            return SIX_M_PERF;
        }

        public void setSIX_M_PERF(String SIX_M_PERF) {
            this.SIX_M_PERF = SIX_M_PERF;
        }

        public String getONE_Y_PERF() {
            return ONE_Y_PERF;
        }

        public void setONE_Y_PERF(String ONE_Y_PERF) {
            this.ONE_Y_PERF = ONE_Y_PERF;
        }

        public String getTWO_Y_PERF() {
            return TWO_Y_PERF;
        }

        public void setTWO_Y_PERF(String TWO_Y_PERF) {
            this.TWO_Y_PERF = TWO_Y_PERF;
        }

        public String getTHREE_Y_PERF() {
            return THREE_Y_PERF;
        }

        public void setTHREE_Y_PERF(String THREE_Y_PERF) {
            this.THREE_Y_PERF = THREE_Y_PERF;
        }

        public String getFIVE_Y_PERF() {
            return FIVE_Y_PERF;
        }

        public void setFIVE_Y_PERF(String FIVE_Y_PERF) {
            this.FIVE_Y_PERF = FIVE_Y_PERF;
        }

        public String getSEVEN_Y_PERF() {
            return SEVEN_Y_PERF;
        }

        public void setSEVEN_Y_PERF(String SEVEN_Y_PERF) {
            this.SEVEN_Y_PERF = SEVEN_Y_PERF;
        }

        public String getTEN_Y_PERF() {
            return TEN_Y_PERF;
        }

        public void setTEN_Y_PERF(String TEN_Y_PERF) {
            this.TEN_Y_PERF = TEN_Y_PERF;
        }

        public String getPERSISTENCE_SCORE() {
            return PERSISTENCE_SCORE;
        }

        public void setPERSISTENCE_SCORE(String PERSISTENCE_SCORE) {
            this.PERSISTENCE_SCORE = PERSISTENCE_SCORE;
        }

        public String getRISK_AVERSION_SCORE() {
            return RISK_AVERSION_SCORE;
        }

        public void setRISK_AVERSION_SCORE(String RISK_AVERSION_SCORE) {
            this.RISK_AVERSION_SCORE = RISK_AVERSION_SCORE;
        }

        public String getRISK_TAKING_SCORE() {
            return RISK_TAKING_SCORE;
        }

        public void setRISK_TAKING_SCORE(String RISK_TAKING_SCORE) {
            this.RISK_TAKING_SCORE = RISK_TAKING_SCORE;
        }

        public String getLONG_TERM_SCORE() {
            return LONG_TERM_SCORE;
        }

        public void setLONG_TERM_SCORE(String LONG_TERM_SCORE) {
            this.LONG_TERM_SCORE = LONG_TERM_SCORE;
        }

        public String getTICKER() {
            return TICKER;
        }

        public void setTICKER(String TICKER) {
            this.TICKER = TICKER;
        }

        public String getDS192() {
            return DS192;
        }

        public void setDS192(String DS192) {
            this.DS192 = DS192;
        }

        public String getSHARE_CLASS() {
            return SHARE_CLASS;
        }

        public void setSHARE_CLASS(String SHARE_CLASS) {
            this.SHARE_CLASS = SHARE_CLASS;
        }

        public String getFUND_NAME() {
            return FUND_NAME;
        }

        public void setFUND_NAME(String FUND_NAME) {
            this.FUND_NAME = FUND_NAME;
        }

        public String getFUND_BENCHMARK() {
            return FUND_BENCHMARK;
        }

        public void setFUND_BENCHMARK(String FUND_BENCHMARK) {
            this.FUND_BENCHMARK = FUND_BENCHMARK;
        }

        public String getFUND_CLASS() {
            return FUND_CLASS;
        }

        public void setFUND_CLASS(String FUND_CLASS) {
            this.FUND_CLASS = FUND_CLASS;
        }

        public String getFUND_CATEGORY() {
            return FUND_CATEGORY;
        }

        public void setFUND_CATEGORY(String FUND_CATEGORY) {
            this.FUND_CATEGORY = FUND_CATEGORY;
        }

        public String getSTARMF_SCHEME_CODE() {
            return STARMF_SCHEME_CODE;
        }

        public void setSTARMF_SCHEME_CODE(String STARMF_SCHEME_CODE) {
            this.STARMF_SCHEME_CODE = STARMF_SCHEME_CODE;
        }

        public String getSTARMF_SCHEME_NAME() {
            return STARMF_SCHEME_NAME;
        }

        public void setSTARMF_SCHEME_NAME(String STARMF_SCHEME_NAME) {
            this.STARMF_SCHEME_NAME = STARMF_SCHEME_NAME;
        }

        public String getSTARMF_RTA_SCHEME_CODE() {
            return STARMF_RTA_SCHEME_CODE;
        }

        public void setSTARMF_RTA_SCHEME_CODE(String STARMF_RTA_SCHEME_CODE) {
            this.STARMF_RTA_SCHEME_CODE = STARMF_RTA_SCHEME_CODE;
        }

        public String getSTARMF_DIV_REINVESTFLAG() {
            return STARMF_DIV_REINVESTFLAG;
        }

        public void setSTARMF_DIV_REINVESTFLAG(String STARMF_DIV_REINVESTFLAG) {
            this.STARMF_DIV_REINVESTFLAG = STARMF_DIV_REINVESTFLAG;
        }

        public String getSTARMF_RTA_CODE() {
            return STARMF_RTA_CODE;
        }

        public void setSTARMF_RTA_CODE(String STARMF_RTA_CODE) {
            this.STARMF_RTA_CODE = STARMF_RTA_CODE;
        }

        public String getNAV_DATE() {
            return NAV_DATE;
        }

        public void setNAV_DATE(String NAV_DATE) {
            this.NAV_DATE = NAV_DATE;
        }
    }
}
