package com.niivo.com.niivo.Activity;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentSuccess extends AppCompatActivity implements ResponseDelegate {

    ImageButton back;
    String amount,schemeName;
    TextView thanks_text,backToHome;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        thanks_text = (TextView)findViewById(R.id.thanks_text);
        back= (ImageButton)findViewById(R.id.back);
        backToHome=(TextView)findViewById(R.id.backToHome);
        backToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentSuccess.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });
        thankyou();
    }

    void thankyou() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-payment.php");
        baseRequestData.setTag(ResponseType.ThankYou);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("orderid",getIntent().getStringExtra("orderid"));//"76453904");
        requestedServiceDataModel.setWebServiceType("thankyou");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        switch (baseRequestData.getTag()) {
            case ResponseType.ThankYou:
                try {

            //        Log.d("Thankyou json PS", json);
                    JSONObject obj = new JSONObject(json);
                    JSONObject jsonObject = new JSONObject(json);
                    amount = jsonObject.getJSONObject("data").getString("amount");
               //     Log.d("amount thankyou",amount);
                    schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
               //     Log.d("schemeName thankyou",schemeName);
                    String str = "Thank you for investing "+"Rs."+amount+" in "+schemeName+".";
                //    Log.d("str thankyou",str);
                    thanks_text.setText(str);
                //    Log.i("Url Response", "onSuccess: "+str);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ThankYou:
             //   Log.d("Thankyou json", json);
                break;
        }
    }
}
