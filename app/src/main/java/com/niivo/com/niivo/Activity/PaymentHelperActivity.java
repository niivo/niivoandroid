package com.niivo.com.niivo.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by deepak on 7/11/17.
 */

public class PaymentHelperActivity extends AppCompatActivity implements View.OnClickListener,ResponseDelegate {
    WebView helperWebView;
    ImageButton back;
    Button gotoHome;
    TextView thanks_text;
    RelativeLayout thankYouScreen;
    String rawWebData = "";
    String aaDharDetails = "";
    ProgressDialog pd;
    private String orderIdValueForProcessing;
    private RequestedServiceDataModel requestedServiceDataModel;
    private String amount,schemeName ,orderno;
    Context contextd;
    final Handler handler = new Handler();
    private static final String TAG="PaymentHelperActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(PaymentHelperActivity.this, Common.getLanguage(PaymentHelperActivity.this), false);
        fixupLocale(PaymentHelperActivity.this, new Locale(Common.getLanguage(PaymentHelperActivity.this)));
        setContentView(R.layout.activity_payment_helper_act);
        back = (ImageButton) findViewById(R.id.back);
        gotoHome = (Button) findViewById(R.id.backToHome);
        thankYouScreen = (RelativeLayout) findViewById(R.id.thankYouScreen);
        thanks_text=findViewById(R.id.thanks_text);
        thankYouScreen.setVisibility(View.GONE);
        back.setOnClickListener(this);
        gotoHome.setOnClickListener(this);
        helperWebView = (WebView) findViewById(R.id.helperWebView);
        rawWebData = getIntent().getStringExtra("rawHtml");
        Log.d("rawWebData..",rawWebData);
        pd = new ProgressDialog(this);
        pd.setMessage(this.getResources().getString(R.string.loadingPleaseWait));
        pd.setIndeterminate(false);
        pd.setCancelable(false);
       // pd.dismiss();
        orderno=getIntent().getStringExtra("orderid");
        loadWebData(rawWebData.replace('\n', ' ').trim());
        CallService.run();

    }
    void loadWebData(String rawData) {
        helperWebView.getSettings().setJavaScriptEnabled(true);
        helperWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        helperWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        helperWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        helperWebView.getSettings().setSupportMultipleWindows(true);
        helperWebView.setWebChromeClient(new CustomChromeClent());
        helperWebView.setWebViewClient(new CustomWebClient());
        helperWebView.loadData(rawData, "text/html", "UTF-8");
    }

    public Runnable CallService = new Runnable() {
        @Override
        public void run() {
           thankyou();
            handler.postDelayed(CallService,5000); // 5 seconds
        }
    };

    void thankyou() {
            requestedServiceDataModel = new RequestedServiceDataModel(this, this);
            BaseRequestData baseRequestData = new BaseRequestData();
            baseRequestData.setWebservice("ws-payment.php");
            baseRequestData.setTag(ResponseType.ThankYou);
            baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
            requestedServiceDataModel.putQurry("orderid",getIntent().getStringExtra("orderid"));
            requestedServiceDataModel.setWebServiceType("thankyou");
            requestedServiceDataModel.setBaseRequestData(baseRequestData);
            requestedServiceDataModel.executeWithoutProgressbar();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backToHome:
                Intent i = new Intent();
                i.putExtra("state", "success");
                setResult(113, i);
                PaymentHelperActivity.this.finish();
                break;
            case R.id.back:
                if (helperWebView != null) {
                    if (helperWebView.canGoBack()) {
                        if (helperWebView.getUrl().trim().contains("http://172.104.187.141/production/public/api/services/thank-you.php")) {
                            back.setVisibility(View.GONE);
                 //           Log.i( "HelperWebViewonClick: ",helperWebView.getUrl().trim());
                           // String[] urlOrderId = helperWebView.getUrl().trim().split("\\?");
                           // String[] orderIdValue = urlOrderId[1].split("&");
                           // Log.i("onClick: ",orderIdValue[0]);
                        try {
                            String url = helperWebView.getUrl().trim();
                            String[] urlOrderId = url.trim().split("\\?");
                            if (urlOrderId.length > 1) {
                                String[] orderIdValue = urlOrderId[1].split("&");
                                //Log.i("onClick: ", orderIdValue[0].split("=")[1]);
                                orderIdValueForProcessing = orderIdValue[0].split("=")[1];
                                Log.i("onClickUrlId: ", orderIdValueForProcessing + "");
                                getOrderDetailsValues();
                            }
                        }
                        catch (Exception e)
                        {

                        }
                            gotoHome.setVisibility(View.VISIBLE);
                            thankYouScreen.setVisibility(View.VISIBLE);
                        } else {
                            helperWebView.goBack();
                        }
                    } else {
                        gotTohome();
                    }
                }
                break;
        }
    }
    private void getOrderDetailsValues() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderDetail);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        //  requestedServiceDataModel.putQurry("userid", Common.getPreferences(getApplicationContext(), "userID"));
        requestedServiceDataModel.putQurry("order_id", orderIdValueForProcessing);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETORDERDETAIL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderDetail:
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    amount = jsonObject.getJSONObject("data").getString("amount");
                    schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
                    String str = "Thank you for investing "+"Rs."+amount+" in "+schemeName+".";
                    thanks_text.setText(str);
              //      Log.i("Url Response", "onSuccess: "+str);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.ThankYou:
                try {
            //        Log.d("Thankyou json PHA", json);
                    JSONObject obj = new JSONObject(json);

                    if (obj.getJSONObject("data").getString("code").equalsIgnoreCase("H")) {
                        pd.dismiss();
//
                    } else if (obj.getJSONObject("data").getString("code").equalsIgnoreCase("S")) {
                        pd.show();
//
                    } else if (obj.getJSONObject("data").getString("code").equalsIgnoreCase("T")) {
                        handler.removeCallbacks(CallService);
                        pd.dismiss();
                        Intent i = new Intent(PaymentHelperActivity.this,PaymentSuccess.class);
                        i.putExtra("orderid",getIntent().getStringExtra("orderid"));
                        startActivityForResult(i,113);
                        finish();
                    } else {
                        handler.removeCallbacks(CallService);
                        pd.dismiss();
                        Intent i = new Intent(PaymentHelperActivity.this,PaymentError.class);
                        i.putExtra("orderid",getIntent().getStringExtra("orderid"));
                        startActivityForResult(i,113);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ThankYou:
                break;
        }

    }

    class CustomWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
      //      Log.i(TAG,"enter into CustomWebClient");
            super.onPageStarted(view, url, favicon);
     //       Log.e("pageStart", url.trim());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        //    Log.e("UrlLoading", url);
            view.loadUrl(url);
            if (!pd.isShowing())
                pd.show();
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
     //       Log.e("pageFinished", url.trim());


//            if (url.trim().contains("http://172.104.187.141/production/public/api/services/thank-you.php")) {
//                Log.e("pageFinished thanku", String.valueOf((url.trim().contains("http://172.104.187.141/production/public/api/services/thank-you.php"))));
//                back.setVisibility(View.GONE);
//                try {
//                    String url1 = url.trim();
//                    String[] urlOrderId = url1.trim().split("\\?");
//                    if (urlOrderId.length > 1) {
//                        String[] orderIdValue = urlOrderId[1].split("&");
//                        //Log.i("onClick: ", orderIdValue[0].split("=")[1]);
//                        orderIdValueForProcessing = orderIdValue[0].split("=")[1];
//                        Log.i("onClickUrlId: ", orderIdValueForProcessing + "");
//                        getOrderDetailsValues();
//                    }
//                }
//                catch (Exception e)
//                {
//
//                }
//                gotoHome.setVisibility(View.VISIBLE);
//                thankYouScreen.setVisibility(View.VISIBLE);
//
//            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            pd.dismiss();
 //           Log.e("receivedError", error.getDescription().toString().trim());
        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
            pd.dismiss();
            Log.e("HttpAuthRequest", host.trim());
        }
    }

    class CustomChromeClent extends WebChromeClient {


        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
   //         Log.i(TAG,"enter into CustomChromeClent");

            WebView newWebView = new WebView(PaymentHelperActivity.this);
            newWebView.getSettings().setJavaScriptEnabled(true);
            newWebView.getSettings().setSupportZoom(true);
            newWebView.getSettings().setBuiltInZoomControls(true);
            newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            newWebView.getSettings().setSupportMultipleWindows(true);
            view.addView(newWebView);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();
            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    try {
         //               Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!pd.isShowing())
                        pd.show();
      //              Log.e("NewUrl", url);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.e("NewUrl", url);
                    try {
                        Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    view.loadUrl(url);
                    if (!pd.isShowing())
                        pd.show();
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    pd.dismiss();
         //           Log.e("UrlFinish", url);
//                    if (url.trim().contains("http://http://172.104.187.141/production/public/api/services/thank-you.php")) {
//                        back.setVisibility(View.GONE);
//                        try {
//                            String url1 = url.trim();
//                            String[] urlOrderId = url1.trim().split("\\?");
//                            if (urlOrderId.length > 1) {
//                                String[] orderIdValue = urlOrderId[1].split("&");
//                                //Log.i("onClick: ", orderIdValue[0].split("=")[1]);
//                                orderIdValueForProcessing = orderIdValue[0].split("=")[1];
//                                Log.i("onClickUrlId: ", orderIdValueForProcessing + "");
//                                getOrderDetailsValues();
//                            }
//                        }
//                        catch (Exception e)
//                        {
//
//                        }
                        gotoHome.setVisibility(View.VISIBLE);
                        thankYouScreen.setVisibility(View.VISIBLE);
//                    }
                }
            });
            return true;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && helperWebView.canGoBack()) {
            helperWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (helperWebView != null) {
            if (helperWebView.canGoBack()) {
//                if (helperWebView.getUrl().trim().contains("http://172.104.187.141/production/public/api/services/thank-you.php")) {
//                    back.setVisibility(View.GONE);
//                    try {
//                        String url1 = helperWebView.getUrl().trim();
//                        String[] urlOrderId = url1.trim().split("\\?");
//                        if (urlOrderId.length > 1) {
//                            String[] orderIdValue = urlOrderId[1].split("&");
//                            //Log.i("onClick: ", orderIdValue[0].split("=")[1]);
//                            orderIdValueForProcessing = orderIdValue[0].split("=")[1];
//                            Log.i("onClickUrlId: ", orderIdValueForProcessing + "");
//                            getOrderDetailsValues();
//                        }
//                    }
//                    catch (Exception e)
//                    {
//
//                    }
//                    gotoHome.setVisibility(View.VISIBLE);
//                    thankYouScreen.setVisibility(View.VISIBLE);
//                } else {
//                    helperWebView.goBack();
//                }
            } else {
                gotTohome();


            }
        }
    }

    private void gotTohome() {
        handler.removeCallbacks(CallService);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PaymentHelperActivity.this, R.style.myDialogTheme);
        builder.setTitle(PaymentHelperActivity.this.getResources().getString(R.string.app_name));
        builder.setMessage(PaymentHelperActivity.this.getResources().getString(R.string.theOrderhasbooked));
        builder.setPositiveButton(PaymentHelperActivity.this.getResources().getString(R.string.goToHome),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      //  Intent i = new Intent();
                        Intent i = new Intent(PaymentHelperActivity.this,MainActivity.class);
                        i.putExtra("state", "success");
                        setResult(113, i);
                        startActivity(i);
                         PaymentHelperActivity.this.finish();
                    }
                });
        builder.setNegativeButton(PaymentHelperActivity.this.getResources().getString(R.string.stay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    void fixupLocale(Context ctx, Locale newLocale) {
        final Resources res = ctx.getResources();
        final Configuration config = res.getConfiguration();
        final Locale curLocale = getLocale(config);
        if (!curLocale.equals(newLocale)) {
            Locale.setDefault(newLocale);
            final Configuration conf = new Configuration(config);
            conf.setLocale(newLocale);
            res.updateConfiguration(conf, res.getDisplayMetrics());
        }
    }

    private static Locale getLocale(Configuration config) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return config.getLocales().get(0);
        } else {
            //noinspection deprecation
            return config.locale;
        }
    }
}

