package com.niivo.com.niivo.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.vansuita.library.CheckNewAppVersion;

import net.alexandroid.utils.indicators.IndicatorsView;

import java.util.ArrayList;

/**
 * Created by deepak on 15/12/17.
 */

public class IndroductionActivity extends Activity {
    ViewPager introPager;
    IndicatorsView pagerIndicators;
    ImageButton next, skip;

    //Non ui Vars
    ArrayList<Integer> images;
    int selectedPos = 0;
    CheckNewAppVersion st;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(IndroductionActivity.this, Common.getLanguage(IndroductionActivity.this), false);
        setContentView(R.layout.acivity_introduction);
        introPager = (ViewPager) findViewById(R.id.introPager);
        pagerIndicators = (IndicatorsView) findViewById(R.id.indicatorsView);
        next = (ImageButton) findViewById(R.id.btn_next);
        skip = (ImageButton) findViewById(R.id.btn_skip);
        setPager();
        st = new CheckNewAppVersion(IndroductionActivity.this).setOnTaskCompleteListener(new CheckNewAppVersion.ITaskComplete() {
            @Override
            public void onTaskComplete(CheckNewAppVersion.Result result) {
                if (result.hasNewVersion()) {
                    ShowUpdateDialog();
                }
            }
        });
        st.execute();
        introPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPos = position;
                if (selectedPos != 6)
                    next.setImageResource(R.drawable.next);
                else if (selectedPos == 6)
                    next.setImageResource(R.drawable.done);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos++;
                if (selectedPos <= 6) {
                    introPager.setCurrentItem(selectedPos, true);
                }
                if (selectedPos > 6) {
                    if (!st.isCancelled())
                        st.cancel(true);
                    startActivity(new Intent(IndroductionActivity.this, ChooseLanguageActivity.class));
                    IndroductionActivity.this.finish();
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!st.isCancelled())
                    st.cancel(true);
                startActivity(new Intent(IndroductionActivity.this, ChooseLanguageActivity.class));
                IndroductionActivity.this.finish();
            }
        });
    }

    void setPager() {
        images = new ArrayList<>();
        images.add(R.drawable.one);
        images.add(R.drawable.two);
        images.add(R.drawable.three);
        images.add(R.drawable.four_new_min);
        images.add(R.drawable.five_new_min);
        images.add(R.drawable.six_new_min);
        images.add(R.drawable.seven_new_min);

        introPager.setAdapter(new SlidingImage_Adapter(images));
        pagerIndicators.setViewPager(introPager);
        pagerIndicators.setSmoothTransition(true);

    }

    class SlidingImage_Adapter extends PagerAdapter {
        private ArrayList<Integer> IMAGES;
        private LayoutInflater inflater;

        public SlidingImage_Adapter(ArrayList<Integer> IMAGES) {
            this.IMAGES = IMAGES;
            inflater = (LayoutInflater) IndroductionActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }

        @Override
        public int getCount() {
            return IMAGES.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View imageLayout = inflater.inflate(R.layout.item_intro_pag, container, false);
            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            imageView.setImageResource(IMAGES.get(position));
            container.addView(imageLayout);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }


    }


    void ShowUpdateDialog() {
        new AlertDialog.Builder(IndroductionActivity.this, R.style.myDialogTheme)
                .setTitle(IndroductionActivity.this.getResources().getString(R.string.app_name))
                .setMessage(IndroductionActivity.this.getResources().getString(R.string.newVersionCheck))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Common.showToast(IndroductionActivity.this, "Unable to find market app");
                        }
                    }
                })
                .show().setCancelable(false);
    }
}
