package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.InvestedAdapter;
import com.niivo.com.niivo.Adapter.OrderAdapter;
import com.niivo.com.niivo.Adapter.PastSipAdapter;
import com.niivo.com.niivo.Adapter.SipDueReportAdapter;
import com.niivo.com.niivo.Fragment.InvestHelper.PendingRedirectingFragment;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.Model.PastSipList;
import com.niivo.com.niivo.Model.RecentOrder;
import com.niivo.com.niivo.Model.SipDueReports;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.SelctedLineareLayout;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Deepak
 */

public class InvestHistoryFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Activity _activity,activity;
    View parentView;
    SelctedLineareLayout select;
    ListView orderdList;
    TextView past, redeemd, pending;
    TextView title;

    //Non ui Vars
    RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    OrderedList list;
    InvestedAdapter adapter;
    String type;
    JSONObject obj;
    SipDueReports sipDueReports;
    SipDueReportAdapter sipAdapter;
    String status,paymentStatus;
    private String order_type;
    private String orderId;
    String typenew;
    PastSipAdapter pastSipAdapter;
    PastSipList pastSipList;
    SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        activity = _activity;
        parentView = inflater.inflate(R.layout.fragment_invest_history, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setVisibility(View.VISIBLE);
        title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        select = (SelctedLineareLayout) parentView.findViewById(R.id.root);
        past = (TextView) parentView.findViewById(R.id.orderedPast);
    //  redeemd = (TextView) parentView.findViewById(R.id.orderedRedeem);
        pending = (TextView) parentView.findViewById(R.id.orderedPending);
        orderdList = (ListView) parentView.findViewById(R.id.orderedList);

        type = getArguments().getString("type");
        typenew = getArguments().getString("type");

        setScreenInfo();
        past.setOnClickListener(this);
     // redeemd.setOnClickListener(this);
        pending.setOnClickListener(this);
        swipeRefreshLayout =(SwipeRefreshLayout)parentView.findViewById(R.id.pullToRefresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.textGreen));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
            //    getInvestmentFundList(status,paymentStatus);
                getPastSip();
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        orderdList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (select.getselected() != 0) {
                    if (type.equals("SIP")  || type.equals("XSIP") ) {
                        if (sipDueReports.getData().get(position).getOrder_detail().size() > 0) {
                            float amount = 0;
                            if (sipDueReports.getData().get(position).getIs_goal().equals("1")) {
                                for (int i = 0; i < sipDueReports.getData().get(position).getOrder_detail().size(); i++) {
                                    amount += Float.parseFloat(sipDueReports.getData().get(position).getOrder_detail().get(i).getAmount().trim());
                                }
                            } else {
                                amount = Float.parseFloat(sipDueReports.getData().get(position).getOrder_detail().get(0).getAmount().trim());
                            }
                            if (getDuraDueDate(sipDueReports.getData().get(position).getDue_date()) == -1) {
                                Common.showDialog(contextd, contextd.getResources().getString(R.string.due_date_of_sip));
                            } else if (getDuraDueDate(sipDueReports.getData().get(position).getDue_date()) == 0) {
                                investInThisFund(true, position, (sipDueReports.getData().get(position).getIs_goal().equals("1") ?
                                                sipDueReports.getData().get(position).getGoal_detail().getGoal_name() :
                                                sipDueReports.getData().get(position).getFund_name()),
                                        (sipDueReports.getData().get(position).getIs_goal().equals("1") ?
                                                "GOAL" :
                                                "FUND"),
                                        amount + "",
                                        "SIP");
                            } else {
                                Common.showDialog(contextd,
                                        (getDuraDueDate(sipDueReports.getData().get(position).getDue_date()) == 1) ?
                                                contextd.getResources().getString(R.string.make_sip_tomorrow) :
                                                (contextd.getResources().getString(R.string.make_payment_after) + getDuraDueDate(sipDueReports.getData().get(position).getDue_date()) + contextd.getResources().getString(R.string.days)));
                            }
                        }
                    } else {
                        if (list.getData().get(position).getIs_goal().equals("1")) {
                            investInThisFund(false, position,
                                    list.getData().get(position).getGoal_detail().getGoal_name(),
                                    "GOAL",
                                    "",
                                    list.getData().get(position).getOrder_type().equals("MONTHLY") ? "SIP" : "ONETIME");
                        } else {
                            investInThisFund(false, position,
                                    list.getData().get(position).getFUND_NAME(),
                                    "FUND",
                                    list.getData().get(position).getAmount(),
                                    list.getData().get(position).getOrder_type().equals("MONTHLY") ? "SIP" : "ONETIME");
                        }
                    }
                }
            }
        });
        if (type.equals("SIP") || type.equals("XSIP"))
            getSipDetails();
        return parentView;
    }

    public int getDuraDueDate(String due_date) {
        int b = -1;
        if (!due_date.equals("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
            Calendar today = Calendar.getInstance(),
                    dueDate = Calendar.getInstance();
            try {
                dueDate.setTime(sdf.parse(due_date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateTime todayJ = new DateTime(today), dueJ = new DateTime(dueDate);
            if (today.get(Calendar.DATE) == dueDate.get(Calendar.DATE)
                    && today.get(Calendar.MONTH) == dueDate.get(Calendar.MONTH)
                    && today.get(Calendar.YEAR) == dueDate.get(Calendar.YEAR)) {
                b = 0;
            } else if (dueDate.after(today)) {
                b = -1;
            } else {
                b = 0;
            }
        } else {
            b = -1;
        }
        return b;
    }

    SimpleDateFormat std = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en")),
            timeF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("en")),
            monthY = new SimpleDateFormat("MMM yyyy", new Locale("en"));

    double total = 0;
    String bseOrders = "";

    void investInThisFund(boolean isSip, final int pos, final String fundName, final String fundType, final String Amount, final String type) {
        total = 0;
        bseOrders = "";
        if (!isSip && fundType.equals("GOAL")) {
            for (int i = 0; i < list.getData().get(pos).getOrder_detail().size(); i++) {
                total += Double.parseDouble(list.getData().get(pos).getOrder_detail().get(i).getAmount());
                bseOrders += list.getData().get(pos).getOrder_detail().get(i).getBse_order_no();
            }
        } else if (!isSip && fundType.equals("FUND")) {
            total = Float.parseFloat(list.getData().get(pos).getAmount());
            bseOrders = list.getData().get(pos).getBse_order_no();
        }

        if (isSip) {
            if (fundType.equals("GOAL"))
                for (int i = 0; i < sipDueReports.getData().get(pos).getOrder_detail().size(); i++) {
                    bseOrders += "," + sipDueReports.getData().get(pos).getOrder_detail().get(i).getSip_registration_no();
                }
            else if (fundType.equals("FUND"))
                bseOrders = sipDueReports.getData().get(pos).getOrder_detail().get(0).getSip_registration_no();
            total = Float.parseFloat(Amount);
        }
        bseOrders = bseOrders.startsWith(",") ? bseOrders.substring(1, bseOrders.length()) : bseOrders;
        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        if(sipDueReports.getData().get(pos).getOrder_detail()!=null && sipDueReports.getData().get(pos).getOrder_detail().size()!=0 && sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_id()!=null)
          orderId = sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_id();
        String pay_type;
        if(typenew.equals("XSIP"))
        {
            pay_type= "Automatic Debit";
        }else{

           pay_type= "Manual";
        }
            builder.setMessage(Html.fromHtml("<p>" + contextd.getResources().getString(R.string.investFor)
                    + " <strong>" + (fundName) + "</strong>.</p>"+"<p>"
                    + contextd.getResources().getString(R.string.amount)
                    + "<strong><span style=\"color: #3fd37e;\">"
                    + "\t\t" + (fundType.equals("FUND") ? Common.currencyString(Amount, false) :
                    Common.currencyString(total + "", false)) + "</span></strong></p>"
                    + "<p>"+ contextd.getResources().getString(R.string.paymenttype)+"<strong>"+pay_type+"<strong>"));


            if(typenew.equals("XSIP")){
                builder.setPositiveButton(contextd.getResources().getString(R.string.Automatic), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int paramInt) {
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int paramInt) {
                        dialog.dismiss();

                    }
                });
            } else{
                builder.setPositiveButton(contextd.getResources().getString(R.string.pay_now), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int paramInt) {
                        Fragment frag = new PendingRedirectingFragment();
                        Bundle b = new Bundle();
                        if (InvestHistoryFragment.this.type.equals("SIP")) {
                            b.putString("userid", Common.getPreferences(contextd, "userID"));
                            b.putString("orders", bseOrders);
                            b.putString("amount", Amount);
                            b.putString("due_date", sipDueReports.getData().get(pos).getDue_date());
                            Log.d("due_date...",sipDueReports.getData().get(pos).getDue_date());
                            if (sipDueReports.getData().get(pos).getOrder_detail() != null && sipDueReports.getData().get(pos).getOrder_detail().size() != 0 && sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_type() != null && sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_type().equalsIgnoreCase("SIP"))
                                //b.putString("orderType", "SIP");
                                b.putString("orderType", "SIP");
                            else
                                b.putString("orderType", "XSIP");

                            //      b.putString("orderType", "SIP");
                        } else {
                            //get it from list
                            b.putString("userid", Common.getPreferences(contextd, "userID"));
                            b.putString("orders", bseOrders);
                            b.putString("amount", total + "");
                            b.putString("due_date", "");
                            b.putString("orderType", type);
                        }
                        frag.setArguments(b);

                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                        dialog.dismiss();
                    }
                });
            }
        if (InvestHistoryFragment.this.type.equals("SIP")) {
            if(sipDueReports.getData().get(pos).getOrder_detail()!=null && sipDueReports.getData().get(pos).getOrder_detail().size()!=0 && sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_type()!=null && sipDueReports.getData().get(pos).getOrder_detail().get(0).getOrder_type().equalsIgnoreCase("SIP"))
                //b.putString("orderType", "SIP");
                order_type = "SIP";
            else
                order_type = "XSIP";
            builder.setNegativeButton(contextd.getResources().getString(R.string.cancelSip), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int paramInt) {
                    dialog.dismiss();
                    cancelSIP();

                }
            });
        }
        else
        {
            builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int paramInt) {
                    dialog.dismiss();

                }
            });
        }
        builder.create().show();
    }


    void setScreenInfo() {
        if (type.equals("SIP")) {
            title.setText(contextd.getResources().getString(R.string.sipInstallments));
            select.setselected(1);
            past.setText(contextd.getResources().getString(R.string.past));
            pending.setText(contextd.getResources().getString(R.string.upcoming));
         //   redeemd.setText(contextd.getResources().getString(R.string.reedemed));
            //call SIP API here for upcoming
        } else if (type.equals("history")) {
            title.setText(contextd.getResources().getString(R.string.transactions));
            select.setselected(0);
            past.setText(contextd.getResources().getString(R.string.past));
            pending.setText(contextd.getResources().getString(R.string.pending));
        //    redeemd.setText(contextd.getResources().getString(R.string.reedemed));
            getInvestmentFundList("CONFIRM", "1");
            //call History API for past
        }
    }

    void getSipDetails() {
        Gson gson = new Gson();
        sipDueReports = gson.fromJson(getArguments().getString("sipInst"), SipDueReports.class);
        if (sipDueReports.getData().size() > 0) {
//            if (sipDueReports.getData().get(0).getOrder_detail().size() > 0) {
            sipAdapter = new SipDueReportAdapter(contextd, sipDueReports);
            orderdList.setAdapter(sipAdapter);
        } else {
            sipAdapter = null;
            orderdList.setAdapter(null);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.orderedPast:
                select.setselected(0);
                orderdList.setAdapter(null);
                getPastSip();
                if (type.equals("history"))
                    getInvestmentFundList("CONFIRM", "1");
                else
                    orderdList.setAdapter(pastSipAdapter);
                break;
            case R.id.orderedPending:
                select.setselected(1);
                orderdList.setAdapter(null);
//                getInvestmentFundList("PENDING", "0");
                if (type.equals("history"))
                    getInvestmentFundList("CONFIRM", "-1");
                else {
                    if (sipAdapter != null)
                        orderdList.setAdapter(sipAdapter);
                }
                break;
//            case R.id.orderedRedeem:
//                select.setselected(2);
//                orderdList.setAdapter(null);
//                break;
        }
    }

    void getInvestmentFundList(String status, String paymentStatus) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("ORDERLIST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("order_status", status);
        requestedServiceDataModel.putQurry("payment_status", paymentStatus);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getPastSip() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.PastSip);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("GETSIPLIST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        Gson gson = new Gson();
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                try {
                    obj = new JSONObject(json);
                    list = gson.fromJson(json, OrderedList.class);
           //         Log.d("List.....", String.valueOf(list));
                    if (list.getData().size() > 0) {
                        adapter = new InvestedAdapter(contextd, list, true);
                        orderdList.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.PastSip:
                pastSipList = gson.fromJson(json, PastSipList.class);
                pastSipAdapter=new PastSipAdapter(contextd,pastSipList);
                orderdList.setAdapter(pastSipAdapter);

                break;
            case ResponseType.CancelSIP:
           //     Log.e("cancelRes", json);
                whenSuccess(message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                Common.showToast(contextd, message);
                break;
            case ResponseType.CancelSIP:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    if (jsonObject.has("retry")) {
                        if (jsonObject.getString("retry").trim().equals("1")) {
                            retryLastRequest(message);
                        }
                    } else {
                        Common.showToast(contextd, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.PastSip:
                Common.showToast(contextd, message);
                break;
        }
    }

    void cancelSIP(String userid, String orderid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("order_type",order_type);
        requestedServiceDataModel.putQurry("orderid", orderid.trim());
        requestedServiceDataModel.putQurry("is_goal", "0");
        requestedServiceDataModel.putQurry("goal_id", "");
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


/*
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                Log.e("cancelRes", json);
                whenSuccess(message);
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    if (jsonObject.has("retry")) {
                        if (jsonObject.getString("retry").trim().equals("1")) {
                            retryLastRequest(message);
                        }
                    } else {
                        Common.showToast(contextd, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }*/

    private void whenSuccess(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.home), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                try {
                    ((MainActivity) activity).getBackToPortfolio();
                }catch (Exception e)
                {

                }
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void retryLastRequest(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), orderId);
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void cancelSIP() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(contextd.getResources().getString(R.string.cancelMonthlyBasedSIP));
        builder.setPositiveButton(contextd.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), orderId);
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
