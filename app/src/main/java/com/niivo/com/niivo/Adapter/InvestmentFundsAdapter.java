package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Model.GoalFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by Deepak
 */

public class InvestmentFundsAdapter extends RecyclerView.Adapter<InvestmentFundsAdapter.MyViewHolder> {

    private GoalFunds list;
    MyViewHolder holder;
    OnItemClickListener listener;
    int[] arrList;
    int count = 0;
    String lang = "";
    Context contextd;

    public InvestmentFundsAdapter(Context context, GoalFunds res, int[] cnt, OnItemClickListener listener) {
        this.list = res;
        this.listener = listener;
        this.arrList = cnt;
        this.contextd = context;
        lang = Common.getLanguage(contextd);
        for (int i = 0; i < arrList.length; i++)
            if (arrList[i] != 0)
                count++;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, oneY, twoY, fiveY, amntInvested;
        LinearLayout fundT;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.fundTitle);
            oneY = (TextView) itemView.findViewById(R.id.oneYrt);
            twoY = (TextView) itemView.findViewById(R.id.twoYrt);
            fiveY = (TextView) itemView.findViewById(R.id.fiveYrt);
            amntInvested = (TextView) itemView.findViewById(R.id.fundAmount);
            fundT = (LinearLayout) itemView.findViewById(R.id.lin_tio);
        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_investment_funds, parent, false);
        holder = new MyViewHolder(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.title.setText(lang.equals("en")
                ? list.getData().get(position).getSCHEME_NAME()
                : lang.equals("hi")
                ? list.getData().get(position).getFUNDS_HINDI()
                : lang.equals("gu")
                ? list.getData().get(position).getFUNDS_GUJARATI()
                : lang.equals("mr")
                ? list.getData().get(position).getFUNDS_MARATHI()
                : list.getData().get(position).getSCHEME_NAME());
        holder.amntInvested.setText(Common.currencyString(arrList[position] + "", false));
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);

        if (list.getData().get(position).getONE_Y_PERF().trim().equals("N.A.")) {
            holder.oneY.setText("N.A.");
        } else {
            holder.oneY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getONE_Y_PERF().trim())) + "%");
        }
        if (list.getData().get(position).getTWO_Y_PERF().trim().equals("N.A.")) {
            holder.twoY.setText("N.A.");
        } else {
            holder.twoY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getTWO_Y_PERF().trim())) + "%");
        }
        if (list.getData().get(position).getFIVE_Y_PERF().trim().equals("N.A.")) {
            holder.fiveY.setText("N.A.");
        } else {
            holder.fiveY.setText(String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getFIVE_Y_PERF().trim())) + "%");
        }
        holder.fundT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(list.getData().get(holder.getAdapterPosition()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.count;
    }

    public interface OnItemClickListener {
        void onItemClick(Object item);
    }
}
