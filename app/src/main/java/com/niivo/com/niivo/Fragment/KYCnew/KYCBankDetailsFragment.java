package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Model.ItemState;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCBankDetailsFragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    TextView continueBtn;
    AppCompatSpinner docAccountTypeSpinner;
    RightGravityTextInputLayout accountHolderNameRTL, accountNumberRTL, ifscRTL;
    TextInputEditText accountHolderNameET, accountNumberET, ifscET;
    ProgressBar ifscProgress;
    View rootView;
    //non ui vars

    ArrayList<ItemState> accountType;
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    JSONObject bankDetail;
    boolean isSubmitted = false;
    MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_bank_details, container, false);
            contextd = container.getContext();
            activity = (MainActivity) getActivity();
            continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
            docAccountTypeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.docAccountTypeSpinner);
            accountHolderNameRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.accountHolderNameRTL);
            accountNumberRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.accountNumberRTL);
            ifscRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.ifscRTL);
            accountHolderNameET = (TextInputEditText) rootView.findViewById(R.id.accountHolderNameET);
            accountHolderNameET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            accountNumberET = (TextInputEditText) rootView.findViewById(R.id.accountNumberET);
            ifscET = (TextInputEditText) rootView.findViewById(R.id.ifscET);
            ifscProgress = (ProgressBar) rootView.findViewById(R.id.ifscProgress);
            continueBtn.setOnClickListener(this);
            getAccountType();
            ifscET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    Log.e("IFSChasFocus", hasFocus + "");
                    if (!hasFocus) {
                        if (ifscET.getText().toString().length() > 0) {
                            ifscProgress.setVisibility(View.VISIBLE);
                            isValidIFSC(ifscET.getText().toString().trim());
                        }
                    } else
                        ifscProgress.setVisibility(View.GONE);
                }
            });
            ifscET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    accountHolderNameRTL.setError(null);
                    accountNumberRTL.setError(null);
                    ifscRTL.setError(null);
                    accountHolderNameRTL.setErrorEnabled(false);
                    accountNumberRTL.setErrorEnabled(false);
                    ifscRTL.setErrorEnabled(false);
                    ifscRTL.setHint(contextd.getResources().getString(R.string.ifscCode));
                    if (editable.toString().length() == 11) {
                        isValidIFSC(editable.toString());
                    }
                }
            });
            ifscProgress.setVisibility(View.GONE);
            if (getArguments().getBoolean("preRegistered"))
                setupPreRegistration();
        }
        return rootView;
    }

    boolean preRegistered = false;
    ItemUserDetail userDetail;
    int step = 0;

    void setupPreRegistration() {
        preRegistered = true;
        if(getArguments()!=null &&  getArguments().getSerializable("userDetail")!=null ) {
            userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
            if(userDetail.getData().getVerification_step()!=null) {
                step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
                if (step > 4) {
                    accountHolderNameET.setText(userDetail.getData().getAccount_holder_name());
                    accountNumberET.setText(userDetail.getData().getAccount_number());
                    ifscET.setText(userDetail.getData().getIfsc_code());
                    for (int i = 0; i < accountType.size(); i++)
                        if (accountType.get(i).getStateCode().equals(userDetail.getData().getAccount_type()))
                            docAccountTypeSpinner.setSelection(i);
                }
            }
        }
    }

    boolean validate() {
        boolean b = false;
        if (accountHolderNameET.getText().toString().trim().equals("")) {
            b = false;
            accountHolderNameRTL.setError("Required Field..!");
        } else if (accountNumberET.getText().toString().trim().equals("")) {
            b = false;
            accountNumberRTL.setError("Required Field..!");
        } else if (ifscET.getText().toString().trim().equals("")) {
            b = false;
            ifscRTL.setError("Required Field..!");
        } else
            b = true;
        return b;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isSubmitted", isSubmitted);
        if (getTargetFragment() != null)
            this.getTargetFragment().onActivityResult(
                    ResponseType.KYC_BankDetails,
                    isSubmitted
                            ? Activity.RESULT_OK
                            : Activity.RESULT_CANCELED,
                    i
            );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                isSubmitted = data.getBooleanExtra("isSubmitted", false);
                activity.onBackPressed();
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBtn:
                if (validate()) {
                    if (bankDetail == null) {
                        Common.showToast(contextd, contextd.getResources().getString(R.string.enter_valid_ifsc_code));
                    } else {
                        Fragment fragment = new KYCBankDetailsImageFragment();
                        Bundle b = new Bundle();
                        b.putString("account_type", accountType.get(docAccountTypeSpinner.getSelectedItemPosition()).getStateCode());
                        b.putString("account_holder_name", accountHolderNameET.getText().toString().trim());
                        b.putString("account_number", accountNumberET.getText().toString().trim());
                        b.putString("ifsc_code", ifscET.getText().toString().trim());
                        b.putString("ifscDetail", bankDetail.toString());
                        fragment.setArguments(b);
                        fragment.setTargetFragment(KYCBankDetailsFragment.this, 101);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, fragment).addToBackStack(null)
                                .commit();
                    }
                }
                break;
        }
    }


    void getAccountType() {
        accountType = new ArrayList<>();
        accountType.add(new ItemState("Saving Bank", "SB"));
        accountType.add(new ItemState("Current Bank", "CB"));
        accountType.add(new ItemState("NRE Account", "NE"));
        accountType.add(new ItemState("NRO Account", "NO"));
        docAccountTypeSpinner.setAdapter(new STateAdapter(contextd, accountType));
    }

    void isValidIFSC(String ifsc) {
        ifscProgress.setVisibility(View.VISIBLE);
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.IFSCVerify);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("ifsc_code", ifsc);
        requestedServiceDataModel.setWebServiceType("VERIFY-IFSC");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.IFSCVerify:
                //get user selfdeclaration
       //         Log.e("ifscVerify", json);
                ifscProgress.setVisibility(View.GONE);
                ifscRTL.setError(null);
                ifscRTL.setErrorEnabled(false);
                json = json.replace("á", "");
                Log.e("IFSC", json.toString());
                try {
                    bankDetail = new JSONObject(json);
                    ifscRTL.setHint(bankDetail.getJSONObject("data").getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.IFSCVerify:
      //          Log.e("ifsc", json);
                ifscProgress.setVisibility(View.GONE);
                bankDetail = null;
                ifscRTL.setError(message);
                ifscRTL.setErrorEnabled(true);
                break;
        }
}


    class STateAdapter extends ArrayAdapter<ItemState> {
        ArrayList<ItemState> list;
        Context mContext;

        public STateAdapter(@NonNull Context context, ArrayList<ItemState> resource) {
            super(context, 0, resource);
            this.list = resource;
            this.mContext = context;
            setDropDownViewResource(R.layout.item_drop_down);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            }
            TextView text = (TextView) convertView;
            text.setPadding(20, 20, 20, 12);
            text.setText(list.get(position).getStateName());
            return convertView;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            }
            TextView text = (TextView) convertView;
            text.setText(list.get(position).getStateName());
            return convertView;
        }

    }
}
