package com.niivo.com.niivo.Fragment.EMandate;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Activity.MandateRegistrationActivity;
import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 10/10/18.
 */

public class EmandateSelectAmountFragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    RadioGroup mandateRange;
    TextView confirmBtn;
    View rootView;
    //Non ui vars
    Context contextd;
    String mandatePrice = "", mendate_id = "", mandateAuthUrl = "";
    ItemUserDetail userDetail;
    RequestedServiceDataModel requestedServiceDataModel;
    ProgressDialog pd;
    MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_choose_mandet_amount, container, false);
            contextd = container.getContext();
            activity = (MainActivity) getActivity();
            mandateRange = (RadioGroup) rootView.findViewById(R.id.mandateRangeGroup);
            confirmBtn = (TextView) rootView.findViewById(R.id.confirmBtn);
            confirmBtn.setOnClickListener(this);
            userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
            pd = new ProgressDialog(contextd);
            pd.setMessage(contextd.getResources().getString(R.string.please_wait_for_e_auth));
            pd.setIndeterminate(false);
            pd.setCancelable(false);
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmBtn:
                if (mandateRange.getCheckedRadioButtonId() == -1) {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.select_amount_for_emandate));
                } else {
                    mandatePrice = mandateRange.getCheckedRadioButtonId() == R.id.tenThousand
                            ? "10000"
                            : mandateRange.getCheckedRadioButtonId() == R.id.twentyThousand
                            ? "20000"
                            : mandateRange.getCheckedRadioButtonId() == R.id.fiftyThousand
                            ? "50000"
                            : "100000";
                    sendMandateRequest();
                }
                break;
        }
    }

    void setUpHandler() {
        pd.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
                sendMandateAuthRequest();
            }
        }, 1000 * 60 *2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                getProfile(userDetail.getData().getUserid());
            } else {
                activity.goToEmandate();
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    void sendMandateRequest() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mandate.php");
        baseRequestData.setTag(ResponseType.MandateRequest);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("MENDATE-REGISTER");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("userid", userDetail.getData().getUserid());
        requestedServiceDataModel.putQurry("amount", mandatePrice);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void sendMandateAuthRequest() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mandate.php");
        baseRequestData.setTag(ResponseType.MandateAuthUrl);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("MENDATE-AUTHURL");
        requestedServiceDataModel.putQurry("userid", userDetail.getData().getUserid());
        requestedServiceDataModel.putQurry("mandate_id", mendate_id);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getProfile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.MandateRequest:
           //     Log.e("mandateRequest", json);
                try {
                    JSONObject obj = new JSONObject(json);
                    mendate_id = obj.getString("mandate_id");
                    Log.d("mandatePrice",mandatePrice);
                    Log.d("mendate_id",mendate_id);
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
                    prefs.edit().putString("mendate_id", mendate_id).commit();
                    prefs.edit().putString("mandatePrice",mandatePrice).commit();
                    setUpHandler();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.MandateAuthUrl: {
        //        Log.e("mandateAuthUrl", json);
                try {
                    JSONObject obj = new JSONObject(json);
                    if ((obj.getString("url") + "").contains("http")) {
                        mandateAuthUrl = obj.getString("url");

                        startActivityForResult(new Intent(contextd, MandateRegistrationActivity.class)
                                .putExtra("url", mandateAuthUrl), 101);
                    } else {
                        setUpHandler();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            break;
            case ResponseType.GetProofType:
           //     Log.e("getProfile", json);
                activity.userDetail = new Gson().fromJson(json, ItemUserDetail.class);
                activity.goToEmandate();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.MandateRequest:
             //   Log.e("mandateRequest", json);
                Common.showToast(getActivity() , message);
                break;
            case ResponseType.MandateAuthUrl:
           //     Log.e("mandateAuthUrl", json);
                Common.showToast(getActivity() , message);
                break;
            case ResponseType.GetProofType:
          //      Log.e("getProfile", json);
                break;
        }
    }
}
