package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.app.Activity;
import android.content.Context;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.HomeFragment;

import com.niivo.com.niivo.Fragment.OrderFragment.OrderFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class TabFragment extends Fragment implements ResponseDelegate  {

    Activity _activity;
    View parentView;
    TextView totalInvested, totalCurrent,button_money_invest,count;
    private RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    MainActivity activity;
    TabLayout tabLayout;
    ViewPager viewPager;
    String lang;
    Boolean flag=true;
    ImageView filter;
    SharedPreferences sharedpreferences;
    public static final String mypreference ="SortJson";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.fragment_mymoney2, null, false);
            contextd = container.getContext();
            activity = (MainActivity) getActivity();
            lang = Common.getLanguage(contextd);
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
            icon_navi.setVisibility(View.VISIBLE);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            textView.setText(contextd.getResources().getString(R.string.myMoney));
//            button_money_invest= (TextView) parentView.findViewById(R.id.button_money_invest);
            totalInvested = (TextView) parentView.findViewById(R.id.totalInvested);
            totalCurrent = (TextView) parentView.findViewById(R.id.totalCurrent);
            count = (TextView) parentView.findViewById(R.id.count);
            tabLayout = (TabLayout) parentView.findViewById(R.id.tabLayout);
            viewPager = (ViewPager) parentView.findViewById(R.id.viewPager);
//            button_money_invest.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                            .replace(R.id.frame_container, new HomeFragment()).addToBackStack(null).
//                            commit();
//                }
//            });
            getAllInvestmentFund();
            getInvestmentFundList();
            setUpTabs();
        }
        return parentView;
    }


    void setUpTabs() {
        for (int i = 0; i < 2; i++) {
            if (i == 0)
                tabLayout.addTab(tabLayout.newTab().setText(R.string.Investment));
            else if (i == 1)
                tabLayout.addTab(tabLayout.newTab().setText(R.string.RecentOrder));
        }
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setAdapter(new PagerAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

    }



    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    InvestedAmountFragment investedAmountFragment = new InvestedAmountFragment();
                    return investedAmountFragment;
                case 1:
                    OrderFragment orderFragment = new OrderFragment();
                    return orderFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            if (i == 0) {
                return contextd.getResources().getString(R.string.Investment);
            } else if (i == 1) {
                return contextd.getResources().getString(R.string.RecentOrder);
            } else
                return "";
        }
    }
    void getAllInvestmentFund(){
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.EquityList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("INVEST");
        //requestedServiceDataModel.putQurry("category","all");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getInvestmentFundList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("HOLDINGAMOUNT");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        Gson gson = new Gson();
        //   Log.e("jsonRes OnSuccess",json);

        switch (baseRequestData.getTag()) {

            case ResponseType.EquityList:
                try {
                    JSONObject odt = new JSONObject(json);
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
                    prefs.edit().putString("key", String.valueOf(odt)).commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
            case ResponseType.HoldingList:
                try {
                    JSONObject odt = new JSONObject(json);
                    //       Log.e("res_odt", json);
                    totalInvested.setText(Common.currencyString(odt.getJSONObject("data").getString("invest"), false));
                    totalCurrent.setText(Common.currencyString((odt.getJSONObject("data").getString("total_holdings").toString().equals("null")
                            ? "0" : odt.getJSONObject("data").getString("total_holdings")), false));
                    count.setText(odt.getJSONObject("data").getString("count"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }


    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        //    Log.e("jsonRes OnFailure",json);
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                Common.showToast(contextd, contextd.getResources().getString(R.string.noInvestMent));
                break;
        }
    }
}


