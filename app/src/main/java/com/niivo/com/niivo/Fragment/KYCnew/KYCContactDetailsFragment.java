package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.CountryListAdapter;
import com.niivo.com.niivo.Adapter.StatesListAdapter;
import com.niivo.com.niivo.Model.ItemCountryList;
import com.niivo.com.niivo.Model.ItemPanDetails;
import com.niivo.com.niivo.Model.ItemStatesList;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;


/**
 * Created by deepak on 24/9/18.
 */

public class KYCContactDetailsFragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    TextView indianBtn, nonIndianBtn, countryLbl,
            continueBtn, pa_countryLbl, corespondentAddLbl, permanentAddLbl;
    AppCompatSpinner countrySpinner, pa_countrySpinner, stateSpinner, pa_stateSpinner;
    CheckBox isPermanentAddress;
    LinearLayout parmanentLL, indianStatesLL, pa_indianSTatesLL;
    RightGravityTextInputLayout postalRTL, addressRTL, cityRTL, emailRTL, pa_postalRTL, pa_addressRTL, pa_cityRTL, stateRTL, pa_stateRTL;
    TextInputEditText postalET, addressET, emailET, stateET, pa_postalET, pa_addressET, pa_stateET, cityET, pa_cityET;

    View rootView;
    //Non ui vars
    boolean isIndian = true, isContactDetailsSubmitted = false, kycStatus = false, preRegistered = false;
    RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    ItemStatesList statesList;
    ItemCountryList countryList;
    String userId = "", doc_type = "", doc_typeId = "", frontImgRef = "", backImgRef = "" ,doc_proof_id = "";
    ItemPanDetails panDetails;

    ItemUserDetail userDetail;
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            postalRTL.setError(null);
            addressRTL.setError(null);
            cityRTL.setError(null);
            emailRTL.setError(null);
            pa_postalRTL.setError(null);
            pa_addressRTL.setError(null);
            pa_cityRTL.setError(null);
            stateRTL.setError(null);
            pa_stateRTL.setError(null);
            postalRTL.setErrorEnabled(false);
            addressRTL.setErrorEnabled(false);
            cityRTL.setErrorEnabled(false);
            emailRTL.setErrorEnabled(false);
            pa_postalRTL.setErrorEnabled(false);
            pa_addressRTL.setErrorEnabled(false);
            pa_cityRTL.setErrorEnabled(false);
            stateRTL.setErrorEnabled(false);
            pa_stateRTL.setErrorEnabled(false);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_contact_details, container, false);
            contextd = container.getContext();
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            textView.setText(R.string.contact_details);
            userId = Common.getPreferences(contextd, "userID");
            indianBtn = (TextView) rootView.findViewById(R.id.indianBtn);
            nonIndianBtn = (TextView) rootView.findViewById(R.id.nonIndianBtn);
            isPermanentAddress = (CheckBox) rootView.findViewById(R.id.isPermanentAddress);
            parmanentLL = (LinearLayout) rootView.findViewById(R.id.permanentAddressLL);
            countryLbl = (TextView) rootView.findViewById(R.id.countryLBL);
            countrySpinner = (AppCompatSpinner) rootView.findViewById(R.id.countrySpinner);
            pa_countryLbl = (TextView) rootView.findViewById(R.id.pa_countryLBL);
            pa_countrySpinner = (AppCompatSpinner) rootView.findViewById(R.id.pa_countrySpinner);
            corespondentAddLbl = (TextView) rootView.findViewById(R.id.corespondentAddLbl);
            permanentAddLbl = (TextView) rootView.findViewById(R.id.permanentAddLbl);
            continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
            stateSpinner = (AppCompatSpinner) rootView.findViewById(R.id.stateSpinner);
            pa_stateSpinner = (AppCompatSpinner) rootView.findViewById(R.id.pa_stateSpinner);
            indianStatesLL = (LinearLayout) rootView.findViewById(R.id.indianStatesLL);
            pa_indianSTatesLL = (LinearLayout) rootView.findViewById(R.id.pa_indianStatesLL);
            stateRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.stateRTL);
            pa_stateRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.pa_stateRTL);
            postalRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.postalRTL);
            addressRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.addressRTL);
            emailRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.emailRTL);
            pa_postalRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.pa_postalRTL);
            pa_addressRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.pa_addressRTL);
            postalET = (TextInputEditText) rootView.findViewById(R.id.postalET);
            addressET = (TextInputEditText) rootView.findViewById(R.id.addressET);
            addressET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            emailET = (TextInputEditText) rootView.findViewById(R.id.emailAddressET);

            emailET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

            stateET = (TextInputEditText) rootView.findViewById(R.id.stateET);
            pa_postalET = (TextInputEditText) rootView.findViewById(R.id.pa_postalET);
            pa_addressET = (TextInputEditText) rootView.findViewById(R.id.pa_addressET);
            pa_stateET = (TextInputEditText) rootView.findViewById(R.id.pa_stateET);
            cityRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.cityRTL);
            pa_cityRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.pa_cityRTL);
            cityET = (TextInputEditText) rootView.findViewById(R.id.cityET);
            cityET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            pa_cityET = (TextInputEditText) rootView.findViewById(R.id.pa_cityET);


            indianBtn.setOnClickListener(this);
            nonIndianBtn.setOnClickListener(this);
            continueBtn.setOnClickListener(this);
            postalET.addTextChangedListener(watcher);
            addressET.addTextChangedListener(watcher);
            emailET.addTextChangedListener(watcher);
            stateET.addTextChangedListener(watcher);
            pa_postalET.addTextChangedListener(watcher);
            pa_addressET.addTextChangedListener(watcher);
            pa_stateET.addTextChangedListener(watcher);
            cityET.addTextChangedListener(watcher);
            pa_cityET.addTextChangedListener(watcher);
            kycStatus = getArguments().getBoolean("kycStatus");
            if (kycStatus)
                panDetails = (ItemPanDetails) getArguments().getSerializable("panDetail");
            if (isIndian)
                setIndianDetails();
            else
                setNonIndianDetails();
            isPermanentAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    parmanentLL.setVisibility(isChecked ? View.GONE : View.VISIBLE);
                }
            });
            if (statesList == null)
                getStatesList();
            if (getArguments().getBoolean("preRegistered"))
                setUpPreRegistration();
        }
        return rootView;
    }

    int step = 0;

    void setUpPreRegistration() {
        preRegistered = true;
        userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
        step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
        if (step > 1) {
            postalET.setText(userDetail.getData().getCa_pincode());
            pa_postalET.setText(userDetail.getData().getPa_pincode());
            addressET.setText(userDetail.getData().getCa_address());
            pa_addressET.setText(userDetail.getData().getPa_address());
            isPermanentAddress.setChecked(userDetail.getData().getCa_address().equals(userDetail.getData().getPa_address()));
            emailET.setText(userDetail.getData().getEmail());
            cityET.setText(userDetail.getData().getCa_city());
            pa_cityET.setText(userDetail.getData().getPa_city());
            doc_typeId = userDetail.getData().getAddress_proof_type();
            frontImgRef = userDetail.getData().getFront_image();
            backImgRef = userDetail.getData().getBack_image();
            if (userDetail.getData().getResidency_type().equals("INDIAN")) {
                isIndian = true;
                indianBtn.setSelected(true);
                nonIndianBtn.setSelected(false);
                setIndianDetails();
                if (statesList == null)
                    getStatesList();
                else {
                    for (int i = 0; i < statesList.getData().size(); i++) {
                        if (statesList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getCa_state())) {
                            stateSpinner.setSelection(i);
                        }
                        if (statesList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getPa_state())) {
                            pa_stateSpinner.setSelection(i);
                        }
                    }
                }
            } else {
                indianBtn.setSelected(false);
                nonIndianBtn.setSelected(true);
                setNonIndianDetails();
                isIndian = false;
                stateET.setText(userDetail.getData().getCa_state());
                pa_stateET.setText(userDetail.getData().getPa_state());
                if (countryList == null)
                    getCountryList();
                else {
                    for (int i = 0; i < countryList.getData().size(); i++) {
                        if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getCa_country()))
                            countrySpinner.setSelection(i);
                        if (countryList.getData().get(i).getNIIVO_CODE().equals(userDetail.getData().getPa_country()))
                            pa_countrySpinner.setSelection(i);
                    }
                }
            }
        }
    }

    void setUpPanDetails() {
        if (kycStatus) {
            boolean permanentAvailable = true;
            postalET.setText(panDetails.getData().getPan_data().getAPP_COR_PINCD());
            postalET.setEnabled(false);
            String corAdd = panDetails.getData().getPan_data().getAPP_COR_ADD1()
                    + " " + panDetails.getData().getPan_data().getAPP_COR_ADD2()
                    + " " + panDetails.getData().getPan_data().getAPP_COR_ADD3();
            cityET.setText(panDetails.getData().getPan_data().getAPP_COR_CITY());
            cityET.setEnabled(false);
            if ((panDetails.getData().getPan_data().getAPP_PER_ADD1() + "").equals("")
                    || (panDetails.getData().getPan_data().getAPP_PER_ADD1() + "").equals("null")) {
                permanentAvailable = false;
            }

            String paAdd = permanentAvailable ? (panDetails.getData().getPan_data().getAPP_PER_ADD1()
                    + " " + panDetails.getData().getPan_data().getAPP_PER_ADD2()
                    + " " + panDetails.getData().getPan_data().getAPP_COR_ADD3())
                    : "null";
            addressET.setText(corAdd);
            addressET.setEnabled(false);
            if (corAdd.equals(paAdd)) {
                isPermanentAddress.setChecked(true);
            }
            if (permanentAvailable) {
                pa_postalET.setText(panDetails.getData().getPan_data().getAPP_PER_PINCD());
                pa_postalET.setEnabled(false);
                pa_addressET.setText(paAdd);
                pa_addressET.setEnabled(false);
                pa_cityET.setText(panDetails.getData().getPan_data().getAPP_PER_CITY());
                pa_cityET.setEnabled(false);
            }
            String email = ((panDetails.getData().getPan_data().getAPP_EMAIL() + "").equals("")
                    || (panDetails.getData().getPan_data().getAPP_EMAIL() + "").equals("null"))
                    ? "" : (panDetails.getData().getPan_data().getAPP_EMAIL() + "");
            emailET.setEnabled(email.equals(""));
            email = email.equals("") ? emailET.getText().toString() : email;
            emailET.setText(email.equals("") ? "" : email);
            if (panDetails.getData().getPan_data().getAPP_RES_STATUS().equals("R")) {
                indianBtn.setSelected(true);
                nonIndianBtn.setSelected(false);
                isIndian = true;
                setIndianDetails();
                for (int i = 0; i < statesList.getData().size(); i++) {
                    if (statesList.getData().get(i).getCVL_CODE().equalsIgnoreCase(panDetails.getData().getPan_data().getAPP_COR_STATE())) {
                        stateSpinner.setSelection(i);
                        stateSpinner.setEnabled(false);
                    }
                    if (permanentAvailable &&
                            (statesList.getData().get(i).getCVL_CODE().equalsIgnoreCase(panDetails.getData().getPan_data().getAPP_PER_STATE()))) {
                        pa_stateSpinner.setSelection(i);
                        pa_stateSpinner.setEnabled(false);
                    }
                }
            } else {
                isIndian = false;
                indianBtn.setSelected(false);
                nonIndianBtn.setSelected(true);
                setNonIndianDetails();
                if (countryList == null)
                    getCountryList();
                else {
                    for (int i = 0; i < countryList.getData().size(); i++) {
                        if (countryList.getData().get(i).getNIIVO_CODE().equals(panDetails.getData().getPan_data().getAPP_COR_CTRY())) {
                            countrySpinner.setSelection(i);
                            countrySpinner.setEnabled(false);
                        }
                        if (permanentAvailable
                                && (countryList.getData().get(i).getNIIVO_CODE().equals(panDetails.getData().getPan_data().getAPP_PER_CTRY()))) {
                            pa_countrySpinner.setSelection(i);
                            pa_countrySpinner.setEnabled(false);
                        }
                    }
                    //check for the state here
                }
            }
            indianBtn.setEnabled(false);
            nonIndianBtn.setEnabled(false);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.indianBtn:
                setIndianDetails();
                break;
            case R.id.nonIndianBtn:
                setNonIndianDetails();
                if (countryList == null)
                    getCountryList();
                break;
            case R.id.continueBtn:
                if (validate()) {
                    if (kycStatus) {
                        Intent i = new Intent();
                        i.putExtra("doc_type", panDetails.getData().getPan_data().getAPP_COR_ADD_PROOF());
                        i.putExtra("doc_typeId", panDetails.getData().getPan_data().getAPP_COR_ADD_PROOF());
                        i.putExtra("addressProofFrontRef", frontImgRef);
                        i.putExtra("addressProofBackRef", backImgRef);
                        onActivityResult(121, Activity.RESULT_OK, i);

                    } else {
                        Bundle b = new Bundle();
                        b.putString("userId", userId);
                        KYCContactDetailsImgFragment fragment = new KYCContactDetailsImgFragment();
                        fragment.setArguments(b);
                        fragment.setTargetFragment(KYCContactDetailsFragment.this, 121);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, fragment).addToBackStack(null)
                                .commit();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 121) {
            if (resultCode == Activity.RESULT_OK) {
                doc_type = data.getStringExtra("doc_type");
                doc_typeId = data.getStringExtra("doc_typeId");
                doc_proof_id = data.getStringExtra("doc_proof_id");
                frontImgRef = data.getStringExtra("addressProofFrontRef");
                backImgRef = data.getStringExtra("addressProofBackRef");
                setContactDetails();
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isIndian", isIndian);
        i.putExtra("isContactDetailsSubmitted", isContactDetailsSubmitted);
        this.getTargetFragment().onActivityResult(ResponseType.KYC_ContactDetails,
                isContactDetailsSubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED, i);
    }

    boolean validate() {
        boolean b = false;
        if (postalET.getText().toString().trim().equals("")) {
            b = false;
            postalRTL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (postalET.getText().toString().trim().length() < 6) {
            b = false;
            postalRTL.setError(contextd.getResources().getString(R.string.invalid_postal_code));
        } else if (addressET.getText().toString().trim().equals("")) {
            b = false;
            addressRTL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (isIndian && cityET.getText().toString().trim().equals("")) {
            b = false;
            cityRTL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (!isIndian && stateET.getText().toString().trim().equals("")) {
            b = false;
            stateRTL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (emailET.getText().toString().trim().equals("")) {
            emailRTL.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (!Common.isValidEmail(emailET.getText().toString().trim())) {
            b = false;
            emailRTL.setError(contextd.getResources().getString(R.string.invalidEmailAddress));
        } else if (!isPermanentAddress.isChecked()) {
            if (pa_postalET.getText().toString().trim().equals("")) {
                b = false;
                pa_postalRTL.setError(contextd.getResources().getString(R.string.requiredField));
            } else if (pa_postalET.getText().toString().trim().length() < 6) {
                b = false;
                pa_postalRTL.setError(contextd.getResources().getString(R.string.invalid_postal_code));
            } else if (pa_addressET.getText().toString().trim().equals("")) {
                b = false;
                pa_addressRTL.setError(contextd.getResources().getString(R.string.requiredField));
            } else if (!isIndian && pa_stateET.getText().toString().trim().equals("")) {
                b = false;
                pa_stateRTL.setError(contextd.getResources().getString(R.string.requiredField));
            } else if (!isIndian && pa_cityET.getText().toString().trim().equals("")) {
                b = false;
                pa_cityRTL.setError(contextd.getResources().getString(R.string.requiredField));
            } else {
                b = true;
            }
        } else {
            b = true;
        }
        return b;
    }


    void setIndianDetails() {
        isIndian = true;
        indianBtn.setSelected(true);
        nonIndianBtn.setSelected(false);
        countryLbl.setVisibility(View.GONE);
        countrySpinner.setVisibility(View.GONE);
        pa_countryLbl.setVisibility(View.GONE);
        pa_countrySpinner.setVisibility(View.GONE);
        stateRTL.setVisibility(View.GONE);
        pa_stateRTL.setVisibility(View.GONE);
        indianStatesLL.setVisibility(View.VISIBLE);
        pa_indianSTatesLL.setVisibility(View.VISIBLE);
        isPermanentAddress.setText(R.string.check_if_your_corespondent_address_is_same_as_permanent_address);
        corespondentAddLbl.setText(R.string.enter_corespondent_address_details);
        permanentAddLbl.setText(R.string.enter_permanent_address_details);
    }

    void setNonIndianDetails() {
        isIndian = false;
        indianBtn.setSelected(false);
        nonIndianBtn.setSelected(true);
        countryLbl.setVisibility(View.VISIBLE);
        countrySpinner.setVisibility(View.VISIBLE);
        pa_countryLbl.setVisibility(View.VISIBLE);
        pa_countrySpinner.setVisibility(View.VISIBLE);
        stateRTL.setVisibility(View.VISIBLE);
        pa_stateRTL.setVisibility(View.VISIBLE);
        indianStatesLL.setVisibility(View.GONE);
        pa_indianSTatesLL.setVisibility(View.GONE);
        corespondentAddLbl.setText(R.string.enter_oversease_address);
        permanentAddLbl.setText(R.string.enter_passport_address);
        isPermanentAddress.setText(R.string.check_if_oversease_address_is_not_same);
    }


    void getCountryList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetCountry);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("COUNTRYCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getStatesList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetStates);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("STATECODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void setContactDetails() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_ContactDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("ADDADDRESS");
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("residency_type", isIndian ? "INDIAN" : "OTHER");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("ca_pincode", postalET.getText().toString().trim());
        requestedServiceDataModel.putQurry("ca_address", addressET.getText().toString().trim());
        requestedServiceDataModel.putQurry("ca_city", cityET.getText().toString().trim());
        requestedServiceDataModel.putQurry("ca_state", isIndian
                ? statesList.getData().get(stateSpinner.getSelectedItemPosition()).getNIIVO_CODE()
                : stateET.getText().toString());
        requestedServiceDataModel.putQurry("ca_country", isIndian ? "101"//"India"
                : countryList.getData().get(countrySpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("pa_pincode", isPermanentAddress.isChecked()
                ? postalET.getText().toString().trim()
                : pa_postalET.getText().toString().trim());
        requestedServiceDataModel.putQurry("pa_address", isPermanentAddress.isChecked()
                ? addressET.getText().toString().trim()
                : pa_addressET.getText().toString().trim());
        requestedServiceDataModel.putQurry("pa_city", pa_cityET.getText().toString().trim());
        requestedServiceDataModel.putQurry("pa_state", isIndian
                ? isPermanentAddress.isChecked()
                ? statesList.getData().get(stateSpinner.getSelectedItemPosition()).getNIIVO_CODE()
                : statesList.getData().get(pa_stateSpinner.getSelectedItemPosition()).getNIIVO_CODE()
                : isPermanentAddress.isChecked()
                ? stateET.getText().toString().trim()
                : pa_stateET.getText().toString().trim());
        requestedServiceDataModel.putQurry("pa_country", isIndian ? "101"//"India"
                : isPermanentAddress.isChecked()
                ? countryList.getData().get(countrySpinner.getSelectedItemPosition()).getNIIVO_CODE()
                : countryList.getData().get(pa_countrySpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("email", emailET.getText().toString());
        if(isPermanentAddress.isChecked())
        {
            requestedServiceDataModel.putQurry("pa_same_as_ca", "0");
        }
        else
        {
            requestedServiceDataModel.putQurry("pa_same_as_ca", "1");
        }
        if(doc_proof_id != null)
        {
            requestedServiceDataModel.putQurry("doc_proof_id", doc_proof_id);
        }
        requestedServiceDataModel.putQurry("address_proof_type", doc_typeId);
        requestedServiceDataModel.putQurry("front_image", frontImgRef);
        requestedServiceDataModel.putQurry("back_image", backImgRef);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetStates:
            //    Log.e("getStates", json);
                statesList = new Gson().fromJson(json, ItemStatesList.class);
                stateSpinner.setAdapter(new StatesListAdapter(contextd, statesList));
                pa_stateSpinner.setAdapter(new StatesListAdapter(contextd, statesList));
                setUpPanDetails();
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.GetCountry:
          //      Log.e("getCountry", json);
                countryList = new Gson().fromJson(json, ItemCountryList.class);
                countrySpinner.setAdapter(new CountryListAdapter(contextd, countryList));
                pa_countrySpinner.setAdapter(new CountryListAdapter(contextd, countryList));
                if (preRegistered)
                    setUpPreRegistration();
                break;
            case ResponseType.KYC_ContactDetails:
            //    Log.e("setContactDetails", json);
                isContactDetailsSubmitted = true;
                getActivity().onBackPressed();
                Common.showToast(contextd, message);
                break;

        }

    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetStates:
                Log.e("getStates", json);
                break;
            case ResponseType.GetCountry:
                Log.e("getCountry", json);
                break;
            case ResponseType.KYC_ContactDetails:
                Log.e("setContactDetails", json);
                break;
        }
    }
}
