package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemOccuptaionList;
import com.niivo.com.niivo.R;

/**
 * Created by deepak on 25/9/18.
 */

public class OccupationListAdapter extends BaseAdapter {

    Context contextd;
    ItemOccuptaionList list;

    public OccupationListAdapter(Context contextd, ItemOccuptaionList list) {
        this.contextd = contextd;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OccupationHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            holder = new OccupationHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OccupationHolder) convertView.getTag();
        }
        holder.textView.setPadding(20, 20, 20, 12);
        holder.textView.setText(list.getData().get(position).getFRONTEND_OPTION_NAME());
        return convertView;
    }

    class OccupationHolder {
        TextView textView;

        public OccupationHolder(View v) {
            textView = (TextView) v;

        }
    }
}
