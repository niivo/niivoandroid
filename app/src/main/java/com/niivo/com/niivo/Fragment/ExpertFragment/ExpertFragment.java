package com.niivo.com.niivo.Fragment.ExpertFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;


/**
 * Created by andro on 22/5/17.
 */

public class ExpertFragment extends Fragment implements View.OnClickListener {
    Activity _activity;
    View parentView;
    RelativeLayout riskAverage, riskTaker, longTermReturn;


    Context contextd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        parentView = inflater.inflate(R.layout.fragment_expert, null, false);
        contextd = container.getContext();
       Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.expert));
        RelativeLayout rel_persist_return = (RelativeLayout) parentView.findViewById(R.id.rel_persist_return);
        riskAverage = (RelativeLayout) parentView.findViewById(R.id.expRiskAverage);
        riskTaker = (RelativeLayout) parentView.findViewById(R.id.expRiskTaker);
        longTermReturn = (RelativeLayout) parentView.findViewById(R.id.expLongTermReturn);
        riskAverage.setOnClickListener(this);
        riskTaker.setOnClickListener(this);
        longTermReturn.setOnClickListener(this);
        rel_persist_return.setOnClickListener(this);
        return parentView;
    }


    @Override
    public void onClick(View v) {
        Fragment frag = new ExpertDetailFragment();
        Bundle b = new Bundle();
        switch (v.getId()) {
            case R.id.rel_persist_return:
                b.putString("title", contextd.getResources().getString(R.string.mostPersistentReturns));
                b.putString("type", "MOSTPERSISTANCE");
                b.putInt("imgRes", R.drawable.returns);
                b.putString("imgDesc", contextd.getResources().getString(R.string.mostPersistentDesc));
                b.putString("from", "expert");
                break;
            case R.id.expRiskAverage:
                b.putString("title", contextd.getResources().getString(R.string.riskAverse));
                b.putString("type", "RISKAVERSE");
                b.putInt("imgRes", R.drawable.riskaverse);
                b.putString("imgDesc", contextd.getResources().getString(R.string.riskAverseDesc));
                b.putString("from", "expert");
                break;
            case R.id.expRiskTaker:
                b.putString("title", contextd.getResources().getString(R.string.riskTaker));
                b.putString("type", "RISKTAKER");
                b.putInt("imgRes", R.drawable.risktaker);
                b.putString("imgDesc", contextd.getResources().getString(R.string.riskAverseDesc));
                b.putString("from", "expert");
                break;
            case R.id.expLongTermReturn:
                b.putString("title", contextd.getResources().getString(R.string.longTermReturn));
                b.putString("type", "LONGTERM");
                b.putInt("imgRes", R.drawable.longtermreturnmodel);
                b.putString("imgDesc", contextd.getResources().getString(R.string.longTermReturnDesc));
                b.putString("from", "expert");
                break;
        }
        frag.setArguments(b);
        UiUtils.hideKeyboard(contextd);
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frag).addToBackStack(null).
                commit();
    }
}
