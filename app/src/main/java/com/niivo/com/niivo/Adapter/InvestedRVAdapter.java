package com.niivo.com.niivo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.niivo.com.niivo.Model.InvestmentList;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.Model.RecentOrder;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.BeginnerHelper.Investment;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 7/8/18.
 */
public class InvestedRVAdapter extends RecyclerView.Adapter {
    Context contextd;
    private Activity activity;
    //OrderedList list;
    InvestmentList list;
    RecentOrder lists;
    String lang = "";
    GetClick click;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));


    public InvestedRVAdapter(Context contextd, InvestmentList list) {
        this.contextd = contextd;
        this.list = list;
        this.lists = lists;
        lang = Common.getLanguage(contextd);
        this.list = removeDuplicates(list);
    }

    public InvestedRVAdapter setClick(GetClick click) {
        this.click = click;
        return this;
    }

    public static InvestmentList removeDuplicates(InvestmentList al) {
        for (int i = 0; i < al.getData().size(); i++) {
            for (int j = i + 1; j < al.getData().size(); j++) {
                if (al.getData().get(i).getFolio_no().equalsIgnoreCase(al.getData().get(j).getFolio_no())) {
                    if (!al.getData().get(i).isGroupEntries()) {
                        al.getData().get(i).setGroupEntries(true);
                        //    OrderedList.OrderedItem orderedItem = new OrderedList.OrderedItem();
                        InvestmentList.InvestmentItem investmentItem = new InvestmentList.InvestmentItem();
                        investmentItem.setAmount(al.getData().get(i).getAmount());
                        investmentItem.setDtdate(al.getData().get(i).getDtdate());
                        investmentItem.setNav(al.getData().get(i).getNav());
                        investmentItem.setQuantity(al.getData().get(i).getQuantity());
                        //  orderedItem.setQuantity(al.getData().get(i).getQuantity());


                        al.getData().get(i).getOrderedItemArrayListSub().add(investmentItem);
                    }
                    al.getData().get(i).getOrderedItemArrayListSub().add(al.getData().get(j));

                    al.getData().get(i).setAmount((Integer.parseInt(al.getData().get(i).getAmount()) + Integer.parseInt(al.getData().get(j).getAmount()) + ""));
                    al.getData().get(i).setWithdrawAmt((Float.parseFloat(al.getData().get(i).getWithdrawAmt()) + Float.parseFloat(al.getData().get(j).getWithdrawAmt()) + ""));
                    al.getData().get(i).setWithdrawQty((Float.parseFloat(al.getData().get(i).getWithdrawQty()) + Float.parseFloat(al.getData().get(j).getWithdrawQty()) + ""));
                    al.getData().get(i).setQuantity((Float.parseFloat(al.getData().get(i).getQuantity()) + Float.parseFloat(al.getData().get(j).getQuantity()) + ""));
                    al.getData().get(i).setCurrent_value((Float.parseFloat(al.getData().get(i).getCurrent_value()) + Float.parseFloat(al.getData().get(j).getCurrent_value()) + ""));
                    //  al.getData().get(i).get((Integer.parseInt(al.getData().get(i).getAmount())+Integer.parseInt(al.getData().get(j).getAmount())+""));
                    al.getData().remove(j);
                    j--;
                }
            }
        }
        return al;
    }


    ////    @Override
////    public int getItemViewType(int position)
////    {
////        if(position==0)
////            return LAYOUT_ONE;
////        else
////            return LAYOUT_TWO;
////    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
////        View rootView =null;
////        RecyclerView.ViewHolder viewHolder = null;
////        if(viewType==LAYOUT_ONE)
////        {
////            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invested_with_status,parent,false);
////            viewHolder = new InvestedHolder(rootView);
////        }
////        else
////        {
//            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_oreders,parent,false);
    //          viewHolder= new RecentHolder(rootView);
////        }
////        return viewHolder;
////    }
//
//    View rootView = LayoutInflater.from(contextd).inflate(R.layout.item_invested_with_status, parent, false);
//        return new InvestedHolder(rootView);
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder h, final int position) {
//      //  if(h.getItemViewType()== LAYOUT_ONE)
//      //  {
//        InvestedRVAdapter.InvestedHolder holder = ((InvestedRVAdapter.InvestedHolder) h);
//        holder.title.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
//                list.getData().get(position).getGoal_detail().getGoal_name() :
//                (lang.equals("en")
//                        ? list.getData().get(position).getFundname()
//                        : lang.equals("hi")
//                        ? list.getData().get(position).getFUNDS_HINDI()
//                        : lang.equals("gu")
//                        ? list.getData().get(position).getFUNDS_GUJARATI()
//                        : lang.equals("mr")
//                        ? list.getData().get(position).getFUNDS_MARATHI()
//                        : list.getData().get(position).getFundname()));
//        holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.stay, 0);
//        try {
//            Calendar ct = Calendar.getInstance();
//            ct.setTime(getSDF.parse(list.getData().get(position).getDtdate().trim()));
//            holder.invesdDate.setText(Common.getDateString(ct));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if (list.getData().get(position).getWithdrawQty().equals("0")) {
//            float total = Float.parseFloat(list.getTotal_holdings().trim()),
//                    amountInvested = Float.parseFloat(list.getData().get(position).getAmount());
//            holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested) / total) * 100) + "%");
//            holder.amntInvested.setText(Common.currencyString(list.getData().get(position).getAmount(), false));
//        } else {
//            float total = Float.parseFloat(list.getTotal_holdings().trim()),
//                    amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
//                    withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt());
//            holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested - withdrawAmnt) / total) * 100) + "%");
//            holder.amntInvested.setText(Common.currencyString(((int) (amountInvested - withdrawAmnt)) + "", false));
//        }
//
//        if (list.getData().get(position).getIs_goal().trim().equals("1")) {
//            int cnt = 0;
//            for (int i = 0; i < list.getData().get(position).getOrder_detail().size(); i++) {
//                try {
//                    cnt += Integer.parseInt(list.getData().get(position).getOrder_detail().get(i).getAmount().trim());
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                    cnt += 0;
//                }
//            }
//            float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) / (float) cnt) * 100;
//            holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret % 100)) + "%");
//            holder.amntInvested.setText(Common.currencyString(cnt + "", false));
//            if (list.getData().get(position).getGoal_detail().getFund_category().equals("1")) {
//                holder.ivLogo.setImageResource(R.drawable.retirementfund);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("2")) {
//                holder.ivLogo.setImageResource(R.drawable.childseducation);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("3")) {
//                holder.ivLogo.setImageResource(R.drawable.childswedding);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("4")) {
//                holder.ivLogo.setImageResource(R.drawable.vacation);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("5")) {
//                holder.ivLogo.setImageResource(R.drawable.buyingcar);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("6")) {
//                holder.ivLogo.setImageResource(R.drawable.buyinghouse);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("7")) {
//                holder.ivLogo.setImageResource(R.drawable.startingbusiness);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("8")) {
//                holder.ivLogo.setImageResource(R.drawable.startinganemergencyfund);
//            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("9")) {
//                holder.ivLogo.setImageResource(R.drawable.wealthcreation);
//            } else {
//                holder.ivLogo.setImageResource(R.drawable.rupee);
//            }
//            holder.type.setText(contextd.getResources().getString(R.string.goal).toUpperCase());
//        } else {
//            holder.ivLogo.setImageResource(R.drawable.rupee);
//            float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) - (Float.parseFloat(list.getData().get(position).getAmount().trim())));
//            ret = ret / (Float.parseFloat(list.getData().get(position).getAmount().trim())) * 100;
//            Log.e("return", ret + "");
//            if (list.getData().get(position).getIs_allotment().equals("0"))
//                holder.fundReturn.setText("0%");
//            else {
//                if (list.getData().get(position).getWithdrawQty().equals("0")) {
//                    holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
//                } else {
//                    float amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
//                            withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt()),
//                            wthdrawUnit = Float.parseFloat(list.getData().get(position).getWithdrawQty()),
//                            investedUnit = Float.parseFloat(list.getData().get(position).getQuantity()),
//                            nav = Float.parseFloat(list.getData().get(position).getNav().trim());
//                    ret = (nav * (investedUnit - wthdrawUnit)) - (amountInvested - withdrawAmnt);
//                    ret = ret / (amountInvested - withdrawAmnt) * 100;
//                    holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
//                }
//            }
//            holder.type.setText(contextd.getResources().getString(R.string.fund).toUpperCase());
//        }
//        holder.status.setVisibility(View.VISIBLE);
//        if (list.getData().get(position).getIs_allotment().equals("0")) {
//            holder.status.setText(contextd.getResources().getString(R.string.pending).toUpperCase());
//            holder.status.setBackground(contextd.getResources().getDrawable(R.drawable.pending_status_bg));
//        } else {
//            holder.status.setText(contextd.getResources().getString(R.string.confirmed));
//            holder.status.setBackground(contextd.getResources().getDrawable(R.drawable.confirmd_status_bg));
//        }
//
//        h.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (click != null)
//                    click.onItemClick(position);
//            }
//        });
//
//        if (list.getData().get(position).getOrder_type().equalsIgnoreCase("SIP") || list.getData().get(position).getOrder_type().equalsIgnoreCase("XSIP")) {
//            holder.tv_status.setText("Registered On");
//            try {
//                Calendar ct = Calendar.getInstance();
//                ct.setTime(getSDF.parse(list.getData().get(position).getSip_registration_date().trim()));
//                holder.invesdDate.setText(Common.getDateString(ct));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            if(list.getData().get(position).getIs_allotment().equalsIgnoreCase("0") && list.getData().get(position).getPayment_status().equalsIgnoreCase("0"))
//            {
//                holder.tv_status.setText("Registered On");
//            }
//            else
//            {
//                holder.tv_status.setText("Invested on");
//            }
//
//        } else {
//            holder.tv_status.setText("Invested on");
//        }
//    }
////    else {
////            RecentHolder holder = (RecentHolder) h;
////            holder.fundTitle.setText(lists.getData().get(position).getFundname().trim());
////            holder.dateInvest.setText(lists.getData().get(position).getInvestdate().trim());
////            holder.amount.setText(lists.getData().get(position).getInvestamount().trim());
////            holder.status.setText(lists.getData().get(position).getStatus().trim());
//
//
//  //  }
//
//    @Override
//    public int getItemCount() {
//        return list.getData().size();
//    }
//
//    class InvestedHolder extends RecyclerView.ViewHolder {
//        TextView title, amntInvested, type, percentPortFolio, fundReturn, status, invesdDate, tv_status;
//        ImageView ivLogo;
//        public InvestedHolder(View itemView) {
//            super(itemView);
//            title = (TextView) itemView.findViewById(R.id.fund_title);
//            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
//            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
//            status = (TextView) itemView.findViewById(R.id.payStatus);
//            type = (TextView) itemView.findViewById(R.id.type);
//            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
//            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
//            invesdDate = (TextView) itemView.findViewById(R.id.investedDateTV);
//            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
//        }
//    }
//
////    class RecentHolder extends RecyclerView.ViewHolder {
////        TextView fundTitle,status, dateInvest, amount;
////
////        public RecentHolder(View itemView) {
////            super(itemView);
////            fundTitle = (TextView) itemView.findViewById(R.id.fund_title);
////            status = (TextView) itemView.findViewById(R.id.payStatus);
////            dateInvest = (TextView) itemView.findViewById(R.id.investedDateTV);
////            amount = (TextView)itemView.findViewById(R.id.tv_amount);
////        }
////    }
//
    public interface GetClick {
        void onItemClick(int pos);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invested_with_status, parent, false);
        viewHolder = new InvestViewHolder(view);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof InvestViewHolder) {
            InvestViewHolder investholder = (InvestViewHolder) holder;
            investholder.title.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
                    list.getData().get(position).getGoal_detail().getGoal_name() :
                    (lang.equals("en")
                            ? list.getData().get(position).getFundname()
                            : lang.equals("hi")
                            ? list.getData().get(position).getFUNDS_HINDI()
                            : lang.equals("gu")
                            ? list.getData().get(position).getFUNDS_GUJARATI()
                            : lang.equals("mr")
                            ? list.getData().get(position).getFUNDS_MARATHI()
                            : list.getData().get(position).getFundname()));
            investholder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.stay, 0);
            try {
                Calendar ct = Calendar.getInstance();
                ct.setTime(getSDF.parse(list.getData().get(position).getDtdate().trim()));
                investholder.invesdDate.setText(Common.getDateString(ct));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (list.getData().get(position).getWithdrawQty().equals("0")) {
                float total = Float.parseFloat(list.getTotal_holdings().trim()),
                        amountInvested = Float.parseFloat(list.getData().get(position).getAmount());
                investholder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested) / total) * 100) + "%");
                investholder.amntInvested.setText(Common.currencyString(list.getData().get(position).getAmount(), false));
            } else {
                float total = Float.parseFloat(list.getTotal_holdings().trim()),
                        amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                        withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt());
                investholder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested - withdrawAmnt) / total) * 100) + "%");
                investholder.amntInvested.setText(Common.currencyString(((int) (amountInvested - withdrawAmnt)) + "", false));
            }

            if (list.getData().get(position).getIs_goal().trim().equals("1")) {
                int cnt = 0;
                for (int i = 0; i < list.getData().get(position).getOrder_detail().size(); i++) {
                    try {
                        cnt += Integer.parseInt(list.getData().get(position).getOrder_detail().get(i).getAmount().trim());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        cnt += 0;
                    }
                }
                float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) / (float) cnt) * 100;
                investholder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret % 100)) + "%");
                investholder.amntInvested.setText(Common.currencyString(cnt + "", false));
                if (list.getData().get(position).getGoal_detail().getFund_category().equals("1")) {
                    investholder.ivLogo.setImageResource(R.drawable.retirementfund);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("2")) {
                    investholder.ivLogo.setImageResource(R.drawable.childseducation);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("3")) {
                    investholder.ivLogo.setImageResource(R.drawable.childswedding);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("4")) {
                    investholder.ivLogo.setImageResource(R.drawable.vacation);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("5")) {
                    investholder.ivLogo.setImageResource(R.drawable.buyingcar);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("6")) {
                    investholder.ivLogo.setImageResource(R.drawable.buyinghouse);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("7")) {
                    investholder.ivLogo.setImageResource(R.drawable.startingbusiness);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("8")) {
                    investholder.ivLogo.setImageResource(R.drawable.startinganemergencyfund);
                } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("9")) {
                    investholder.ivLogo.setImageResource(R.drawable.wealthcreation);
                } else {
                    investholder.ivLogo.setImageResource(R.drawable.rupee);
                }
                investholder.type.setText(contextd.getResources().getString(R.string.goal).toUpperCase());
            } else {
                investholder.ivLogo.setImageResource(R.drawable.rupee);
                float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) - (Float.parseFloat(list.getData().get(position).getAmount().trim())));
                ret = ret / (Float.parseFloat(list.getData().get(position).getAmount().trim())) * 100;
                Log.e("return", ret + "");
                if (list.getData().get(position).getIs_allotment().equals("0"))
                    investholder.fundReturn.setText("0%");
                else {
                    if (list.getData().get(position).getWithdrawQty().equals("0")) {
                        investholder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                    } else {
                        float amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                                withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt()),
                                wthdrawUnit = Float.parseFloat(list.getData().get(position).getWithdrawQty()),
                                investedUnit = Float.parseFloat(list.getData().get(position).getQuantity()),
                                nav = Float.parseFloat(list.getData().get(position).getNav().trim());
                        ret = (nav * (investedUnit - wthdrawUnit)) - (amountInvested - withdrawAmnt);
                        ret = ret / (amountInvested - withdrawAmnt) * 100;
                        investholder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                    }
                }
                investholder.type.setText((list.getData().get(position).getOrder_type().trim()));
            }
            investholder.status.setVisibility(View.VISIBLE);
            if (list.getData().get(position).getIs_allotment().equals("0")) {
                investholder.status.setText(contextd.getResources().getString(R.string.pending).toUpperCase());
                investholder.status.setBackground(contextd.getResources().getDrawable(R.drawable.pending_status_bg));
            } else {
                investholder.status.setText(contextd.getResources().getString(R.string.confirmed));
                investholder.status.setBackground(contextd.getResources().getDrawable(R.drawable.confirmd_status_bg));
            }

            investholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click != null)
                        click.onItemClick(position);
                }
            });

            if (list.getData().get(position).getOrder_type().equalsIgnoreCase("SIP") || list.getData().get(position).getOrder_type().equalsIgnoreCase("XSIP")) {
                investholder.tv_status.setText("Registered On");
                try {
                    Calendar ct = Calendar.getInstance();
                    ct.setTime(getSDF.parse(list.getData().get(position).getSip_registration_date().trim()));
                    investholder.invesdDate.setText(Common.getDateString(ct));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (list.getData().get(position).getIs_allotment().equalsIgnoreCase("0") && list.getData().get(position).getPayment_status().equalsIgnoreCase("0")) {
                    investholder.tv_status.setText("Registered On");
                } else {
                    investholder.tv_status.setText("Invested on");
                }

            } else {
                investholder.tv_status.setText("Invested on");
            }

        }
    }


    @Override
    public int getItemCount() {
        return list.getData().size();
    }

    private class InvestViewHolder extends RecyclerView.ViewHolder {
        TextView title, amntInvested, type, percentPortFolio, fundReturn, status, invesdDate, tv_status;
        ImageView ivLogo;

        public InvestViewHolder(View view) {
            super(view);
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            status = (TextView) itemView.findViewById(R.id.payStatus);
            type = (TextView) itemView.findViewById(R.id.type);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
            invesdDate = (TextView) itemView.findViewById(R.id.investedDateTV);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);

        }
    }
}

