package com.niivo.com.niivo.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.UserProfileAdapter;
import com.niivo.com.niivo.Model.ProfileUser;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UserProfileFragment extends Fragment implements ResponseDelegate {
    Context contextd;
    ListView my_profile_list;
    ImageButton back;
    ProfileUser profileUser;
    ImageView imageView;

    String lang = "";
    UserProfileAdapter adapter;
    RequestedServiceDataModel requestedServiceDataModel;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    MainActivity activity;

    LinearLayout layout;
     String Kycstatuss;

   TextView name,city,state,country,email,phone,occupation,dob,accountHolderNameET,accountNumberET,ifscET,acctype,emptyprofile;
  boolean flag=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.profilelayout, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        imageView=(ImageView)toolbar.findViewById(R.id.profilepic);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.profile).toUpperCase());
        activity = (MainActivity) getActivity();
        name=(TextView)rootView.findViewById(R.id.tv_name);
        city=(TextView)rootView.findViewById(R.id.tv_city);
        state=(TextView)rootView.findViewById(R.id.tv_state);
        country=(TextView)rootView.findViewById(R.id.tv_country);
        email=(TextView)rootView.findViewById(R.id.tv_email);
        phone=(TextView)rootView.findViewById(R.id.tv_phone);
        occupation=(TextView)rootView.findViewById(R.id.tv_occupation);
        dob=(TextView)rootView.findViewById(R.id.tv_dob);
        accountHolderNameET =(TextView)rootView.findViewById(R.id.accountHolderNameET);
        accountNumberET=(TextView)rootView.findViewById(R.id.accountNumberET);
        ifscET=(TextView)rootView.findViewById(R.id.ifscET);
        acctype=(TextView)rootView.findViewById(R.id.acctype);
       // update=(TextView)rootView.findViewById(R.id.update);
        layout=(LinearLayout)rootView.findViewById(R.id.layout);
          //      my_profile_list =(ListView)rootView.findViewById(R.id.my_Recent_list);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        Kycstatuss = prefs.getString("Kycstatuss", null);
        emptyprofile = (TextView) rootView.findViewById(R.id.emptyprofile);
        Log.d("Kycstatuss12",Kycstatuss);
        if(Kycstatuss.equals("SUBMITTED")){
            emptyprofile.setText(contextd.getResources().getString(R.string.kycSubmit));
           // layout.setVisibility(View.GONE);
        }else if(Kycstatuss.equals("COMPLETE")) {
            emptyprofile.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
        }else {
            emptyprofile.setText(contextd.getResources().getString(R.string.profilemsg));
        }
        getUserProfile();

//                update.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(Intent.ACTION_SENDTO);
//                        //  intent.setData(Uri.parse("mailto:anshuman.saxena@claroinvestments.com"));
//                        intent.setData(Uri.parse("mailto:support@niivo.in"));
//                        intent.putExtra(Intent.EXTRA_SUBJECT, "Niivo Support");
//                        startActivity(Intent.createChooser(intent, "Send Email Using..!"));
//                    }
//                });
                return rootView;
    }

    private void getUserProfile() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.Profile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("PROFILE");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
    //    Log.e("UserProfileFragment onSuccess", json);
       // emptyprofile.setVisibility(View.GONE);
       // layout.setVisibility(View.VISIBLE);
        switch (baseRequestData.getTag()) {
            case ResponseType.Profile:
                Gson gson = new Gson();
                JSONObject object = new JSONObject(json);

                name.setText(object.getJSONArray("data").getJSONObject(0).getString("name"));
                city.setText(object.getJSONArray("data").getJSONObject(0).getString("ca_city"));
                state.setText(object.getJSONArray("data").getJSONObject(0).getString("ca_state"));
                country.setText(object.getJSONArray("data").getJSONObject(0).getString("ca_country"));
                email.setText(object.getJSONArray("data").getJSONObject(0).getString("email"));
                phone.setText(object.getJSONArray("data").getJSONObject(0).getString("mobile"));
                occupation.setText(object.getJSONArray("data").getJSONObject(0).getString("occupation_string"));
                accountHolderNameET.setText(object.getJSONArray("data").getJSONObject(0).getString("account_holder_name"));
                accountNumberET.setText(object.getJSONArray("data").getJSONObject(0).getString("account_number"));
                ifscET.setText(object.getJSONArray("data").getJSONObject(0).getString("ifsc_code"));
                acctype.setText(object.getJSONArray("data").getJSONObject(0).getString("account_type_string"));
                Calendar ct = Calendar.getInstance();
                try {
                    ct.setTime(sdf.parse(object.getJSONArray("data").getJSONObject(0).getString("dob")));
                    dob.setText(Common.getDateString(ct));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
     //   Log.e("UserProfileFragment onFailure" , json);

        switch (baseRequestData.getTag()) {
            case ResponseType.Profile:
                emptyprofile.setVisibility(View.VISIBLE);
                //Common.showToast(contextd, contextd.getResources().getString(R.string.noprofile));
                break;
        }
    }
}
