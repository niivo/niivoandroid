package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 8/6/17.
 */

public class ItemInvested {
    String type, title;
    int logo;
    //add more accordingly


    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public ItemInvested(String type, String title, int logo) {
        this.type = type;
        this.title = title;
        this.logo = logo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
