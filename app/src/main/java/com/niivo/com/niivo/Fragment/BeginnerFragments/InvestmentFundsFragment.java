package com.niivo.com.niivo.Fragment.BeginnerFragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.InvestmentFundsAdapter;
import com.niivo.com.niivo.Fragment.InvestHelper.RedirectringFragment;
import com.niivo.com.niivo.Fragment.InvestHelper.SelectTimeFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCRegistrationFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCStatusFragment;
import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.Model.GoalFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.CartHelper.CartModle;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static org.acra.ACRA.log;


/**
 * Created by Deepak.
 */

public class InvestmentFundsFragment extends Fragment implements ResponseDelegate {
    Activity _activity;
    RecyclerView recyclerView;
    InvestmentFundsAdapter adapter;
    AlertDialog b;
    TextView addToCart;
    TextView investHeader,investgoal;
    //Non ui Vars
    boolean cartAdded = false;
    Context contextd;
    int cnt = 0;
    int[] arr;
    GoalFunds funds;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", new Locale("en")),
            monthY = new SimpleDateFormat("MMM yyyy", new Locale("en")),
            cartSDF = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    String schemesJson = "";
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    String sipDate = "", cartItemId = "", lang = "";
    boolean isCams = false;
    boolean flag=false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        View parentView = inflater.inflate(R.layout.fragment_investment_funds, null, false);
        contextd = container.getContext();
        activity = (MainActivity) getActivity();
        recyclerView = (RecyclerView) parentView.findViewById(R.id.recfycle_inter);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.funds));
        setListData();
        lang = Common.getLanguage(contextd);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        investgoal= (TextView) parentView.findViewById(R.id.investgoal);
        String goalname = getArguments().getString("goalNameAlt");
        investgoal.setText("For your goal "+goalname+""+", we recommend following fund. Please check on invest to confirm the investment.");
        TextView button_investfunds = (TextView) parentView.findViewById(R.id.button_investfunds);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        final String kycstatus = prefs.getString("kycstatus", null);
        button_investfunds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kycstatus.equals("COMPLETE")){
                    Log.d("kycstatussss11",kycstatus);
                    showAcceptTerms();
                }else if(kycstatus.equals("SUBMITTED")) {
                    Log.d("kycstatussss2",kycstatus);
                    showSubmitPopup();
                }else {
                    Log.d("kycstatussss3",kycstatus);
                    showAccountSetup();
                }
            }
        });
        addToCart = (TextView) parentView.findViewById(R.id.button_addToCart);
        investHeader = (TextView) parentView.findViewById(R.id.investHeader);
        investHeader.setText(getArguments().getString("orderType").equals("ONETIME") ? contextd.getResources().getString(R.string.investmentAmount)
                : contextd.getResources().getString(R.string.monthlyAmount));
        addToCart.setText(cartAdded ? contextd.getResources().getString(R.string.remove) : contextd.getResources().getString(R.string.addToCart));
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartAdded) {
                    if (!cartItemId.equals(""))
                        removeFromCart(Common.getPreferences(contextd, "userID"), cartItemId);
                } else {
                    addGoal();
                }
            }
        });

        return parentView;
    }
    void showSubmitPopup(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.kycsubmitpopup, null);
        dialogBuilder.setView(dialogView);
        TextView home = (TextView) dialogView.findViewById(R.id.invest_cancel);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }


    String cartSchemeCode = "";

    private void addGoal() {
        CartModle cart;
        JSONArray jsonArr = new JSONArray();
        for (int i = 0; i < cnt; i++)
            if (arr[i] != 0) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("scheme_code", funds.getData().get(i).getSTARMF_SCHEME_CODE());
                    obj.put("scheme_amount", arr[i] + "");
                    obj.put("scheme_nav", funds.getData().get(i).getNAV().toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArr.put(obj);
            }

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Log.e("before", std.format(calendar.getTime()));
        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(new Locale("en"));
        DateTime from = dtf.parseDateTime(std.format(calendar.getTime()).toString()), to;
        calendar.set(Calendar.YEAR, Integer.parseInt(getArguments().getString("targetYear")));
        calendar.add(Calendar.MONTH, -1);
        Log.e("after", std.format(calendar.getTime()));
        to = dtf.parseDateTime(std.format(calendar.getTime()).toString());
        int noOfMonth = (Months.monthsBetween(from, to).getMonths());
        cartSchemeCode = System.currentTimeMillis() + "Goal";


        cart = new CartModle();
        cart.setSchem_json(jsonArr.toString());
        cart.setAdded_date(cartSDF.format(Calendar.getInstance().getTime()));
        if (!getArguments().getString("orderType").equals("ONETIME"))
            cart.setNumberofmonths(noOfMonth + "");
        cart.setTarget_year(getArguments().getString("targetYear"));
        cart.setOrderType(getArguments().getString("orderType"));
        cart.setGoal_name(getArguments().getString("goalName"));
        cart.setGoal_amount(getArguments().getString("amountAmount"));
        cart.setGoal_cate_id(getArguments().getString("cateId"));

        addGoalToCart(Common.getPreferences(contextd, "userID"), cart);
    }

    private void setListData() {
        arr = getArguments().getIntArray("fundArr");
        Log.d("array data....", String.valueOf(arr));
        for(int i=0;i<arr.length;i++);
        cnt = 0;
        Gson gson = new Gson();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        String json = prefs.getString("response", null);
        Log.e("json_json_json", json);
       // funds = gson.fromJson(getArguments().getString("response"), GoalFunds.class);
        funds = gson.fromJson(json, GoalFunds.class);
//        Log.d("response",getArguments().getString("response"));
        if (!getArguments().getString("orderType").equals("ONETIME"))
            sipDate = getSipDate(funds, arr.length);
        Log.e("SipDate", sipDate);
        Log.e("arr.length...", String.valueOf(arr.length));
        Log.e("arr.funds...", String.valueOf(funds));

        JSONArray jsonArr = new JSONArray();
        for (int i = 0; i < arr.length; i++) {
            Log.d("i-----", String.valueOf(i));
            Log.d("array value----",String.valueOf(arr));
            if (arr[i] != 0) {
                cnt++;
                JSONObject obj = new JSONObject();
                try {
                    obj.put("scheme_name", funds.getData().get(i).getSCHEME_NAME().toString());
                    obj.put("scheme_code", funds.getData().get(i).getSTARMF_SCHEME_CODE());
                    obj.put("scheme_amount", arr[i] + "");
                    obj.put("nav", funds.getData().get(i).getNAV().toString().trim());
                    obj.put("scheme_qty", "0");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArr.put(obj);
            }
                    if (funds.getData().get(i).getSTARMF_RTA_CODE().equals("CAMS"))
                        isCams = true;

        }
        Log.e("NumberOfFunds", cnt + "");
        schemesJson = jsonArr.toString();
        adapter = new InvestmentFundsAdapter(contextd, funds, arr, new InvestmentFundsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Object item) {
                GoalFunds.FundItem obj = (GoalFunds.FundItem) item;
                Bundle b = new Bundle();
                Fragment frag = new FundDetailFragment();
                b.putString("schemeCode", obj.getSTARMF_SCHEME_CODE());
                b.putString("schemeName", lang.equals("en")
                        ? obj.getSCHEME_NAME()
                        : lang.equals("hi")
                        ? obj.getFUNDS_HINDI()
                        : lang.equals("gu")
                        ? obj.getFUNDS_GUJARATI()
                        : lang.equals("mr")
                        ? obj.getFUNDS_MARATHI()
                        : obj.getSCHEME_NAME());
                b.putString("fundName", obj.getFUND_NAME());
                b.putString("minimumAmnt", obj.getSTARMF_MIN_PUR_AMT());

                //  b.putString("minimumAmnt", obj.getMIN_INIT());
                b.putString("nav", obj.getNAV());
                b.putString("isin", obj.getISIN());
                b.putString("isSIP", obj.getSTARMF_SIP_FLAG().trim());
                b.putString("from", getArguments().getString("from"));
                b.putString("minimumPurchase", obj.getSTARMF_MIN_PUR_AMT());
                Log.d("getSTARMF_MIN_PUR_AMT..",obj.getSTARMF_MIN_PUR_AMT());
                b.putString("1MonthPref", obj.getONE_M_PERF());
                b.putString("3MonthPref", obj.getTHREE_M_PERF());
                b.putString("6MonthPref", obj.getSIX_M_PERF());
                b.putString("1YearPref", obj.getONE_Y_PERF());
                b.putString("2YearPref", obj.getTWO_Y_PERF());
                b.putString("3YearPref", obj.getTHREE_Y_PERF());
                b.putString("5YearPref", obj.getFIVE_Y_PERF());
                b.putString("7YearPref", obj.getSEVEN_Y_PERF());
                b.putString("10YearPref", obj.getTEN_Y_PERF());
                b.putString("fundClass", obj.getFUND_CLASS());
                b.putString("maincategory",obj.getMAIN_CATEGORY());
                b.putString("subcategory", obj.getSUB_CATEGORY());
                b.putString("fundCategory", obj.getFUND_CATEGORY());
                b.putString("fundCategorization", obj.getFUND_CATEGORIZATION());
                b.putString("fundExpense", obj.getEXPENSE_RATIO());
                b.putString("fundDesciption", obj.getDESCRIPTION());
                b.putString("minimumSIPAmnt", obj.getSIP_MIN_INSTALLMENT_AMT() == null ? "" : obj.getSIP_MIN_INSTALLMENT_AMT());


                Log.e("Send", "NAV:" + obj.getNAV() +
                        "\nISIN:" + obj.getISIN() +
                        "\nisSIP:" + obj.getSTARMF_SIP_FLAG().trim() +
                        "\nStarMFSchemeCode:" + obj.getSTARMF_SCHEME_CODE() +
                        "\nSchemeName:" + obj.getSCHEME_NAME() +
                        "\nSchemeCode:" + obj.getSTARMF_SCHEME_CODE() +
                        "\nMinimumAmnt:" + obj.getSTARMF_MIN_PUR_AMT());
                      //  "\nMinimumAmnt:" + obj.getMIN_INIT());

                frag.setArguments(b);
                UiUtils.hideKeyboard(contextd);
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, frag).addToBackStack(null).
                        commit();
            }
        });
    }

    String getSipDate(GoalFunds funds, int len) {
        String cateIdd=getArguments().getString("cateId");
        Log.d("cateiddddddddddd....",cateIdd);
        //if(cateIdd == "2" || cateIdd == "8" || cateIdd == "9" ){
        if(cateIdd == "1" || cateIdd == "3" || cateIdd == "4" || cateIdd == "5" || cateIdd == "6" || cateIdd == "7"){
        Log.d("len.........", String.valueOf(len));
        Log.d("funds.........", String.valueOf(funds));
        String date = "";
        String tmp = funds.getData().get(0).getSIP_DATES(),
                tmp2 = funds.getData().get(1).getSIP_DATES(),
                tmp3 = funds.getData().get(2).getSIP_DATES();
        Log.d("tmp.........", String.valueOf(funds.getData().get(0).getSIP_DATES()));
        Log.d("tmp1.........", String.valueOf(funds.getData().get(1).getSIP_DATES()));
        Log.d("tmp2.........", String.valueOf(funds.getData().get(2).getSIP_DATES()));
        tmp = tmp.startsWith(",") ? tmp.substring(1, tmp.length()) : tmp;
        tmp = tmp.endsWith(",") ? tmp.substring(0, (tmp.length() - 1)) : tmp;

        tmp2 = tmp2.startsWith(",") ? tmp2.substring(1, tmp2.length()) : tmp2;
        tmp2 = tmp2.endsWith(",") ? tmp2.substring(0, (tmp2.length() - 1)) : tmp2;

        tmp3 = tmp3.startsWith(",") ? tmp3.substring(1, tmp3.length()) : tmp3;
        tmp3 = tmp3.endsWith(",") ? tmp3.substring(0, (tmp3.length() - 1)) : tmp3;

        String[] dt = tmp.split(","), dt2 = tmp2.split(","), dt3 = tmp3.split(",");
            log.d("dt..if...", String.valueOf(dt));
        if (len > 2) {
            Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length + "\n3. " + dt3.length);
            int lenth = 0;
            lenth = dt.length < (dt2.length < dt3.length ? dt2.length : dt3.length)
                    ? dt.length :
                    (dt2.length < dt3.length ? dt2.length : dt3.length);
            Log.e("MinimumLength", lenth + "");
            for (int i = 0; i < lenth; i++) {
                if (dt[i].equals(dt2[i]) && dt[i].equals(dt3[i]))
                    if (date.equals(""))
                        date = dt[i];
            }
        } else if (len > 1) {
            Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length);
            int lenth = 0;
            lenth = dt.length < dt2.length ? dt.length : dt2.length;
            Log.e("MinimumLength", lenth + "");
            for (int i = 0; i < lenth; i++) {
                if (dt[i].equals(dt2[i]))
                    if (date.equals(""))
                        date = dt[i];
            }
        } else if (len > 0) {
            date = dt[0];
        } else {
            date = "";
        }
        return date;
    }else{
            Log.d("len..ELSE.......", String.valueOf(len));
            Log.d("funds..ELSE.......", String.valueOf(funds));
            String date = "";
            String tmp = funds.getData().get(0).getSIP_DATES(),
                    tmp2 = funds.getData().get(1).getSIP_DATES();
            //      tmp3 = funds.getData().get(2).getSIP_DATES();
            tmp = tmp.startsWith(",") ? tmp.substring(1, tmp.length()) : tmp;
            tmp = tmp.endsWith(",") ? tmp.substring(0, (tmp.length() - 1)) : tmp;

            tmp2 = tmp2.startsWith(",") ? tmp2.substring(1, tmp2.length()) : tmp2;
            tmp2 = tmp2.endsWith(",") ? tmp2.substring(0, (tmp2.length() - 1)) : tmp2;

//        tmp3 = tmp3.startsWith(",") ? tmp3.substring(1, tmp3.length()) : tmp3;
//        tmp3 = tmp3.endsWith(",") ? tmp3.substring(0, (tmp3.length() - 1)) : tmp3;

            String[] dt = tmp.split(","), dt2 = tmp2.split(",");//, dt3 = tmp3.split(",");
            log.d("dt. else....", String.valueOf(dt));

//        if (len > 2) {
//            Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length);// + "\n3. " + dt3.length);
//            int lenth = 0;
//            lenth = dt.length < (dt2.length < dt3.length ? dt2.length : dt3.length)
//                    ? dt.length :
//                    (dt2.length < dt3.length ? dt2.length : dt3.length);
//            Log.e("MinimumLength", lenth + "");
//            for (int i = 0; i < lenth; i++) {
//                if (dt[i].equals(dt2[i]) && dt[i].equals(dt3[i]))
//                    if (date.equals(""))
//                        date = dt[i];
//            }
//        } else
            if (len == 2) {
                Log.e("Lenghts", "1. " + dt.length + "\n2. " + dt2.length);
                int lenth = 0;
                lenth = dt.length < dt2.length ? dt.length : dt2.length;
                Log.e("MinimumLength", lenth + "");
                for (int i = 0; i < lenth; i++) {
                    if (dt[i].equals(dt2[i]))
                        if (date.equals(""))
                            date = dt[i];
                }
            } else if (len > 0) {
                date = dt[0];
            } else {
                date = "";
            }
            return date;
        }
    }

    Bundle bundle;

    public void showAcceptTerms() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dio_investmentplandetail_popup, null);
        dialogBuilder.setView(dialogView);

        String first = contextd.getResources().getString(R.string.ihaveReadAndAccept) + "  ";
        String next = "<font color='#1B7D0C'>" +
                contextd.getResources().getString(R.string.termsAndConditions) + "</font>";
        TextView popup_text_frst = (TextView) dialogView.findViewById(R.id.popup_text_frst);
        popup_text_frst.setText(Html.fromHtml(first + next));
        TextView savingFor = (TextView) dialogView.findViewById(R.id.popup_tv2);
        savingFor.setText(contextd.getResources().getString(R.string.savingFor) + " " + getArguments().getString("goalNameAlt"));
        String first1 = contextd.getResources().getString(R.string.ihaveReadAndAccept) + "  ";
        String next1 = "<font color='#1B7D0C'>" + contextd.getResources().getString(R.string.schemeOffers) + "</font>";
        TextView popup_text_second = (TextView) dialogView.findViewById(R.id.popup_text_second);
        popup_text_second.setText(Html.fromHtml(first1 + next1));
        TextView invest_cancel = (TextView) dialogView.findViewById(R.id.invest_cancel);
        invest_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        TextView button_popup = (TextView) dialogView.findViewById(R.id.button_popup);
        button_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, 1);
                Log.e("before", std.format(calendar.getTime()));
                String sipFrom = monthY.format(calendar.getTime());
                DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(new Locale("en"));
                DateTime from = dtf.parseDateTime(std.format(calendar.getTime()).toString()), to;
                calendar.set(Calendar.YEAR, Integer.parseInt(getArguments().getString("targetYear")));
                calendar.add(Calendar.MONTH, -1);
                Log.e("after", std.format(calendar.getTime()));
                String sipTo = monthY.format(calendar.getTime());
                to = dtf.parseDateTime(std.format(calendar.getTime()).toString());
                int noOfMonth = (Months.monthsBetween(from, to).getMonths());
                Log.e("NoOfMonth", noOfMonth + "");

                Log.e("BeginnerRes", "Goal Name:" + getArguments().getString("goalName")
                        + "\nCategoryID:" + getArguments().getString("cateId")
                        + "\nFrom:" + getArguments().getString("from")
                        + "\nTarget Year:" + getArguments().getString("targetYear")
                        + "\nOrder Type:" + getArguments().getString("orderType")
                        + "\nTarget Amount:" + getArguments().getString("amountAmount")
                        + "\nTotal Pay:" + getArguments().getString("sipOneTime")
                        + "\nGrowth:" + getArguments().getString("percentage")
                        + "\nSchemeJson:" + schemesJson
                );
                bundle = getArguments();
                bundle.putString("schemeJson", schemesJson);
                if (!getArguments().getString("orderType").equals("ONETIME")) {
                    bundle.putString("sipDate", sipDate);
                    bundle.putString("sipFrom", sipFrom);
                    bundle.putString("sipTo", sipTo);
                }
                bundle.putString("isInCart", cartAdded ? cartItemId : "0");
                bundle.putString("months", noOfMonth + "");
                bundle.putString("remark", (getArguments().getString("orderType").equals("ONETIME")
                        ? "Purchase Order for "
                        : "SIP Registration for ") + "GOAL" + " of " + Common.getPreferences(contextd, "userID") + " at " + timeF.format(Calendar.getInstance().getTime()));
                frag = new RedirectringFragment();
                frag.setArguments(bundle);
                UiUtils.hideKeyboard(contextd);
                getProfileStatus(Common.getPreferences(contextd, "userID"));
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }

    void addGoalToCart(String userid, CartModle cart) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.AddToCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("order_type", cart.getOrderType());
        if (!getArguments().getString("orderType").equals("ONETIME"))
            requestedServiceDataModel.putQurry("no_of_months", cart.getNumberofmonths());
        requestedServiceDataModel.putQurry("target_year", cart.getTarget_year());
        requestedServiceDataModel.putQurry("added_date", cart.getAdded_date());
        requestedServiceDataModel.putQurry("is_goal", "1");
        requestedServiceDataModel.putQurry("goal_amount", cart.getGoal_amount());
        requestedServiceDataModel.putQurry("goal_name", cart.getGoal_name());
        requestedServiceDataModel.putQurry("goal_cat_id", cart.getGoal_cate_id());
        requestedServiceDataModel.putQurry("scheme_json", cart.getSchem_json());
        requestedServiceDataModel.setWebServiceType("ADDCART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void removeFromCart(String userid, String cartid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.RemoveFromCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("cart_id", cartid);
        requestedServiceDataModel.setWebServiceType("REMOVECART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    SimpleDateFormat std = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en")),
            timeF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("en"));
    Fragment frag;

    void getProfileStatus(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void showAccountSetup(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.do_it_now, null);
        dialogBuilder.setView(dialogView);
        TextView home = (TextView) dialogView.findViewById(R.id.invest_cancel);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        TextView kyc = (TextView) dialogView.findViewById(R.id.button_popup);
        kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new KYCRegistrationFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }
    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                try {
                    JSONObject obj = new JSONObject(json);
                    if (obj.getJSONObject("data").getString("kyc_status").equals("COMPLETE")) {
                        if (isCams && (obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("PENDING") ||
                                obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("REJECTED"))) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
                            builder.setTitle(contextd.getResources().getString(R.string.app_name));
                            builder.setMessage(contextd.getResources().getString(R.string.yourDocumentNotVerifiedAddtoCart));
                            String positiveText = contextd.getResources().getString(R.string.addToCart);
                            builder.setPositiveButton(positiveText,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            addGoal();
                                        }
                                    });
                            builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.setCancelable(false);
                            dialog.show();
                        } else {
                            if (obj.getJSONObject("data").getString("is_mendate").equalsIgnoreCase("VERIFIED")) {
                                bundle.putBoolean("isXSIP", true);
                                bundle.putString("mandate_id", obj
                                        .getJSONObject("data")
                                        .getString("mandate_id"));
                            } else {
                                bundle.putBoolean("isXSIP", false);
                            }
                            frag.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                    .replace(R.id.frame_container, frag).addToBackStack(null).
                                    commit();
                        }
                    } else {
                        Log.d("json::::::::::::",json);
//                        AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
//                        builder.setTitle(contextd.getResources().getString(R.string.app_name));
//                        builder.setMessage(contextd.getResources().getString(R.string.kycPending));
//                        String positiveText = contextd.getResources().getString(R.string.doItNow);
//                        builder.setPositiveButton(positiveText,
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                        activity.getDocBackHome(contextd.getResources().getString(R.string.documentVerification), false);
//                                    }
//                                });
//                        AlertDialog dialog = builder.create();
//                        dialog.setCancelable(false);
//                        dialog.show();
                        //showAccountSetup();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.AddToCart:
                addToCart.setText(contextd.getResources().getString(R.string.remove));
                Toast.makeText(contextd, contextd.getResources().getString(R.string.fundAddedToCart), Toast.LENGTH_SHORT).show();
                try {
                    JSONObject obj = new JSONObject(json);
                    cartItemId = obj.getString("cart_id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cartAdded = true;
                break;
            case ResponseType.RemoveFromCart:
                addToCart.setText(contextd.getResources().getString(R.string.addToCart));
                Toast.makeText(contextd, contextd.getResources().getString(R.string.fundRemovedFromCart), Toast.LENGTH_SHORT).show();
                cartAdded = false;
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
     //           Log.e("res", json);
                Common.showToast(contextd, message);
                break;
            case ResponseType.AddToCart:
                Common.showToast(contextd, message);
                break;
            case ResponseType.RemoveFromCart:
                Common.showToast(contextd, message);
                break;
        }
    }
}
