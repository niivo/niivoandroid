package com.niivo.com.niivo.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by manish on 3/1/19.
 */

public class MyMoneyAdapter extends RecyclerView.Adapter<MyMoneyAdapter.MyViewHolder> {
    Context context;
    OrderedList.OrderedItem orderedItemList;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));
    //SimpleDateFormat getSDF = new SimpleDateFormat("dd MMM yyyy", new Locale("en"));


    public MyMoneyAdapter(Context context, OrderedList.OrderedItem orderedItem) {
        this.context = context;
        orderedItemList = orderedItem;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.xml_mymoneylayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // set the data in itemst

        // TextView tv;
        // tv.setText(orderedItemList.getOrderedItemArrayListSub().get(position).getDtdate());
//
        try {
            Calendar ct = Calendar.getInstance();
            ct.setTime(getSDF.parse(orderedItemList.getOrderedItemArrayListSub().get(orderedItemList.getOrderedItemArrayListSub().size()-position-1).getDtdate()));
            holder.textViewDate.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //    holder.textViewDate.setText(orderedItemList.getOrderedItemArrayListSub().get(position).getDtdate());
        holder.tvAmount.setText(orderedItemList.getOrderedItemArrayListSub().get(orderedItemList.getOrderedItemArrayListSub().size()-position-1).getAmount());
        holder.tvNav.setText(orderedItemList.getOrderedItemArrayListSub().get(orderedItemList.getOrderedItemArrayListSub().size()-position-1).getNav());
        holder.tvUnits.setText(orderedItemList.getOrderedItemArrayListSub().get(orderedItemList.getOrderedItemArrayListSub().size()-position-1).getQuantity());
        holder.tvSerial.setText((position+1)+"/"+orderedItemList.getOrderedItemArrayListSub().size());


    }


    @Override
    public int getItemCount() {

        return orderedItemList.getOrderedItemArrayListSub().size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewDate,tvAmount,tvNav,tvUnits,tvSerial;
        public MyViewHolder(View itemView) {
            super(itemView);
            textViewDate = itemView.findViewById(R.id.tv_date);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            tvNav = itemView.findViewById(R.id.tv_nav);
            tvUnits = itemView.findViewById(R.id.tv_units);
            tvSerial = itemView.findViewById(R.id.tv_serial);

        }
    }


}


