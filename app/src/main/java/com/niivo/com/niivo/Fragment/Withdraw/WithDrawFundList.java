package com.niivo.com.niivo.Fragment.Withdraw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.InvestedAdapter;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by deepak on 7/11/17.
 */

public class WithDrawFundList extends Fragment implements ResponseDelegate {
    TextView totalInvested, totalCurrent;
    ListView investments;
    ImageButton back;


    //Non UI vars
    private RequestedServiceDataModel requestedServiceDataModel;
    Bundle b;
    Context contextd;
    Fragment frag = null;
    OrderedList orderedList;
    InvestedAdapter adapter;
    JSONObject response;
    private boolean isViewShown;
    private boolean isVisible;
    private boolean isStarted;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_withdraw_funds, container, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.withDraw));
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);

        totalInvested = (TextView) rootView.findViewById(R.id.totalInvested);
        totalCurrent = (TextView) rootView.findViewById(R.id.totalCurrent);
        investments = (ListView) rootView.findViewById(R.id.investedList);
        fetchData();
        getListener();
        return rootView;
    }


    void getInvestmentFundList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("ORDERLIST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
//        requestedServiceDataModel.putQurry("userid", "10000005");
        requestedServiceDataModel.putQurry("order_status", "CONFIRM");
        requestedServiceDataModel.putQurry("payment_status", "1");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        Gson gson = new Gson();
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                try {
                    response = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                orderedList = gson.fromJson(json, OrderedList.class);
                if (orderedList.getData().size() > 0) {
                    for (int i = 0; i < orderedList.getData().size(); i++) {
                        if (orderedList.getData().get(i).getFull_withdraw().equals("1"))
                            orderedList.getData().remove(i);
                    }
                    totalInvested.setText(Common.currencyString(orderedList.getTotal_amount(),false));
                    float current = Float.parseFloat(orderedList.getTotal_current_value());
                    String st = String.format(new Locale("en"), "%.2f", current);
                    totalCurrent.setText(Common.currencyString(st,false));
                    adapter = new InvestedAdapter(contextd, orderedList, false);
                    investments.setAdapter(adapter);
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                Common.showToast(contextd, message);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
      //  Log.i("Fragment", "setUserVisibleHint: ");
        super.setUserVisibleHint(isVisibleToUser);

    }

    @Override
    public boolean getUserVisibleHint() {
        Log.i("Fragment", "getUserVisibleHint: ");
        return super.getUserVisibleHint();
    }

    private void fetchData() {
       if(investments!=null)
       {
           investments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   b = new Bundle();
                   try {
                       String str = new Gson().toJson(orderedList.getData().get(position));
                       b.putString("fund", str);
                       //b.putString("fund", response.getJSONArray("data").getJSONObject(position).toString());
//                    b.putString("fund", null);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   if (orderedList.getData().get(position).getIs_allotment().equals("1") &&
                           orderedList.getData().get(position).getRequested().equals("0")) {
                       frag = new WithdrawFragment();
                       frag.setArguments(b);
                   } else {
                       frag = null;
                       if (orderedList.getData().get(position).getIs_allotment().equals("0"))
                           Common.showDialog(contextd, contextd.getResources().getString(R.string.alottment_is_still_pending));
                       else if (!orderedList.getData().get(position).getRequested().equals("0"))
                           Common.showDialog(contextd, contextd.getResources().getString(R.string.already_initiated_a_withdraw_req));
                   }
                   if (frag != null) {
                       UiUtils.hideKeyboard(contextd);
                       getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                               .add(R.id.frame_container, frag).addToBackStack(null).
                               commit();
                   }
               }
           });
           getInvestmentFundList();
       }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("Fragment", "setUserVisibleHint: ");
        fetchData();

    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                if (manager != null) {
                    int backStackEntryCount = manager.getBackStackEntryCount();
                    if (backStackEntryCount == 0) {

                    }
                    Fragment fragment = manager.getFragments()
                            .get(backStackEntryCount - 1);
                    fragment.onResume();
                }
            }
        };
        return result;
    }
}
