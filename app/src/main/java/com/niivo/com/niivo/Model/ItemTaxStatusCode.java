package com.niivo.com.niivo.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deepak on 28/9/18.
 */

public class ItemTaxStatusCode implements Serializable {
    String status, msg;
    ArrayList<TaxStatusCode> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<TaxStatusCode> getData() {
        return data;
    }

    public void setData(ArrayList<TaxStatusCode> data) {
        this.data = data;
    }

    public class TaxStatusCode implements Serializable {
        String TYPE, NIIVO_CODE, FRONTENT_OPTION_NAME, CATEGORY;

        public String getTYPE() {
            return TYPE;
        }

        public void setTYPE(String TYPE) {
            this.TYPE = TYPE;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getFRONTENT_OPTION_NAME() {
            return FRONTENT_OPTION_NAME;
        }

        public void setFRONTENT_OPTION_NAME(String FRONTENT_OPTION_NAME) {
            this.FRONTENT_OPTION_NAME = FRONTENT_OPTION_NAME;
        }

        public String getCATEGORY() {
            return CATEGORY;
        }

        public void setCATEGORY(String CATEGORY) {
            this.CATEGORY = CATEGORY;
        }
    }
}
