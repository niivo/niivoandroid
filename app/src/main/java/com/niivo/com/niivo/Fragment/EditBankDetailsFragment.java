package com.niivo.com.niivo.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Model.ItemState;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 2/11/17.
 */

public class EditBankDetailsFragment extends Fragment implements ResponseDelegate, View.OnClickListener {
    Context contextd;
    AppCompatSpinner accountTypeSpinner;

    TextView submit;
    ProgressDialog pd;
    ProgressBar ifscProgress;
    ImageView cancleCheque;
    TextInputEditText accountNumber, accountIFSC, nameOnCheque;
    TextInputLayout accountNumberTIL, accountIFSCTIL, nameOnChequeTIL;

    //Non ui vars
    ArrayList<ItemState> accountType;
    File image = null, AofJsonFile, chequeFile;
    Bitmap imgBitmap;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    String dob, nomniDob, chequeBase = "", mobileNo = "", userid = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en")),
            sdfAlt = new SimpleDateFormat("dd-MM-yyyy", new Locale("en"));
    JSONObject response, bankDetail = null, aofJson = null;
    int CameraRequest = 111, GalleryRequest = 112, CropRequest = 113;
    Uri imageUri;
    Bitmap chequeBitmap = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_bank_details, container, false);
        contextd = container.getContext();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.editBankDetail).toUpperCase());
        ImageButton back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);

        accountTypeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.docAccountType);
        accountNumber = (TextInputEditText) rootView.findViewById(R.id.docAccountNumberET);
        accountIFSC = (TextInputEditText) rootView.findViewById(R.id.docIFSCET);
        nameOnCheque = (TextInputEditText) rootView.findViewById(R.id.docNameOnChequeET);
        submit = (TextView) rootView.findViewById(R.id.submit);
        ifscProgress = (ProgressBar) rootView.findViewById(R.id.ifscProgress);
        cancleCheque = (ImageView) rootView.findViewById(R.id.addChequeCancle);
        accountNumberTIL = (TextInputLayout) rootView.findViewById(R.id.docAccountNumberTIL);
        accountIFSCTIL = (TextInputLayout) rootView.findViewById(R.id.docIFSCTIL);
        nameOnChequeTIL = (TextInputLayout) rootView.findViewById(R.id.docNameOnChequeTIL);
        ifscProgress.setVisibility(View.GONE);
        accountIFSC.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        accountIFSC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.e("IFSChasFocus", hasFocus + "");
                if (!hasFocus) {
                    if (accountIFSC.getText().toString().length() > 0) {
                        ifscProgress.setVisibility(View.VISIBLE);
                        isValidIFSC(accountIFSC.getText().toString().trim());
                    }
                } else
                    ifscProgress.setVisibility(View.GONE);
            }
        });
        submit.setOnClickListener(this);
        cancleCheque.setOnClickListener(this);
        activity = (MainActivity) getActivity();
        pd = new ProgressDialog(contextd, R.style.myDialogTheme);
        pd.setIndeterminate(false);
        pd.setMessage(contextd.getResources().getString(R.string.pleaseWait));
        pd.setCancelable(false);
        getAccountType();
        return rootView;
    }

    boolean validate() {
        boolean b = false;
        if (accountNumber.getText().toString().trim().equals("")) {
            b = false;
            accountNumberTIL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (accountIFSC.getText().toString().trim().equals("")) {
            b = false;
            accountIFSCTIL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (nameOnCheque.getText().toString().trim().equals("")) {
            b = false;
            nameOnChequeTIL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (image == null) {
            b = false;
            Common.showToast(contextd, contextd.getResources().getString(R.string.tryAgain));
        } else if (chequeBitmap == null) {
            b = false;
            Common.showToast(contextd, contextd.getResources().getString(R.string.getChequeImageFirst));
        } else if (bankDetail == null) {
            b = false;
            accountIFSC.setError(contextd.getResources().getString(R.string.invalidIFSC));
        } else {
            b = true;
        }
        return b;
    }

    ProgressDialog pdt;

    void createJsonAndSubmit() {
        pdt = new ProgressDialog(contextd, R.style.myDialogTheme);
        pdt.setMessage(contextd.getResources().getString(R.string.pleaseWait));
        pdt.setIndeterminate(false);
        pdt.setCancelable(false);
        pdt.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (chequeBase.equals(""))
                    chequeBase = Common.getBitmapBase64(chequeBitmap);
                try {
                    aofJson.put("BankMandateAcNo", accountNumber.getText().toString().trim());
                    aofJson.put("BankMandateAcType", "SAVING");
                    aofJson.put("BankMandateIfscCode", accountIFSC.getText().toString().trim());
                    aofJson.put("BankMandateNameOfBank", bankDetail.getJSONObject("data").getString("name") + "");
                    aofJson.put("BankMandateBranch", bankDetail.getJSONObject("data").getString("branch") + "");
                    aofJson.put("BankMandateBankAddress", bankDetail.getJSONObject("data").getString("address") + "");
                    aofJson.put("BankMandateBankCity", bankDetail.getJSONObject("data").getString("city") + "");
                    aofJson.put("BankMandateBankState", bankDetail.getJSONObject("data").getString("state") + "");
                    aofJson.put("Date", sdfAlt.format(Calendar.getInstance().getTime()));
                    aofJson.put("ChequeBase64", chequeBase);
                    AofJsonFile = writeToFile(aofJson.toString());
                    pdt.dismiss();
                    sendJsonFile(userid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, 1000);
    }

    void getAccountType() {
        accountType = new ArrayList<>();
        accountType.add(new ItemState(contextd.getResources().getString(R.string.savingBank), "SB"));
//        accountType.add(new ItemState(contextd.getResources().getString(R.string.current_bank), "CB"));
//        accountType.add(new ItemState(contextd.getResources().getString(R.string.nre_account), "NE"));
//        accountType.add(new ItemState(contextd.getResources().getString(R.string.nro_account), "NO"));
        accountTypeSpinner.setAdapter(new STateAdapter(contextd, accountType));
        getProfileStatus(Common.getPreferences(contextd, "userID"));
    }

    void sendJsonFile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UploadIFSC);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putFiles("json", AofJsonFile);
        requestedServiceDataModel.setWebServiceType("json-upload");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void isValidIFSC(String ifsc) {
        ifscProgress.setVisibility(View.VISIBLE);
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.IFSCVerify);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.putQurry("ifsc_code", ifsc);
        requestedServiceDataModel.setWebServiceType("VERIFY-IFSC");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    void getProfileStatus(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getJsonObj(String jsonUrl) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice(jsonUrl);
        baseRequestData.setTag(ResponseType.GetJson);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET_woData);
        requestedServiceDataModel.setWebServiceType("");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void createAClient(String userid, String name, String dob, String mobile, String gender, String occupation_code, String occupation, String add1, String add2, String add3, String state, String state_code, String city, String pin, String country, String pencard, String bank_type, String account_no, String ifsc, String account_name, String father_name, String mother_name, String email, String nominee_name, String nominee_dob, String nominee_relationship, String nominee_address, String is_aadhar_verify, File image, String bank_name
            , String bank_id, String anualIncomeCode, String occupationType
            , String wealthSourceCode, String aadhar_no) {
        Log.e("Received Info", "UserID:" + userid + "\nName:" +
                name + "\nDOB:" +
                dob + "\nMobile:" +
                mobile + "\nGender:" +
                gender + "\nOccupationCode:" +
                occupation_code + "\nOccupation:" +
                occupation + "\nAddressLine1:" +
                add1 + "\nAddressLine2:" +
                add2 + "\nAddressLine3:" +
                add3 + "\nState:" +
                state + "\nStateCode:" +
                state_code + "\nCity:" +
                city + "\nPincode:" +
                pin + "\nCountry:" +
                country + "\nPanCard:" +
                pencard + "\nBankAccType:" +
                bank_type + "\nAccountNumber:" +
                account_no + "\nIFSC:" +
                ifsc + "\nAccountNAme:" +
                account_name + "\nFatherName:" +
                father_name + "\nMotherName:" +
                mother_name + "\nEmail:" +
                email + "\nNomineeName:" +
                nominee_name + "\nNomineeDob:" +
                nominee_dob + "\nNomineeRelation:" +
                nominee_relationship + "\nNomineeAddress:" +
                nominee_address + "\nIsAadharVerify:" +
                is_aadhar_verify + "\nImagePath:" +
                image.getPath().toString());


        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UpdateUSer);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("name", name);
        requestedServiceDataModel.putQurry("dob", dob);
        requestedServiceDataModel.putQurry("mobile", mobile);
        requestedServiceDataModel.putQurry("gender", gender);
        requestedServiceDataModel.putQurry("occupation_code", occupation_code);
        requestedServiceDataModel.putQurry("occupation", occupation);
        requestedServiceDataModel.putQurry("add1", add1);
        requestedServiceDataModel.putQurry("add2", add2);
        requestedServiceDataModel.putQurry("add3", add3);
        requestedServiceDataModel.putQurry("state", state);
        requestedServiceDataModel.putQurry("state_code", state_code);
        requestedServiceDataModel.putQurry("city", city);
        requestedServiceDataModel.putQurry("pin", pin);
        requestedServiceDataModel.putQurry("country", country);
        requestedServiceDataModel.putQurry("pencard", pencard);
        requestedServiceDataModel.putQurry("bank_type", bank_type);
        requestedServiceDataModel.putQurry("account_no", account_no);
        requestedServiceDataModel.putQurry("ifsc", ifsc);
        requestedServiceDataModel.putQurry("account_name", account_name);
        requestedServiceDataModel.putQurry("father_name", father_name);
        requestedServiceDataModel.putQurry("mother_name", mother_name);
        requestedServiceDataModel.putQurry("email", email);
        requestedServiceDataModel.putQurry("nominee_name", nominee_name);
        requestedServiceDataModel.putQurry("nominee_dob", nominee_dob);
        requestedServiceDataModel.putQurry("nominee_relationship", nominee_relationship);
        requestedServiceDataModel.putQurry("nominee_address", nominee_address);
        requestedServiceDataModel.putQurry("is_aadhar_verify", is_aadhar_verify);
        requestedServiceDataModel.putFiles("image", image);
        requestedServiceDataModel.putQurry("bank_name", bank_name);
        requestedServiceDataModel.putQurry("bank_id", bank_id);
        requestedServiceDataModel.putQurry("anualIncomeCode", anualIncomeCode);
        requestedServiceDataModel.putQurry("wealthSourceCode", wealthSourceCode);
        requestedServiceDataModel.putQurry("occupationType", occupationType);
        requestedServiceDataModel.putQurry("aadhar_no", aadhar_no);
        requestedServiceDataModel.setWebServiceType("UPDATE-PROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.IFSCVerify:
                //get user selfdeclaration
                ifscProgress.setVisibility(View.GONE);
                try {
                    bankDetail = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.UserProfile:
                try {
                    response = new JSONObject(json);
                    if (response.getJSONObject("data").getString("kyc_status").equals("COMPLETE")) {
                        pd.show();
                        accountNumber.setText(response.getJSONObject("data").getString("account_no"));
                        accountIFSC.setText(response.getJSONObject("data").getString("ifsc").toUpperCase());
                        nameOnCheque.setText(response.getJSONObject("data").getString("account_name"));
                        userid = response.getJSONObject("data").getString("userid");
                        isValidIFSC(accountIFSC.getText().toString().trim());
                        Calendar calendar = Calendar.getInstance();
                        dob = response.getJSONObject("data").getString("dob").toString().trim();
                        nomniDob = response.getJSONObject("data").getString("nominee_dob").toString().trim();
                        Log.e("before", dob + "\n" + nomniDob);
                        try {
                            calendar.setTime(sdf.parse(dob));
                            dob = sdf.format(calendar.getTime());
                            calendar.setTime(sdf.parse(nomniDob));
                            if ((calendar.get(Calendar.YEAR)) > 0)
                                nomniDob = "";
                            else
                                nomniDob = sdf.format(calendar.getTime());
                            Log.e("after", dob + "\n" + nomniDob);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        new Handler().post(new Runnable() {
                            URL url;

                            @Override
                            public void run() {
                                try {
                                    url = new URL(Constant.imageBaseUrl + response.getJSONObject("data").getString("image"));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Bitmap bt = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                    imgBitmap = bt;
                                    if (imgBitmap != null) {
                                        image = persistImage(imgBitmap, System.currentTimeMillis() + "");
                                    }
                                    getJsonObj(response.getJSONObject("data").getString("userid") + ".json");
                                    pd.dismiss();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


//                        Log.e("Img", Constant.imageBaseUrl + response.getJSONObject("data").getString("image"));
//                        Picasso.with(contextd).load(Constant.imageBaseUrl + response.getJSONObject("data").getString("image"))
//                                .into(new Target() {
//                                    @Override
//                                    public void onBitmapLoaded(Bitmap bt, Picasso.LoadedFrom from) {
//                                        imgBitmap = bt;
//                                        if (imgBitmap != null) {
//                                            image = persistImage(imgBitmap, System.currentTimeMillis() + "");
//                                        }
//                                        try {
//                                            getJsonObj(response.getJSONObject("data").getString("userid") + ".json");
//                                        } catch (JSONException e) {
//                                            e.printStackTrace();
//                                        }
//                                        pd.dismiss();
//                                    }
//
//                                    @Override
//                                    public void onBitmapFailed(Drawable errorDrawable) {
//                                        pd.dismiss();
//                                        Common.showDialog(contextd, "Something went wrong please try again..!");
//                                    }
//
//                                    @Override
//                                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                                    }
//                                });
                    } else {
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
                        builder.setTitle(contextd.getResources().getString(R.string.app_name));
                        builder.setMessage(contextd.getResources().getString(R.string.kycPending));
                        String positiveText = contextd.getResources().getString(R.string.doItNow);
                        builder.setPositiveButton(positiveText,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        activity.getDocBackHome(contextd.getResources().getString(R.string.documentVerification), false);
                                    }
                                });
                        android.support.v7.app.AlertDialog dialog = builder.create();
                        dialog.setCancelable(false);
                        dialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.UpdateUSer:
                Log.e("verifiedDoc", json);
                createJsonAndSubmit();
                break;
            case ResponseType.GetJson:
                try {
                    JSONObject object = new JSONObject(json);
                    aofJson = object;
                    Log.e("json", object.getString("Applicant1Name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.UploadIFSC:
                setAlertHere(contextd.getResources().getString(R.string.bankDetailUpdated));
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                Common.showToast(contextd, message);
                break;
            case ResponseType.IFSCVerify:
                ifscProgress.setVisibility(View.GONE);
                accountIFSC.setError(message);
                bankDetail = null;
                break;
            case ResponseType.UpdateUSer:
                Common.showToast(contextd, message);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Log.e("res", json);
                    if (jsonObject.getString("retry").trim().equals("1"))
                        retryOnWebError(message);
                    else {
                        setAlertHere(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.GetJson:
                setAlertHere(contextd.getResources().getString(R.string.imageUploadNotDone));
                break;
            case ResponseType.UploadIFSC:
                Common.showDialog(contextd, message);
                break;
        }
    }

    private File persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getBaseContext().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg"); //jpg
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            android.util.Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    File writeToFile(String data) {
        ProgressDialog pd = new ProgressDialog(contextd, R.style.myDialogTheme);
        pd.setCancelable(false);
        pd.setIndeterminate(false);
        pd.setMessage("Saving Base64..!");
        pd.show();
        File filesDir = getActivity().getBaseContext().getFilesDir();
        File file = new File(filesDir, "AOFBse.json");
        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            e.printStackTrace();
            pd.dismiss();
            file = null;
        }
        pd.dismiss();
        return file;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                if (validate()) {
                    if (response != null)
                        try {
                            createAClient(response.getJSONObject("data").getString("userid"),
                                    response.getJSONObject("data").getString("name"),
                                    dob,
                                    response.getJSONObject("data").getString("mobile"),
                                    response.getJSONObject("data").getString("gender"),
                                    response.getJSONObject("data").getString("occupation_code"),
                                    response.getJSONObject("data").getString("occupation"),
                                    response.getJSONObject("data").getString("address1"),
                                    response.getJSONObject("data").getString("address2"),
                                    response.getJSONObject("data").getString("address3"),
                                    response.getJSONObject("data").getString("state"),
                                    response.getJSONObject("data").getString("state_code"),
                                    response.getJSONObject("data").getString("city"),
                                    response.getJSONObject("data").getString("pin"),
                                    response.getJSONObject("data").getString("country"),
                                    response.getJSONObject("data").getString("pencard"),
                                    accountType.get(accountTypeSpinner.getSelectedItemPosition()).getStateCode(),
                                    accountNumber.getText().toString().trim(),
                                    accountIFSC.getText().toString().trim(),
                                    nameOnCheque.getText().toString().trim(),
                                    response.getJSONObject("data").getString("father_name"),
                                    response.getJSONObject("data").getString("mother_name") + "",
                                    response.getJSONObject("data").getString("email"),
                                    response.getJSONObject("data").getString("nominee_name") + "",
                                    nomniDob,
                                    response.getJSONObject("data").getString("nominee_relationship") + "",
                                    response.getJSONObject("data").getString("nominee_address") + "",
                                    "1",
                                    image,
                                    bankDetail.getJSONObject("data").getString("name"),
                                    bankDetail.getJSONObject("data").getString("bank_id"),
                                    response.getJSONObject("data").getString("anualIncomeCode"),
                                    response.getJSONObject("data").getString("occupation_type"),
                                    response.getJSONObject("data").getString("wealth_source"),
                                    response.getJSONObject("data").getString("aadhar_no"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
                break;
            case R.id.addChequeCancle:
                if (Utility.getCameraAndExpernalPermission(contextd)) {
                    imagePickerDialog(CameraRequest, GalleryRequest);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Utility.GetCameraAndGallery:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                } else {
                    Log.e("Permission", "Not Granted");
                    Utility.getCameraAndExpernalPermission(contextd);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CameraRequest) {
                Log.e("Image From", "Camera");
                Bitmap bm = null;//(Bitmap) data.getExtras().get("data");
                try {
                    bm = MediaStore.Images.Media.getBitmap(
                            contextd.getContentResolver(), imageUri);
                    //get Image if Rotated
                    bm = Common.rotatedBitmap(contextd, bm, imageUri);
                    chequeFile = new File(imageUri.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (bm != null) {
                    //compressing
                    chequeBitmap = bm;
                    if (chequeBitmap.getHeight() < chequeBitmap.getWidth()) {
                        chequeBitmap = getResizedBitmap(chequeBitmap, 1000, 1414);
                    } else {
                        chequeBitmap = getResizedBitmap(chequeBitmap, 1414, 1000);
                    }
                    cancleCheque.setImageBitmap(chequeBitmap);
                    if (chequeBitmap.getHeight() < chequeBitmap.getWidth())
                        chequeBitmap = Common.rotateImage(chequeBitmap, 90);
                }
            } else if (requestCode == GalleryRequest) {
                Log.e("Image From", "Gallery");
                Bitmap bm = null;
                if (data != null) {
                    try {
                        bm = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), data.getData());
                        bm = Common.rotatedBitmap(contextd, bm, data.getData());
                        //compressing
                        chequeBitmap = bm;
                        if (chequeBitmap.getHeight() < chequeBitmap.getWidth()) {

                            chequeBitmap = getResizedBitmap(chequeBitmap, 1000, 1414);
                        } else {
                            chequeBitmap = getResizedBitmap(chequeBitmap, 1414, 1000);
                        }
                        cancleCheque.setImageBitmap(chequeBitmap);
                        if (chequeBitmap.getHeight() < chequeBitmap.getWidth())
                            chequeBitmap = Common.rotateImage(chequeBitmap, 90);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            Log.e("Image", "Not here");
        }
    }

    Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    void imagePickerDialog(final int CAMERArequest, final int GALLERYrequest) {
        CharSequence[] items = {contextd.getResources().getString(R.string.takePhoto),
                contextd.getResources().getString(R.string.chooseFromLibrary)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
//        if (!message.equals(""))
//            builder.setMessage(message);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    android.util.Log.e("Take", "Take Photo");
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, "NiivoCheque");
                    values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                    imageUri = contextd.getContentResolver().insert(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    EditBankDetailsFragment.this.startActivityForResult(intent, CAMERArequest);
                } else if (item == 1) {
                    android.util.Log.e("Take", "Choose from Files");
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    EditBankDetailsFragment.this.startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERYrequest);
                }
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    class STateAdapter extends ArrayAdapter<ItemState> {
        ArrayList<ItemState> list;
        Context mContext;

        public STateAdapter(@NonNull Context context, ArrayList<ItemState> resource) {
            super(context, 0, resource);
            this.list = resource;
            this.mContext = context;
            setDropDownViewResource(R.layout.item_drop_down);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            }
            TextView text = (TextView) convertView;
            text.setPadding(20, 20, 20, 12);
            text.setText(list.get(position).getStateName());
            return convertView;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            }
            TextView text = (TextView) convertView;
            text.setText(list.get(position).getStateName());
            return convertView;
        }

    }


    private void retryOnWebError(final String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(contextd.getResources().getString(R.string.retry),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (response != null)
                            try {
                                createAClient(response.getJSONObject("data").getString("userid"),
                                        response.getJSONObject("data").getString("name"),
                                        dob,
                                        response.getJSONObject("data").getString("mobile"),
                                        response.getJSONObject("data").getString("gender"),
                                        response.getJSONObject("data").getString("occupation_code"),
                                        response.getJSONObject("data").getString("occupation"),
                                        response.getJSONObject("data").getString("address1"),
                                        response.getJSONObject("data").getString("address2"),
                                        response.getJSONObject("data").getString("address3"),
                                        response.getJSONObject("data").getString("state"),
                                        response.getJSONObject("data").getString("state_code"),
                                        response.getJSONObject("data").getString("city"),
                                        response.getJSONObject("data").getString("pin"),
                                        response.getJSONObject("data").getString("country"),
                                        response.getJSONObject("data").getString("pencard"),
                                        accountType.get(accountTypeSpinner.getSelectedItemPosition()).getStateCode(),
                                        accountNumber.getText().toString().trim(),
                                        accountIFSC.getText().toString().trim(),
                                        nameOnCheque.getText().toString().trim(),
                                        response.getJSONObject("data").getString("father_name"),
                                        response.getJSONObject("data").getString("mother_name") + "",
                                        response.getJSONObject("data").getString("email"),
                                        response.getJSONObject("data").getString("nominee_name") + "",
                                        nomniDob,
                                        response.getJSONObject("data").getString("nominee_relationship") + "",
                                        response.getJSONObject("data").getString("nominee_address") + "",
                                        "1",
                                        image, bankDetail.getJSONObject("data").getString("name"),
                                        bankDetail.getJSONObject("data").getString("bank_id"),
                                        response.getJSONObject("data").getString("anualIncomeCode"),
                                        response.getJSONObject("data").getString("occupation_type"),
                                        response.getJSONObject("data").getString("wealth_source"),
                                        response.getJSONObject("data").getString("aadhar_no"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        dialog.dismiss();
                    }
                });
        builder.setNegativeButton(contextd.getResources().getString(R.string.backAndValidate),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.getDocBackHome(msg, false);
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    void setAlertHere(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setPositiveButton(contextd.getResources().getString(R.string.goToHome),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.getBackToPortfolio();
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


}
