package com.niivo.com.niivo.Fragment.InvestHelper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Activity.PaymentHelperActivity;
import com.niivo.com.niivo.Fragment.SuccessFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Deepak
 */

public class RedirectringFragment extends Fragment implements ResponseDelegate {
    Activity _activity;
    View parentView;
    TextView notifyTxt;

    //Non ui Vars
    Context contextd;
    boolean hand = false;
    Fragment frag = new SuccessFragment();
    private RequestedServiceDataModel requestedServiceDataModel;
    String orderType = "";
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("en"));
    MainActivity activity;
    String date = "",orderid;
    Calendar calendar;
    boolean proceedPayment = false;
    //forBeginnerOnly
    String is_goal = "0",
            fund_category = "",
            goal_name = "",
            investment_type = "",
            goal_year = "",
            goal_ammount = "",
            scheme_json = "";
    String dt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        parentView = inflater.inflate(R.layout.fragment_redirectoring, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.redirecting));
        notifyTxt = (TextView) parentView.findViewById(R.id.notifyText);
        notifyTxt.setText(contextd.getResources().getString(R.string.pleaseWait));
        orderType = getArguments().getString("orderType");
        calendar = Calendar.getInstance();

        Log.d("data", "schemeCode:" + getArguments().getString("schemeCode") + "\nOrderType:" + getArguments().getString("orderType") + "\nammount:" + getArguments().getString("ammount"));

        activity = (MainActivity) getActivity();
        if (getArguments().getString("from").equals("biginner")) {
            is_goal = "1";
            fund_category = getArguments().getString("cateId");
            goal_name = getArguments().getString("goalName");
            investment_type = getArguments().getString("orderType");
            goal_year = getArguments().getString("targetYear");
            goal_ammount = getArguments().getString("amountAmount");
            scheme_json = getArguments().getString("schemeJson");
            Log.e("Response", "IS_GOAL:" + is_goal + "\nFundCategory:" +
                    fund_category + "\nGoal Name:" +
                    goal_name + "\nInvestmentType:" +
                    investment_type + "\nGoalYear:" +
                    goal_year + "\nGoalAmmount:" +
                    goal_ammount + "\nSchemeJson:" +
                    scheme_json);

            if (orderType.equals("MONTHLY")) {
                if ((Integer.parseInt(getArguments().getString("sipDate")) - calendar.get(Calendar.DATE)) > 2) {
                    Calendar ctd = Calendar.getInstance();
                    ctd.set(Calendar.DATE, Integer.parseInt(getArguments().getString("sipDate")));
                    date = sdf.format(ctd.getTime());
                } else {
                    Calendar ctd = Calendar.getInstance();
                    ctd.set(Calendar.DATE, Integer.parseInt(getArguments().getString("sipDate")));
                    ctd.add(Calendar.MONTH, 1);
                    date = sdf.format(ctd.getTime());
                }
                setPurchaseFundSIP("", "", getArguments().getString("remark"), "", getArguments().getString("months") , date);
            } else if (orderType.equals("ONETIME")) {
                setPurchaseFundOneTime("", "", getArguments().getString("remark"), "");
            }
        } else {
            is_goal = "0";
            fund_category = "";
            goal_name = "";
            investment_type = "";
            goal_year = "";
            goal_ammount = "";
            scheme_json = getArguments().getString("schemeJson");

            if (orderType.equals("MONTHLY")) {
                dt = getArguments().getString("SIPDates");
                dt = dt.endsWith(",") ? dt.substring(0, (dt.length() - 1)).trim() : dt;
                dt = dt.startsWith(",") ? dt.substring(1, (dt.length())) : dt;
                String[] dates = dt.split(",");
                String da = "";
                for (int i = 0; i < dates.length; i++) {
                    if ((Integer.parseInt(dates[i]) - calendar.get(Calendar.DATE)) > 2) {
                        if (da.equals("") || da.equals("false")) {
                            Calendar ctd = Calendar.getInstance();
                            ctd.set(Calendar.DATE, Integer.parseInt(dates[i].trim()));
                            da = sdf.format(ctd.getTime());
                        }
                    } else {
                        da = "false";
                    }
                }
                if (da.equals("false") || da.equals("")) {
                    calendar.add(Calendar.MONTH, 1);
                    calendar.set(Calendar.DATE, Integer.parseInt(dates[0].trim()));
                    da = sdf.format(calendar.getTime());
                    date = da;
                } else {
                    date = da;
                }
                Log.e("SIPDate", "Available Dates:" + dt + "\nDate:" + date);
                setPurchaseFundSIP(getArguments().getString("schemeCode"),
                        getArguments().getString("ammount"),
                        getArguments().getString("remark"),
                        getArguments().getString("quantity"),
                        getArguments().getString("months") , date);
            } else if (orderType.equals("ONETIME")) {
                setPurchaseFundOneTime(getArguments().getString("schemeCode"),
                        getArguments().getString("ammount"),
                        getArguments().getString("remark"),
                        getArguments().getString("quantity"));
            }
        }
        return parentView;
    }

    void setPurchaseFundOneTime(String schemeCode, String ammount, String remark, String quantity) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.PurchaseFundOneTime);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("scheme_code", schemeCode);
        requestedServiceDataModel.putQurry("amount", ammount);
        requestedServiceDataModel.putQurry("remarks", remark);
        requestedServiceDataModel.putQurry("order_type", "ONETIME");
        requestedServiceDataModel.putQurry("quantity", quantity);
        requestedServiceDataModel.putQurry("is_goal", is_goal);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("fund_category", fund_category);
        requestedServiceDataModel.putQurry("goal_name", goal_name);
        requestedServiceDataModel.putQurry("investment_type", investment_type);
        requestedServiceDataModel.putQurry("goal_year", goal_year);
        requestedServiceDataModel.putQurry("goal_ammount", goal_ammount);
        requestedServiceDataModel.putQurry("scheme_json", scheme_json);
        requestedServiceDataModel.setWebServiceType("PURCHASE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        Log.e("ONETIME_req", "userid " + Common.getPreferences(contextd, "userID")
                + "\nscheme_code: " + schemeCode
                + "\namount: " + ammount
                + "\nremarks: " + remark
                + "\norder_type: " + "ONETIME"
                + "\nquantity: " + quantity
                + "\nis_goal: " + is_goal
                + "\nfund_category: " + fund_category
                + "\ngoal_name: " + goal_name
                + "\ninvestment_type: " + investment_type
                + "\ngoal_year: " + goal_year
                + "\ngoal_ammount: " + goal_ammount
                + "\nscheme_json: " + scheme_json);
        requestedServiceDataModel.execute();
    }

    void setPurchaseFundSIP(String schemeCode, String ammount, String remark, String quantity, String installment , String date) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.PurchaseFundMonthly);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("scheme_code", schemeCode);
        requestedServiceDataModel.putQurry("amount", ammount);
        requestedServiceDataModel.putQurry("remarks", remark);
        requestedServiceDataModel.putQurry("order_type", getArguments().getBoolean("isXSIP") ? "XSIP" : "SIP");
       // requestedServiceDataModel.putQurry("order_type","SIP");
        if (getArguments().getBoolean("isXSIP"))
            requestedServiceDataModel.putQurry("mandate_id", getArguments().getString("mandate_id"));

        Log.e("startingDate........" , date+"");


        requestedServiceDataModel.putQurry("startDate",  updateDate(date));
        requestedServiceDataModel.putQurry("frequency", "MONTHLY");
        requestedServiceDataModel.putQurry("installment", installment);
        requestedServiceDataModel.putQurry("quantity", quantity);
        requestedServiceDataModel.putQurry("is_goal", is_goal);
        requestedServiceDataModel.putQurry("fund_category", fund_category);
        requestedServiceDataModel.putQurry("goal_name", goal_name);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("investment_type", investment_type);
        requestedServiceDataModel.putQurry("goal_year", goal_year);
        requestedServiceDataModel.putQurry("goal_ammount", goal_ammount);
        requestedServiceDataModel.putQurry("scheme_json", scheme_json);
        requestedServiceDataModel.setWebServiceType("SIP-PURCHASE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        Log.e("SIP_req", "userid " + Common.getPreferences(contextd, "userID")
                + "\nscheme_code: " + schemeCode
                + "\namount: " + ammount
                + "\nremarks: " + remark
                + "\norder_type: " + "SIP"
                + "\nstartDate: " + date
                + "\nfrequency: " + "MONTHLY"
                + "\ninstallment: " + installment
                + "\nquantity: " + quantity
                + "\nis_goal: " + is_goal
                + "\nfund_category: " + fund_category
                + "\ngoal_name: " + goal_name
                + "\ninvestment_type: " + investment_type
                + "\ngoal_year: " + goal_year
                + "\ngoal_ammount: " + goal_ammount
                + "\nscheme_json: " + scheme_json);

        requestedServiceDataModel.execute();
    }

    void proceedPayment(String userid, String orders, String amount, String payment_type) {
        notifyTxt.setText(contextd.getResources().getString(R.string.redirectingPaymentGateWay));
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-payment.php");
        baseRequestData.setTag(ResponseType.DoPayment);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("orders", orders.trim());
        requestedServiceDataModel.putQurry("amount", amount.trim());
        requestedServiceDataModel.putQurry("paymemt_type", payment_type);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        // requestedServiceDataModel.putQurry("due_date", due_date);
        requestedServiceDataModel.putQurry("sip_registration_no", orders);
        requestedServiceDataModel.setWebServiceType("payment");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.PurchaseFundMonthly:
          //      Log.e("SIP-Order", json);
                try {
                    JSONObject obj = new JSONObject(json);
                    if (!getArguments().getString("from").equals("biginner")) {
                        if (obj.getJSONArray("schemes").getJSONObject(0).getString("status").equals("false")) {
                            withJustError(obj.getJSONArray("schemes").getJSONObject(0).getString("msg"), false);
                        } else {
                            String order_id = "", bseOrderno = "";
                            for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                                order_id += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("order_id").trim();
                                bseOrderno += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("bse_order_no").trim();
                            }
                            JSONArray amntArr = new JSONArray(scheme_json);
                            int amnt = 0;
                            for (int i = 0; i < amntArr.length(); i++) {
                                amnt += (int)Float.parseFloat(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                           //     amnt += Integer.parseInt(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                            }
                            String uid = Common.getPreferences(contextd, "userID"),
                                    bseno = bseOrderno.replace(" ", ""),
                                    amount = amnt + "";
                            Log.e("Request", "userid:" + Common.getPreferences(contextd, "userID")
                                    + "\norder_ids:" + order_id.substring(1, order_id.length())
                                    + "\nbse_order_nos:" + bseOrderno.substring(1, bseOrderno.length())
                                    + "\namount:" + amnt
                                    + "\norder_type:SIP");
                            bseno = bseno.replace(",", "");
                            SIPcreatedDialog(getArguments().getString("schemeName"), amount, getArguments().getString("sipFrom"), getArguments().getString("sipTo"), date);
//                            proceedPayment(uid, bseno, amount, "SIP");
                        }
                    } else {
                        boolean b = true;
                        String msg = "";
                        for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                            if (obj.getJSONArray("schemes").getJSONObject(0).getString("status").equals("false")) {
                                msg += "\n" + ((i + 1) + ". " + obj.getJSONArray("schemes").getJSONObject(0).getString("msg"));
                                b = false;
                            } else {
                                msg += "\n" + ((i + 1) + ". Success");
                            }
                        }
                        if (b) {
                            String order_id = "", bseOrderno = "";
                            for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                                order_id += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("order_id");
                                bseOrderno += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("bse_order_no");
                            }
                            JSONArray amntArr = new JSONArray(scheme_json);
                            int amnt = 0;
                            for (int i = 0; i < amntArr.length(); i++) {
                                amnt += (int)Float.parseFloat(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                           //     amnt += Integer.parseInt(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                            }

                            String uid = Common.getPreferences(contextd, "userID"),

                                    bseno = bseOrderno.replace(" ", ""),

                                    amount = amnt + "";
                            Log.e("Request", "userid:" + Common.getPreferences(contextd, "userID")
                                    + "\norder_ids:" + order_id.substring(1, order_id.length())
                                    + "\nbse_order_nos:" + bseOrderno.substring(1, bseOrderno.length())
                                    + "\namount:" + amnt
                                    + "\norder_type:SIP");
                            SIPcreatedDialog(getArguments().getString("goalName"),
                                    amount,
                                    getArguments().getString("sipFrom"),
                                    getArguments().getString("sipTo"), date);
//                            proceedPayment(uid, bseno, amount, "SIP");

                        } else {
                            withJustError(msg, false);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Common.showToast(contextd, message);
                break;
            case ResponseType.PurchaseFundOneTime:
              //  Log.e("res", json);
                Common.showToast(contextd, message);
                try {
                    JSONObject obj = new JSONObject(json);
                    if (!getArguments().getString("from").equals("biginner")) {
                        if (obj.getJSONArray("schemes").getJSONObject(0).getString("status").equals("false")) {
                            withJustError(obj.getJSONArray("schemes").getJSONObject(0).getString("msg"), false);
                        } else {
                            String order_id = "", bseOrderno = "";
                            for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                                order_id += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("order_id").trim();
                                bseOrderno += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("bse_order_no").trim();
                            }
                            JSONArray amntArr = new JSONArray(scheme_json);
                            int amnt = 0;
                            for (int i = 0; i < amntArr.length(); i++) {
                                amnt += (int)Float.parseFloat(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                            //    amnt += Integer.parseInt(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                            }

                            String uid = Common.getPreferences(contextd, "userID"),

                                    bseno = bseOrderno.substring(1, bseOrderno.length()).split(" ")[0].toString().trim()
                                            .equals("") ? bseOrderno.substring(1, bseOrderno.length()) : bseOrderno.substring(1, bseOrderno.length()).split(" ")[0].toString().trim(),

                                    amount = amnt + "";
                            Log.e("Request", "userid:" + Common.getPreferences(contextd, "userID")
                                    + "\norder_ids:" + order_id.substring(1, order_id.length())
                                    + "\nbse_order_nos:" + bseOrderno.substring(1, bseOrderno.length())
                                    + "\namount:" + amnt
                                    + "\norder_type:ONETIME");
                            bseno = bseno.replace(" ", "");
                            orderid=bseno = bseno.replace(" ", "");

                            proceedPayment(uid, bseno, amount, "ONETIME");
                   //         Log.d("proceedPayment.....", String.valueOf(proceedPayment));
//                            activity.getBackToHome();
//
                        }
                    } else {
                        boolean b = true;
                        String msg = "";
                        for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                            if (obj.getJSONArray("schemes").getJSONObject(0).getString("status").equals("false")) {
                                msg += "\n" + ((i + 1) + ". " + obj.getJSONArray("schemes").getJSONObject(0).getString("msg"));
                                b = false;
                            } else {
                                Log.e("Fund" + (i + 1), "success");
                            }
                        }
                        if (msg.trim().equals("")) {
                            String order_id = "", bseOrderno = "";
                            for (int i = 0; i < obj.getJSONArray("schemes").length(); i++) {
                                order_id += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("order_id");
                                bseOrderno += "," + obj.getJSONArray("schemes").getJSONObject(i).getString("bse_order_no");
                            }
                            JSONArray amntArr = new JSONArray(scheme_json);
                            int amnt = 0;
                            for (int i = 0; i < amntArr.length(); i++) {
                                amnt += (int) Float.parseFloat(amntArr.getJSONObject(i).getString("scheme_amount").trim());
                            }

                            String uid = Common.getPreferences(contextd, "userID"),

                                    bseno = bseOrderno.substring(1, bseOrderno.length()).split(" ")[0].toString().trim()
                                            .equals("") ? bseOrderno.substring(1, bseOrderno.length()) : bseOrderno.substring(1, bseOrderno.length()).split(" ")[0].toString().trim(),

                                    amount = amnt + "";
                            Log.e("Request", "userid:" + Common.getPreferences(contextd, "userID")
                                    + "\norder_ids:" + order_id.substring(1, order_id.length())
                                    + "\nbse_order_nos:" + bseOrderno.substring(1, bseOrderno.length())
                                    + "\namount:" + amnt
                                    + "\norder_type:ONETIME");
                            proceedPayment(uid, bseno, amount, "ONETIME");
                            orderid=bseno;
                  //          Log.d("proceedPayment123.....", String.valueOf(proceedPayment));


                        } else {
                            withJustError(msg, false);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.DoPayment:
              //  Log.d("DoPayment Response", json);
                proceedPayment = true;
                Intent i = new Intent(contextd, PaymentHelperActivity.class);
                i.putExtra("orderid",orderid);
                i.putExtra("rawHtml",message);
                startActivityForResult(i, 113);
                getActivity().finish();
              //  startActivityForResult(new Intent(contextd, PaymentHelperActivity.class).putExtra("rawHtml", message), 113);
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.PurchaseFundMonthly:
                try {
                    JSONObject jsonObject = new JSONObject(json);
              //      Log.e("res", json);
                    if (jsonObject.getString("retry").trim().equals("1"))
                        retryOnWebError(message, false);
                    else {
                        withJustError(message, false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Common.showToast(contextd, message);
                break;
            case ResponseType.PurchaseFundOneTime:
                try {
                    JSONObject jsonObject = new JSONObject(json);
            //        Log.e("res", json);
                    if (jsonObject.getString("retry").trim().equals("1"))
                        retryOnWebError(message, true);
                    else {
                        withJustError(message, false);
                    }
                } catch (JSONException
                        e) {
                    e.printStackTrace();
                }
                Common.showToast(contextd, message);
                break;
            case ResponseType.DoPayment:
             //   Log.e("response", json);
              //  Log.e("responseFailled payment",json);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 113 && resultCode == 113) {
            if (getArguments().getString("isInCart").equals("0")) {
                activity.getBackToHome();
            } else {
                activity.getBackToHomeAndRemoveFromCart(getArguments().getString("isInCart"));
            }
            //rate here
        }
    }

    private void retryOnWebError(final String msg, final boolean oneTime) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(contextd.getResources().getString(R.string.retry),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (getArguments().getString("from").equals("biginner")) {
                            is_goal = "1";
                            fund_category = getArguments().getString("cateId");
                            goal_name = getArguments().getString("goalName");
                            investment_type = getArguments().getString("orderType");
                            goal_year = getArguments().getString("targetYear");
                            goal_ammount = getArguments().getString("amountAmount");
                            scheme_json = getArguments().getString("schemeJson");

                            if (orderType.equals("MONTHLY")) {
                                setPurchaseFundSIP("", "", getArguments().getString("remark"), "", "" ,date);
                            } else if (orderType.equals("ONETIME")) {
                                setPurchaseFundOneTime("", "", getArguments().getString("remark"), "");
                            }
                        } else {
                            if (oneTime)
                                setPurchaseFundOneTime(getArguments().getString("schemeCode"),
                                        getArguments().getString("ammount"),
                                        getArguments().getString("remark"),
                                        getArguments().getString("orderType"));
                            else
                                setPurchaseFundSIP(getArguments().getString("schemeCode"),
                                        getArguments().getString("ammount"),
                                        getArguments().getString("remark"),
                                        getArguments().getString("orderType"),
                                        getArguments().getString("months") , date);
                        }
                        dialog.dismiss();
                    }
                });
        builder.setNegativeButton(contextd.getResources().getString(R.string.backToHome),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.getBackToHome();
                        dialog.dismiss();
                    }
                });

        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void withJustError(final String msg, final boolean oneTime) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg.replaceFirst("Failed ", ""));
        builder.setNegativeButton(contextd.getResources().getString(R.string.backAndValidate), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });


        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    AlertDialog sipDio;


    void SIPcreatedDialog(String fundName, String amount, String from, String to, String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("en"));
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(contextd, R.style.full_screen_dialog);
        View cust = LayoutInflater.from(contextd).inflate(R.layout.dio_sip_created, null);
        TextView schemeName = (TextView) cust.findViewById(R.id.sipSchemeName);
        TextView sipAmount = (TextView) cust.findViewById(R.id.sipAmount);
        TextView sipFrom = (TextView) cust.findViewById(R.id.sipFrom), sipTo = (TextView) cust.findViewById(R.id.sipTo);
        TextView sipPaymentDate = (TextView) cust.findViewById(R.id.sipPaymentDate);
        TextView btn = (TextView) cust.findViewById(R.id.sipGoHome);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sipDio.dismiss();
                if (getArguments().getString("isInCart").equals("0")) {
                    activity.getBackToHome();
                } else {
                    activity.getBackToHomeAndRemoveFromCart(getArguments().getString("isInCart"));
                }
            }
        });
        schemeName.setText(fundName);
        sipAmount.setText(Common.currencyString(amount + "", false));
        sipFrom.setText(from);
        sipTo.setText(to);
        Calendar ct = Calendar.getInstance();
        try {
            ct.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formatedAte = Common.getDateString(ct);
        sipPaymentDate.setText(formatedAte);
        builder.setView(cust);
        sipDio = builder.create();
        Window window = sipDio.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.width = WindowManager.LayoutParams.FILL_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawableResource(android.R.color.white);
        sipDio.setCancelable(false);
        sipDio.show();
    }


  public String updateDate(String date_object)
  {
      String update = "";
      try {
          Date date = sdf.parse(date_object);
          Calendar c = Calendar.getInstance();
          c.setTime(date);
          c.add(Calendar.DATE, 5);
          String dayLongName = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, new Locale("en"));
          if(dayLongName.equalsIgnoreCase("Saturday"))
          {
              c.add(Calendar.DATE, 7);
          }
          else if(dayLongName.equalsIgnoreCase("Sunday"))
          {
              c.add(Calendar.DATE, 6);
          }
          Date currentDatePlusOne = c.getTime();
          update =  sdf.format(c.getTime());

      } catch (ParseException e) {
          e.printStackTrace();
      }
       return update;

  }


}

