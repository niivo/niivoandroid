package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by deepak on 9/6/17.
 */

public class InvestedGoalDetailFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Context contextd;
    TextView goalName, goalTarget, goalInvestmentType, goalAmountInvested, goalInvestmentValue,
            firstSchemeName, firstValue, secondSchemeName, secondValue, thirdSchemeName, thirdValue;
    ImageView categoryImage;
    LinearLayout firstFund, secondFund, thirdFund;

    Button cancelGoal;
    //Non ui vars
    JSONObject obj;
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    String goalId = "", orderId = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_invested_goal, container, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.myMoney));

        activity = (MainActivity) getActivity();
        goalName = (TextView) rootView.findViewById(R.id.detailGoalName);
        goalTarget = (TextView) rootView.findViewById(R.id.detailGoalTarget);
        goalInvestmentType = (TextView) rootView.findViewById(R.id.detailGoalInvestmentType);
        goalAmountInvested = (TextView) rootView.findViewById(R.id.goalAmountInvested);
        goalInvestmentValue = (TextView) rootView.findViewById(R.id.goalInvestmentValue);
        firstSchemeName = (TextView) rootView.findViewById(R.id.firstFundName);
        firstValue = (TextView) rootView.findViewById(R.id.firstFundValue);
        secondSchemeName = (TextView) rootView.findViewById(R.id.secondFundName);
        secondValue = (TextView) rootView.findViewById(R.id.secondFundValue);
        thirdSchemeName = (TextView) rootView.findViewById(R.id.thirdFundName);
        thirdValue = (TextView) rootView.findViewById(R.id.thirdFundValue);
        categoryImage = (ImageView) rootView.findViewById(R.id.detailGoalCateImage);
        cancelGoal = (Button) rootView.findViewById(R.id.cancelGoalBtn);


        firstFund = (LinearLayout) rootView.findViewById(R.id.firstFund);
        secondFund = (LinearLayout) rootView.findViewById(R.id.secondFund);
        thirdFund = (LinearLayout) rootView.findViewById(R.id.thirdFund);
        firstFund.setOnClickListener(this);
        secondFund.setOnClickListener(this);
        thirdFund.setOnClickListener(this);
        cancelGoal.setOnClickListener(this);
        setDetails(getArguments().getString("goalDetail"));
        return rootView;
    }

    void setDetails(String json) {
        firstFund.setVisibility(View.GONE);
        secondFund.setVisibility(View.GONE);
        thirdFund.setVisibility(View.GONE);

        try {
            obj = new JSONObject(json);
            goalName.setText(obj.getJSONObject("goal_detail").getString("goal_name"));
            goalId = obj.getJSONObject("goal_detail").getString("id");
            orderId = obj.getString("order_id");
            goalTarget.setText(obj.getJSONObject("goal_detail").getString("goal_year"));
            goalInvestmentType.setText(obj.getJSONObject("goal_detail").getString("investment_type").equals("ONETIME") ? "One Time" : "SIP");
            String cateid = obj.getJSONObject("goal_detail").getString("fund_category");
            cancelGoal.setVisibility(obj.getJSONObject("goal_detail").getString("investment_type").equals("ONETIME") ? View.GONE : View.VISIBLE);
            if (cateid.equals("1")) {
                categoryImage.setImageResource(R.drawable.retirementfund);
            } else if (cateid.equals("2")) {
                categoryImage.setImageResource(R.drawable.childswedding);
            } else if (cateid.equals("3")) {
                categoryImage.setImageResource(R.drawable.goal_tax);
            } else if (cateid.equals("4")) {
                categoryImage.setImageResource(R.drawable.vacation);
            } else if (cateid.equals("5")) {
                categoryImage.setImageResource(R.drawable.buyingcar);
            } else if (cateid.equals("6")) {
                categoryImage.setImageResource(R.drawable.buyinghouse);
            } else if (cateid.equals("7")) {
                categoryImage.setImageResource(R.drawable.startingbusiness);
            } else if (cateid.equals("8")) {
                categoryImage.setImageResource(R.drawable.startinganemergencyfund);
            } else if (cateid.equals("9")) {
                categoryImage.setImageResource(R.drawable.wealthcreation);
            } else {
                categoryImage.setImageResource(R.drawable.rupee);
            }
            float amntInvested = 0, investedValue = 0;
            for (int i = 0; i < obj.getJSONArray("order_detail").length(); i++) {
                amntInvested += Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("amount"));
                investedValue += (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("nav")) *
                        Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("quantity")));
                if (i == 0) {
                    firstFund.setVisibility(View.VISIBLE);
                    firstSchemeName.setText(obj.getJSONArray("order_detail").getJSONObject(i).getString("scheme_name"));
                    firstValue.setText(Common.currencyString(String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("quantity")))), true));
                } else if (i == 1) {
                    secondFund.setVisibility(View.VISIBLE);
                    secondSchemeName.setText(obj.getJSONArray("order_detail").getJSONObject(i).getString("scheme_name"));
                    secondValue.setText(Common.currencyString(String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("quantity")))), true));
                } else if (i == 2) {
                    thirdFund.setVisibility(View.VISIBLE);
                    thirdSchemeName.setText(obj.getJSONArray("order_detail").getJSONObject(i).getString("scheme_name"));
                    thirdValue.setText(Common.currencyString(String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("quantity")))), true));
                }

            }
            goalAmountInvested.setText(Common.currencyString(String.format(new Locale("en"), "%.1f", amntInvested), false));
            goalInvestmentValue.setText(Common.currencyString(String.format(new Locale("en"), "%.1f", investedValue), false));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        Fragment frag = new InvestedSchemeDetailFragment();
        Bundle b = new Bundle();
        switch (v.getId()) {
            case R.id.firstFund:
                try {
                    b.putString("from", "goal");
                    b.putString("orderType", obj.getString("order_type"));
                    b.putString("ammountInvested", obj.getJSONArray("order_detail").getJSONObject(0).getString("amount"));
                    b.putString("investmentValue", String.format(new Locale("en"), "%.1f", (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("nav")) *
                            Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("quantity")))));
                    b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("quantity"))) -
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("amount"))));
                    b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("nav"))));
                    b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(0).getString("quantity"))));
                    b.putString("fundClass", obj.getJSONArray("order_detail").getJSONObject(0).getString("fund_class"));
                    b.putString("investmentType", obj.getString("order_type").equals("ONETIME") ? "One Time" : "SIP");
                    b.putString("paidInstallment", "N/A");
                    b.putString("schemeName", obj.getJSONArray("order_detail").getJSONObject(0).getString("scheme_name"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.secondFund:
                try {
                    b.putString("from", "goal");
                    b.putString("orderType", obj.getString("order_type"));
                    b.putString("ammountInvested", obj.getJSONArray("order_detail").getJSONObject(1).getString("amount"));
                    b.putString("investmentValue", String.format(new Locale("en"), "%.1f", (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("nav")) *
                            Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("quantity")))));
                    b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("quantity"))) -
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("amount"))));
                    b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("nav"))));
                    b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(1).getString("quantity"))));
                    b.putString("fundClass", obj.getJSONArray("order_detail").getJSONObject(1).getString("fund_class"));
                    b.putString("investmentType", obj.getString("order_type").equals("ONETIME") ? "One Time" : "SIP");
                    b.putString("paidInstallment", "N/A");
                    b.putString("schemeName", obj.getJSONArray("order_detail").getJSONObject(1).getString("scheme_name"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.thirdFund:
                try {
                    b.putString("from", "goal");
                    b.putString("orderType", obj.getString("order_type"));
                    b.putString("ammountInvested", obj.getJSONArray("order_detail").getJSONObject(2).getString("amount"));
                    b.putString("investmentValue", String.format(new Locale("en"), "%.1f", (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("nav")) *
                            Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("quantity")))));
                    b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
                            (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("nav")) *
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("quantity"))) -
                                    Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("amount"))));
                    b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("nav"))));
                    b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(2).getString("quantity"))));
                    b.putString("fundClass", obj.getJSONArray("order_detail").getJSONObject(1).getString("fund_class"));
                    b.putString("investmentType", obj.getString("order_type").equals("ONETIME") ? "One Time" : "SIP");
                    b.putString("paidInstallment", "N/A");
                    b.putString("schemeName", obj.getJSONArray("order_detail").getJSONObject(2).getString("scheme_name"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancelGoalBtn:
                if (obj != null) {
                    try {
                        if (!obj.getJSONObject("goal_detail").getString("investment_type").equals("ONETIME")) {
                            cancelGoal();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
        frag.setArguments(b);

        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.frame_container, frag).addToBackStack(null).
                commit();
    }

    void cancelSIP(String userid, String orderid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid.trim());
        requestedServiceDataModel.putQurry("orderid", orderid.trim());
        requestedServiceDataModel.putQurry("is_goal", "1");
        requestedServiceDataModel.putQurry("goal_id", goalId);
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                whenSuccess(message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(json);
                    if (jsonObject.has("retry")) {
                        if (jsonObject.getString("retry").trim().equals("1")) {
                            retryLastRequest(message);
                        }
                    } else {
                        Common.showToast(contextd, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void whenSuccess(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.home), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.getBackToPortfolio();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void retryLastRequest(String msg) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setNegativeButton(contextd.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), orderId);
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void cancelGoal() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(contextd.getResources().getString(R.string.cancelMonthlyBasedGoal));
        builder.setPositiveButton(contextd.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cancelSIP(Common.getPreferences(contextd, "userID"), orderId);
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
