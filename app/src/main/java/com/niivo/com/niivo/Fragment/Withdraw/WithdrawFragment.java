package com.niivo.com.niivo.Fragment.Withdraw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Activity.WithDrawOTPActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;


/**
 * Created by andro on 22/5/17.
 */

public class WithdrawFragment extends Fragment {
    Activity _activity;
    View parentView;
    ImageButton back;
    TextView fundGoalName, investedAmnt, currentAmnt, withdrawableAmnt, errorTxt, remainingBal;
    EditText withdrawAmount;
    RadioButton withdrawFull, withdrawPartial;
    LinearLayout partialLL;
    //Non ui Vars
    Context contextd;
    MainActivity activity;
    String schemeCode = "", id = "", lang = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        contextd = container.getContext();
        parentView = inflater.inflate(R.layout.fragment_withdraw, null, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.withDraw));
        lang = Common.getLanguage(contextd);
        activity = (MainActivity) getActivity();
        TextView button_withfraw1 = (TextView) parentView.findViewById(R.id.button_withfraw1);
        button_withfraw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtils.hideKeyboard(contextd);
                if (validate()) {
                    Intent intent = new Intent(getActivity(), WithDrawOTPActivity.class);
                    intent.putExtra("amount", withdrawFull.isChecked() ? (totalCrnt + "") : (withdrawAmount.getText().toString().trim()));
                    intent.putExtra("schemeCode", schemeCode);
                    intent.putExtra("id", id);
                    intent.putExtra("full_withdraw", withdrawFull.isChecked() ? "Y" : "N");
                    startActivityForResult(intent, 101);

                }
            }
        });
        fundGoalName = (TextView) parentView.findViewById(R.id.wdFundName);
        investedAmnt = (TextView) parentView.findViewById(R.id.wdFundAmntInvested);
        currentAmnt = (TextView) parentView.findViewById(R.id.wdFundAmntCurrent);
        withdrawableAmnt = (TextView) parentView.findViewById(R.id.wdWithdrawableAmnt);
        errorTxt = (TextView) parentView.findViewById(R.id.wdErrorTxt);
        remainingBal = (TextView) parentView.findViewById(R.id.wdRemainingBal);
        withdrawAmount = (EditText) parentView.findViewById(R.id.wdWithdAmount);
        withdrawFull = (RadioButton) parentView.findViewById(R.id.withdrawFull);
        withdrawPartial = (RadioButton) parentView.findViewById(R.id.withdrawPartial);
        partialLL = (LinearLayout) parentView.findViewById(R.id.partialLL);
        partialLL.setVisibility(View.GONE);
        withdrawFull.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    partialLL.setVisibility(View.GONE);
                    Common.hideKeyboard(_activity);
                } else {
                    partialLL.setVisibility(View.VISIBLE);
                }
            }
        });
        withdrawAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!withdrawAmount.getText().toString().trim().equals("")) {
                    if (Float.parseFloat(withdrawAmount.getText().toString().trim()) > totalCrnt) {
                        withdrawAmount.setError(contextd.getResources().getString(R.string.youCannotWithdraw));
                    } else {
                        withdrawAmount.setError(null);
                        errorTxt.setText("");
                    }
                    remainingBal.setText(String.format(new Locale("en"), "%.2f", (totalCrnt - Float.parseFloat(withdrawAmount.getText().toString().trim()))) + "");
                } else if (withdrawAmount.getText().toString().startsWith("0")) {
                    withdrawAmount.setText("");
                } else {
                    remainingBal.setText(tCr + "");
                }
            }
        });
        setWithdrawDetails();
        return parentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == 101) {
                if (data.getStringExtra("done").equals("done")) {
                    activity.getBackToPortfolio();
                }
            }
        }
    }

    boolean validate() {
        boolean b = false;
        if (withdrawFull.isChecked()) {
            b = true;
        } else if (withdrawAmount.getText().toString().trim().equals("")) {
            withdrawAmount.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (withdrawAmount.getText().toString().trim().length() <= 1) {
            b = false;
            withdrawAmount.setError(contextd.getResources().getString(R.string.invalidAmount));
            errorTxt.setText(contextd.getResources().getString(R.string.youHaveToChoose));
        } else if (Float.parseFloat(withdrawAmount.getText().toString()) > totalCrnt) {
            b = false;
            withdrawAmount.setError(contextd.getResources().getString(R.string.invalidAmount));
            errorTxt.setText(contextd.getResources().getString(R.string.ammountCannotExceed));
        } else {
            b = true;
        }
        return b;
    }

    float crAmnt = 0, wdAmnt = 100, totalCrnt = 0;
    String tCr = "";

    void setWithdrawDetails() {
        try {
            JSONObject obj = new JSONObject(getArguments().getString("fund").toString());
            fundGoalName.setText(obj.getString("is_goal").equals("0")
                    ? (lang.equals("en")
                    ? obj.getString("FUND_NAME")
                    : lang.equals("hi")
                    ? obj.getString("FUNDS_HINDI")
                    : lang.equals("gu")
                    ? obj.getString("FUNDS_GUJARATI")
                    : lang.equals("mr")
                    ? obj.getString("FUNDS_MARATHI")
                    : obj.getString("FUND_NAME"))
                    : obj.getJSONObject("goal_detail").getString("goal_name"));
            schemeCode = obj.getString("scheme_code").toString().trim();
            id = obj.getString("id").toString().trim();

            if (obj.getString("is_goal").equals("0")) {
                if (obj.getString("withdrawQty").equals("0")) {
                    totalCrnt = (Float.parseFloat(obj.getString("nav")) * Float.parseFloat(obj.getString("quantity")));
                    String st = String.format(new Locale("en"), "%.2f", totalCrnt);
                    investedAmnt.setText(Common.currencyString(obj.getString("amount"), false));
                    currentAmnt.setText(Common.currencyString(st, false));
                    tCr = st;
                    Log.e("TotalCurrentValue", totalCrnt + "");
                    crAmnt = Float.parseFloat(String.format(new Locale("en"), "%.2f", (Float.parseFloat(obj.getString("current_value")))));
                } else {
                    totalCrnt = (Float.parseFloat(obj.getString("nav")) * (Float.parseFloat(obj.getString("quantity")) - Float.parseFloat(obj.getString("withdrawQty"))));
                    String st = String.format(new Locale("en"), "%.2f", totalCrnt);
                    investedAmnt.setText(Common.currencyString(((int) (Float.parseFloat(obj.getString("amount")) - Float.parseFloat(obj.getString("withdrawAmt")))) + "", false));
                    currentAmnt.setText(Common.currencyString(st, false));
                    tCr = st;
                    Log.e("TotalCurrentValue", totalCrnt + "");
                    crAmnt = Float.parseFloat(String.format(new Locale("en"), "%.2f", (totalCrnt)));
                }
            } else {
                int cnt = 0;
                for (int i = 0; i < obj.getJSONArray("order_detail").length(); i++) {
                  //  cnt += Integer.parseInt(obj.getJSONArray("order_detail").getJSONObject(i).getString("amount").trim());
                    cnt += (int)Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("amount").trim());
                    totalCrnt = totalCrnt + (Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("nav")) * Float.parseFloat(obj.getJSONArray("order_detail").getJSONObject(i).getString("quantity")));
                }
                Log.e("current", totalCrnt + "");
                String st = String.format(new Locale("en"), "%.2f", totalCrnt);
                investedAmnt.setText(Common.currencyString(cnt + "", false));
                currentAmnt.setText(Common.currencyString(st, false));
                tCr = st;
                crAmnt = Float.parseFloat(String.format(new Locale("en"), "%.2f", Float.parseFloat(obj.getString("current_value"))));
            }
            withdrawableAmnt.setText(String.format(new Locale("en"), "%.2f", totalCrnt) + "");
            remainingBal.setText(String.format(new Locale("en"), "%.2f", (totalCrnt - wdAmnt)) + "");
            withdrawAmount.setText(String.format(new Locale("en"), "%.0f", wdAmnt) + "");
            errorTxt.setText("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
