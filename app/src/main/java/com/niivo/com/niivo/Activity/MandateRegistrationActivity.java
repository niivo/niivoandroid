package com.niivo.com.niivo.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 11/10/18.
 */

public class MandateRegistrationActivity extends AppCompatActivity implements ResponseDelegate {
    WebView helperWebView;
    ImageView back;
    Button backToHome;
    ProgressDialog pd;
    SwipeRefreshLayout swipeRefresh;
    TextView mandateid, mandatedate,mandateamount,status,backaccount,gotoHome;
    //Non ui vars
    boolean mandateSuccess = false;
    String url = "";
    String mendate_id,mandatePrice,accountNumber;
    RequestedServiceDataModel requestedServiceDataModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mandate_auth);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        helperWebView = (WebView) findViewById(R.id.helperWebView);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        backToHome = (Button) findViewById(R.id.backToHome);
        backToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("mandateSuccess", mandateSuccess);
                MandateRegistrationActivity.this.setResult(RESULT_CANCELED, i);
                MandateRegistrationActivity.this.finish();
            }
        });
        pd = new ProgressDialog(this);
        pd.setMessage(this.getResources().getString(R.string.loadingPleaseWait));
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();
        url = getIntent().getStringExtra("url");
        loadWebData(url);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                helperWebView.reload();
                swipeRefresh.setRefreshing(true);
            }
        });
    }

    void loadWebData(String url) {
        helperWebView.getSettings().setJavaScriptEnabled(true);
        helperWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        helperWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        helperWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        helperWebView.getSettings().setSupportZoom(true);
        helperWebView.getSettings().setBuiltInZoomControls(true);
        helperWebView.getSettings().setSupportMultipleWindows(true);
        helperWebView.setWebChromeClient(new CustomChromeClent());
        helperWebView.setWebViewClient(new CustomWebClient());
        helperWebView.loadUrl(url);
    }


    class CustomWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e("pageStart", url.trim());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("UrlLoading", url);
            view.loadUrl(url);
            if (!pd.isShowing())
                pd.show();
            return true;
        }
        void popup() {
            final Dialog dialog = new Dialog(MandateRegistrationActivity.this);
            dialog.setContentView(R.layout.mandate_popup);
            dialog.setTitle("Connection Error");
            dialog.getWindow().getAttributes().windowAnimations = R.anim.slide_in_left;
            status = (TextView) dialog.findViewById(R.id.status);
            gotoHome = (TextView) dialog.findViewById(R.id.gotoHome);

            status.setText(getResources().getString(R.string.mandateerror));
            gotoHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MandateRegistrationActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
            if (swipeRefresh.isRefreshing())
                swipeRefresh.setRefreshing(false);
            Log.e("pageFinished", url.trim());
            view.evaluateJavascript("document.getElementById('lblStatus').innerHTML;",
                    new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String html) {
                            Log.e("html", html + "");
                            if (html != null && html.contains("success")) {
                                Log.d("html.lblStatus", String.valueOf(html.contains("success")));
                                mandateSuccess = true;
                                getMandateStatus();
                                Intent intent = new Intent(MandateRegistrationActivity.this, MandateSuccessActivity.class);
                                startActivity(intent);
                                finish();
                                //popup();
                               // backToHome.setVisibility(View.VISIBLE);
                                back.setVisibility(View.GONE);
                            }
                        }
            });
        }
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            pd.dismiss();
            if (swipeRefresh.isRefreshing())
                swipeRefresh.setRefreshing(false);
            Log.e("receivedError", error.getDescription().toString().trim());
            popup();
        }
        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
            pd.dismiss();
            if (swipeRefresh.isRefreshing())
                swipeRefresh.setRefreshing(false);
            Log.e("HttpAuthRequest", host.trim());
        }
    }

    class CustomChromeClent extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {

            WebView newWebView = new WebView(MandateRegistrationActivity.this);
            newWebView.getSettings().setJavaScriptEnabled(true);
            newWebView.getSettings().setSupportZoom(true);
            newWebView.getSettings().setBuiltInZoomControls(true);
            newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            newWebView.getSettings().setSupportMultipleWindows(true);
            view.addView(newWebView);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();
            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    try {
                        Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!pd.isShowing())
                        pd.show();
                    Log.e("NewUrl", url);
                }
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.e("NewUrl", url);
                    try {
                        Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    view.loadUrl(url);
                    if (!pd.isShowing())
                        pd.show();
                    return true;
                }
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    Log.e("Chrome->WebView->FinishedUrl", url);
                    pd.dismiss();
                    if (swipeRefresh.isRefreshing())
                        swipeRefresh.setRefreshing(false);
                    view.evaluateJavascript("document.getElementById('lblStatus').innerHTML;",
                            new ValueCallback<String>() {
                                @Override
                                public void onReceiveValue(String html) {
                                    Log.e("html", html + "");
                                    if (html != null && html.contains("success")) {
                                        mandateSuccess = true;
                                        getMandateStatus();
                                        Intent intent = new Intent(MandateRegistrationActivity.this, MandateSuccessActivity.class);
                                        startActivity(intent);
                                        finish();
                                        //popup();
                                        //backToHome.setVisibility(View.VISIBLE);
                                        back.setVisibility(View.GONE);
                                    }
                                }
                    });
                }
                void popup() {
                    final Dialog dialog = new Dialog(MandateRegistrationActivity.this);
                    dialog.setContentView(R.layout.mandate_popup);
                    dialog.setTitle("Connection Error");
                    dialog.getWindow().getAttributes().windowAnimations = R.anim.slide_in_left;
                    status = (TextView) dialog.findViewById(R.id.status);
                    status.setText(getResources().getString(R.string.mandatestatus));
                    gotoHome = (TextView) dialog.findViewById(R.id.gotoHome);
                    gotoHome.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(MandateRegistrationActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    dialog.show();
                }
                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
                    //Your code to do
                    popup();
                    Toast.makeText(getApplicationContext(), "Your Internet Connection May not be active Or some Problems Please try again " + error , Toast.LENGTH_LONG).show();

                }
            });
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && helperWebView.canGoBack()) {
            helperWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (helperWebView.canGoBack())
            helperWebView.goBack();
        else {
            if (mandateSuccess) {
                Intent i = new Intent();
                i.putExtra("mandateSuccess", mandateSuccess);
                MandateRegistrationActivity.this.setResult(RESULT_CANCELED, i);
                MandateRegistrationActivity.this.finish();
            } else
                new AlertDialog.Builder(MandateRegistrationActivity.this)
                        .setMessage("Are you sure you want to cancel the SIP mandate registration.")//By canceling this, you will not able to re-register the E-mandate at least for 3 days
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                Intent i = new Intent();
                                i.putExtra("mandateSuccess", mandateSuccess);
                                MandateRegistrationActivity.this.setResult(RESULT_CANCELED, i);
                                MandateRegistrationActivity.this.finish();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create().show();
        }
    }

    void getMandateStatus() {
        requestedServiceDataModel = new RequestedServiceDataModel(MandateRegistrationActivity.this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mandate.php");
        baseRequestData.setTag(ResponseType.MandateStatus);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("MANDATE-SUCCESS");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(getApplicationContext(), "userID"));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        switch (baseRequestData.getTag()) {
            case ResponseType.MandateStatus:
                break;
       }
      }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

    }
}
