package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCNomineeDetailsFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView skipBtn, continueBtn;
    RightGravityTextInputLayout docNomineeNameTIL, docRelationWithTIL, docNomineeAddressTIL;
    TextInputEditText docNomineeName, docRelationWith, docNomineeAddress;

    //non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    String userId = "";
    boolean isNomineeDetailsSubmitted = false, isSkipped = false;
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            docNomineeNameTIL.setError(null);
            docRelationWithTIL.setError(null);
            docNomineeAddressTIL.setError(null);
            docNomineeNameTIL.setErrorEnabled(false);
            docRelationWithTIL.setErrorEnabled(false);
            docNomineeAddressTIL.setErrorEnabled(false);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kyc_nominee_details, container, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText("NOMINEE DETAILS");
        skipBtn = (TextView) rootView.findViewById(R.id.skipBtn);
        continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
        docNomineeNameTIL = (RightGravityTextInputLayout) rootView.findViewById(R.id.docNomineeNameTIL);
        docRelationWithTIL = (RightGravityTextInputLayout) rootView.findViewById(R.id.docRelationWithTIL);
        docNomineeAddressTIL = (RightGravityTextInputLayout) rootView.findViewById(R.id.docNomineeAddressTIL);
        docNomineeName = (TextInputEditText) rootView.findViewById(R.id.docNomineeName);
        docRelationWith = (TextInputEditText) rootView.findViewById(R.id.docRelationWith);
        docNomineeAddress = (TextInputEditText) rootView.findViewById(R.id.docNomineeAddress);
        skipBtn.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
        userId = Common.getPreferences(contextd, "userID");
        docNomineeName.addTextChangedListener(watcher);
        docNomineeName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        docRelationWith.addTextChangedListener(watcher);
        docRelationWith.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        docNomineeAddress.addTextChangedListener(watcher);
        docNomineeAddress.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        if(getArguments().getBoolean("preRegistered"))
            setUpPreRegistration();
        return rootView;
    }

    boolean preRegistered = false;
    ItemUserDetail userDetail;
    int step = 0;

    void setUpPreRegistration() {
        preRegistered = true;
        userDetail = (ItemUserDetail) getArguments().getSerializable("userDetail");
        step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
        if (step > 3) {
            docNomineeName.setText(userDetail.getData().getNominee_name());
            docRelationWith.setText(userDetail.getData().getNominee_relationship());
            docNomineeAddress.setText(userDetail.getData().getNominee_address());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skipBtn:
                isSkipped = true;
                getActivity().onBackPressed();
                break;
            case R.id.continueBtn:
                if (validate()) {
                    setNomineeDetails();
                }
                break;
        }
    }

    boolean validate() {
        boolean b = false;
        if (docNomineeName.getText().toString().trim().equals("")) {
            b = false;
            docNomineeNameTIL.setError("Required Field..!");
        } else if (docRelationWith.getText().toString().trim().equals("")) {
            b = false;
            docRelationWithTIL.setError("Required Field..!");
        } else if (docNomineeAddress.getText().toString().trim().equals("")) {
            b = false;
            docNomineeAddressTIL.setError("Required Field..!");
        } else
            b = true;
        return b;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isNomineeDetailsSubmitted", isNomineeDetailsSubmitted);
        i.putExtra("isSkiped", isSkipped);
        getTargetFragment().onActivityResult(
                ResponseType.KYC_NomineeDetails,
                isNomineeDetailsSubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED,
                i);
    }

    void setNomineeDetails() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_NomineeDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("nominee_name", docNomineeName.getText().toString().trim());
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("nominee_relationship", docRelationWith.getText().toString().trim());
        requestedServiceDataModel.putQurry("nominee_address", docNomineeAddress.getText().toString().trim());
        requestedServiceDataModel.setWebServiceType("ADDNOMINEE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_NomineeDetails:
            //    Log.e("setNomineeDetails", json);
                isNomineeDetailsSubmitted = true;
                Common.showToast(contextd, message);
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_NomineeDetails:
          //      Log.e("setNomineeDetails", json);
                break;
        }
    }
}
