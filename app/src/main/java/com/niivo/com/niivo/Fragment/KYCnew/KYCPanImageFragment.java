package com.niivo.com.niivo.Fragment.KYCnew;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.niivo.com.niivo.BuildConfig;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.AWS_Helper.ProgressViewDialog;
import com.niivo.com.niivo.Utils.AWS_Helper.S3UploadService;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.PickerHelper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCPanImageFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView continueBtn, panCardTV;
    LinearLayout camImgBtn;
    ImageView panImg;

    //Non ui vars
    Context contextd;
    boolean isUploaded = false;
    String panRef = "";
    File imgFile = null;
    PickerHelper helper;
    ProgressViewDialog progressViewDialog;
    private static final int PICK_IMAGE_CAMERA = 111;
    private static final int PICK_IMAGE_GALLERY = 222;
    Uri mHighQualityImageUri=null;
    RequestedServiceDataModel requestedServiceDataModel;
    String pancarddata;
    String first_name,father_name,dob,panno,pandetails;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kyc_pan_image, container, false);
        contextd = container.getContext();
        continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
        camImgBtn = (LinearLayout) rootView.findViewById(R.id.camImgBtn);
        panImg = (ImageView) rootView.findViewById(R.id.panCardImage);
        panCardTV = (TextView) rootView.findViewById(R.id.panCardTV);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.basic_details);
        panCardTV.setText(getArguments().getString("panCard").toUpperCase());
        continueBtn.setOnClickListener(this);
        camImgBtn.setOnClickListener(this);
        panImg.setOnClickListener(this);
        doPermissionWork();
        return rootView;
    }

    void doPermissionWork() {
        progressViewDialog = ProgressViewDialog.with(contextd);
        progressViewDialog.setCancelable(false);
        helper = PickerHelper.with(contextd, KYCPanImageFragment.this);
        if ((ContextCompat.checkSelfPermission(contextd, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 125);
        }
    }
    private Uri generateTimeStampPhotoFileUri() {
        Uri photoFileUri = null;
        File outputDir = getPhotoDirectory();
        if (outputDir != null) {
            File photoFile = new File(outputDir, System.currentTimeMillis() + ".jpg");
            //photoFileUri = Uri.fromFile(photoFile);
            photoFileUri =  FileProvider.getUriForFile(contextd, BuildConfig.APPLICATION_ID + ".provider",photoFile);
        }
        return photoFileUri;
    }

    private File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageStagte = Environment.getExternalStorageState();
        if (externalStorageStagte.equals(Environment.MEDIA_MOUNTED)) {
            File photoDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            outputDir = new File(photoDir, getString(R.string.app_name));
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(contextd, "Failed to create directory " + outputDir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    outputDir = null;
                }
        }
        return outputDir;
    }
    private void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();
                        mHighQualityImageUri = generateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                       // intent.setClassName("com.android.camera", "com.android.camera.Camera");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camImgBtn:
            case R.id.panCardImage:
                selectImage();
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.OFF)
//                        .setAllowFlipping(false)
//                        .setAutoZoomEnabled(true)
//                        .setBorderCornerColor(Color.parseColor("#42E598"))
//                        .setBorderLineColor(Color.parseColor("#3CB5B9"))
//                        .setMinCropResultSize(640, 360)
//                        .setAspectRatio(16, 9)
//                        .start(contextd, this);
                break;
            case R.id.continueBtn:
                if (imgFile == null) {
                    Common.showDialog(contextd, getString(R.string.pan_card_image_is_important));
                } else {
                    progressViewDialog.show();
                    LocalBroadcastManager.getInstance(contextd).registerReceiver(getUploadCallBack, new IntentFilter("onUploadStatus"));
                    Intent i = new Intent(contextd, S3UploadService.class);
                    i.putExtra("userId", getArguments().getString("userId"));
                    i.putExtra("fileType", "PAN_CARD");
                    i.putExtra("file",    imgFile);
                    contextd.startService(i);

                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && mHighQualityImageUri != null) {
                   Uri resultUri=  mHighQualityImageUri;
                    try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                    bitmap = helper.getResizedBitmap(bitmap, 1920);
                    bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                    imgFile = helper.saveBitmapToFile(bitmap, "pan_card");
//                    pancarddata = Common.getBitmapBase64(bitmap);
                    Picasso.with(contextd)
                            .load(imgFile)
                            .memoryPolicy(MemoryPolicy.NO_STORE)
                            .into(panImg);
                    camImgBtn.setVisibility(View.INVISIBLE);
                    pancarddata = Common.getBitmapBase64(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                 PythonService();
            } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {
                    Uri resultUri = data.getData();
                    try {
                      Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                      bitmap = helper.getResizedBitmap(bitmap, 1920);
                        bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                      imgFile = helper.saveBitmapToFile(bitmap, "pan_card");
//                        pancarddata = Common.getBitmapBase64(bitmap);
 //                       Log.d("pancarddata....",pancarddata);

                      Picasso.with(contextd).load(imgFile).memoryPolicy(MemoryPolicy.NO_STORE).into(panImg);
                      camImgBtn.setVisibility(View.INVISIBLE);
                      } catch (IOException e) {
                         e.printStackTrace();
                     }
//                PythonService();
                }
                super.onActivityResult(requestCode, resultCode, data);
            }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(contextd).unregisterReceiver(getUploadCallBack);
        Intent i = new Intent();
        i.putExtra("panRef", panRef);
        Fragment fragment = getTargetFragment();
        fragment.onActivityResult(121, isUploaded ?
                RESULT_OK : Activity.RESULT_CANCELED, i);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    BroadcastReceiver getUploadCallBack = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressViewDialog.dismiss();
            if (intent.getStringExtra("state").equals("COMPLETED")) {
                isUploaded = true;
                panRef = intent.getStringExtra("panRef");
                Log.e("panRef", panRef);
                getActivity().onBackPressed();
            } else {
                Common.showToast(contextd, contextd.getResources().getString(R.string.upload_failed_try_again));
            }
        }
    };


    void PythonService() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd,this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("template");
        baseRequestData.setTag(ResponseType.PancardService);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("expression",pancarddata);
        requestedServiceDataModel.setWebServiceType("PAN");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.pythonServiceExcute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        switch (baseRequestData.getTag()) {
            case ResponseType.PancardService:
                Log.d("S_PancardService....",json);
                JSONObject jsonObject = new JSONObject(json);

                AlertDialog b;
                final TextView panno,first_name,father_name,dob;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.pandetails_layout, null);
                dialogBuilder.setView(dialogView);

                first_name = (TextView) dialogView.findViewById(R.id.name);
                father_name = (TextView) dialogView.findViewById(R.id.father);
                dob = (TextView) dialogView.findViewById(R.id.dob);
                panno = (TextView) dialogView.findViewById(R.id.number);

                dob.setText(jsonObject.getString("DOB"));
                father_name.setText(jsonObject.getString("father_name"));
                first_name.setText(jsonObject.getString("first_name"));
                panno.setText(jsonObject.getString("pan_no"));

                b = dialogBuilder.create();
                b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                b.show();

            break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.PancardService:
                Log.d("F_PancardService....",json);
                break;
        }
    }
}
