package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemSalaryDependent;
import com.niivo.com.niivo.R;

import java.util.ArrayList;

/**
 * Created by deepak on 28/9/18.
 */

public class SalaryDependentAdapter extends BaseAdapter {
    Context contextd;
    ArrayList<ItemSalaryDependent> list;

    public SalaryDependentAdapter(Context contextd, ArrayList<ItemSalaryDependent> list) {
        this.contextd = contextd;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WealthSourceHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_drop_down, parent, false);
            holder = new WealthSourceHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (WealthSourceHolder) convertView.getTag();
        }
        holder.textView.setPadding(20, 20, 20, 12);
        holder.textView.setText(list.get(position).getFRONTEND_OPTION_VALUE());
        return convertView;
    }

    class WealthSourceHolder {
        TextView textView;

        public WealthSourceHolder(View v) {
            textView = (TextView) v;

        }
    }
}
