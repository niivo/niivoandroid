package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.niivo.com.niivo.R;

import java.util.List;

/**
 * Created by deepak on 6/11/17.
 */

public class FaqExpandAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> listQuestion, listAnswer;

    public FaqExpandAdapter(Context context, List<String> listQuestion,
                            List<String> listAnswers) {
        this._context = context;
        this.listQuestion = listQuestion;
        this.listAnswer = listAnswers;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listAnswer.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.item_expand_list_item, parent, false);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listQuestion.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listQuestion.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.item_list_group, parent, false);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setText(Html.fromHtml(headerTitle));
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
