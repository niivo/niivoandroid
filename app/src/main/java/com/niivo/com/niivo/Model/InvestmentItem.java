package com.niivo.com.niivo.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class InvestmentItem implements Serializable {
    public InvestmentItem() {

    }

    String id;
    String order_id;
    String order_type;
    String dtdate;
    String advisor_id;
    String userid;
    String scheme_code;
    String l1_flag;
    String nav;
    String amount;
    String quantity;
    String bse_order_no;
    String sip_registration_no;
    String folio_no;
    String is_allotment;
    String order_status;
    String is_goal;
    String failed_reson;
    String payment_status;
    String is_childorder;
    String child_order_id;
    String xml_file;
    String remarks;
    String tag;
    String withdrawQty;
    String withdrawAmt;
    String requested;
    String fundname;
    String current_value;
    String FUNDS_GUJARATI;
    String full_withdraw;
    String FUNDS_HINDI;
    String FUNDS_MARATHI;
    String fund_invested;
    String INWARD_TXN_NO;

    public String getInvestment_amount() {
        return investment_amount;
    }

    public void setInvestment_amount(String investment_amount) {
        this.investment_amount = investment_amount;
    }

    String investment_amount;

    public String getBSE_SCHEME_CODE() {
        return BSE_SCHEME_CODE;
    }

    public void setBSE_SCHEME_CODE(String BSE_SCHEME_CODE) {
        this.BSE_SCHEME_CODE = BSE_SCHEME_CODE;
    }

    String BSE_SCHEME_CODE;

    public String getINWARD_TXN_NO() {
        return INWARD_TXN_NO;
    }

    public void setINWARD_TXN_NO(String INWARD_TXN_NO) {
        this.INWARD_TXN_NO = INWARD_TXN_NO;
    }

    public String getReturns() {
        return returns;
    }

    public void setReturns(String returns) {
        this.returns = returns;
    }

    String returns;



    public String getPortfolio_percent() {
        return portfolio_percent;
    }

    public void setPortfolio_percent(String portfolio_percent) {
        this.portfolio_percent = portfolio_percent;
    }

    String portfolio_percent	;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    String unit;



    public String getRequested() {
        return requested;
    }

    public void setRequested(String requested) {
        this.requested = requested;
    }

    public String getSip_registration_date() {
        return sip_registration_date;
    }

    public void setSip_registration_date(String sip_registration_date) {
        this.sip_registration_date = sip_registration_date;
    }

    String sip_registration_date;

    boolean isGroupEntries;

    public String getFUNDS_GUJARATI() {
        return FUNDS_GUJARATI;
    }

    public void setFUNDS_GUJARATI(String FUNDS_GUJARATI) {
        this.FUNDS_GUJARATI = FUNDS_GUJARATI;
    }

    public String getFUNDS_HINDI() {
        return FUNDS_HINDI;
    }

    public void setFUNDS_HINDI(String FUNDS_HINDI) {
        this.FUNDS_HINDI = FUNDS_HINDI;
    }

    public String getFUNDS_MARATHI() {
        return FUNDS_MARATHI;
    }

    public void setFUNDS_MARATHI(String FUNDS_MARATHI) {
        this.FUNDS_MARATHI = FUNDS_MARATHI;
    }

    public String getFund_invested() {
        return fund_invested;
    }

    public void setFund_invested(String fund_invested) {
        this.fund_invested = fund_invested;
    }

    public void setOrderedItemArrayListSub(ArrayList<InvestmentList.InvestmentItem> orderedItemArrayListSub) {
        this.orderedItemArrayListSub = orderedItemArrayListSub;
    }


    public String getCurrent_value() {
        return current_value;
    }

    public void setCurrent_value(String current_value) {
        this.current_value = current_value;
    }

    public String getFundname() {
        return fundname;
    }

    public void setFundname(String fundname) {
        this.fundname = fundname;
    }

    public String getWithdrawQty() {
        return withdrawQty;
    }

    public void setWithdrawQty(String withdrawQty) {
        this.withdrawQty = withdrawQty;
    }

    public String getWithdrawAmt() {
        return withdrawAmt;
    }

    InvestmentList.GoalDetial goal_detail;
    ArrayList<InvestmentList.GoalDetial.GoalScheme> order_detail;

    public void setWithdrawAmt(String withdrawAmt) {
        this.withdrawAmt = withdrawAmt;
    }

    public String getFull_withdraw() {
        return full_withdraw;
    }

    public void setFull_withdraw(String full_withdraw) {
        this.full_withdraw = full_withdraw;
    }

    public ArrayList<InvestmentList.GoalDetial.GoalScheme> getOrder_detail() {
        return order_detail;
    }

    public void setOrder_detail(ArrayList<InvestmentList.GoalDetial.GoalScheme> order_detail) {
        this.order_detail = order_detail;
    }

    public boolean isGroupEntries() {
        return isGroupEntries;
    }

    public void setGroupEntries(boolean groupEntries) {
        isGroupEntries = groupEntries;
    }
    ArrayList<InvestmentList.InvestmentItem> orderedItemArrayListSub = new ArrayList<>();

    public ArrayList<InvestmentList.InvestmentItem> getOrderedItemArrayListSub() {
        return orderedItemArrayListSub;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }
    public InvestmentList.GoalDetial getGoal_detail() {
        return goal_detail;
    }

    public void setGoal_detail(InvestmentList.GoalDetial goal_detail) {
        this.goal_detail = goal_detail;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getDtdate() {
        return dtdate;
    }

    public void setDtdate(String dtdate) {
        this.dtdate = dtdate;
    }

    public String getAdvisor_id() {
        return advisor_id;
    }

    public void setAdvisor_id(String advisor_id) {
        this.advisor_id = advisor_id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getScheme_code() {
        return scheme_code;
    }

    public void setScheme_code(String scheme_code) {
        this.scheme_code = scheme_code;
    }

    public String getL1_flag() {
        return l1_flag;
    }

    public void setL1_flag(String l1_flag) {
        this.l1_flag = l1_flag;
    }

    public String getNav() {
        return nav;
    }

    public void setNav(String nav) {
        this.nav = nav;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBse_order_no() {
        return bse_order_no;
    }

    public void setBse_order_no(String bse_order_no) {
        this.bse_order_no = bse_order_no;
    }

    public String getSip_registration_no() {
        return sip_registration_no;
    }

    public void setSip_registration_no(String sip_registration_no) {
        this.sip_registration_no = sip_registration_no;
    }

    public String getFolio_no() {
        return folio_no;
    }

    public void setFolio_no(String folio_no) {
        this.folio_no = folio_no;
    }

    public String getIs_allotment() {
        return is_allotment;
    }

    public void setIs_allotment(String is_allotment) {
        this.is_allotment = is_allotment;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getIs_goal() {
        return is_goal;
    }

    public void setIs_goal(String is_goal) {
        this.is_goal = is_goal;
    }

    public String getFailed_reson() {
        return failed_reson;
    }

    public void setFailed_reson(String failed_reson) {
        this.failed_reson = failed_reson;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getIs_childorder() {
        return is_childorder;
    }

    public void setIs_childorder(String is_childorder) {
        this.is_childorder = is_childorder;
    }

    public String getChild_order_id() {
        return child_order_id;
    }

    public void setChild_order_id(String child_order_id) {
        this.child_order_id = child_order_id;
    }

    public String getXml_file() {
        return xml_file;
    }

    public void setXml_file(String xml_file) {
        this.xml_file = xml_file;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
