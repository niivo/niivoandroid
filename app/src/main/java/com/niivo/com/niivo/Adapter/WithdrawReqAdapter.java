package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.WithdrawList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 16/2/18.
 */

public class WithdrawReqAdapter extends BaseAdapter {
    Context contextd;
    WithdrawList list;
    InvestedHolder holder;
    boolean isInvestment = false;
    String lang = "";

    public WithdrawReqAdapter(Context context, WithdrawList res) {
        this.contextd = context;
        this.list = res;
        lang = Common.getLanguage(contextd);
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_withdraw_req, parent, false);
            holder = new InvestedHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (InvestedHolder) convertView.getTag();
        }
        holder.title.setText((lang.equals("en")
                ? list.getData().get(position).getSTARMF_SCHEME_NAME()
                : lang.equals("hi")
                ? list.getData().get(position).getFUNDS_HINDI()
                : lang.equals("gu")
                ? list.getData().get(position).getFUNDS_GUJARATI()
                : lang.equals("mr")
                ? list.getData().get(position).getFUNDS_MARATHI()
                : list.getData().get(position).getSTARMF_SCHEME_NAME()));
        holder.amount.setText(list.getData().get(position).getFull_withdraw().equals("Y")
                ? "(Full Withdraw)" :
                Common.currencyString(list.getData().get(position).getAmount(), false));
        holder.amntTitle.setVisibility(list.getData().get(position).getFull_withdraw().equals("Y") ? View.GONE : View.VISIBLE);
        return convertView;
    }

    class InvestedHolder {
        TextView title, amount, amntTitle;
        ImageView ivLogo;

        public InvestedHolder(View itemView) {
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amount = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            amntTitle = (TextView) itemView.findViewById(R.id.amntTitle);
        }
    }
}
