package com.niivo.com.niivo.Utilities.Sign_Helper;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.PickerHelper;

import java.io.File;

/**
 * Created by deepak on 15/11/17.
 */

public class SignDrawActivity extends Activity implements View.OnClickListener {
    Button clear, done;
    ImageView back;
    SignaturePad signaturePad;
    PickerHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(SignDrawActivity.this, Common.getLanguage(SignDrawActivity.this), false);
        setContentView(R.layout.activity_draw_signature);
        clear = (Button) findViewById(R.id.clear);
        done = (Button) findViewById(R.id.doneSign);
        back = (ImageView) findViewById(R.id.back);
        signaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        signaturePad.setBackgroundColor(Color.TRANSPARENT);
        clear.setVisibility(View.GONE);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                clear.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSigned() {

            }

            @Override
            public void onClear() {
                clear.setVisibility(View.GONE);
            }
        });
        clear.setOnClickListener(this);
        done.setOnClickListener(this);
        back.setOnClickListener(this);
        helper = PickerHelper.with(SignDrawActivity.this, SignDrawActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                signaturePad.clear();
                Intent back = new Intent();
                back.putExtra("Sign", "NotDone");
                setResult(103, back);
                finish();
                break;
            case R.id.doneSign:
                if (signaturePad.isEmpty()) {
                    Common.showDialog(SignDrawActivity.this, SignDrawActivity.this.getResources().getString(R.string.please_sign_first));
                } else {
//                    KYC_BankDetailsFragment.signBitmap = signaturePad.getSignatureBitmap();
//                    Intent i = new Intent();
//                    i.putExtra("Sign", "Done");
//                    setResult(103, i);
//                    finish();

                    Bitmap bitmap = signaturePad.getTransparentSignatureBitmap();//getSignatureBitmap()
                    bitmap = helper.getResizedBitmap(bitmap, 1080);
                    File file = helper.saveBitmapTosignFile(bitmap, "signature");//saveBitmapTosignFile
                    Intent i = new Intent();
                    i.putExtra("signatureFile", file);
                    i.putExtra("Sign", "done");
                    setResult(103, i);
                    finish();
                }
                break;
            case R.id.clear:
                signaturePad.clear();
                break;
        }
    }

}
