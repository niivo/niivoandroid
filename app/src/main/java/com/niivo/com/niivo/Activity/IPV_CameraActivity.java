package com.niivo.com.niivo.Activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.IPVHelpers.CameraPreview;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by deepak on 12/9/18.
 */

public class IPV_CameraActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressBar progressBar;
    View startRecording, flipCamera, toggelFlash;
    CameraPreview mPreview;
    LinearLayout camView;
    Chronometer videoTimerTV;

    //non  ui vars
    private Camera mCamera;
    private MediaRecorder mediaRecorder;
    public static boolean cameraFront = false, isRecording = false, flash = false;
    int maxQualitySupported = -1;
    String url_file = "/mnt/sdcard/Niivo/ipv.mp4";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipv_camera);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        startRecording = (View) findViewById(R.id.recordBtn);
        camView = (LinearLayout) findViewById(R.id.camView);
        flipCamera = findViewById(R.id.flipCamera);
        toggelFlash = findViewById(R.id.toggelFlash);
        videoTimerTV = (Chronometer) findViewById(R.id.videoTimer);

        doPermissionWork();
        camView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    try {
                        focusOnTouch(event);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        });
        startRecording.setOnClickListener(this);
        flipCamera.setOnClickListener(this);
        toggelFlash.setOnClickListener(this);
        videoTimerTV.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recordBtn: {
                if (!isRecording) {
                    if (!prepareMediaRecorder()) {
                        releaseCamera();
                        releaseMediaRecorder();
                        Common.showToast(IPV_CameraActivity.this, getResources().getString(R.string.please_try_again));
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mediaRecorder.start();
                                startChronometer();
                                flipCamera.setVisibility(View.GONE);
                                isRecording = true;
                                ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 20); // see this max value coming back here, we animate towards that value
                                animation.setDuration(20000); // in milliseconds
                                animation.setInterpolator(new LinearInterpolator());
                                animation.start();
                            }
                        });
                    }
                } else {
//                    mediaRecorder.stop();
//                    videoTimerTV.setVisibility(View.GONE);
//                    videoTimerTV.stop();
//                    releaseMediaRecorder();
//                    isRecording = false;
//                    releaseCamera();
                    Common.showToast(IPV_CameraActivity.this,getResources().getString(R.string.you_can_not_stop));
                }
            }
            break;
            case R.id.flipCamera: {
                if (!isRecording) {
                    int camerasNumber = Camera.getNumberOfCameras();
                    if (camerasNumber > 1) {
                        releaseCamera();
                        chooseCamera();
                    } else {
                        Common.showToast(IPV_CameraActivity.this, "You have only one camera.");
                    }
                }
            }
            break;
            case R.id.toggelFlash: {
                if (!cameraFront)
                    if (flash) {
                        flash = false;
                        toggelFlash.setSelected(false);
                        setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    } else {
                        flash = true;
                        toggelFlash.setSelected(true);
                        setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    }
            }
            break;
        }
    }

    long countUp;

    void startChronometer() {
        final long startTime = SystemClock.elapsedRealtime();
        videoTimerTV.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                countUp = (SystemClock.elapsedRealtime() - startTime) / 1000;
                Log.e("TimeProgress", "Progress: " + progressBar.getProgress()
                        + "\nmm:ss " + String.format("%02d", countUp / 60) + ":" + String.format("%02d", countUp % 60));
                if (countUp % 60 > 20) {
                    mediaRecorder.stop();
                    videoTimerTV.stop();
                    videoTimerTV.setVisibility(View.GONE);
                    flipCamera.setVisibility(View.VISIBLE);
                    releaseMediaRecorder();
                    releaseCamera();
                    Intent i = new Intent();
                    i.putExtra("path", url_file);
                    IPV_CameraActivity.this.setResult(RESULT_OK, i);
                    IPV_CameraActivity.this.finish();

                } else {
                    videoTimerTV.setText(String.format("%02d", countUp % 60));
                }
            }
        });
        videoTimerTV.start();
    }

    @Override
    public void onBackPressed() {
        if (isRecording) {

            Common.showToast(IPV_CameraActivity.this, getResources().getString(R.string.wait_till_stop));
        } else {
            Intent i = new Intent();
            IPV_CameraActivity.this.setResult(RESULT_CANCELED, i);
            IPV_CameraActivity.this.finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!hasCamera(IPV_CameraActivity.this.getApplicationContext())) {
            Common.showToast(IPV_CameraActivity.this, "Camera Not Fount..!");
            releaseCamera();
            releaseMediaRecorder();
            finish();
        }

        if (mCamera == null) {
            releaseCamera();
            boolean camFr = cameraFront;
            int cameraId = findFrontFacingCamera();
            if (cameraId < 0) {
                cameraId = findBackFacingCamera();
                if (flash) {
                    mPreview.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    toggelFlash.setSelected(true);
                }
            } else if (!camFr) {
                cameraId = findBackFacingCamera();
                if (flash) {
                    mPreview.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    toggelFlash.setSelected(true);
                }
            }
            mCamera = Camera.open(cameraId);
            mPreview.refreshCamera(mCamera);
            reloadQualities(cameraId);
        }
    }

    void doPermissionWork() {
        if ((ContextCompat.checkSelfPermission(IPV_CameraActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(IPV_CameraActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(IPV_CameraActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(IPV_CameraActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                ) {
            doSetUpCamera();
        } else {
            ActivityCompat.requestPermissions(IPV_CameraActivity.this,
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 125);
        }
    }

    void doSetUpCamera() {
        flipCamera.setSelected(cameraFront);
        mPreview = new CameraPreview(IPV_CameraActivity.this, mCamera);
        camView.addView(mPreview);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 125) {
            if (hasAllPermissionsGranted(grantResults)) {
                doSetUpCamera();
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    boolean hasAllPermissionsGranted(int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }


    //focus work
    private void focusOnTouch(MotionEvent event) {
        if (mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getMaxNumMeteringAreas() > 0) {
                Rect rect = calculateFocusArea(event.getX(), event.getY());
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                meteringAreas.add(new Camera.Area(rect, 800));
                parameters.setFocusAreas(meteringAreas);
                mCamera.setParameters(parameters);
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            } else {
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }
        }
    }

    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / mPreview.getWidth()) * 2000 - 1000).intValue(), 500);
        int top = clamp(Float.valueOf((y / mPreview.getHeight()) * 2000 - 1000).intValue(), 500);
        return new Rect(left, top, left + 500, top + 500);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper) + focusAreaSize / 2 > 1000) {
            if (touchCoordinateInCameraReper > 0) {
                result = 1000 - focusAreaSize / 2;
            } else {
                result = -1000 + focusAreaSize / 2;
            }
        } else {
            result = touchCoordinateInCameraReper - focusAreaSize / 2;
        }
        return result;
    }

    private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {
                // do something...
                Log.i("tap_to_focus", "success!");
            } else {
                // do something...
                Log.i("tap_to_focus", "fail!");
            }
        }
    };

    //different Camera validated Operations
    private boolean hasCamera(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void chooseCamera() {
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                // open the backFacingCamera
                // set a picture callback
                // refresh the preview
                mCamera = Camera.open(cameraId);
                // mPicture = getPictureCallback();
                mPreview.refreshCamera(mCamera);
                reloadQualities(cameraId);
                flipCamera.setSelected(false);
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                // open the backFacingCamera
                // set a picture callback
                // refresh the preview
                mCamera = Camera.open(cameraId);
                if (flash) {
                    flash = false;
                    toggelFlash.setSelected(false);
                    mPreview.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
                // mPicture = getPictureCallback();
                flipCamera.setSelected(true);
                mPreview.refreshCamera(mCamera);
                reloadQualities(cameraId);
            }
        }
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            mCamera.lock();
        }
    }

    private void reloadQualities(int camId) {
        if (CamcorderProfile.hasProfile(camId, CamcorderProfile.QUALITY_480P)) {
            maxQualitySupported = CamcorderProfile.QUALITY_480P;
        }
        if (CamcorderProfile.hasProfile(camId, CamcorderProfile.QUALITY_720P)) {
            maxQualitySupported = CamcorderProfile.QUALITY_720P;
        }
    }

    void setFlashMode(String mode) {
        try {
            if (getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA_FLASH)
                    && mCamera != null
                    && !cameraFront) {

                mPreview.setFlashMode(mode);
                mPreview.refreshCamera(mCamera);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean prepareMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
//        if (Build.MANUFACTURER.toLowerCase().contains("samsung")) {
//            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
//        } else {
//            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
//        }
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
//        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//        mediaRecorder.setVideoEncodingBitRate(690000);
//        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
//        mediaRecorder.setVideoFrameRate(30);
//        mediaRecorder.setVideoSize(640, 480);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (cameraFront) {
                mediaRecorder.setOrientationHint(270);
            } else {
                mediaRecorder.setOrientationHint(90);
            }
        }

        CamcorderProfile camcorderProfile = CamcorderProfile.get(maxQualitySupported);
        camcorderProfile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
        camcorderProfile.audioCodec = MediaRecorder.AudioEncoder.AMR_NB;
        camcorderProfile.videoBitRate = 690000;
        camcorderProfile.videoCodec = MediaRecorder.VideoEncoder.H264;
        camcorderProfile.videoFrameRate = 30;

        mediaRecorder.setProfile(camcorderProfile);
        File file = new File("/mnt/sdcard/Niivo");
        if (!file.exists()) {
            file.mkdirs();
        }
        Date d = new Date();
//        String timestamp = String.valueOf(d.getTime());
//        url_file = Environment.getExternalStorageDirectory().getPath() + "/videoKit" + timestamp + ".mp4";
        url_file = "/mnt/sdcard/Niivo/ipv.mp4";
//        url_file = "/mnt/sdcard/videokit/" + timestamp + ".mp4";

        File file1 = new File(url_file);
        if (file1.exists()) {
            file1.delete();
        }

        mediaRecorder.setOutputFile(url_file);

//        https://developer.android.com/reference/android/media/MediaRecorder.html#setMaxDuration(int) 不设置则没有限制
//        mediaRecorder.setMaxDuration(CameraConfig.MAX_DURATION_RECORD); //设置视频文件最长时间 60s.
//        https://developer.android.com/reference/android/media/MediaRecorder.html#setMaxFileSize(int) 不设置则没有限制
//        mediaRecorder.setMaxFileSize(CameraConfig.MAX_FILE_SIZE_RECORD); //设置视频文件最大size 1G

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            releaseMediaRecorder();
            return false;
        }
        return true;
    }
}
