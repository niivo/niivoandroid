package com.niivo.com.niivo.Fragment.KYCnew;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.IPV_CameraActivity;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.AWS_Helper.ProgressViewDialog;
import com.niivo.com.niivo.Utils.AWS_Helper.S3UploadService;
import com.niivo.com.niivo.Utils.Common;

import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by deepak on 24/9/18.
 */

public class KYC_IPV_processFragment extends Fragment implements View.OnClickListener  , ResponseDelegate {
    LinearLayout mediaLL, cameraBtn;
    TextView submitBtn;
    VideoView videoPalyer;
    View rootView;
    //Non ui vars;
    Context contextd;
    File file;
    String userId = "", ipvRef = "";
    ProgressViewDialog progressViewDialog;
    boolean isuploaded = false;
    RequestedServiceDataModel requestedServiceDataModel;
    ItemUserDetail userDetail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_ipv_process, container, false);
            contextd = container.getContext();
            mediaLL = (LinearLayout) rootView.findViewById(R.id.mediaLL);
            cameraBtn = (LinearLayout) rootView.findViewById(R.id.cameraBtn);
            submitBtn = (TextView) rootView.findViewById(R.id.submitBtn);
            videoPalyer = (VideoView) rootView.findViewById(R.id.videoPalyer);
            cameraBtn.setOnClickListener(this);
            submitBtn.setOnClickListener(this);
            userId = Common.getPreferences(getActivity(), "userID");
            popup();
            doPermissionWork();
        }
        return rootView;
    }

    void popup(){
        final Dialog dialog = new Dialog(contextd);
        dialog.setContentView(R.layout.image_popup);
        dialog.setTitle("N I I V O");
        TextView text = (TextView) dialog.findViewById(R.id.text);
       // text.setText("Android custom dialog example!");
        ImageView image = (ImageView) dialog.findViewById(R.id.fullimage);
        image.setImageResource(R.drawable.ipvenglish);

        TextView proceesedButton = (TextView) dialog.findViewById(R.id.proceesed);
        proceesedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(contextd, IPV_CameraActivity.class), 111);
            }
        });
        TextView cancelButton = (TextView) dialog.findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void doPermissionWork() {
        progressViewDialog = ProgressViewDialog.with(contextd);
        progressViewDialog.setCancelable(false);
        if ((ContextCompat.checkSelfPermission(contextd, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)

                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                ) {
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 125);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isuploaded", isuploaded);
        getTargetFragment().onActivityResult(
                ResponseType.KYC_IPV_Details,
                isuploaded ? Activity.RESULT_OK : Activity.RESULT_CANCELED, i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cameraBtn:
                startActivityForResult(new Intent(contextd, IPV_CameraActivity.class), 111);
                break;
            case R.id.submitBtn: {
                if (file == null) {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.pleae_add_your_ipv));
                } else {
                    progressViewDialog.show();
                    LocalBroadcastManager.getInstance(contextd).registerReceiver(getUploadCallBack, new IntentFilter("onUploadStatus"));
                    Intent i = new Intent(contextd, S3UploadService.class);
                    i.putExtra("userId", userId);
                    i.putExtra("fileType", "IPV");
                    i.putExtra("file",    file);
                    contextd.startService(i);
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                String url = data.getStringExtra("path");
                mediaLL.setVisibility(View.VISIBLE);
                MediaController mc = new MediaController(contextd);
                mc.setAnchorView(videoPalyer);
                mc.setMediaPlayer(videoPalyer);
                Uri video = Uri.parse(url);
                file = new File(video.getPath());
                videoPalyer.setMediaController(mc);
                videoPalyer.setVideoURI(video);
                videoPalyer.start();
                setBasicDetails(file);

            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }


    BroadcastReceiver getUploadCallBack = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressViewDialog.dismiss();
            if (intent.getStringExtra("state").equals("COMPLETED")) {
                ipvRef = intent.getStringExtra("panRef");
                Log.e("panRef", ipvRef);
                LocalBroadcastManager.getInstance(contextd).unregisterReceiver(this);
                isuploaded = true;
                getActivity().onBackPressed();
            } else {
                Common.showToast(contextd, contextd.getResources().getString(R.string.upload_failed_try_again));
            }
        }
    };

    void setBasicDetails(File file) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_BasicDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid",    userId);
        requestedServiceDataModel.putQurry("ipv_path",   userId+"/"+"ipv"+"/"+file.getName() +".mp4");
        requestedServiceDataModel.setWebServiceType("ADDIPV");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        userDetail = new Gson().fromJson(json, ItemUserDetail.class);
        if(userDetail.getStatus().equalsIgnoreCase("true"))
        {
            Common.showToast(getActivity() , userDetail.getMsg());
        }

    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

    }
}
