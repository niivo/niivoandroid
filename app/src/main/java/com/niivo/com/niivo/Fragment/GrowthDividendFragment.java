package com.niivo.com.niivo.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.niivo.com.niivo.Fragment.InterMediateFragment.IntermidiateFragment;
import com.niivo.com.niivo.R;

/**
 * Created by deepak on 28/5/18.
 */

public class GrowthDividendFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    //non ui vars
    Context contextd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaction, container, false);
        contextd = getActivity();
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);

        setUpTabs();
        return rootView;
    }

    void setUpTabs() {
        for (int i = 0; i < 2; i++)
            if (i == 0)
                tabLayout.addTab(tabLayout.newTab().setText(R.string.growth));
            else if (i == 1)
                tabLayout.addTab(tabLayout.newTab().setText(R.string.dividend));
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setAdapter(new PagerAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Bundle b = new Bundle();
                b.putAll(GrowthDividendFragment.this.getArguments());
                b.putString("tabType", "growth");
                return IntermidiateFragment.getInstance(b);
            } else if (position == 1) {
                Bundle b2 = new Bundle();
                b2.putAll(GrowthDividendFragment.this.getArguments());
                b2.putString("tabType", "dividend");
                return IntermidiateFragment.getInstance(b2);
            } else return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            if (i == 0) {
                return contextd.getResources().getString(R.string.growth);
            } else if (i == 1) {
                return contextd.getResources().getString(R.string.dividend);
            } else
                return "";
        }
    }
}
