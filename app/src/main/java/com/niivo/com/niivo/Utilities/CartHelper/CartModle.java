package com.niivo.com.niivo.Utilities.CartHelper;

/**
 * Created by deepak on 12/12/17.
 */

public class CartModle {
    String id, scheme_name, amount, minAmount, orderType, type, scheme_code, added_date, target_year, nav, qty, numberofmonths, goal_name, goal_amount, schem_json, goal_cate_id;
    int cateImage;

    public CartModle() {
    }

    public CartModle(String scheme_name, String amount, String minAmount, String orderType, String type, String scheme_code, String added_date, String target_year, String nav, String qty, String numberofmonths, String goal_name, String goal_amount, String schem_json, int cateImage, String goal_cate_id) {
        this.scheme_name = scheme_name;
        this.amount = amount;
        this.goal_cate_id = goal_cate_id;
        this.orderType = orderType;
        this.type = type;
        this.scheme_code = scheme_code;
        this.added_date = added_date;
        this.target_year = target_year;
        this.nav = nav;
        this.qty = qty;
        this.numberofmonths = numberofmonths;
        this.goal_name = goal_name;
        this.goal_amount = goal_amount;
        this.schem_json = schem_json;
        this.cateImage = cateImage;
        this.minAmount = minAmount;
    }

    public CartModle(String scheme_name, String amount, String minAmount, String orderType, String type, String scheme_code, String added_date, String target_year, String nav, String qty, String numberofmonths, String schem_json) {
        this.scheme_name = scheme_name;
        this.amount = amount;
        this.orderType = orderType;
        this.type = type;
        this.scheme_code = scheme_code;
        this.added_date = added_date;
        this.target_year = target_year;
        this.nav = nav;
        this.qty = qty;
        this.minAmount = minAmount;
        this.numberofmonths = numberofmonths;
        this.schem_json = schem_json;
    }

    public CartModle(String scheme_code, String orderType, String type, String added_date, String target_year, String numberofmonths, String goal_name, String goal_amount, String schem_json, int cateImage, String goal_cate_id) {
        this.orderType = orderType;
        this.scheme_code = scheme_code;
        this.goal_cate_id = goal_cate_id;
        this.type = type;
        this.added_date = added_date;
        this.target_year = target_year;
        this.numberofmonths = numberofmonths;
        this.goal_name = goal_name;
        this.goal_amount = goal_amount;
        this.schem_json = schem_json;
        this.cateImage = cateImage;
    }

    public String getGoal_cate_id() {
        return goal_cate_id;
    }

    public void setGoal_cate_id(String goal_cate_id) {
        this.goal_cate_id = goal_cate_id;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScheme_name() {
        return scheme_name;
    }

    public void setScheme_name(String scheme_name) {
        this.scheme_name = scheme_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScheme_code() {
        return scheme_code;
    }

    public void setScheme_code(String scheme_code) {
        this.scheme_code = scheme_code;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }

    public String getTarget_year() {
        return target_year;
    }

    public void setTarget_year(String target_year) {
        this.target_year = target_year;
    }

    public String getNav() {
        return nav;
    }

    public void setNav(String nav) {
        this.nav = nav;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getNumberofmonths() {
        return numberofmonths;
    }

    public void setNumberofmonths(String numberofmonths) {
        this.numberofmonths = numberofmonths;
    }

    public String getGoal_name() {
        return goal_name;
    }

    public void setGoal_name(String goal_name) {
        this.goal_name = goal_name;
    }

    public String getGoal_amount() {
        return goal_amount;
    }

    public void setGoal_amount(String goal_amount) {
        this.goal_amount = goal_amount;
    }

    public String getSchem_json() {
        return schem_json;
    }

    public void setSchem_json(String schem_json) {
        this.schem_json = schem_json;
    }

    public int getCateImage() {
        return cateImage;
    }

    public void setCateImage(int cateImage) {
        this.cateImage = cateImage;
    }
}
