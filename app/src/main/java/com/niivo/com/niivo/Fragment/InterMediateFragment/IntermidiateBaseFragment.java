package com.niivo.com.niivo.Fragment.InterMediateFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Fragment.GrowthDividendFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.SquareRelativeLayout;


/**
 * Created by deepak on 9/6/17.
 */

public class IntermidiateBaseFragment extends Fragment implements View.OnClickListener {
    Context contextd;

    SquareRelativeLayout equityFund, debtFund, balancedFund, taxSaving,newfunds,otherfunds;
    boolean flag=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_intermediate, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.intermediate));
        equityFund = (SquareRelativeLayout) rootView.findViewById(R.id.fundEquity);
        debtFund = (SquareRelativeLayout) rootView.findViewById(R.id.fundDebt);
        balancedFund = (SquareRelativeLayout) rootView.findViewById(R.id.fundBalanced);
        taxSaving = (SquareRelativeLayout) rootView.findViewById(R.id.taxSaving);
        newfunds = (SquareRelativeLayout) rootView.findViewById(R.id.newfund);
        otherfunds = (SquareRelativeLayout) rootView.findViewById(R.id.otherfund);

        equityFund.setOnClickListener(this);
        debtFund.setOnClickListener(this);
        balancedFund.setOnClickListener(this);
        taxSaving.setOnClickListener(this);
        newfunds.setOnClickListener(this);
        otherfunds.setOnClickListener(this);
        SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(contextd);
        prefss.edit().putString("flag", String.valueOf(flag)).commit();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Fragment frag = null;
        Bundle b = new Bundle();

        switch (v.getId()) {
            case R.id.fundEquity:
                b.putString("for", "equity");
                b.putString("from", "intermediate");
                frag = new EquityFundsFragment();
                frag.setArguments(b);
                break;
            case R.id.fundDebt:
                b.putString("for", "debt");
               // b.putString("sub_section", "");
            //    b.putString("type", "DEBTFUNDS");
                b.putString("from", "intermediate");
                frag = new EquityFundsFragment();
                //frag = new GrowthDividendFragment();
                frag.setArguments(b);
                break;
            case R.id.fundBalanced:
                b.putString("for", "HYBRIDFUND");
                b.putString("from", "intermediate");
                frag = new EquityFundsFragment();
                frag.setArguments(b);
                break;
//            case R.id.fundBalanced:
//                b.putString("for", "balanced");
//                b.putString("from", "intermediate");
//                frag = new EquityFundsFragment();
//                frag.setArguments(b);
//                break;
            case R.id.taxSaving:
                b.putString("sub_section", "");
                b.putString("type", "TAXSAVINGFUND");//TAXSAVINGFUNDS
                b.putString("from", "intermediate");
                frag = new GrowthDividendFragment();
                frag.setArguments(b);
                break;
            case R.id.otherfund:
                b.putString("for", "OTHERFUND");
                b.putString("from", "intermediate");
                frag = new EquityFundsFragment();
                frag.setArguments(b);
                break;
            case R.id.newfund:
                b.putString("sub_section", "");
                b.putString("type", "NEWFUND");//TAXSAVINGFUNDS
                b.putString("from", "intermediate");
                frag = new GrowthDividendFragment();
                frag.setArguments(b);
                break;
        }

        if (frag != null) {
            UiUtils.hideKeyboard(contextd);
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, frag).addToBackStack(null).
                    commit();
        }
    }
}

