package com.niivo.com.niivo.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bhaskar on 13/7/17.
 */


public class UserProfile implements  Parcelable{
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public UserProfile(String status, String msg, User data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public UserProfile(Parcel in) {
        this.status = in.readString();
        this.msg = in.readString();
        this.data = in.readParcelable(new ClassLoader() {
            @Override
            public Class<User> loadClass(String name) throws ClassNotFoundException {
                return (Class<User>) super.loadClass(name);
            }
        });
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.msg);
        dest.writeParcelable(this.data,PARCELABLE_WRITE_RETURN_VALUE);
    }


    String status, msg;
    User data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }

    public class User implements Parcelable {
        public final Parcelable.Creator CREATOR = new Parcelable.Creator() {
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            public User[] newArray(int size) {
                return new User[size];
            }
        };

        String userid, name, dob, gender, occupation_code, occupation, address1, address2, address3,
                state, state_code, city, pin, country, pencard, bank_ac_type, account_no,
                ifsc, bank_name, bank_id, account_name, email, nominee_name, nominee_dob, nominee_relationship,
                nominee_address, image, thumb_image, mobile, father_name, mother_name,
                wealth_source, anualIncomeCode, occupation_type, aadhar_no, status, verification_step, pancard_verify,
                address_verify, identity_verify, nominee_verify, bank_verify, aadhar_verify, image_upload_status,
                image_upload_msg, aof_json, advisor_id, default_lang, wealthSourceCode, occupationType;

        public User(String userid, String name, String dob, String gender, String occupation_code, String occupation, String address1, String address2, String address3, String state, String state_code, String city, String pin, String country, String pencard, String bank_ac_type, String account_no, String ifsc, String bank_name, String bank_id, String account_name, String email, String nominee_name, String nominee_dob, String nominee_relationship, String nominee_address, String image, String thumb_image, String mobile, String father_name, String mother_name, String wealth_source, String anualIncomeCode, String occupation_type, String aadhar_no, String status, String verification_step, String pancard_verify, String address_verify, String identity_verify, String nominee_verify, String bank_verify, String aadhar_verify, String image_upload_status, String image_upload_msg, String aof_json, String advisor_id, String default_lang, String wealthSourceCode, String occupationType) {
            this.userid = userid;
            this.name = name;
            this.dob = dob;
            this.gender = gender;
            this.occupation_code = occupation_code;
            this.occupation = occupation;
            this.address1 = address1;
            this.address2 = address2;
            this.address3 = address3;
            this.state = state;
            this.state_code = state_code;
            this.city = city;
            this.pin = pin;
            this.country = country;
            this.pencard = pencard;
            this.bank_ac_type = bank_ac_type;
            this.account_no = account_no;
            this.ifsc = ifsc;
            this.bank_name = bank_name;
            this.bank_id = bank_id;
            this.account_name = account_name;
            this.email = email;
            this.nominee_name = nominee_name;
            this.nominee_dob = nominee_dob;
            this.nominee_relationship = nominee_relationship;
            this.nominee_address = nominee_address;
            this.image = image;
            this.thumb_image = thumb_image;
            this.mobile = mobile;
            this.father_name = father_name;
            this.mother_name = mother_name;
            this.wealth_source = wealth_source;
            this.anualIncomeCode = anualIncomeCode;
            this.occupation_type = occupation_type;
            this.aadhar_no = aadhar_no;
            this.status = status;
            this.verification_step = verification_step;
            this.pancard_verify = pancard_verify;
            this.address_verify = address_verify;
            this.identity_verify = identity_verify;
            this.nominee_verify = nominee_verify;
            this.bank_verify = bank_verify;
            this.aadhar_verify = aadhar_verify;
            this.image_upload_status = image_upload_status;
            this.image_upload_msg = image_upload_msg;
            this.aof_json = aof_json;
            this.advisor_id = advisor_id;
            this.default_lang = default_lang;
            this.wealthSourceCode = wealthSourceCode;
            this.occupationType = occupationType;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getOccupation_code() {
            return occupation_code;
        }

        public void setOccupation_code(String occupation_code) {
            this.occupation_code = occupation_code;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress3() {
            return address3;
        }

        public void setAddress3(String address3) {
            this.address3 = address3;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getState_code() {
            return state_code;
        }

        public void setState_code(String state_code) {
            this.state_code = state_code;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPencard() {
            return pencard;
        }

        public void setPencard(String pencard) {
            this.pencard = pencard;
        }

        public String getBank_ac_type() {
            return bank_ac_type;
        }

        public void setBank_ac_type(String bank_ac_type) {
            this.bank_ac_type = bank_ac_type;
        }

        public String getAccount_no() {
            return account_no;
        }

        public void setAccount_no(String account_no) {
            this.account_no = account_no;
        }

        public String getIfsc() {
            return ifsc;
        }

        public void setIfsc(String ifsc) {
            this.ifsc = ifsc;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getBank_id() {
            return bank_id;
        }

        public void setBank_id(String bank_id) {
            this.bank_id = bank_id;
        }

        public String getAccount_name() {
            return account_name;
        }

        public void setAccount_name(String account_name) {
            this.account_name = account_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNominee_name() {
            return nominee_name;
        }

        public void setNominee_name(String nominee_name) {
            this.nominee_name = nominee_name;
        }

        public String getNominee_dob() {
            return nominee_dob;
        }

        public void setNominee_dob(String nominee_dob) {
            this.nominee_dob = nominee_dob;
        }

        public String getNominee_relationship() {
            return nominee_relationship;
        }

        public void setNominee_relationship(String nominee_relationship) {
            this.nominee_relationship = nominee_relationship;
        }

        public String getNominee_address() {
            return nominee_address;
        }

        public void setNominee_address(String nominee_address) {
            this.nominee_address = nominee_address;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getThumb_image() {
            return thumb_image;
        }

        public void setThumb_image(String thumb_image) {
            this.thumb_image = thumb_image;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFather_name() {
            return father_name;
        }

        public void setFather_name(String father_name) {
            this.father_name = father_name;
        }

        public String getMother_name() {
            return mother_name;
        }

        public void setMother_name(String mother_name) {
            this.mother_name = mother_name;
        }

        public String getWealth_source() {
            return wealth_source;
        }

        public void setWealth_source(String wealth_source) {
            this.wealth_source = wealth_source;
        }

        public String getAnualIncomeCode() {
            return anualIncomeCode;
        }

        public void setAnualIncomeCode(String anualIncomeCode) {
            this.anualIncomeCode = anualIncomeCode;
        }

        public String getOccupation_type() {
            return occupation_type;
        }

        public void setOccupation_type(String occupation_type) {
            this.occupation_type = occupation_type;
        }

        public String getAadhar_no() {
            return aadhar_no;
        }

        public void setAadhar_no(String aadhar_no) {
            this.aadhar_no = aadhar_no;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVerification_step() {
            return verification_step;
        }

        public void setVerification_step(String verification_step) {
            this.verification_step = verification_step;
        }

        public String getPancard_verify() {
            return pancard_verify;
        }

        public void setPancard_verify(String pancard_verify) {
            this.pancard_verify = pancard_verify;
        }

        public String getAddress_verify() {
            return address_verify;
        }

        public void setAddress_verify(String address_verify) {
            this.address_verify = address_verify;
        }

        public String getIdentity_verify() {
            return identity_verify;
        }

        public void setIdentity_verify(String identity_verify) {
            this.identity_verify = identity_verify;
        }

        public String getNominee_verify() {
            return nominee_verify;
        }

        public void setNominee_verify(String nominee_verify) {
            this.nominee_verify = nominee_verify;
        }

        public String getBank_verify() {
            return bank_verify;
        }

        public void setBank_verify(String bank_verify) {
            this.bank_verify = bank_verify;
        }

        public String getAadhar_verify() {
            return aadhar_verify;
        }

        public void setAadhar_verify(String aadhar_verify) {
            this.aadhar_verify = aadhar_verify;
        }

        public String getImage_upload_status() {
            return image_upload_status;
        }

        public void setImage_upload_status(String image_upload_status) {
            this.image_upload_status = image_upload_status;
        }

        public String getImage_upload_msg() {
            return image_upload_msg;
        }

        public void setImage_upload_msg(String image_upload_msg) {
            this.image_upload_msg = image_upload_msg;
        }

        public String getAof_json() {
            return aof_json;
        }

        public void setAof_json(String aof_json) {
            this.aof_json = aof_json;
        }

        public String getAdvisor_id() {
            return advisor_id;
        }

        public void setAdvisor_id(String advisor_id) {
            this.advisor_id = advisor_id;
        }

        public String getDefault_lang() {
            return default_lang;
        }

        public void setDefault_lang(String default_lang) {
            this.default_lang = default_lang;
        }

        public String getWealthSourceCode() {
            return wealthSourceCode;
        }

        public void setWealthSourceCode(String wealthSourceCode) {
            this.wealthSourceCode = wealthSourceCode;
        }

        public String getOccupationType() {
            return occupationType;
        }

        public void setOccupationType(String occupationType) {
            this.occupationType = occupationType;
        }

        public User(Parcel in) {
            this.userid = in.readString();
            this.name = in.readString();
            this.dob = in.readString();
            this.gender = in.readString();
            this.occupation_code = in.readString();
            this.occupation = in.readString();
            this.address1 = in.readString();
            this.address2 = in.readString();
            this.address3 = in.readString();
            this.state = in.readString();
            this.state_code = in.readString();
            this.city = in.readString();
            this.pin = in.readString();
            this.country = in.readString();
            this.pencard = in.readString();
            this.bank_ac_type = in.readString();
            this.account_no = in.readString();
            this.ifsc = in.readString();
            this.bank_name = in.readString();
            this.bank_id = in.readString();
            this.account_name = in.readString();
            this.email = in.readString();
            this.nominee_name = in.readString();
            this.nominee_dob = in.readString();
            this.nominee_relationship = in.readString();
            this.nominee_address = in.readString();
            this.image = in.readString();
            this.thumb_image = in.readString();
            this.mobile = in.readString();
            this.father_name = in.readString();
            this.mother_name = in.readString();
            this.wealth_source = in.readString();
            this.anualIncomeCode = in.readString();
            this.occupation_type = in.readString();
            this.aadhar_no = in.readString();
            this.status = in.readString();
            this.verification_step = in.readString();
            this.pancard_verify = in.readString();
            this.address_verify = in.readString();
            this.identity_verify = in.readString();
            this.nominee_verify = in.readString();
            this.bank_verify = in.readString();
            this.aadhar_verify = in.readString();
            this.image_upload_status = in.readString();
            this.image_upload_msg = in.readString();
            this.aof_json = in.readString();
            this.advisor_id = in.readString();
            this.default_lang = in.readString();
            this.wealthSourceCode = in.readString();
            this.occupationType = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.name);
            dest.writeString(this.userid);
            dest.writeString(name);
            dest.writeString(dob);
            dest.writeString(gender);
            dest.writeString(occupation_code);
            dest.writeString(occupation);
            dest.writeString(address1);
            dest.writeString(address2);
            dest.writeString(address3);
            dest.writeString(state);
            dest.writeString(state_code);
            dest.writeString(city);
            dest.writeString(pin);
            dest.writeString(country);
            dest.writeString(pencard);
            dest.writeString(bank_ac_type);
            dest.writeString(account_no);
            dest.writeString(ifsc);
            dest.writeString(bank_name);
            dest.writeString(bank_id);
            dest.writeString(account_name);
            dest.writeString(email);
            dest.writeString(nominee_name);
            dest.writeString(nominee_dob);
            dest.writeString(nominee_relationship);
            dest.writeString(nominee_address);
            dest.writeString(image);
            dest.writeString(thumb_image);
            dest.writeString(mobile);
            dest.writeString(father_name);
            dest.writeString(mother_name);
            dest.writeString(wealth_source);
            dest.writeString(anualIncomeCode);
            dest.writeString(occupation_type);
            dest.writeString(aadhar_no);
            dest.writeString(status);
            dest.writeString(verification_step);
            dest.writeString(pancard_verify);
            dest.writeString(address_verify);
            dest.writeString(identity_verify);
            dest.writeString(nominee_verify);
            dest.writeString(bank_verify);
            dest.writeString(aadhar_verify);
            dest.writeString(image_upload_status);
            dest.writeString(image_upload_msg);
            dest.writeString(aof_json);
            dest.writeString(advisor_id);
            dest.writeString(default_lang);
            dest.writeString(wealthSourceCode);
            dest.writeString(occupationType);
        }

    }


}