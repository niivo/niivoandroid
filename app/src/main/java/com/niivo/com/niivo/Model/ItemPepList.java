package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 28/9/18.
 */

public class ItemPepList {
    String status, msg;
    ArrayList<Pep> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<Pep> getData() {
        return data;
    }

    public void setData(ArrayList<Pep> data) {
        this.data = data;
    }

    public class Pep {
        String FRONTEND_OPTION_NAME, NIIVO_CODE, FATCA_CODE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getFATCA_CODE() {
            return FATCA_CODE;
        }

        public void setFATCA_CODE(String FATCA_CODE) {
            this.FATCA_CODE = FATCA_CODE;
        }
    }
}
