package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 25/9/18.
 */

public class ItemStatesList {
    String status, msg;
    ArrayList<State> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<State> getData() {
        return data;
    }

    public void setData(ArrayList<State> data) {
        this.data = data;
    }

    public class State {
        String FRONTEND_OPTION_NAME, NIIVO_CODE, UCC_CODE, CVL_CODE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getUCC_CODE() {
            return UCC_CODE;
        }

        public void setUCC_CODE(String UCC_CODE) {
            this.UCC_CODE = UCC_CODE;
        }

        public String getCVL_CODE() {
            return CVL_CODE;
        }

        public void setCVL_CODE(String CVL_CODE) {
            this.CVL_CODE = CVL_CODE;
        }
    }
}
