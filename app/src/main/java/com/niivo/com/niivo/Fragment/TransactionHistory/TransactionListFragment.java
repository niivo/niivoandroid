package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.InvestedAdapter;
import com.niivo.com.niivo.Adapter.SipDueReportAdapter;
import com.niivo.com.niivo.Adapter.WithdrawReqAdapter;
import com.niivo.com.niivo.Fragment.InvestHelper.PendingRedirectingFragment;
import com.niivo.com.niivo.Fragment.InvestmentPlan.InvestedGoalDetailFragment;
import com.niivo.com.niivo.Fragment.InvestmentPlan.InvestedSchemeDetailFragment;
import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.Model.SipDueReports;
import com.niivo.com.niivo.Model.WithdrawList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by deepak on 8/2/18.
 */

public class TransactionListFragment extends Fragment implements ResponseDelegate {

    ListView orderdList;

    //Non ui vars
    RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    OrderedList list;
    WithdrawList wList;
    InvestedAdapter adapter;
    String type;
    JSONObject obj;
    SipDueReports sipDueReports;
    SipDueReportAdapter sipAdapter;
    String lang = "";
    boolean isPending = false;

    public TransactionListFragment() {
    }

    public static TransactionListFragment getInstance(Bundle bD) {
        TransactionListFragment mfragment = new TransactionListFragment();
        Bundle b = bD;
        mfragment.setArguments(b);
        return mfragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        if (b != null) {
            Log.e("b", "NotNull");
        }
    }

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_transaction_list, container, false);
            Log.e("Type", "Purchase");
            contextd = MainActivity.activity;
            lang = Common.getLanguage(contextd);
            orderdList = (ListView) rootView.findViewById(R.id.orderedList);
            orderdList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (getArguments().getString("orderStatus").equals("CONFIRM"))
                        if (getArguments().getString("paymentStatus").equals("-1")) {
                            investInThisFund(false, position,
                                    list.getData().get(position).getFUND_NAME(),
                                    "FUND",
                                    list.getData().get(position).getAmount(),
                                    list.getData().get(position).getOrder_type().equals("MONTHLY") ? "SIP" : "ONETIME");
                        } else if (list.getData().get(position).getIs_goal().equals("1")) {
                            Log.e("Niivo", "Its a goal.");
                            Fragment frag = new InvestedGoalDetailFragment();
                            Bundle b = new Bundle();
                            try {
                                b.putString("goalDetail", obj.getJSONArray("data").getJSONObject(position).toString());
                                frag.setArguments(b);
                                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                        .replace(R.id.frame_container, frag).addToBackStack(null).
                                        commit();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            //trnsact here
                            Fragment frag = new InvestedSchemeDetailFragment();
                            Bundle b = new Bundle();
                            b.putString("from", "direct");
                            b.putString("orderId", list.getData().get(position).getOrder_id());
                            b.putString("ammountInvested", list.getData().get(position).getAmount());
                            b.putString("investmentValue", list.getData().get(position).getCurrent_value());
                            b.putString("totalEarnings", String.format(new Locale("en"), "%.1f",
                                    (Float.parseFloat(list.getData().get(position).getCurrent_value()) - Float.parseFloat(list.getData().get(position).getAmount()))));
                            b.putString("nav", String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getNav())));
                            b.putString("numOfUnit", String.format(new Locale("en"), "%.1f", Float.parseFloat(list.getData().get(position).getQuantity())));
                            b.putString("fundClass", "N/A");
                            b.putString("investmentType", list.getData().get(position).getOrder_type().equals("ONETIME")
                                    ? contextd.getResources().getString(R.string.oneTime)
                                    : contextd.getResources().getString(R.string.monthly));
                            b.putString("paidInstallment", "N/A");
                            b.putString("orderType", list.getData().get(position).getOrder_type());
                            b.putString("schemeName", (lang.equals("en")
                                    ? list.getData().get(position).getFUND_NAME()
                                    : lang.equals("hi")
                                    ? list.getData().get(position).getFUNDS_HINDI()
                                    : lang.equals("gu")
                                    ? list.getData().get(position).getFUNDS_GUJARATI()
                                    : lang.equals("mr")
                                    ? list.getData().get(position).getFUNDS_MARATHI()
                                    : list.getData().get(position).getFUND_NAME()));
                            frag.setArguments(b);

                            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                    .replace(R.id.frame_container, frag).addToBackStack(null).
                                    commit();
                        }
                }
            });
        }
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            contextd = MainActivity.activity;
            type = getArguments().getString("type");
            if (type.equals("sip"))
                getSipDetails();
            else {
                if (getArguments().getString("orderStatus").contains("draw"))
                    getWithdrawList(getArguments().getString("orderStatus"));
                else
                    getInvestmentFundList(getArguments().getString("orderStatus"), getArguments().getString("paymentStatus"));
            }
        }
    }

    void getSipDetails() {
        Gson gson = new Gson();
        sipDueReports = gson.fromJson(getArguments().getString("sipInst"), SipDueReports.class);
        if (sipDueReports.getData().size() > 0)
            if (sipDueReports.getData().get(0).getOrder_detail().size() > 0) {
                sipAdapter = new SipDueReportAdapter(contextd, sipDueReports);
                orderdList.setAdapter(sipAdapter);
            } else {
                sipAdapter = null;
                orderdList.setAdapter(null);
            }
    }


    void getInvestmentFundList(String status, String paymentStatus) {
        isPending = paymentStatus.equals("-1");
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("ORDERLIST");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("order_status", status);
        requestedServiceDataModel.putQurry("payment_status", paymentStatus);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        if (!status.equals("not"))
            requestedServiceDataModel.execute();
    }

    void getWithdrawList(String status) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.WithDrawReq);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("WITHDRAW-REPORT");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("order_status", status.equals("pendingWithdraw") ? "PENDING" : "CONFIRM");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        Gson gson = new Gson();
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                try {
                    obj = new JSONObject(json);
                    OrderedList temp = gson.fromJson(json, OrderedList.class);
                    HashMap<String, List<String>> test = new HashMap<>();
                    for (int i = 0; i < temp.getData().size(); i++) {
                        if (temp.getData().get(i).getIs_goal().equals("1")) {
                            if (test.containsKey(temp.getData().get(i).getOrder_id())) {
                                test.get(temp.getData().get(i).getOrder_id()).add(i + "");
                            } else {
                                test.put(temp.getData().get(i).getOrder_id(), new ArrayList<String>());
                                test.get(temp.getData().get(i).getOrder_id()).add(i + "");
                            }
                        }
                    }
                    if (!test.isEmpty()) {
                        List<OrderedList.OrderedItem> goals = new ArrayList<>();
                        JSONArray arr = new JSONArray();
                        while (test.keySet().iterator().hasNext()) {
                            String key = test.keySet().iterator().next();
                            OrderedList.OrderedItem goal = new OrderedList.OrderedItem();
                            OrderedList.OrderedItem td = temp.getData().get(Integer.parseInt(test.get(key).get(0)));
                            arr.put(obj.getJSONArray("data").getJSONObject(Integer.parseInt(test.get(key).get(0))));
                            goal.setId(td.getId());
                            goal.setOrder_id(td.getOrder_id());
                            goal.setOrder_type(td.getOrder_type());
                            goal.setUserid(td.getUserid());
                            goal.setScheme_code("");
                            float amount = 0f, currentValue = 0f;
                            for (int i = 0; i < td.getOrder_detail().size(); i++) {
                                amount += Float.parseFloat(td.getOrder_detail().get(i).getAmount().trim());
                                currentValue += (Float.parseFloat(td.getOrder_detail().get(i).getNav().trim())
                                        * Float.parseFloat(td.getOrder_detail().get(i).getQuantity().trim()));
                            }
                            goal.setAmount(String.format("%.3f", amount));
                            goal.setBse_order_no("");
                            goal.setOrder_status(td.getOrder_status());
                            goal.setFailed_reson(td.getFailed_reson());
                            goal.setDtdate(td.getDtdate());
                            goal.setPayment_status(td.getPayment_status());
                            goal.setRemarks(td.getRemarks());
                            goal.setFUND_NAME("");
                            goal.setIs_goal(td.getIs_goal());
                            goal.setCurrent_value(String.format("%.3f", currentValue));
                            goal.setFund_invested(String.format("%.1f", ((amount / Float.parseFloat(temp.getTotal_amount())) * 100f)));
                            goal.setNav("");
                            goal.setQuantity("");
                            goal.setIs_allotment(td.getIs_allotment());
                            goal.setWithdrawQty(td.getWithdrawQty());
                            goal.setWithdrawAmt(td.getWithdrawAmt());
                            goal.setRequested(td.getRequested());
                            goal.setFull_withdraw(td.getFull_withdraw());
                            goal.setFUNDS_GUJARATI("");
                            goal.setFUNDS_HINDI("");
                            goal.setFUNDS_MARATHI("");
                            goal.setGoal_detail(td.getGoal_detail());
                            goal.setOrder_detail(td.getOrder_detail());
                            goals.add(goal);
                            test.remove(key);
                        }
                        int[] insertPos = new int[goals.size()];
                        for (int i = 0; i < goals.size(); i++) {
                            for (int j = 0; j < temp.getData().size(); j++) {
                                if (temp.getData().get(j).getOrder_id().equals(goals.get(i).getOrder_id())) {
                                    temp.getData().remove(j);
                                    obj.getJSONArray("data").remove(j);
                                    insertPos[i] = j;
                                    j--;
                                }
                            }
                        }
                        for (int p = 0; p < goals.size(); p++) {
                           // temp.getData().add(goals.get(p));
                            fillAndSet(insertPos[p] + p,goals.get(p),temp.getData());
                         //  temp.getData().add(insertPos[p] + p, goals.get(p));
                            obj.getJSONArray("data").put(insertPos[p] + p, arr.getJSONObject(p));
                        }
                    }
                    list = temp;
                    if (list.getData().size() > 0) {
                        adapter = new InvestedAdapter(contextd, list, true, isPending);
                        orderdList.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.WithDrawReq:
                try {
                    obj = new JSONObject(json);
                    wList = gson.fromJson(json, WithdrawList.class);
                    if (wList.getData().size() > 0) {
                        orderdList.setAdapter(new WithdrawReqAdapter(contextd, wList));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
    public  <T> void fillAndSet(int index, T object, List<T> list)
    {
        if (index > (list.size() - 1))
        {
            for (int i = list.size(); i < index; i++)
            {
                list.add(null);
            }
            list.add(object);
        }
        else
        {
            list.set(index, object);
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderList:
                Common.showToast(contextd, message);
                break;
            case ResponseType.WithDrawReq:
                Common.showToast(contextd, message);
                break;
        }
    }


    SimpleDateFormat std = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en")),
            timeF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("en")),
            monthY = new SimpleDateFormat("MMM yyyy", new Locale("en"));

    double total = 0;
    String bseOrders = "";

    void investInThisFund(boolean isSip, final int pos, final String fundName, final String fundType, final String Amount, final String type) {
        total = 0;
        bseOrders = "";
        if (!isSip && fundType.equals("GOAL")) {
            for (int i = 0; i < list.getData().get(pos).getOrder_detail().size(); i++) {
                total += Double.parseDouble(list.getData().get(pos).getOrder_detail().get(i).getAmount());
                bseOrders += list.getData().get(pos).getOrder_detail().get(i).getBse_order_no();
            }
        } else if (!isSip && fundType.equals("FUND")) {
            total = Float.parseFloat(list.getData().get(pos).getAmount());
            bseOrders = list.getData().get(pos).getBse_order_no();
        }

        if (isSip) {
            if (fundType.equals("GOAL"))
                for (int i = 0; i < sipDueReports.getData().get(pos).getOrder_detail().size(); i++) {
                    bseOrders += "," + sipDueReports.getData().get(pos).getOrder_detail().get(i).getSip_registration_no();
                }
            else if (fundType.equals("FUND"))
                bseOrders = sipDueReports.getData().get(pos).getOrder_detail().get(0).getSip_registration_no();
            total = Float.parseFloat(Amount);
        }
        bseOrders = bseOrders.startsWith(",") ? bseOrders.substring(1, bseOrders.length()) : bseOrders;
        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(Html.fromHtml("<p>" + contextd.getResources().getString(R.string.amount)
                + ":" + "<strong><span style=\"color: #3fd37e;\">"
                + "\t\t" + (fundType.equals("FUND") ? Common.currencyString(Amount, false)
                : Common.currencyString(total + "", false)) + "</span></strong></p>" +
                "<p>" + contextd.getResources().getString(R.string.investFor) + " <strong>" + (fundName) + "</strong>.</p>"));

        builder.setPositiveButton(contextd.getResources().getString(R.string.pay), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
                if (getArguments().getString("orderStatus").equals("CONFIRM"))
                    if (getArguments().getString("paymentStatus").equals("-1")) {
                        Fragment frag = new PendingRedirectingFragment();
                        Bundle b = new Bundle();
                        b.putString("userid", Common.getPreferences(contextd, "userID"));
                        b.putString("orders", bseOrders);
                        b.putString("amount", total + "");
                        b.putString("due_date", "");
                        b.putString("orderType", type);
                        frag.setArguments(b);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    }
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


}
