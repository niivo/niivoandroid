package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 16/2/18.
 */

public class WithdrawList {
    String status,msg;
    ArrayList<WithDrawOrder> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<WithDrawOrder> getData() {
        return data;
    }

    public void setData(ArrayList<WithDrawOrder> data) {
        this.data = data;
    }

    public class WithDrawOrder{
        String id,order_id,dtdate,bse_order_no,remarks,userid,scheme_code,amount,status,
                FUND_NAME,STARMF_SCHEME_NAME,full_withdraw,
                FUNDS_GUJARATI,FUNDS_HINDI,FUNDS_MARATHI;

        public String getFUNDS_GUJARATI() {
            return FUNDS_GUJARATI;
        }

        public void setFUNDS_GUJARATI(String FUNDS_GUJARATI) {
            this.FUNDS_GUJARATI = FUNDS_GUJARATI;
        }

        public String getFUNDS_HINDI() {
            return FUNDS_HINDI;
        }

        public void setFUNDS_HINDI(String FUNDS_HINDI) {
            this.FUNDS_HINDI = FUNDS_HINDI;
        }

        public String getFUNDS_MARATHI() {
            return FUNDS_MARATHI;
        }

        public void setFUNDS_MARATHI(String FUNDS_MARATHI) {
            this.FUNDS_MARATHI = FUNDS_MARATHI;
        }

        public String getFull_withdraw() {
            return full_withdraw;
        }

        public void setFull_withdraw(String full_withdraw) {
            this.full_withdraw = full_withdraw;
        }

        public String getSTARMF_SCHEME_NAME() {
            return STARMF_SCHEME_NAME;
        }

        public void setSTARMF_SCHEME_NAME(String STARMF_SCHEME_NAME) {
            this.STARMF_SCHEME_NAME = STARMF_SCHEME_NAME;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }

        public String getBse_order_no() {
            return bse_order_no;
        }

        public void setBse_order_no(String bse_order_no) {
            this.bse_order_no = bse_order_no;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFUND_NAME() {
            return FUND_NAME;
        }

        public void setFUND_NAME(String FUND_NAME) {
            this.FUND_NAME = FUND_NAME;
        }
    }
}
