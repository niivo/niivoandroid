package com.niivo.com.niivo.Fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;


public class ChangePasswordFragment extends Fragment implements ResponseDelegate {
    Activity _activity;
    Context contextd;
    TextView done;
    ImageButton back;
    TextInputEditText oldPasswordET, newPasswordET, confirmPasswordET;
    TextInputLayout oldPasswordTIL, newPasswordTIL, confirmPasswordTIL;
    ImageView logo;

    //Non ui vars

    private RequestedServiceDataModel requestedServiceDataModel;
    String oldPass, newPass, userid;
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            oldPasswordTIL.setError(null);
            newPasswordTIL.setError(null);
            confirmPasswordTIL.setError(null);
            oldPasswordTIL.setErrorEnabled(false);
            newPasswordTIL.setErrorEnabled(false);
            confirmPasswordTIL.setErrorEnabled(false);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        View parentView = inflater.inflate(R.layout.fragment_change_password, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        textView.setText(contextd.getResources().getString(R.string.changePassword).toUpperCase());

        oldPasswordET = (TextInputEditText) parentView.findViewById(R.id.oldPasswordET);
        newPasswordET = (TextInputEditText) parentView.findViewById(R.id.newPasswordET);
        confirmPasswordET = (TextInputEditText) parentView.findViewById(R.id.retypePasswordET);
        oldPasswordTIL = (TextInputLayout) parentView.findViewById(R.id.oldPasswordTIL);
        newPasswordTIL = (TextInputLayout) parentView.findViewById(R.id.newPasswordTIL);
        confirmPasswordTIL = (TextInputLayout) parentView.findViewById(R.id.retypePasswordTIL);
        logo = (ImageView) parentView.findViewById(R.id.logo);
        logo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        done = (TextView) parentView.findViewById(R.id.button_done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    oldPass = oldPasswordET.getText().toString().trim();
                    newPass = newPasswordET.getText().toString().trim();
                    userid = Common.getPreferences(contextd, "userID");
                    changePassword();
                }
            }
        });
        oldPasswordET.addTextChangedListener(watcher);
        newPasswordET.addTextChangedListener(watcher);
        confirmPasswordET.addTextChangedListener(watcher);
        return parentView;
    }

    boolean validate() {
        boolean b = false;
        if (oldPasswordET.getText().toString().trim().equals("")) {
            oldPasswordTIL.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (oldPasswordET.getText().toString().trim().length() < 6) {
            oldPasswordTIL.setError(contextd.getResources().getString(R.string.useAtLeast6Digits));
            b = false;
        } else if (oldPasswordET.getText().toString().trim().length() > 16) {
            oldPasswordTIL.setError(contextd.getResources().getString(R.string.notMoreThan16Digits));
            b = false;
        } else if (newPasswordET.getText().toString().trim().equals("")) {
            newPasswordTIL.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (newPasswordET.getText().toString().trim().length() < 6) {
            newPasswordTIL.setError(contextd.getResources().getString(R.string.useAtLeast6Digits));
            b = false;
        } else if (newPasswordET.getText().toString().trim().length() > 16) {
            newPasswordTIL.setError(contextd.getResources().getString(R.string.notMoreThan16Digits));
            b = false;
        } else if (confirmPasswordET.getText().toString().trim().equals("")) {
            confirmPasswordTIL.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (confirmPasswordET.getText().toString().trim().length() < 6) {
            confirmPasswordTIL.setError(contextd.getResources().getString(R.string.useAtLeast6Digits));
            b = false;
        } else if (confirmPasswordET.getText().toString().trim().length() > 16) {
            confirmPasswordTIL.setError(contextd.getResources().getString(R.string.notMoreThan16Digits));
            b = false;
        } else if (!confirmPasswordET.getText().toString().trim().equals(newPasswordET.getText().toString().trim())) {
            confirmPasswordTIL.setError(contextd.getResources().getString(R.string.passwordMismatch));
            b = false;
        } else {
            b = true;
        }
        return b;
    }

    void changePassword() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ChangePass);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("oldpass", oldPass);
        requestedServiceDataModel.putQurry("newpass", newPass);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.setWebServiceType("CHANGEPASS");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ChangePass:
          //      Log.d("res", json);
                Common.showToast(contextd, message);
                getActivity().getSupportFragmentManager().popBackStackImmediate();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ChangePass:
          //      Log.d("res", json);
                break;
        }
        Common.showToast(contextd, message);
    }
}

