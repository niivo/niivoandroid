package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 26/3/18.
 */

public class ChooseLanguageActivity extends AppCompatActivity implements View.OnClickListener {
    TextView englishBtn, hindiBtn, gujratiBtn, marathiBtn;
    TextView nextBtn;

    //non ui vars

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(ChooseLanguageActivity.this, Common.getLanguage(ChooseLanguageActivity.this), false);
        setContentView(R.layout.activity_choose_language);
        englishBtn = (TextView) findViewById(R.id.englishBtn);
        hindiBtn = (TextView) findViewById(R.id.hindiBtn);
        gujratiBtn = (TextView) findViewById(R.id.gujratiBtn);
        marathiBtn = (TextView) findViewById(R.id.marathiBtn);
        nextBtn = (TextView) findViewById(R.id.btn_next);
        englishBtn.setOnClickListener(this);
        hindiBtn.setOnClickListener(this);
        gujratiBtn.setOnClickListener(this);
        marathiBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        setLangs();
    }

    void setLangs() {
        if (Common.getLanguage(this).equals("en"))
            englishBtn.setSelected(true);
        else if (Common.getLanguage(this).equals("hi"))
            hindiBtn.setSelected(true);
        else if (Common.getLanguage(this).equals("gu"))
            gujratiBtn.setSelected(true);
        else if (Common.getLanguage(this).equals("mr"))
            marathiBtn.setSelected(true);
        Common.setLanguage(this, "en", false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.englishBtn:
                englishBtn.setSelected(true);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(false);
                Common.setLanguage(this, "en", true);
                break;
            case R.id.hindiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(true);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(false);
                Common.setLanguage(this, "hi", true);
                break;
            case R.id.gujratiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(true);
                marathiBtn.setSelected(false);
                Common.setLanguage(this, "gu", true);
                break;
            case R.id.marathiBtn:
                englishBtn.setSelected(false);
                hindiBtn.setSelected(false);
                gujratiBtn.setSelected(false);
                marathiBtn.setSelected(true);
                Common.setLanguage(this, "mr", true);
                break;
            case R.id.btn_next:
                startActivity(new Intent(ChooseLanguageActivity.this, CommonSignUpSignInActivity.class));
                ChooseLanguageActivity.this.finish();
                break;
        }
    }
}
