package com.niivo.com.niivo.Utilities.BeginnerHelper;

import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Investment {
    private int roundedLumpSumAmt;
    private int roundedSipAmt;
    private int[] lumpSumAallocationAmts;
    private int[] sipAllocationAmts;
    private int lowestLumpSumAmt;
    private int lowestSipAmt;
    private LumpSumComparator lumpSumComparator;
    private SipComparator sipComparator;

    public Investment(double finalAmt, double timeInYrs, double annualRatePerc, List<InputFund> listOfInputFunds, boolean isSIP) {
        lumpSumComparator = new LumpSumComparator();
        sipComparator = new SipComparator();
        annualRatePerc = annualRatePerc / 100;

        //Calculate the rounded lump sum amount up to the next multiple of 500
        double lumpSumAmt = finalAmt * Math.pow((1 + (annualRatePerc)), (-1d * timeInYrs));
        allocateLumpSum(lumpSumAmt, listOfInputFunds);
        Log.d("listOfInputFunds...", String.valueOf(listOfInputFunds.size()));
        Log.d("lumpSumAmt...", String.valueOf(lumpSumAmt));
        //Calculate the rounded SIP amount up to the next multiple of 100
        double multiplyingFactor = 0;
        double timeInMths = timeInYrs * 12;
        for (int i = 0; i <= (int) timeInMths; i++) {
            multiplyingFactor += Math.pow((1 + annualRatePerc / 12d), i);
        }

        double sipAmt = finalAmt / multiplyingFactor;
        //roundedSipAmt = (((int)sipAmt + (roundOffToNearestSipNumber - 1)) / roundOffToNearestSipNumber ) * roundOffToNearestSipNumber;
        Log.d("finalAmt", String.valueOf(finalAmt));
        Log.d("multiplyingFactor", String.valueOf(multiplyingFactor));
        Log.d("sipAmt1", String.valueOf(sipAmt));
        if (isSIP)
            allocateSip(sipAmt, listOfInputFunds);
             Log.d("listOfInputFunds...", String.valueOf(listOfInputFunds));
    }

    private void allocateLumpSum(double lumpSumAmt, List<InputFund> listOfInputFunds) {

        if (listOfInputFunds.size() == 3) {
            if (lumpSumAmt > (listOfInputFunds.get(0).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(1).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(2).getMinLumpSumInvestmentAmt())) {
                int roundingFactor1 = listOfInputFunds.get(0).getMinLumpSumInvestmentAmt();
                double fortyPercExact = 0.4d * lumpSumAmt;
                int fortyPercRounded = (((int) fortyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;
                Log.d("roundingFactor1...", String.valueOf(roundingFactor1));

                int roundingFactor2 = listOfInputFunds.get(1).getMinLumpSumInvestmentAmt();
                double thirtyPercExact1 = 0.3d * lumpSumAmt;
                int thirtyPercRounded1 = (((int) thirtyPercExact1 + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;
                Log.d("roundingFactor2...", String.valueOf(roundingFactor2));

                int roundingFactor3 = listOfInputFunds.get(2).getMinLumpSumInvestmentAmt();
                double thirtyPercExact2 = 0.3d * lumpSumAmt;
                int thirtyPercRounded2 = (((int) thirtyPercExact2) / roundingFactor3) * roundingFactor3;
                Log.d("roundingFactor3...", String.valueOf(roundingFactor3));

                double remainingFinal = lumpSumAmt - (fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2);
                InputFund lowestLumpSumFund = Collections.min(listOfInputFunds, lumpSumComparator);
                int roundingFactorFinal = lowestLumpSumAmt = lowestLumpSumFund.getMinLumpSumInvestmentAmt();
                Log.d("roundingFactorFinal...", String.valueOf(roundingFactorFinal));

                int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;

                roundedLumpSumAmt = fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2 + roundedRemaining;
                Log.d("roundedLumpSumAmt...", String.valueOf(roundedLumpSumAmt));

                lumpSumAallocationAmts = new int[3];
                lumpSumAallocationAmts[0] = fortyPercRounded;
                lumpSumAallocationAmts[1] = thirtyPercRounded1;
                lumpSumAallocationAmts[2] = thirtyPercRounded2;

                int lowestLumpSumpIdx = listOfInputFunds.indexOf(lowestLumpSumFund);
                if (lowestLumpSumpIdx != lumpSumAallocationAmts.length)
                    lumpSumAallocationAmts[lowestLumpSumpIdx] = lumpSumAallocationAmts[lowestLumpSumpIdx] + roundedRemaining;

            } else if (lumpSumAmt > (listOfInputFunds.get(0).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(1).getMinLumpSumInvestmentAmt())
                    && lumpSumAmt < (listOfInputFunds.get(0).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(1).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(2).getMinLumpSumInvestmentAmt())) {
                List<InputFund> copyList = Arrays.asList(listOfInputFunds.get(0), listOfInputFunds.get(1));
                int roundingFactor1 = copyList.get(0).getMinLumpSumInvestmentAmt();
                double sixtyPercExact = 0.6d * lumpSumAmt;
                int sixtyPercRounded = (((int) sixtyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;

                int roundingFactor2 = listOfInputFunds.get(1).getMinLumpSumInvestmentAmt();
                double fortyPercExact = 0.4d * lumpSumAmt;
                int fortyPercRounded = (((int) fortyPercExact + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;

                double remainingFinal = lumpSumAmt - (sixtyPercRounded + fortyPercRounded);
                InputFund lowestLumpSumFund = Collections.min(listOfInputFunds, lumpSumComparator);
                int roundingFactorFinal = lowestLumpSumAmt = lowestLumpSumFund.getMinLumpSumInvestmentAmt();

                int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;
                roundedLumpSumAmt = sixtyPercRounded + fortyPercRounded + roundedRemaining;
                lumpSumAallocationAmts = new int[2];
                lumpSumAallocationAmts[0] = sixtyPercRounded;
                lumpSumAallocationAmts[1] = fortyPercRounded;

                int lowestLumpSumpIdx = listOfInputFunds.indexOf(lowestLumpSumFund);
                if (lowestLumpSumpIdx != lumpSumAallocationAmts.length)
                    lumpSumAallocationAmts[lowestLumpSumpIdx] = lumpSumAallocationAmts[lowestLumpSumpIdx] + roundedRemaining;
            } else {
                InputFund lowestLumpSumFund = Collections.min(listOfInputFunds, lumpSumComparator);
                int roundingFactorFinal = lowestLumpSumAmt = lowestLumpSumFund.getMinLumpSumInvestmentAmt();
                roundedLumpSumAmt = (int) Math.ceil(lumpSumAmt / roundingFactorFinal) * roundingFactorFinal;
                lumpSumAallocationAmts = new int[1];
                lumpSumAallocationAmts[0] = roundedLumpSumAmt;
            }

        }else{
            if (lumpSumAmt > (listOfInputFunds.get(0).getMinLumpSumInvestmentAmt() + listOfInputFunds.get(1).getMinLumpSumInvestmentAmt() )) {
                int roundingFactor1 = listOfInputFunds.get(0).getMinLumpSumInvestmentAmt();
                double fortyPercExact = 0.5d * lumpSumAmt;
                int fortyPercRounded = (((int) fortyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;
                Log.d("roundingFactor1...", String.valueOf(roundingFactor1));

                int roundingFactor2 = listOfInputFunds.get(1).getMinLumpSumInvestmentAmt();
                double thirtyPercExact1 = 0.5d * lumpSumAmt;
                int thirtyPercRounded1 = (((int) thirtyPercExact1 + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;
                Log.d("roundingFactor2...", String.valueOf(roundingFactor2));

                double remainingFinal = lumpSumAmt - (fortyPercRounded + thirtyPercRounded1 );
                InputFund lowestLumpSumFund = Collections.min(listOfInputFunds, lumpSumComparator);
                int roundingFactorFinal = lowestLumpSumAmt = lowestLumpSumFund.getMinLumpSumInvestmentAmt();
                Log.d("roundingFactorFinal...", String.valueOf(roundingFactorFinal));

                int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;

                roundedLumpSumAmt = fortyPercRounded + thirtyPercRounded1  + roundedRemaining;
                Log.d("roundedLumpSumAmt...", String.valueOf(roundedLumpSumAmt));

                lumpSumAallocationAmts = new int[2];
                lumpSumAallocationAmts[0] = fortyPercRounded;
                lumpSumAallocationAmts[1] = thirtyPercRounded1;


                int lowestLumpSumpIdx = listOfInputFunds.indexOf(lowestLumpSumFund);
                if (lowestLumpSumpIdx != lumpSumAallocationAmts.length)
                    lumpSumAallocationAmts[lowestLumpSumpIdx] = lumpSumAallocationAmts[lowestLumpSumpIdx] + roundedRemaining;

            }else {
                InputFund lowestLumpSumFund = Collections.min(listOfInputFunds, lumpSumComparator);
                int roundingFactorFinal = lowestLumpSumAmt = lowestLumpSumFund.getMinLumpSumInvestmentAmt();
                roundedLumpSumAmt = (int) Math.ceil(lumpSumAmt / roundingFactorFinal) * roundingFactorFinal;
                lumpSumAallocationAmts = new int[1];
                lumpSumAallocationAmts[0] = roundedLumpSumAmt;

            }
        }
    }

    private void allocateSip(double sipAmt, List<InputFund> listOfInputFunds) {
        Log.d("sipAmt", String.valueOf(sipAmt));
        Log.d("listOfInputFunds", String.valueOf(listOfInputFunds));
        if(listOfInputFunds.size()==3){
        if (sipAmt > (listOfInputFunds.get(0).getMinSipInvestmentAmt() + listOfInputFunds.get(1).getMinSipInvestmentAmt() + listOfInputFunds.get(2).getMinSipInvestmentAmt())) {
            int roundingFactor1 = listOfInputFunds.get(0).getMinSipInvestmentAmt();
            double fortyPercExact = 0.4d * sipAmt;
            int fortyPercRounded = (((int) fortyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;

            int roundingFactor2 = listOfInputFunds.get(1).getMinSipInvestmentAmt();
            double thirtyPercExact1 = 0.3d * sipAmt;
            int thirtyPercRounded1 = (((int) thirtyPercExact1 + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;


            int roundingFactor3 = listOfInputFunds.get(2).getMinSipInvestmentAmt();
            double thirtyPercExact2 = 0.3d * sipAmt;
            int thirtyPercRounded2 = (((int) thirtyPercExact2) / roundingFactor3) * roundingFactor3;

            double remainingFinal = sipAmt - (fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2);
            InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
            int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();

            int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;

            roundedSipAmt = fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2 + roundedRemaining;

            sipAllocationAmts = new int[3];
            sipAllocationAmts[0] = fortyPercRounded;
            sipAllocationAmts[1] = thirtyPercRounded1;
            sipAllocationAmts[2] = thirtyPercRounded2;

            int lowestSipIdx = listOfInputFunds.indexOf(lowestSipFund);
            sipAllocationAmts[lowestSipIdx] = sipAllocationAmts[lowestSipIdx] + roundedRemaining;

        } else if (sipAmt > (listOfInputFunds.get(0).getMinSipInvestmentAmt() + listOfInputFunds.get(1).getMinSipInvestmentAmt())
                        && sipAmt < (listOfInputFunds.get(0).getMinSipInvestmentAmt() + listOfInputFunds.get(1).getMinSipInvestmentAmt() + listOfInputFunds.get(2).getMinSipInvestmentAmt())) {

//            List<InputFund> copyList = Arrays.asList(listOfInputFunds.get(0), listOfInputFunds.get(1));
//            int roundingFactor1 = copyList.get(0).getMinSipInvestmentAmt();
//            double sixtyPercExact = 0.6d * sipAmt;
//            int sixtyPercRounded = (((int) sixtyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;
//
//            int roundingFactor2 = listOfInputFunds.get(1).getMinSipInvestmentAmt();
//            double fortyPercExact = 0.4d * sipAmt;
//            int fortyPercRounded = (((int) fortyPercExact + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;
//
//            double remainingFinal = sipAmt - (sixtyPercRounded + fortyPercRounded);
//            InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
//            int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();
//
//            int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;
//            roundedSipAmt = sixtyPercRounded + fortyPercRounded + roundedRemaining;
//            sipAllocationAmts = new int[2];
//            sipAllocationAmts[0] = sixtyPercRounded;
//            sipAllocationAmts[1] = fortyPercRounded;
//
//            int lowestSipIdx = listOfInputFunds.indexOf(lowestSipFund);
//            sipAllocationAmts[lowestSipIdx] = sipAllocationAmts[lowestSipIdx] + roundedRemaining;

            int roundingFactor1 = listOfInputFunds.get(0).getMinSipInvestmentAmt();
            double fortyPercExact = 0.4d * sipAmt;
            int fortyPercRounded = (((int) fortyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;

            int roundingFactor2 = listOfInputFunds.get(1).getMinSipInvestmentAmt();
            double thirtyPercExact1 = 0.3d * sipAmt;
            int thirtyPercRounded1 = (((int) thirtyPercExact1 + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;


            int roundingFactor3 = listOfInputFunds.get(2).getMinSipInvestmentAmt();
            double thirtyPercExact2 = 0.3d * sipAmt;
            int thirtyPercRounded2 = (((int) thirtyPercExact2) / roundingFactor3) * roundingFactor3;

            double remainingFinal = sipAmt - (fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2);
            InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
            int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();

            int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;

            roundedSipAmt = fortyPercRounded + thirtyPercRounded1 + thirtyPercRounded2 + roundedRemaining;

            sipAllocationAmts = new int[3];
            sipAllocationAmts[0] = fortyPercRounded;
            sipAllocationAmts[1] = thirtyPercRounded1;
            sipAllocationAmts[2] = thirtyPercRounded2;

            int lowestSipIdx = listOfInputFunds.indexOf(lowestSipFund);
            sipAllocationAmts[lowestSipIdx] = sipAllocationAmts[lowestSipIdx] + roundedRemaining;


        } else {
            InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
            int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();
            roundedSipAmt = (int) Math.ceil(sipAmt / roundingFactorFinal) * roundingFactorFinal;
            sipAllocationAmts = new int[1];
            sipAllocationAmts[0] = roundedSipAmt;
        }
        } else {
            if (sipAmt > (listOfInputFunds.get(0).getMinSipInvestmentAmt() + listOfInputFunds.get(1).getMinSipInvestmentAmt())) {
                int roundingFactor1 = listOfInputFunds.get(0).getMinSipInvestmentAmt();
                double fortyPercExact = 0.5d * sipAmt;
                int fortyPercRounded = (((int) fortyPercExact + (roundingFactor1 - 1)) / roundingFactor1) * roundingFactor1;

                int roundingFactor2 = listOfInputFunds.get(1).getMinSipInvestmentAmt();
                double thirtyPercExact1 = 0.5d * sipAmt;
                int thirtyPercRounded1 = (((int) thirtyPercExact1 + (roundingFactor2 - 1)) / roundingFactor2) * roundingFactor2;


                double remainingFinal = sipAmt - (fortyPercRounded + thirtyPercRounded1);
                InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
                int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();

                int roundedRemaining = (int) Math.ceil(remainingFinal / roundingFactorFinal) * roundingFactorFinal;

                roundedSipAmt = fortyPercRounded + thirtyPercRounded1 + roundedRemaining;

                sipAllocationAmts = new int[2];
                sipAllocationAmts[0] = fortyPercRounded;
                sipAllocationAmts[1] = thirtyPercRounded1;

                int lowestSipIdx = listOfInputFunds.indexOf(lowestSipFund);
                sipAllocationAmts[lowestSipIdx] = sipAllocationAmts[lowestSipIdx] + roundedRemaining;

            } else {

                InputFund lowestSipFund = Collections.min(listOfInputFunds, sipComparator);
                int roundingFactorFinal = lowestSipAmt = lowestSipFund.getMinSipInvestmentAmt();
                roundedSipAmt = (int) Math.ceil(sipAmt / roundingFactorFinal) * roundingFactorFinal;
                sipAllocationAmts = new int[1];
                sipAllocationAmts[0] = roundedSipAmt;
            }
        }
    }

    public int getLumpSumAmt() {
        return roundedLumpSumAmt;
    }

    public int getSipAmt() {
        return roundedSipAmt;
    }

    public int[] getLumpSumAllocations() {
        return lumpSumAallocationAmts;
    }

    public int[] getSipAllocations() {
        return sipAllocationAmts;
    }

    public int getLowestLumpSumAmt() {
        return lowestLumpSumAmt;
    }

    public int getLowestSipAmt() {
        return lowestSipAmt;
    }

    private class LumpSumComparator implements Comparator<InputFund> {

        @Override
        public int compare(InputFund o1, InputFund o2) {
            int val = 0;
            if (o1.getMinLumpSumInvestmentAmt() < o2.getMinLumpSumInvestmentAmt()) {
                val = -1;
            } else if (o1.getMinLumpSumInvestmentAmt() == o2.getMinLumpSumInvestmentAmt()) {
                val = 0;
            } else if (o1.getMinLumpSumInvestmentAmt() > o2.getMinLumpSumInvestmentAmt()) {
                val = 1;
            }
            return val;
        }

    }

    private class SipComparator implements Comparator<InputFund> {

        @Override
        public int compare(InputFund o1, InputFund o2) {
            int val = 0;
            if (o1.getMinSipInvestmentAmt() < o2.getMinSipInvestmentAmt()) {
                val = -1;
            } else if (o1.getMinSipInvestmentAmt() == o2.getMinSipInvestmentAmt()) {
                val = 0;
            } else if (o1.getMinSipInvestmentAmt() > o2.getMinSipInvestmentAmt()) {
                val = 1;
            }
            return val;
        }

    }

}