package com.niivo.com.niivo.Utils;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.NetWorkCheck.NetworkDialogAct;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 25/5/17.
 */

public class Common {
    public Common() {
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    public static void showDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.myAlertDialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void SetPreferences(Context con, String key, String value) {
        // save the data
        SharedPreferences preferences = con.getSharedPreferences("prefs_login", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getPreferences(Context con, String key) {
        // save the data1
        SharedPreferences preferences = con.getSharedPreferences("prefs_login", 0);
        return preferences.getString(key, "");
    }

    public static void addToken(Context context, String value) {
        // save the data
        SharedPreferences preferences = context.getSharedPreferences("NiivoToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", value);
        editor.commit();
    }

    public static String getToken(Context context) {
        // save the data
        SharedPreferences preferences = context.getSharedPreferences("NiivoToken", Context.MODE_PRIVATE);
        if (preferences.getString("token", "").isEmpty()) {
            addToken(context, FirebaseInstanceId.getInstance().getToken() + "");
        }
        return preferences.getString("token", "");
    }

    public static boolean getConnectivityStatus(Activity activity) {
        ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getActiveNetworkInfo();
        if (info != null)
            if (info.isConnected()) {
                return true;
            } else {
                return false;
            }
        else
            return false;
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void hideKeyboard(Activity _activity) {
        View view = _activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) _activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage, String title) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, title, null);
        return Uri.parse(path);
    }

    public static String getImagePath(Context context, Uri pickedImage) {
        boolean isGreaterKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        String imagePath = "";
        if (isGreaterKitKat && DocumentsContract.isDocumentUri(context, pickedImage)) {
//        Whether the Uri authority is ExternalStorageProvider.
            if ("com.android.externalstorage.documents".equals(pickedImage.getAuthority())) {
                final String docId = DocumentsContract.getDocumentId(pickedImage);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    imagePath = Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
//        Whether the Uri authority is DownloadsProvider.
            else if ("com.android.providers.downloads.documents".equals(pickedImage.getAuthority())) {
                String id = DocumentsContract.getDocumentId(pickedImage);
                Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                imagePath = getDataColumn(context, pickedImage, null, null);
            }
//        Whether the Uri authority is MediaProvider.
            else if ("com.android.providers.media.documents".equals(pickedImage.getAuthority())) {
                String docId = DocumentsContract.getDocumentId(pickedImage);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };
                imagePath = getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(pickedImage.getScheme())) {

//       Whether the Uri authority is Google Photos.
            if ("com.google.android.apps.photos.content".equals(pickedImage.getAuthority())) {
                imagePath = pickedImage.getLastPathSegment();
            } else
                imagePath = getDataColumn(context, pickedImage, null, null);
        } else if ("file".equalsIgnoreCase(pickedImage.getScheme())) {
            imagePath = pickedImage.getPath();
        }
//        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//        Cursor cursor = context.getContentResolver().query(pickedImage, filePathColumn, null, null, null);
//        cursor.moveToFirst();
//        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//        String picturePath = cursor.getString(columnIndex);
        return imagePath;
    }

    public static String getDataColumn(Context context, Uri pickedImage, String sel, String[] SelARg) {
        String selection = null;
        if (sel != null)
            selection = sel;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        String[] selectionARg = null;
        if (SelARg != null)
            selectionARg = SelARg;
        Cursor cursor = context.getContentResolver().query(pickedImage, filePathColumn, selection, selectionARg, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        return picturePath;
    }


    public static Bitmap rotatedBitmap(Context contextd, Bitmap bm, Uri imageUri) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(getImagePath(contextd, imageUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = Common.rotateImage(bm, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = Common.rotateImage(bm, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = Common.rotateImage(bm, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bm;
        }
        return rotatedBitmap;
    }


    public static String getBitmapBase64(Bitmap bit) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        return encoded;
    }

    public static String getDateString(Calendar ct) {
        String formatedDate = "";
        SimpleDateFormat date = new SimpleDateFormat("dd", new Locale("en")),
                monthYear = new SimpleDateFormat("MMM yyyy", new Locale("en"));
        formatedDate = date.format(ct.getTime());
        int dt = Integer.parseInt(date.format(ct.getTime()));
        if (dt >= 11 && dt <= 13) {
            formatedDate = formatedDate + "th";
        } else
            switch (dt % 10) {
                case 1:
                    formatedDate = formatedDate + "st";
                    break;
                case 2:
                    formatedDate = formatedDate + "nd";
                    break;
                case 3:
                    formatedDate = formatedDate + "rd";
                    break;
                default:
                    formatedDate = formatedDate + "th";
                    break;
            }
        formatedDate = formatedDate + " " + monthYear.format(ct.getTime());
        return formatedDate;
    }


    public static void setLanguage(Activity activity, String lang, boolean recreate) {
        SharedPreferences preferences = activity.getSharedPreferences("language", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lan", lang);
        editor.commit();
        Configuration config = activity.getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.util.Log.e("Language", "local:" + lang + "\nPersent:" + config.locale.getLanguage());
        config.locale = locale;
        activity.getBaseContext().getResources().updateConfiguration(config, activity.getBaseContext().getResources().getDisplayMetrics());
        activity.onConfigurationChanged(config);
        if (recreate) {
            if (!lang.equals(config.locale.getLanguage()))
                activity.recreate();
            else
                activity.recreate();
        }

    }

    public static String getLanguage(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("language", 0);
        return preferences.getString("lan", "");
    }

    public static String sendLanguage(Context context) {
        String lang = "";
        lang = getLanguage(context);
        lang = lang.equals("en") ? "EN" :
                lang.equals("hi") ? "HI" :
                        lang.equals("gu") ? "GJ" :
                                lang.equals("mr") ? "MH" : "EN";
        return lang;
    }

    public static String currencyString(String string, boolean isNAV) {
        String str = "";
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setCurrencySymbol("\u20B9"); // Don't use null.
        formatter.setDecimalFormatSymbols(symbols);
        str = formatter.format(Double.parseDouble(string));
        if (str.contains(".") && !isNAV)
            str = str.substring(0, str.length() - 3);
        return str;
    }


    public static String currencyString(String value, boolean withCurrencySymbol, boolean withDecimals) {

        String str = "";
        if (value.isEmpty()) {
        } else {
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            if (withCurrencySymbol)
                symbols.setCurrencySymbol("\u20B9");
            else
                symbols.setCurrencySymbol("");
            // Don't use null.
            formatter.setDecimalFormatSymbols(symbols);
            str = formatter.format(Double.parseDouble(value));
            if (str.contains(".") && !withDecimals)
                str = str.substring(0, str.length() - 3);
        }
        return str.trim();
    }

    public static void showNetworkDio(Context context) {
        context.startActivity(new Intent(context, NetworkDialogAct.class));
    }

    public static boolean validatePanCard(String string) {
        boolean matchPanCard = false;
        if (string == null || string.isEmpty()) {
            matchPanCard = false;
        } else {
            char c = string.charAt(string.length() - 1);
            if (string.length() <= 5)
                matchPanCard = !Character.isDigit(c);
            else if (string.length() <= 9)
                matchPanCard = Character.isDigit(c);
            else
                matchPanCard = !Character.isDigit(c);
            if (string.length() > 10)
                matchPanCard = false;
        }
        return matchPanCard;
    }
}
