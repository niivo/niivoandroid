package com.niivo.com.niivo.Utils;

/**
 * Created by user4 on 30/5/16.
 */
public class Log {

    public static void log(String data) {

        android.util.Log.e("MyShopie", data + "");
        System.out.print("Niivo:\n" + data);
    }

    public static void log(String tag, String data) {

        android.util.Log.e(tag, data);
    }
}
