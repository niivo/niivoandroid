package com.niivo.com.niivo.Utilities.BeginnerHelper;

public class InputFund{
	private String isin;
	private int minLumpSumInvestmentAmt;
	private int minSipInvestmentAmt;
	private double score;
	
	public InputFund(String isin, int minLumpSumInvestmentAmt, int minSipInvestmentAmt, double score){
		this.isin = isin;
		this.minLumpSumInvestmentAmt = minLumpSumInvestmentAmt;
		this.minSipInvestmentAmt = minSipInvestmentAmt;
		this.score = score;
	}
	
	public String getIsin() {
		return isin;
	}

	public int getMinLumpSumInvestmentAmt() {
		return minLumpSumInvestmentAmt;
	}

	public int getMinSipInvestmentAmt() {
		return minSipInvestmentAmt;
	}
	
	public double getScore(){
		return score;
	}
}