package com.niivo.com.niivo.Utils.NotificationHelpers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.niivo.com.niivo.Activity.SplashActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        System.out.println("RemoteMessage: " + remoteMessage.getData());
        // Check if message contains a notification payload.
        Map<String, String> map = remoteMessage.getData();

        Log.e("mapLength", map.size() + "");

        if (remoteMessage.getData() != null) {
            if (!Common.getPreferences(this, "collapse").equals(remoteMessage.getCollapseKey())) {
                Common.SetPreferences(this, "collapse", remoteMessage.getCollapseKey());
                sendNotification(remoteMessage.getData().get("message") + "");
            } else
                Log.e("Notic", "Existing Notification.");
//            if (nt.getBody() != null)
//                sendNotification(nt.getBody() + "");
        }


    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this)
                .setAutoCancel(true)
                .setTicker("Niivo")
                .setSmallIcon(R.drawable.notify)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            notificationBuilder = notificationBuilder.setContent(getComplexNotificationView(messageBody));
//        } else {
        NotificationCompat.BigTextStyle bigStyle =
                new NotificationCompat.BigTextStyle();
        bigStyle.setBigContentTitle("Niivo");
        bigStyle.bigText(messageBody);

        notificationBuilder = notificationBuilder
                .setSmallIcon(R.drawable.notify)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Niivo")
                .setContentText(messageBody)
                .setStyle(bigStyle);
//        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder = notificationBuilder.setColor(Color.parseColor("#3fd37e"));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel("Niivo");
            if (mChannel == null) {
                mChannel = new NotificationChannel("Niivo", "Niivo", importance);
                mChannel.setDescription("Niivo");
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationBuilder = notificationBuilder.setChannelId("Niivo");
        }
        notificationManager.notify(
                ((int) System.currentTimeMillis()), notificationBuilder.build());
    }

}