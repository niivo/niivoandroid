package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Model.CartItemList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 12/6/17.
 */

public class MyCartAdapter extends BaseAdapter {
    Context contextd;
    CartItemList list;
    CartHolder holder;
    OnViewHelper helper;
    String lang = "";

    public MyCartAdapter(Context context, CartItemList res, OnViewHelper help) {
        this.contextd = context;
        this.list = res;
        this.helper = help;
        lang = Common.getLanguage(contextd);
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    int selectedPos = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.item_cart, parent, false);
            holder = new CartHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CartHolder) convertView.getTag();
        }
        if (list.getData().get(position).getIs_goal().equals("0")) {
            holder.fundTitle.setText(lang.equals("en")
                    ? list.getData().get(position).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME()
                    : lang.equals("hi")
                    ? list.getData().get(position).getCart_detail().get(0).getScheme_detail().getFUNDS_HINDI()
                    : lang.equals("gu")
                    ? list.getData().get(position).getCart_detail().get(0).getScheme_detail().getFUNDS_GUJARATI()
                    : lang.equals("mr")
                    ? list.getData().get(position).getCart_detail().get(0).getScheme_detail().getFUNDS_MARATHI()
                    : list.getData().get(position).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME());
            holder.fundType.setText(R.string.fund_td);
            holder.amount.setText(Common.currencyString(list.getData().get(position).getCart_detail().get(0).getScheme_amount().trim(), false));
            holder.fundIcon.setImageResource(R.drawable.rupee);
            holder.cartEdit.setVisibility(View.VISIBLE);
        } else {
            holder.fundType.setText(R.string.goal_td);
            holder.fundTitle.setText(list.getData().get(position).getCart_detail().get(0).getGoal_name());
            holder.cartEdit.setVisibility(View.GONE);
            double total = 0;
            for (int i = 0; i < list.getData().get(position).getCart_detail().size(); i++) {
                total += Double.parseDouble(list.getData().get(position).getCart_detail().get(i).getScheme_amount());
            }
            holder.amount.setText(Common.currencyString(total + "", false));
            holder.fundIcon.setImageResource(getCateImageID(list.getData().get(position).getCart_detail().get(0).getGoal_cat_id()));
        }
        holder.dateAdded.setText(list.getData().get(position).getAdded_date());

        holder.cartEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = position;
                editFund(selectedPos);
            }
        });
        holder.cartDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = position;
                deleteFund(selectedPos);
            }
        });
        holder.parentPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPos = position;
                helper.investFund(selectedPos);
            }
        });
        return convertView;
    }

    int getCateImageID(String cateid) {
        Log.e("Res", cateid);
        int cate = 0;
        if (cateid.equals("1"))
            cate = R.drawable.retirementfund;
        else if (cateid.equals("2"))
            cate = R.drawable.childswedding;
        else if (cateid.equals("3"))
            cate = R.drawable.goal_tax;
        else if (cateid.equals("4"))
            cate = R.drawable.vacation;
        else if (cateid.equals("5"))
            cate = R.drawable.buyingcar;
        else if (cateid.equals("6"))
            cate = R.drawable.buyinghouse;
        else if (cateid.equals("7"))
            cate = R.drawable.startingbusiness;
        else if (cateid.equals("8"))
            cate = R.drawable.startinganemergencyfund;
        else if (cateid.equals("9"))
            cate = R.drawable.wealthcreation;
        else
            cate = R.drawable.rupee;
        return cate;
    }

    class CartHolder {
        TextView fundTitle, dateAdded, amount, fundType, cartEdit, cartDelete;
        ImageView fundIcon;
        LinearLayout parentPanel;

        public CartHolder(View v) {
            fundTitle = (TextView) v.findViewById(R.id.title);
            dateAdded = (TextView) v.findViewById(R.id.dateAdded);
            amount = (TextView) v.findViewById(R.id.fundAmount);
            fundType = (TextView) v.findViewById(R.id.type);
            fundIcon = (ImageView) v.findViewById(R.id.logo);
            cartDelete = (TextView) v.findViewById(R.id.cartDelete);
            cartEdit = (TextView) v.findViewById(R.id.cartEdit);
            parentPanel = (LinearLayout) v.findViewById(R.id.parentPanel);
        }
    }

    void deleteFund(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(R.string.you_want_to_delete);
        builder.setPositiveButton(contextd.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
                helper.deduct(pos);
                MyCartAdapter.this.notifyDataSetChanged();
                selectedPos = -1;
            }
        });
        builder.setNegativeButton(contextd.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    AlertDialog editFundDialog;

    void editFund(final int pos) {
        selectedPos = pos;
        TextView fundTitle, editTitle, forYear, forPrice;
        final String minAmmount = list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSTARMF_MIN_PUR_AMT().trim(),
                // final String minAmmount = list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getMIN_INIT().trim(),
                sipMultiplier = list.getData().get(pos).getOrder_type().equals("ONETIME") ? "" :
                        ((list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSIP_MULTPL_AMT() == null ||
                                list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSIP_MULTPL_AMT().equals("null")) ? "1" :
                                list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSIP_MULTPL_AMT());
        final EditText fundAmmount;
        TextView save;
        AlertDialog.Builder builder = new AlertDialog.Builder(
                contextd, R.style.myAlertDialogTheme);
        View dialogView = LayoutInflater.from(contextd).inflate(R.layout.dio_cart_edit_amnt, null);
        builder.setView(dialogView);
        fundTitle = (TextView) dialogView.findViewById(R.id.fundTitle);
        editTitle = (TextView) dialogView.findViewById(R.id.editTitle);
        forYear = (TextView) dialogView.findViewById(R.id.forYear);
        forPrice = (TextView) dialogView.findViewById(R.id.forPrice);
        fundAmmount = (EditText) dialogView.findViewById(R.id.editAmnt);
        save = (TextView) dialogView.findViewById(R.id.editSave);
        fundTitle.setText(lang.equals("en")
                ? list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME()
                : lang.equals("hi")
                ? list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getFUNDS_HINDI()
                : lang.equals("gu")
                ? list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getFUNDS_GUJARATI()
                : lang.equals("mr")
                ? list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getFUNDS_MARATHI()
                : list.getData().get(pos).getCart_detail().get(0).getScheme_detail().getSCHEME_NAME());
        editTitle.setText(list.getData().get(pos).getOrder_type().toLowerCase().trim().equals("onetime")
                ? contextd.getResources().getString(R.string.i_will_invest)
                : contextd.getResources().getString(R.string.i_can_set_aside));
        if (!list.getData().get(pos).getOrder_type().toLowerCase().trim().equals("onetime"))
            forYear.setText(contextd.getResources().getString(R.string.no_of_months) + list.getData().get(pos).getNo_of_months());
        else
            forYear.setVisibility(View.GONE);
        fundAmmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (fundAmmount.getText().toString().trim().startsWith("0")) {
                    fundAmmount.setText("");
                }
            }
        });

        forPrice.setText(contextd.getResources().getString(R.string.last_amount_r) + list.getData().get(pos).getCart_detail().get(0).getScheme_amount());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fundAmmount.getText().toString().trim().equals("")) {
                    fundAmmount.setError(contextd.getResources().getString(R.string.requiredField));
                } else if (fundAmmount.getText().toString().trim().length() < 3) {
                    fundAmmount.setError(contextd.getResources().getString(R.string.invalidAmount));
                } else if (Double.parseDouble(minAmmount) > Double.parseDouble(fundAmmount.getText().toString().trim())) {
                    fundAmmount.setError(contextd.getResources().getString(R.string.amountIsTooLow));
                } else if (!list.getData().get(pos).getOrder_type().equals("ONETIME")) {
                    if ((Double.parseDouble(fundAmmount.getText().toString().trim()) % ((int) Float.parseFloat(sipMultiplier))) != 0) {
                        fundAmmount.setError(contextd.getResources().getString(R.string.amountShouldMultiple) + (int) Float.parseFloat(sipMultiplier));
                    } else {
                        list.getData().get(pos).getCart_detail().get(0).setScheme_amount(fundAmmount.getText().toString().trim());
                        MyCartAdapter.this.notifyDataSetChanged();
                        helper.editedAmnt(list.getData().get(selectedPos), selectedPos);
                        editFundDialog.dismiss();
                    }
                } else {
                    list.getData().get(pos).getCart_detail().get(0).setScheme_amount(fundAmmount.getText().toString().trim());
                    MyCartAdapter.this.notifyDataSetChanged();
                    helper.editedAmnt(list.getData().get(selectedPos), selectedPos);
                    editFundDialog.dismiss();
                }
            }
        });
        editFundDialog = builder.create();
        editFundDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        editFundDialog.show();
    }


    public interface OnViewHelper {
        void deduct(int pos);

        void editedAmnt(CartItemList.CartItem cart, int pos);

        void investFund(int pos);
    }
}
