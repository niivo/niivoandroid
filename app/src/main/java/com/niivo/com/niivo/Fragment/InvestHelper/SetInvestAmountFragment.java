package com.niivo.com.niivo.Fragment.InvestHelper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.HomeFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCRegistrationFragment;
import com.niivo.com.niivo.Fragment.KYCnew.KYCStatusFragment;
import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.CartHelper.CartModle;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 13/6/17.
 */

public class SetInvestAmountFragment extends Fragment implements ResponseDelegate {
    Context contextd;
    TextView addToCart;
    EditText investAmmount;
    TextView minAmount;
    ImageView logo;
    boolean flag=false;

    //Non ui vars
    boolean cartAdded = false;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en")),
            timeF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", new Locale("en"));
    String minAmnt = "";
    RequestedServiceDataModel requestedServiceDataModel;
    MainActivity activity;
    String cartItemId = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_set_invest_amount, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        contextd = container.getContext();
        activity = (MainActivity) getActivity();
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.fundAmount));
        TextView button_investfunds = (TextView) rootView.findViewById(R.id.button_investfunds);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);
        final String kycstatus = prefs.getString("kycstatusselect", null);
        button_investfunds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
                    if(kycstatus.equals("COMPLETE")){
                        Log.d("kycstatussss1",kycstatus);
                        showAcceptTerms();
                    }else if(kycstatus.equals("SUBMITTED")) {
                        Log.d("kycstatussss2",kycstatus);
                        showSubmitPopup();
                    }else {
                        Log.d("kycstatussss3",kycstatus);
                        showAccountSetup();
                    }
                }

            }
        });
        addToCart = (TextView) rootView.findViewById(R.id.button_addToCart);
        investAmmount = (EditText) rootView.findViewById(R.id.investmentAmmount);
        logo = (ImageView) rootView.findViewById(R.id.logo);
        logo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        minAmount = (TextView) rootView.findViewById(R.id.minAmountTxt);
        minAmnt = (getArguments().getString("orderType").equals("ONETIME") ? (int) Float.parseFloat(getArguments().getString("minimumAmnt").equals("N.A.") ? 1000 + "" : getArguments().getString("minimumAmnt")) + "" :
                ((int) Float.parseFloat(getArguments().getString("minimumSIPAmnt").equals("N.A.") || getArguments().getString("minimumSIPAmnt").trim().equals("") ? "500.0" : getArguments().getString("minimumSIPAmnt"))) + "");
        minAmount.setText(contextd.getResources().getString(R.string.minimumInvestmentAmount) + "(" + Common.currencyString(minAmnt, false, false) + ")");
        if (!minAmnt.isEmpty())
            investAmmount.setText(Common.currencyString(minAmnt, true, false));
        addToCart.setText(cartAdded ? contextd.getResources().getString(R.string.remove) :
                contextd.getResources().getString(R.string.addToCart));
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate())
                    if (cartAdded) {
                        removeFromCart(Common.getPreferences(contextd, "userID"), cartItemId);
                    } else {
                        addToCart();
                    }
            }
        });
        investAmmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    investAmmount.setText("\u20B9");
                    investAmmount.setSelection("\u20B9".length());
                } else {
                    investAmmount.removeTextChangedListener(this);
                    if (s.toString().contains("\u20B9")) {
                        String temp = s.toString()
                                .replace("\u20B9", "")//rupees Symbol
                                .replace(",", "")//commas
                                .replace(" ", "")//normal space
                                .replace(" ", "");//special space
                        String currency = Common.currencyString(temp, true, false);
                        investAmmount.setText(currency);
                        if (currency.equals("")) {
                            investAmmount.setText("\u20B9");
                            investAmmount.setSelection("\u20B9".length());
                        } else
                            investAmmount.setSelection(currency.toString().length());
                    }
                    investAmmount.addTextChangedListener(this);
                }
            }
        });
        return rootView;
    }

    void addToCart() {
        CartModle cart;
        JSONArray arr = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("scheme_code", getArguments().getString("schemeCode").toString());
            obj.put("scheme_amount", investAmmount.getText().toString().replace("\u20B9", "")//rupees Symbol
                    .replace(",", "")//commas
                    .replace(" ", "")//normal space
                    .replace(" ", ""));
            obj.put("scheme_nav", getArguments().getString("nav"));
            arr.put(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cart = new CartModle();

        cart.setOrderType(getArguments().getString("orderType").toString());
        if (!getArguments().getString("orderType").equals("ONETIME"))
            cart.setNumberofmonths(getArguments().getString("months"));
        cart.setTarget_year("");
        cart.setAdded_date(sdf.format(Calendar.getInstance().getTime()).toString());
        cart.setAmount(investAmmount.getText().toString().replace("\u20B9", "")//rupees Symbol
                .replace(",", "")//commas
                .replace(" ", "")//normal space
                .replace(" ", ""));
        cart.setScheme_code(getArguments().getString("schemeCode").toString());
        cart.setSchem_json(arr.toString());
        addFundToCart(Common.getPreferences(contextd, "userID"), cart);
    }

    boolean validate() {
        boolean b = false;
        if (investAmmount.getText().toString().trim().equals("\u20B9")) {
            investAmmount.setError(contextd.getResources().getString(R.string.amountIsTooLow));
            b = false;
        } else if (Double.parseDouble(minAmnt) > Double.parseDouble(investAmmount.getText().toString().trim().replace("\u20B9", "")//rupees Symbol
                .replace(",", "")//commas
                .replace(" ", "")//normal space
                .replace(" ", ""))) {
            b = false;
            investAmmount.setError(contextd.getResources().getString(R.string.amountIsTooLow));
        } else if (!getArguments().getString("orderType").equals("ONETIME")) {
            if ((Double.parseDouble(investAmmount.getText().toString().trim().replace("\u20B9", "")//rupees Symbol
                    .replace(",", "")//commas
                    .replace(" ", "")//normal space
                    .replace(" ", "")) % ((int) Float.parseFloat(getArguments().getString("SIPMultiplier").toString().trim()))) != 0) {
                b = false;
                investAmmount.setError(contextd.getResources().getString(R.string.amountShouldMultiple) + " " + (int) Float.parseFloat(getArguments().getString("SIPMultiplier").toString().trim()));
            } else {
                b = true;
            }
        } else if (Double.parseDouble(investAmmount.getText().toString().trim().replace("\u20B9", "")//rupees Symbol
                .replace(",", "")//commas
                .replace(" ", "")//normal space
                .replace(" ", "")) > 200000) {
            if (getArguments().getString("l1Flag").equals("Y")) {
                b = true;
            } else {
                investAmmount.setError(contextd.getResources().getString(R.string.you_cannot_invest_more_than));
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }


    AlertDialog b;
    Bundle bun;

    public void showAcceptTerms() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dio_investmentplandetail_popup, null);
        dialogBuilder.setView(dialogView);

        String first = contextd.getResources().getString(R.string.ihaveReadAndAccept);
        String next = "<font color='#1B7D0C'> " + contextd.getResources().getString(R.string.termsAndConditions) + "</font>";
        TextView popup_text_frst = (TextView) dialogView.findViewById(R.id.popup_text_frst);
        popup_text_frst.setText(Html.fromHtml(first + next));
        TextView savingFor = (TextView) dialogView.findViewById(R.id.popup_tv2);
        savingFor.setVisibility(View.GONE);
        String first1 = contextd.getResources().getString(R.string.ihaveReadAndAccept);
        String next1 = "<font color='#1B7D0C'> " + contextd.getResources().getString(R.string.schemeOffers) + "</font>";
        TextView popup_text_second = (TextView) dialogView.findViewById(R.id.popup_text_second);
        popup_text_second.setText(Html.fromHtml(first1 + next1));
        TextView invest_cancel = (TextView) dialogView.findViewById(R.id.invest_cancel);

        invest_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        TextView button_popup = (TextView) dialogView.findViewById(R.id.button_popup);
        button_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                frag = new RedirectringFragment();
                bun = getArguments();
                bun.putString("ammount", investAmmount.getText().toString().replace("\u20B9", "")//rupees Symbol
                        .replace(",", "")//commas
                        .replace(" ", "")//normal space
                        .replace(" ", ""));
                bun.putString("quantity", "0");
                bun.putString("remark",
                        (getArguments().getString("orderType").equals("ONETIME")
                                ? "Purchase Order for "
                                : "SIP Registration for ") + getArguments().getString("schemeCode") + " of " + Common.getPreferences(getActivity(), "userID") + " at " + timeF.format(Calendar.getInstance().getTime()));
                JSONArray arr = new JSONArray();
                JSONObject obj = new JSONObject();
                try {
                    obj.put("scheme_name", getArguments().getString("schemeName").toString());
                    obj.put("scheme_code", getArguments().getString("schemeCode").toString());
                    obj.put("scheme_amount", investAmmount.getText().toString().replace("\u20B9", "")//rupees Symbol
                            .replace(",", "")//commas
                            .replace(" ", "")//normal space
                            .replace(" ", ""));
                    obj.put("scheme_qty", "0");
                    obj.put("nav", getArguments().getString("nav").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                arr.put(obj);
                bun.putString("isInCart", cartAdded ? cartItemId : "0");
                bun.putString("schemeJson", arr.toString());

                frag.setArguments(bun);
                UiUtils.hideKeyboard(contextd);
                getProfileStatus(Common.getPreferences(contextd, "userID"));
                b.dismiss();
            }
        });

        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }


    Fragment frag;

    void getProfileStatus(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void addFundToCart(String userid, CartModle cart) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.AddToCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("order_type", cart.getOrderType());
        if (!getArguments().getString("orderType").equals("ONETIME"))
            requestedServiceDataModel.putQurry("no_of_months", cart.getNumberofmonths());
        requestedServiceDataModel.putQurry("target_year", cart.getTarget_year());
        requestedServiceDataModel.putQurry("added_date", cart.getAdded_date());
        requestedServiceDataModel.putQurry("is_goal", "0");
        requestedServiceDataModel.putQurry("goal_amount", "");
        requestedServiceDataModel.putQurry("goal_name", "");
        requestedServiceDataModel.putQurry("goal_cat_id", "");
        requestedServiceDataModel.putQurry("scheme_json", cart.getSchem_json());
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("ADDCART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void removeFromCart(String userid, String cartid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cart.php");
        baseRequestData.setTag(ResponseType.RemoveFromCart);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.putQurry("cart_id", cartid);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("REMOVECART");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    void showSubmitPopup(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.kycsubmitpopup, null);
        dialogBuilder.setView(dialogView);
        TextView home = (TextView) dialogView.findViewById(R.id.invest_cancel);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }

    void showAccountSetup(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.do_it_now, null);
        dialogBuilder.setView(dialogView);
        TextView home = (TextView) dialogView.findViewById(R.id.invest_cancel);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        TextView kyc = (TextView) dialogView.findViewById(R.id.button_popup);
        kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new KYCRegistrationFragment()).addToBackStack(null).
                        commit();
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
                try {
                    JSONObject obj = new JSONObject(json);
                    if (obj.getJSONObject("data").getString("kyc_status").equals("COMPLETE")) {//||obj.getJSONObject("data").getString("kyc_status").equals("SUBMITTED"))
                        if (getArguments().getString("isCams").equals("CAMS") && (obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("PENDING") ||
                                obj.getJSONObject("data").getString("image_upload_status").toUpperCase().equalsIgnoreCase("REJECTED"))) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
                            builder.setTitle(contextd.getResources().getString(R.string.app_name));
                            builder.setMessage(contextd.getResources().getString(R.string.yourDocumentNotVerifiedAddtoCart));
                            String positiveText = contextd.getResources().getString(R.string.addToCart);
                            builder.setPositiveButton(positiveText,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            addToCart();
                                        }
                                    });
                            builder.setNegativeButton(contextd.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.setCancelable(false);
                            dialog.show();
                        } else {
                            if (obj.getJSONObject("data").getString("is_mendate").equalsIgnoreCase("VERIFIED")) {
                                bun.putBoolean("isXSIP", true);
                                bun.putString("mandate_id", obj
                                        .getJSONObject("data")
                                        .getString("mandate_id"));
                            } else {
                                bun.putBoolean("isXSIP", false);
                            }
                            frag.setArguments(bun);
                            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                    .replace(R.id.frame_container, frag).addToBackStack(null).
                                    commit();
                        }
                    } else {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
//                        builder.setTitle(contextd.getResources().getString(R.string.app_name));
//                        builder.setMessage(contextd.getResources().getString(R.string.kycPending));
//                        String positiveText = contextd.getResources().getString(R.string.doItNow);
//                        builder.setPositiveButton(positiveText,
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                        activity.getDocBackHome(contextd.getResources().getString(R.string.documentVerification), false);
//                                    }
//                                });
//                        AlertDialog dialog = builder.create();
//                        dialog.setCancelable(false);
//                        dialog.show();
                    //    showAccountSetup();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.AddToCart:
                addToCart.setText(contextd.getResources().getString(R.string.remove));
                Toast.makeText(contextd, contextd.getResources().getString(R.string.fundAddedToCart), Toast.LENGTH_SHORT).show();
                cartAdded = true;
                try {
                    JSONObject obj = new JSONObject(json);
                    cartItemId = obj.getString("cart_id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case ResponseType.RemoveFromCart:
             //   Log.e("Res", json);
                cartAdded = false;
                addToCart.setText(contextd.getResources().getString(R.string.addToCart));
                Toast.makeText(contextd, contextd.getResources().getString(R.string.fundRemovedFromCart), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
             //   Log.e("res", json);
                Common.showToast(contextd, message);
                break;
            case ResponseType.AddToCart:
                Common.showToast(contextd, message);
                break;
            case ResponseType.RemoveFromCart:
                Common.showToast(contextd, message);
                break;
        }
    }
}
