package com.niivo.com.niivo.Requests;

import org.json.JSONException;

/**
 * Created by user4 on 27/5/16.
 */
public interface ResponseDelegate {

    public void onNoNetwork(String message, BaseRequestData baseRequestData);

    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException;

    public void onFailure(String message, String json, BaseRequestData baseRequestData);
}
