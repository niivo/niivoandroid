package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 7/8/18.
 */

public class InvestedRVDetailsAdapter extends RecyclerView.Adapter {
    Context contextd;
    OrderedList list;
    String lang = "";
    GetClick click;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));

    public InvestedRVDetailsAdapter(Context contextd, OrderedList list) {
        this.contextd = contextd;
        this.list = list;
        lang = Common.getLanguage(contextd);
        this.list = removeDuplicates(list);

    }

    public InvestedRVDetailsAdapter setClick(GetClick click) {
        this.click = click;
        return this;
    }

    public static OrderedList removeDuplicates(OrderedList al) {
        for(int i = 0; i < al.getData().size(); i++) {
             for(int j = i + 1; j < al.getData().size(); j++) {
                if(al.getData().get(i).getFolio_no().equalsIgnoreCase(al.getData().get(j).getFolio_no())){
                    if(!al.getData().get(i).isGroupEntries()) {
                        al.getData().get(i).setGroupEntries(true);
                        al.getData().get(i).getOrderedItemArrayListSub().add(al.getData().get(i));
                    }
                    al.getData().get(i).getOrderedItemArrayListSub().add(al.getData().get(j));

                    al.getData().get(i).setAmount((Integer.parseInt(al.getData().get(i).getAmount())+Integer.parseInt(al.getData().get(j).getAmount())+""));
                    al.getData().get(i).setWithdrawAmt((Float.parseFloat(al.getData().get(i).getWithdrawAmt())+Float.parseFloat(al.getData().get(j).getWithdrawAmt())+""));
                    al.getData().get(i).setWithdrawQty((Float.parseFloat(al.getData().get(i).getWithdrawQty())+Float.parseFloat(al.getData().get(j).getWithdrawQty())+""));
                    al.getData().get(i).setQuantity((Float.parseFloat(al.getData().get(i).getQuantity())+Float.parseFloat(al.getData().get(j).getQuantity())+""));
                    al.getData().get(i).setCurrent_value((Float.parseFloat(al.getData().get(i).getCurrent_value())+Float.parseFloat(al.getData().get(j).getCurrent_value())+""));
                    //  al.getData().get(i).get((Integer.parseInt(al.getData().get(i).getAmount())+Integer.parseInt(al.getData().get(j).getAmount())+""));
                    al.getData().remove(j);
                    j--;
                }
            }
        }
        return al;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(contextd).inflate(R.layout.item_invested_with_status, parent, false);
        return new InvestedHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, final int position) {
        InvestedHolder holder = ((InvestedHolder) h);
        holder.title.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
                list.getData().get(position).getGoal_detail().getGoal_name() :
                (lang.equals("en")
                        ? list.getData().get(position).getFUND_NAME()
                        : lang.equals("hi")
                        ? list.getData().get(position).getFUNDS_HINDI()
                        : lang.equals("gu")
                        ? list.getData().get(position).getFUNDS_GUJARATI()
                        : lang.equals("mr")
                        ? list.getData().get(position).getFUNDS_MARATHI()
                        : list.getData().get(position).getFUND_NAME()));
        holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.stay, 0);
        try {
            Calendar ct = Calendar.getInstance();
            ct.setTime(getSDF.parse(list.getData().get(position).getDtdate().trim()));
            holder.invesdDate.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (list.getData().get(position).getWithdrawQty().equals("0")) {
            float total = Float.parseFloat(list.getTotal_amount().trim()),
                    amountInvested = Float.parseFloat(list.getData().get(position).getAmount());
            holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested) / total) * 100) + "%");
            holder.amntInvested.setText(Common.currencyString(list.getData().get(position).getAmount(), false));
        } else {
            float total = Float.parseFloat(list.getTotal_amount().trim()),
                    amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                    withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt());
            holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested - withdrawAmnt) / total) * 100) + "%");
            holder.amntInvested.setText(Common.currencyString(((int) (amountInvested - withdrawAmnt)) + "", false));
        }

        if (list.getData().get(position).getIs_goal().trim().equals("1")) {
            int cnt = 0;
            for (int i = 0; i < list.getData().get(position).getOrder_detail().size(); i++) {
                try {
                    cnt += Integer.parseInt(list.getData().get(position).getOrder_detail().get(i).getAmount().trim());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    cnt += 0;
                }
            }
            float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) / (float) cnt) * 100;
            holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret % 100)) + "%");
            holder.amntInvested.setText(Common.currencyString(cnt + "", false));
            if (list.getData().get(position).getGoal_detail().getFund_category().equals("1")) {
                holder.ivLogo.setImageResource(R.drawable.retirementfund);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("2")) {
                holder.ivLogo.setImageResource(R.drawable.childseducation);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("3")) {
                holder.ivLogo.setImageResource(R.drawable.childswedding);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("4")) {
                holder.ivLogo.setImageResource(R.drawable.vacation);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("5")) {
                holder.ivLogo.setImageResource(R.drawable.buyingcar);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("6")) {
                holder.ivLogo.setImageResource(R.drawable.buyinghouse);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("7")) {
                holder.ivLogo.setImageResource(R.drawable.startingbusiness);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("8")) {
                holder.ivLogo.setImageResource(R.drawable.startinganemergencyfund);
            } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("9")) {
                holder.ivLogo.setImageResource(R.drawable.wealthcreation);
            } else {
                holder.ivLogo.setImageResource(R.drawable.rupee);
            }
            holder.type.setText(contextd.getResources().getString(R.string.goal).toUpperCase());
        } else {
            holder.ivLogo.setImageResource(R.drawable.rupee);
            float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) - (Float.parseFloat(list.getData().get(position).getAmount().trim())));
            ret = ret / (Float.parseFloat(list.getData().get(position).getAmount().trim())) * 100;
            Log.e("return", ret + "");
            if (list.getData().get(position).getIs_allotment().equals("0"))
                holder.fundReturn.setText("0%");
            else {
                if (list.getData().get(position).getWithdrawQty().equals("0")) {
                    holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                } else {
                    float amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                            withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt()),
                            wthdrawUnit = Float.parseFloat(list.getData().get(position).getWithdrawQty()),
                            investedUnit = Float.parseFloat(list.getData().get(position).getQuantity()),
                            nav = Float.parseFloat(list.getData().get(position).getNav().trim());
                    ret = (nav * (investedUnit - wthdrawUnit)) - (amountInvested - withdrawAmnt);
                    ret = ret / (amountInvested - withdrawAmnt) * 100;
                    holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                }
            }
            holder.type.setText(contextd.getResources().getString(R.string.fund).toUpperCase());
        }
        holder.status.setVisibility(View.VISIBLE);
        if (list.getData().get(position).getIs_allotment().equals("0")) {
            holder.status.setText(contextd.getResources().getString(R.string.pending).toUpperCase());
            holder.status.setBackground(contextd.getResources().getDrawable(R.drawable.pending_status_bg));
        } else {
            holder.status.setText(contextd.getResources().getString(R.string.confirmed));
            holder.status.setBackground(contextd.getResources().getDrawable(R.drawable.confirmd_status_bg));
        }
        h.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (click != null)
                    click.onItemClick(position);
            }
        });

        if(list.getData().get(position).getOrder_type().equalsIgnoreCase("SIP") || list.getData().get(position).getOrder_type().equalsIgnoreCase("XSIP"))
        {
            holder.tv_status.setText("Registered On");
            try {
                Calendar ct = Calendar.getInstance();
                ct.setTime(getSDF.parse(list.getData().get(position).getSip_registration_date().trim()));
                holder.invesdDate.setText(Common.getDateString(ct));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            /*if(list.getData().get(position).getIs_allotment().equalsIgnoreCase("0") && list.getData().get(position).getPayment_status().equalsIgnoreCase("0"))
            {
                holder.tv_status.setText("Registered On");
            }
            else
            {
                holder.tv_status.setText("Invested on");
            }*/

        }
        else
        {
            holder.tv_status.setText("Invested on");
        }
    }

    @Override
    public int getItemCount() {
        return list.getData().size();
    }

    static class InvestedHolder extends RecyclerView.ViewHolder {
        TextView title, amntInvested, type, percentPortFolio, fundReturn, status, invesdDate ,tv_status;
        ImageView ivLogo;


        public InvestedHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            status = (TextView) itemView.findViewById(R.id.payStatus);
            type = (TextView) itemView.findViewById(R.id.type);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
            invesdDate = (TextView) itemView.findViewById(R.id.investedDateTV);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);
        }
    }

    public interface GetClick {
        void onItemClick(int pos);
    }

}
