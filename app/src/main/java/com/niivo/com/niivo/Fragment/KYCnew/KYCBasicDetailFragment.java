package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.EntityTypeAdapter;
import com.niivo.com.niivo.Model.ItemPanDetails;
import com.niivo.com.niivo.Model.ItemTaxStatusCode;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCBasicDetailFragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    TextView individualBtn, nonIndividualBtn, continueBtn , submitBtn;
    RightGravityTextInputLayout panNumberRTL, yourNameRTL,
            dobRTL, fatherNameRTL;
    TextInputEditText panNumberET, yourNameET,
            dobET, fatherNameET;
    View rootView;
    TextView entityTypeLbl;
    AppCompatSpinner entityTypeSpinner;
    LinearLayout ll_info;
    int day,mon,year;
    //Non ui vars
    boolean individual = true, isBasicDetailsSubmitted = false, kycStatus = false, preRegistered = false;
    Context contextd;
    SimpleDateFormat setSDF = new SimpleDateFormat("dd/MM/yyyy", new Locale("en")),
            getSDF = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
    Calendar current = Calendar.getInstance();
    String userId = "", dob = "", pan_image = "", individualTaxStatusCode = "";
    RequestedServiceDataModel requestedServiceDataModel;
    public String currentdate = "";
    public String ddmmyyyy = "DDMMYYYY";
    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            removeAllErrors();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    ItemTaxStatusCode entityTypeList;
    ItemPanDetails panDetails;
    ItemUserDetail userDetails;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kyc_basic_details, container, false);
            contextd = container.getContext();
            userId = Common.getPreferences(contextd, "userID");
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
            textView.setText(R.string.basic_details);
            individualBtn = (TextView) rootView.findViewById(R.id.individualBtn);
            nonIndividualBtn = (TextView) rootView.findViewById(R.id.nonIndividualBtn);
            panNumberRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.panCardRTL);
            yourNameRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.yourNameRTL);
            dobRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.dobRTL);
            fatherNameRTL = (RightGravityTextInputLayout) rootView.findViewById(R.id.fatherNameRTL);
            panNumberET = (TextInputEditText) rootView.findViewById(R.id.panCardET);
            submitBtn = (TextView)rootView.findViewById(R.id.submitBtn);
            yourNameET = (TextInputEditText) rootView.findViewById(R.id.yourNameET);
            dobET=(TextInputEditText)rootView.findViewById(R.id.dobET);
            fatherNameET = (TextInputEditText) rootView.findViewById(R.id.fatherNameET);
            continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
            entityTypeLbl = (TextView) rootView.findViewById(R.id.entityTypeLbl);
            entityTypeSpinner = (AppCompatSpinner) rootView.findViewById(R.id.entityTypeSpinner);
            ll_info  = (LinearLayout)rootView.findViewById(R.id.ll_info);
            individualBtn.setOnClickListener(this);
            nonIndividualBtn.setOnClickListener(this);
            individualBtn.setSelected(true);
            dobET.setOnClickListener(this);
            dobET.setSelection(0);
            continueBtn.setOnClickListener(this);
            submitBtn.setOnClickListener(this);
            if (individual)
                setIndividualDetails();
            else
                setNonIndividualDetails();
            panNumberET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


            panNumberET.addTextChangedListener(new TextWatcher() {
                String preText = "";

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    preText = s.toString();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    removeAllErrors();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().trim().isEmpty()) {
                        if (!Common.validatePanCard(s.toString())) {
                            panNumberET.removeTextChangedListener(this);
                            panNumberET.setText(preText.toUpperCase());
                            panNumberET.setSelection(preText.length());
                            panNumberET.addTextChangedListener(this);
                        }

                    }
                }
            });

            yourNameET.addTextChangedListener(watcher);
            yourNameET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            fatherNameET.addTextChangedListener(watcher);
            fatherNameET.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //    dobET.addTextChangedListener(watcher);
              dobET.addTextChangedListener(tw);
            if (entityTypeList == null)
                getEntityTypeList();
            if (getArguments().getBoolean("preRegistered"))
                setUpPreRegistration();
        }

        if(panNumberET.getText().toString().equalsIgnoreCase(""))
        {
                submitBtn.setVisibility(View.VISIBLE);
                ll_info.setVisibility(View.GONE);
        }
        else {
            submitBtn.setVisibility(View.GONE);
            ll_info.setVisibility(View.VISIBLE);
        }


        return rootView;
    }

    TextWatcher tw = new TextWatcher() {

        private Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(currentdate)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = currentdate.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8){
                    clean = clean + ddmmyyyy.substring(clean.length());
                }else{
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                     day  = Integer.parseInt(clean.substring(0,2));
                     mon  = Integer.parseInt(clean.substring(2,4));
                     year = Integer.parseInt(clean.substring(4,8));


//                    if(day > 31){
//                        dobET.setText("Invalid Day");
//                    }

                  //  mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
//                    if(mon > 12){
//                        dobET.setText("Invalid Month");
//                    }

                    cal.set(Calendar.MONTH, mon-1);
                    year = (year<1900)?1900:(year>2100)?2100:year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                  // day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;

               //     clean = String.format("%02d%02d%02d",day, mon, year);
                }


                clean = String.format("%s/%s/%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                currentdate = clean;
                dobET.setText(currentdate);
                Log.d("current...",currentdate);
                dobET.setSelection(sel < currentdate.length() ? sel : currentdate.length());
                Log.d("dobETin...", String.valueOf(dobET));

//                if(day >= 31){
//                    dobET.setError("invalid day");
//                }else if(mon >= 12) {
//                    dobET.setError("invalid Month");
//                }
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
        @Override
        public void afterTextChanged(Editable s) {

                if(day > 31){
                    dobET.setError("Invalid Day");
                }
                if(mon > 12) {
                    dobET.setError("Invalid Month");
                }

            }
    };

    int step = 0;

    void setUpPreRegistration() {
        preRegistered = true;
        userDetails = (ItemUserDetail) getArguments().getSerializable("userDetail");
        step = Integer.parseInt(userDetails.getData().getVerification_step().trim());
        if (step > 0)
            pan_image = userDetails.getData().getPancard_image();
        if (userDetails.getData().getPancard_type().equals("INDIVIDUAL")) {
            panNumberET.setText(userDetails.getData().getPencard());
            yourNameET.setText(userDetails.getData().getName());
            fatherNameET.setText(userDetails.getData().getFather_name());
            try {
                dobET.setText(setSDF.format(getSDF.parse(userDetails.getData().getDob())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dob = userDetails.getData().getDob();
            individualTaxStatusCode = userDetails.getData().getEntity_type();
        } else {
            if (entityTypeList == null)
                getEntityTypeList();
            else {
                panNumberET.setText(userDetails.getData().getPencard());
                yourNameET.setText(userDetails.getData().getName());
                for (int i = 0; i < entityTypeList.getData().size(); i++)
                    if (userDetails.getData().getEntity_type().equals(entityTypeList
                            .getData().get(i).getNIIVO_CODE()))
                        entityTypeSpinner.setSelection(i);
                try {
                    dobET.setText(setSDF.format(getSDF.parse(userDetails.getData().getDob())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                dob = userDetails.getData().getDob();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.individualBtn:
                individualBtn.setSelected(true);
                nonIndividualBtn.setSelected(false);
                setIndividualDetails();
                break;
            case R.id.nonIndividualBtn:
                individualBtn.setSelected(false);
                nonIndividualBtn.setSelected(true);
                setNonIndividualDetails();
                break;
          /*  case R.id.dobET:
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, -18);
                DatePickerDialog datePickerDialog = new DatePickerDialog(contextd, R.style.myDialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        current.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        current.set(Calendar.MONTH, month);
                        current.set(Calendar.YEAR, year);
                        dobET.setText(setSDF.format(current.getTime()));
                        dob = getSDF.format(current.getTime());
                        dobRTL.setError(null);
                        dobRTL.setErrorEnabled(false);
                    }
                }, current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.setTitle(contextd.getResources().getString(R.string.app_name));
                datePickerDialog.show();
                break;*/

            case R.id.submitBtn:

                if (!panNumberET.getText().toString().equalsIgnoreCase("")) {
                    removeAllErrors();
                    if (!Common.validatePanCard(panNumberET.toString())) {
                        panNumberET.setText(panNumberET.getText().toString().toUpperCase());
                        panNumberET.setSelection(panNumberET.getText().toString().length());

                    }
                    if (panNumberET.getText().toString().length() == 10) {
                        getPANDetails(panNumberET.getText().toString());
                    }
                }
                else
                {
                    Common.showToast(getActivity() ,"Enter PAN Number.");
                }
                break;

            case R.id.continueBtn:
                if (validate()) {
                    if (panDetails == null)
                        getPANDetails(panNumberET.getText().toString());
                    else if (panDetails.getData().getKyc_status().equals("0")
                            && !userDetails.getData().getPencard().equals(panNumberET.getText().toString().trim())) {
                        Bundle b = new Bundle();
                        b.putString("userId", userId);
                        b.putString("panCard", panNumberET.getText().toString().trim());
                        KYCPanImageFragment fragment = new KYCPanImageFragment();
                        fragment.setArguments(b);
                        fragment.setTargetFragment(KYCBasicDetailFragment.this, 121);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, fragment).addToBackStack(null)
                                .commit();
                    } else {

                        Intent i = new Intent();
                        i.putExtra("panRef", preRegistered
                                ? userDetails.getData().getPancard_image() + ""
                                : "");
                        onActivityResult(121, Activity.RESULT_OK, i);
                        //calling self with result as approved kyc no need to take image of PAN
                    }
                }
                break;
        }
    }

    boolean validate() {
        boolean b = false;
        if (panNumberET.getText().toString().trim().equals("")) {
            b = false;
            panNumberRTL.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (panNumberET.getText().toString().trim().length() < 10) {
            b = false;
            panNumberRTL.setError(contextd.getResources().getString(R.string.invalid_pan_card));
        } else if (yourNameET.getText().toString().trim().equals("")) {
            b = false;
            yourNameRTL.setError(contextd.getResources().getString(R.string.requiredField));
        }else if (!Character.isDigit(dobET.getText().toString().charAt(0))) {
            b = false;
            dobRTL.setError(contextd.getResources().getString(R.string.requiredField));
        }else if (day > 31) {
            b = false;
            dobRTL.setError("Invalid day");
        } else if (mon > 12) {
            b = false;
            dobRTL.setError("Invalid Month");
        }  else {
            b = true;
        }
        return b;
    }


    void setUpPanCardDetails() {
        yourNameET.setText(panDetails.getData().getPan_data().getAPP_NAME());
        yourNameET.setEnabled(false);
        fatherNameET.setText(panDetails.getData().getPan_data().getAPP_F_NAME());
        fatherNameET.setEnabled(panDetails.getData().getPan_data().getAPP_F_NAME().equals(""));
        if (panDetails.getData().getPan_data().getAPP_TYPE().equals("I")) {
            individual = true;
            individualBtn.setSelected(true);
            nonIndividualBtn.setSelected(false);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {

                current.setTime(sdf.parse(panDetails.getData().getPan_data().getAPP_DOB_DT().substring(0,10)));

            } catch (ParseException e) {

                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    current.setTime(sdf.parse(panDetails.getData().getPan_data().getAPP_DOB_DT().substring(0,10)));
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }

            dobET.setText(setSDF.format(currentdate));
            Log.d("DOBET getTime....", String.valueOf(dobET));
            dob = getSDF.format(currentdate);
            Log.d("DOB getTime....", String.valueOf(dob));
            dobET.setEnabled(false);
        } else {
            individual = false;
            individualBtn.setSelected(false);
            nonIndividualBtn.setSelected(false);
        }

        individualBtn.setEnabled(false);
        nonIndividualBtn.setEnabled(false);
    }

    void removeAllErrors() {
        panNumberRTL.setErrorEnabled(false);
        panNumberRTL.setError(null);
        yourNameRTL.setErrorEnabled(false);
        yourNameRTL.setError(null);
        dobRTL.setErrorEnabled(false);
        dobRTL.setError(null);
        fatherNameRTL.setErrorEnabled(false);
        fatherNameRTL.setError(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 121) {
            if (resultCode == Activity.RESULT_OK) {
                pan_image = data.getStringExtra("panRef");
              setBasicDetails();
            } else {
                pan_image = "";
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent();
        i.putExtra("isBasicDetailsSubmitted", isBasicDetailsSubmitted);
        i.putExtra("isIndividual", individual);
        i.putExtra("dob", dob);
        Log.d("dob,,,,,,",dob);
        i.putExtra("kycStatus", kycStatus);
        i.putExtra("panDetail", panDetails);
        this.getTargetFragment().onActivityResult(ResponseType.KYC_BasicDetails,
                isBasicDetailsSubmitted
                        ? Activity.RESULT_OK
                        : Activity.RESULT_CANCELED, i);
    }

    void setIndividualDetails() {
        individual = true;
        dobRTL.setHint(contextd.getResources().getString(R.string.dobb));
        fatherNameRTL.setHint(contextd.getResources().getString(R.string.father));
        entityTypeLbl.setVisibility(View.GONE);
        entityTypeSpinner.setVisibility(View.GONE);
        fatherNameRTL.setVisibility(View.VISIBLE);
    }

    void setNonIndividualDetails() {
        individual = false;
        dobRTL.setHint(contextd.getResources().getString(R.string.doc));
        fatherNameRTL.setHint(contextd.getResources().getString(R.string.entity_type));
        entityTypeLbl.setVisibility(View.VISIBLE);
        entityTypeSpinner.setVisibility(View.VISIBLE);
        fatherNameRTL.setVisibility(View.GONE);
    }

    void getPANDetails(String panCard) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cams-api.php");
        baseRequestData.setTag(ResponseType.KYC_Get_PAN_Details);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("pan-verify");
        requestedServiceDataModel.putQurry("pancard_no", panCard);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void setBasicDetails() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-verificaton.php");
        baseRequestData.setTag(ResponseType.KYC_BasicDetails);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", userId);
        requestedServiceDataModel.putQurry("pancard_type", individual ? "INDIVIDUAL" : "NON_INDIVIDUAL");
        requestedServiceDataModel.putQurry("pencard", panNumberET.getText().toString().trim());
        requestedServiceDataModel.putQurry("name", yourNameET.getText().toString().trim());
        requestedServiceDataModel.putQurry("dob", dobET.getText().toString().trim());
        Log.d("dobETdob...",dobET.getText().toString().trim());
        requestedServiceDataModel.putQurry("father_name", individual ? fatherNameET.getText().toString().trim() : "");
        requestedServiceDataModel.putQurry("entity_type", individual
                ? individualTaxStatusCode
                : entityTypeList.getData().get(entityTypeSpinner.getSelectedItemPosition()).getNIIVO_CODE());
        requestedServiceDataModel.putQurry("pancard_image", pan_image);
        requestedServiceDataModel.putQurry("kyc_status", kycStatus ? "1" : "");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("ADDPANCARD");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    void getEntityTypeList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetTaxStatuscode);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("ENTITYTAXCODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_BasicDetails:
                isBasicDetailsSubmitted = true;
         //       Log.e("setBasicDetails", json);
                Common.showToast(contextd, message);
                getActivity().onBackPressed();
                break;
            case ResponseType.GetTaxStatuscode:
         //       Log.e("getTaxStatusCode", json);
                entityTypeList = new Gson().fromJson(json, ItemTaxStatusCode.class);
                ArrayList<ItemTaxStatusCode.TaxStatusCode> nt = new ArrayList<>();
                for (int i = 0; i < entityTypeList.getData().size(); i++) {
                    if (entityTypeList.getData().get(i).getTYPE().equals("Indiividual"))
                        individualTaxStatusCode = entityTypeList.getData().get(i).getNIIVO_CODE();
                    if (!entityTypeList.getData().get(i).getTYPE().equals("Non-individual"))
                        nt.add(entityTypeList.getData().get(i));
                }
                Log.e("entities", "before: " + entityTypeList.getData().size());
                entityTypeList.getData().removeAll(nt);
                Log.e("entities", "after: " + entityTypeList.getData().size());
                entityTypeSpinner.setAdapter(new EntityTypeAdapter(contextd, entityTypeList));
                setUpPreRegistration();
                break;
            case ResponseType.KYC_Get_PAN_Details:
           //     Log.e("getPanDetails", json);
                panDetails = new Gson().fromJson(json, ItemPanDetails.class);
                Common.SetPreferences(getActivity() ,"kyc_status" , panDetails.getData().getKyc_status());
                if (panDetails.getData().getKyc_status().equals("1")) {
                    kycStatus = true;
                    ll_info.setVisibility(View.VISIBLE);
                    submitBtn.setVisibility(View.GONE);
                    setUpPanCardDetails();
                    //from here
                    if (panDetails == null)
                        getPANDetails(panNumberET.getText().toString());
                    else if (panDetails.getData().getKyc_status().equals("0")
                            && !userDetails.getData().getPencard().equals(panNumberET.getText().toString().trim())) {
                        Bundle b = new Bundle();
                        b.putString("userId", userId);
                        b.putString("panCard", panNumberET.getText().toString().trim());
                        KYCPanImageFragment fragment = new KYCPanImageFragment();
                        fragment.setArguments(b);
                        fragment.setTargetFragment(KYCBasicDetailFragment.this, 121);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, fragment).addToBackStack(null)
                                .commit();
                    } else {
                        Intent i = new Intent();
                        i.putExtra("panRef", preRegistered
                                ? userDetails.getData().getPancard_image() + ""
                                : "");
                        onActivityResult(121, Activity.RESULT_OK, i);
                        //calling self with result as approved kyc no need to take image of PAN
                    }
                    // finish

                } else {
                    submitBtn.setVisibility(View.GONE);
                    ll_info.setVisibility(View.VISIBLE);
                    yourNameET.setEnabled(true);
                    fatherNameET.setEnabled(true);
                    dobET.setEnabled(true);
                    individualBtn.setEnabled(true);
                    nonIndividualBtn.setEnabled(true);
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.KYC_BasicDetails:
         //       Log.e("setBasicDetails", json);
                break;
            case ResponseType.GetTaxStatuscode:
          //      Log.e("getTaxStatusCode", json);
                break;
            case ResponseType.KYC_Get_PAN_Details:
          //      Log.e("getPanDetails", json);
                try {
                    org.json.JSONObject obj = new org.json.JSONObject(json);
                    if (obj.has("retry")) {
                        getPANDetails(panNumberET.getText().toString());
                    } else {
                        yourNameET.setEnabled(true);
                        fatherNameET.setEnabled(true);
                        dobET.setEnabled(true);
                        individualBtn.setEnabled(true);
                        nonIndividualBtn.setEnabled(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
