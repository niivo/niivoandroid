package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 23/10/17.
 */

public class CartHelperModle {
    String fundName, fundType, orderType, schemeCode, amount, id, goalName, addedDate, targetYear, sipOneTime, ammountWant, percentage;

    public CartHelperModle(String fundName, String fundType, String orderType, String schemeCode, String amount, String targetYear, String addedDate, String per) {
        this.fundName = fundName;
        this.fundType = fundType;
        this.orderType = orderType;
        this.schemeCode = schemeCode;
        this.amount = amount;
        this.targetYear = targetYear;
        this.addedDate = addedDate;
        this.percentage = per;
    }


    public CartHelperModle(String fundName, String fundType, String orderType, String schemeCode, String amount, String goalName, String addedDate, String targetYear, String sipOneTime, String ammountWant, String per) {
        this.fundName = fundName;
        this.fundType = fundType;
        this.orderType = orderType;
        this.schemeCode = schemeCode;
        this.amount = amount;
        this.goalName = goalName;
        this.addedDate = addedDate;
        this.targetYear = targetYear;
        this.sipOneTime = sipOneTime;
        this.ammountWant = ammountWant;
        this.percentage = per;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getTargetYear() {
        return targetYear;
    }

    public void setTargetYear(String targetYear) {
        this.targetYear = targetYear;
    }

    public String getSipOneTime() {
        return sipOneTime;
    }

    public void setSipOneTime(String sipOneTime) {
        this.sipOneTime = sipOneTime;
    }

    public String getAmmountWant() {
        return ammountWant;
    }

    public void setAmmountWant(String ammountWant) {
        this.ammountWant = ammountWant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getFundType() {
        return fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
