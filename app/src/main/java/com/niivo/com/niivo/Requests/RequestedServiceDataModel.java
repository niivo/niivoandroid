package com.niivo.com.niivo.Requests;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.MyApplication;
import com.niivo.com.niivo.Utils.NetWorkCheck.NetworkDialogAct;


public class RequestedServiceDataModel extends DataModel {

    private ServerRequest dostoomServerRequest;


    public RequestedServiceDataModel(Context context, ResponseDelegate delegate) {
        super(context, delegate);
    }

    public void execute() {

        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.executeRequest();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, context.getResources().getString(R.string.checkNetworkConnection), Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    context.registerReceiver(netHelpReceiver, new IntentFilter("tryAgainState"));
                    responseDelegate.onNoNetwork(context.getResources().getString(R.string.checkNetworkConnection), getBaseRequestData());

                   if(!MyApplication.isNoNetworkDialogShown()) {
                       MyApplication.setNoNetworkDialogShown(true);
                       Common.showToast(context, context.getResources().getString(R.string.checkNetworkConnection));
                       context.startActivity(new Intent(context, NetworkDialogAct.class));
                   }
                }
            }

        }

    }

    public void executeRequestTransaction() {

        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.executeRequestTransaction();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, context.getResources().getString(R.string.checkNetworkConnection), Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    context.registerReceiver(netHelpReceiver, new IntentFilter("tryAgainState"));
                    responseDelegate.onNoNetwork(context.getResources().getString(R.string.checkNetworkConnection), getBaseRequestData());

                    if(!MyApplication.isNoNetworkDialogShown()) {
                        MyApplication.setNoNetworkDialogShown(true);
                        Common.showToast(context, context.getResources().getString(R.string.checkNetworkConnection));
                        context.startActivity(new Intent(context, NetworkDialogAct.class));
                    }
                }
            }

        }

    }
    public void pythonServiceExcute() {

        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.pythonServiceExcute();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, context.getResources().getString(R.string.checkNetworkConnection), Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    context.registerReceiver(netHelpReceiver, new IntentFilter("tryAgainState"));
                    responseDelegate.onNoNetwork(context.getResources().getString(R.string.checkNetworkConnection), getBaseRequestData());

                    if(!MyApplication.isNoNetworkDialogShown()) {
                        MyApplication.setNoNetworkDialogShown(true);
                        Common.showToast(context, context.getResources().getString(R.string.checkNetworkConnection));
                        context.startActivity(new Intent(context, NetworkDialogAct.class));
                    }
                }
            }

        }

    }


    public void executeWithoutProgressbar() {
        if (Utils.isOnline(context)) {
            dostoomServerRequest = new ServerRequest(context, this);
            try {
                dostoomServerRequest.executeRequestWithoutProgressbar();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            if (isShowNetworkTost()) {
                Toast.makeText(context, context.getResources().getString(R.string.checkNetworkConnection), Toast.LENGTH_LONG).show();
            } else {
                if (responseDelegate != null) {
                    context.registerReceiver(netHelpReceiver, new IntentFilter("tryAgainState"));
                    responseDelegate.onNoNetwork(context.getResources().getString(R.string.checkNetworkConnection), getBaseRequestData());
                    if(!MyApplication.isNoNetworkDialogShown()) {
                        MyApplication.setNoNetworkDialogShown(true);
                        Common.showToast(context, context.getResources().getString(R.string.checkNetworkConnection));
                        context.startActivity(new Intent(context, NetworkDialogAct.class));
                    }
                }
            }
        }
    }

    private BroadcastReceiver netHelpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            android.util.Log.e("tryAgainReq", "" + intent.getAction());
            context.unregisterReceiver(this);
            RequestedServiceDataModel.this.execute();
        }
    };
}
