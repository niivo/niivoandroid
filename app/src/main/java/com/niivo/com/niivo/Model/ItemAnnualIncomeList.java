package com.niivo.com.niivo.Model;

import java.util.ArrayList;

/**
 * Created by deepak on 25/9/18.
 */

public class ItemAnnualIncomeList {
    String status, msg;
    ArrayList<AnnualIncome> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<AnnualIncome> getData() {
        return data;
    }

    public void setData(ArrayList<AnnualIncome> data) {
        this.data = data;
    }

    public class AnnualIncome {
        String FRONTEND_OPTION_NAME, NIIVO_CODE, FATCA_CODE, CVL_CODE, PROFILE_SCORE;

        public String getFRONTEND_OPTION_NAME() {
            return FRONTEND_OPTION_NAME;
        }

        public void setFRONTEND_OPTION_NAME(String FRONTEND_OPTION_NAME) {
            this.FRONTEND_OPTION_NAME = FRONTEND_OPTION_NAME;
        }

        public String getNIIVO_CODE() {
            return NIIVO_CODE;
        }

        public void setNIIVO_CODE(String NIIVO_CODE) {
            this.NIIVO_CODE = NIIVO_CODE;
        }

        public String getFATCA_CODE() {
            return FATCA_CODE;
        }

        public void setFATCA_CODE(String FATCA_CODE) {
            this.FATCA_CODE = FATCA_CODE;
        }

        public String getCVL_CODE() {
            return CVL_CODE;
        }

        public void setCVL_CODE(String CVL_CODE) {
            this.CVL_CODE = CVL_CODE;
        }

        public String getPROFILE_SCORE() {
            return PROFILE_SCORE;
        }

        public void setPROFILE_SCORE(String PROFILE_SCORE) {
            this.PROFILE_SCORE = PROFILE_SCORE;
        }
    }
}
