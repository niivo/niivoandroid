package com.niivo.com.niivo.Fragment.InvestHelper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import java.util.Locale;


/**
 * Created by deepak on 13/6/17.
 */

public class FundDetailAltFragment extends Fragment {
    Activity _activity;
    View parentView;
    TextView next;
    TextView fundNav, fundSchemeName, fundFundClass,subcategory,maincategory,
            fundSipText, pref1M, pref3M, pref6M, pref1Y, pref2Y, pref3Y, pref5Y, pref7Y, pref10Y, fundMinimumPurchase, fundMinimumSIPAmnt;

    Context contextd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        parentView = inflater.inflate(R.layout.fragment_fund_detail, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(getArguments().getString("schemeName").trim().toUpperCase());
        fundNav = (TextView) parentView.findViewById(R.id.fundNAV);
        fundSchemeName = (TextView) parentView.findViewById(R.id.fundShcemeName);
        maincategory = (TextView) parentView.findViewById(R.id.maincategory);
        subcategory = (TextView) parentView.findViewById(R.id.subcategory);
        fundSipText = (TextView) parentView.findViewById(R.id.fundSIPTxt);
        pref1M = (TextView) parentView.findViewById(R.id.fund1MonthPerf);
        pref3M = (TextView) parentView.findViewById(R.id.fund3MonthPref);
        pref6M = (TextView) parentView.findViewById(R.id.fund6MonthPref);
        pref1Y = (TextView) parentView.findViewById(R.id.fund1YearPref);
        pref2Y = (TextView) parentView.findViewById(R.id.fund2YearPref);
        pref3Y = (TextView) parentView.findViewById(R.id.fund3YearPref);
        pref5Y = (TextView) parentView.findViewById(R.id.fund5YearPref);
        pref7Y = (TextView) parentView.findViewById(R.id.fund7YearPref);
        pref10Y = (TextView) parentView.findViewById(R.id.fund10YearPref);
        fundMinimumPurchase = (TextView) parentView.findViewById(R.id.fundMinimumPurchaseAmnt);
        fundMinimumSIPAmnt = (TextView) parentView.findViewById(R.id.fundMinimumSIPAmnt);

        String nav = getArguments().getString("nav");
        Log.d("nav====",nav);
        if(nav.equals(""))
        {
            nav ="0.00";
            Log.d("nav1====",nav);
            fundNav.setText(Common.currencyString(String.format(new Locale("en"), "%.1f", Float.parseFloat(nav)),true));

        }else {
            fundNav.setText(Common.currencyString(String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("nav"))), true));
        }
            fundSchemeName.setText(getArguments().getString("schemeName"));
        maincategory.setText(getArguments().getString("maincategory"));
        subcategory.setText(getArguments().getString("subcategory"));
        fundSipText.setText((getArguments().getString("isSIP").equals("Y") ? contextd.getResources().getString(R.string.yes) :
                contextd.getResources().getString(R.string.no)));
        fundSipText.setTextColor(Color.parseColor((getArguments().getString("isSIP").equals("Y") ? "#3fd37e" : "#d74c48")));
        pref1M.setText((getArguments().getString("1MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("1MonthPref"))) + "%"));
        pref3M.setText((getArguments().getString("3MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("3MonthPref"))) + "%"));
        pref6M.setText((getArguments().getString("6MonthPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("6MonthPref"))) + "%"));
        pref1Y.setText((getArguments().getString("1YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("1YearPref"))) + "%"));
        pref2Y.setText((getArguments().getString("2YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("2YearPref"))) + "%"));
        pref3Y.setText((getArguments().getString("3YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("3YearPref"))) + "%"));
        pref5Y.setText((getArguments().getString("5YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("5YearPref"))) + "%"));
        pref7Y.setText((getArguments().getString("7YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("7YearPref"))) + "%"));
        pref10Y.setText((getArguments().getString("10YearPref").equals("N.A.") ? contextd.getResources().getString(R.string.na) : String.format(new Locale("en"), "%.1f", Float.parseFloat(getArguments().getString("10YearPref"))) + "%"));
        fundMinimumPurchase.setText(Common.currencyString((int) (Float.parseFloat(getArguments().getString("minimumPurchase").trim())) + "",false));
        fundMinimumSIPAmnt.setText(getArguments().getString("minimumSIPAmnt").equals("")
                ? contextd.getResources().getString(R.string.na) :
                Common.currencyString((int) (Float.parseFloat(getArguments().getString("minimumSIPAmnt").trim())) + "",false));


        next = (TextView) parentView.findViewById(R.id.btn_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frag = new SelectTimeFragment();
                frag.setArguments(getArguments());
                UiUtils.hideKeyboard(contextd);
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, frag).addToBackStack(null).
                        commit();
            }
        });

        return parentView;
    }
}
