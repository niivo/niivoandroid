package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.niivo.com.niivo.Model.ItemReturns;
import com.niivo.com.niivo.R;

import java.util.ArrayList;

/**
 * Created by deepak on 7/6/17.
 */

public class ReturnsAnalyticAdapter extends ArrayAdapter<ItemReturns> {
    Context contextd;
    ArrayList<ItemReturns> list;

    public ReturnsAnalyticAdapter(@NonNull Context context, ArrayList<ItemReturns> resource) {
        super(context, 0, resource);
        this.contextd = context;
        this.list = resource;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(contextd).inflate(R.layout.item_return_analytic, parent, false);
        TextView title = (TextView) convertView.findViewById(R.id.fundTitle);
        title.setText(list.get(position).getReturnName());
        return convertView;
    }
}
