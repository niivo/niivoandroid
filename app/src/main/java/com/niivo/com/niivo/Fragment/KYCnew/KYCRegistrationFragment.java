package com.niivo.com.niivo.Fragment.KYCnew;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Model.ItemPanDetails;
import com.niivo.com.niivo.Model.ItemUserDetail;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;

/**
 * Created by deepak on 11/9/18.
 */

public class KYCRegistrationFragment extends Fragment implements View.OnClickListener, ResponseDelegate {

    TextView continuBtn;
    //Non ui vars
    Context contextd;
    ItemPanDetails panDetails;
    RequestedServiceDataModel requestedServiceDataModel;
    String userId = "";
    ItemUserDetail userDetail;
    MainActivity activity;
    String KycDirect;
    LinearLayout ragistrationlayout;
    ProgressBar progress_bar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kyc_registration, container, false);
        contextd = container.getContext();
        continuBtn = (TextView) rootView.findViewById(R.id.continu);
        continuBtn.setOnClickListener(this);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.registration);
        MainActivity.icon_navi.setVisibility(View.VISIBLE);
        userId = Common.getPreferences(contextd, "userID");
        activity = (MainActivity) getActivity();
        ragistrationlayout=(LinearLayout) rootView.findViewById(R.id.ragistrationlayout);
        progress_bar=(ProgressBar) rootView.findViewById(R.id.progress_bar);
        getProfile(userId);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continu:
                if ((userDetail.getData().getKyc_status().equals("NOT STARTED")
                        && userDetail.getData().getVerification_step().equals("0"))
                        || userDetail.getData().getKyc_status().equals("REJECTED"))
                    openFragmentWithTarget(ResponseType.KYC_BasicDetails);
                else {
                    int step = Integer.parseInt(userDetail.getData().getVerification_step().trim());
                    if (step <= 1)
                        openFragmentWithTarget(ResponseType.KYC_BasicDetails);
                    else if (step == 2)
                        openFragmentWithTarget(ResponseType.KYC_ContactDetails);
                    else if (step == 3)
                        openFragmentWithTarget(ResponseType.KYC_IdentityDetails);
                    else if (step == 4)
                        openFragmentWithTarget(ResponseType.KYC_NomineeDetails);
                    else if (step == 5)
                        openFragmentWithTarget(ResponseType.KYC_BankDetails);
                }
                break;
        }
    }

    void openFragmentWithTarget(int Target) {
        Fragment fragment = null;
        switch (Target) {
            case ResponseType.KYC_BasicDetails: {
                fragment = new KYCBasicDetailFragment();
                Bundle b = new Bundle();
                b.putBoolean("preRegistered", preRegistered);
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_BasicDetails);
            }
            break;
            case ResponseType.KYC_ContactDetails: {
                fragment = new KYCContactDetailsFragment();
                Bundle b = new Bundle();
                b.putBoolean("kycStatus", kycStatus);
                b.putSerializable("panDetail", panDetails);
                b.putBoolean("preRegistered", preRegistered);
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_ContactDetails);
            }
            break;
            case ResponseType.KYC_IdentityDetails: {
                fragment = new KYCIdentityDetails_1_Fragment();
                Bundle b = new Bundle();
                b.putBoolean("isIndian", isIndian);
                b.putString("dob", dob);
                b.putBoolean("kycStatus", kycStatus);
                b.putSerializable("panDetail", panDetails);
                b.putBoolean("preRegistered", preRegistered);
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_IdentityDetails);
            }
            break;
            case ResponseType.KYC_NomineeDetails: {
                fragment = new KYCNomineeDetailsFragment();
                Bundle b = new Bundle();
                b.putBoolean("preRegistered", preRegistered);
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_NomineeDetails);
            }
            break;
            case ResponseType.KYC_BankDetails: {
                fragment = new KYCBankDetailsFragment();
                Bundle b = new Bundle();
                b.putBoolean("preRegistered", preRegistered);
                b.putSerializable("userDetail", userDetail);
                fragment.setArguments(b);
                fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_BankDetails);
            }
            break;
            case ResponseType.KYC_IPV_Details:
                if(Common.getPreferences(getActivity(),"kyc_status").equalsIgnoreCase("1"))
                {
                        UiUtils.hideKeyboard(getActivity());
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, new KYCStatusFragment()).addToBackStack(null).
                                commit();

                }
                else
                {
                    fragment = new KYC_IPV_processFragment();
                    fragment.setTargetFragment(KYCRegistrationFragment.this, ResponseType.KYC_IPV_Details);
                }

                break;
        }
        if (fragment != null)
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, fragment).addToBackStack(null)
                    .commit();
    }

    boolean isBasicDetailsSubmitted, isIndividual, isIndian=true,
            isContactDetailsSubmitted, isIdentitySubmitted,
            isNomineeDetailsSubmitted, isBankDetailsSubmitted, preRegistered = false, kycStatus;
    String dob = "";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED)
            getProfile(userId);
        if (requestCode == ResponseType.KYC_BasicDetails) {
            if (resultCode == Activity.RESULT_OK) {
                isBasicDetailsSubmitted = data.getBooleanExtra("isBasicDetailsSubmitted", false);
                isIndividual = data.getBooleanExtra("isIndividual", false);
                dob = data.getStringExtra("dob");
                kycStatus = data.getBooleanExtra("kycStatus", false);
                if (kycStatus)
                    panDetails = (ItemPanDetails) data.getSerializableExtra("panDetail");

                openFragmentWithTarget(ResponseType.KYC_ContactDetails);
            }
        } else if (requestCode == ResponseType.KYC_ContactDetails) {
            if (resultCode == Activity.RESULT_OK) {
                isIndian = data.getBooleanExtra("isIndian", false);
                isContactDetailsSubmitted = data.getBooleanExtra("isContactDetailsSubmitted", false);
                if (isIndividual)
                    openFragmentWithTarget(ResponseType.KYC_IdentityDetails);
                else
                    showNonIndividualDio();
            }
        } else if (requestCode == ResponseType.KYC_IdentityDetails) {
            if (resultCode == Activity.RESULT_OK) {
                isIdentitySubmitted = data.getBooleanExtra("isIdentitySubmitted", false);
                openFragmentWithTarget(ResponseType.KYC_NomineeDetails);
            }
        } else if (requestCode == ResponseType.KYC_NomineeDetails) {
            if (resultCode == Activity.RESULT_OK || data.getBooleanExtra("isSkiped", false)) {
                isNomineeDetailsSubmitted = data.getBooleanExtra("isNomineeDetailsSubmitted", false);
                openFragmentWithTarget(ResponseType.KYC_BankDetails);
            }
        } else if (requestCode == ResponseType.KYC_BankDetails) {
            if (resultCode == Activity.RESULT_OK) {
                isBankDetailsSubmitted = data.getBooleanExtra("isSubmitted", false);
                if (isBankDetailsSubmitted)
                    openFragmentWithTarget(ResponseType.KYC_IPV_Details);
            }
        } else if (requestCode == ResponseType.KYC_IPV_Details) {
            if (resultCode == Activity.RESULT_OK) {
                Common.showToast(contextd, "All kyc details submitted successfully.");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().onBackPressed();
                    }
                }, 1000);
            }
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    void showNonIndividualDio() {
        final Dialog dialog = new Dialog(contextd);
        dialog.setContentView(R.layout.dio_kyc_non_individual_reg_confirmation);
        TextView submit = (TextView) dialog.findViewById(R.id.submitBtn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        dialog.setCancelable(false);
        dialog.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    void getProfile(String userid) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", userid);
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        if (userDetail == null)
            requestedServiceDataModel.execute();
        else
            requestedServiceDataModel.executeWithoutProgressbar();
    }

    void getPANDetails(String panCard) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-cams-api.php");
        baseRequestData.setTag(ResponseType.KYC_Get_PAN_Details);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.setWebServiceType("pan-verify");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.putQurry("pancard_no", panCard);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
    void notStartKyc(){
        if (!activity.userDetail.getData().getKyc_status().equals("NOT STARTED")){
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, new KYCStatusFragment()).addToBackStack(null).
                    commit();
             //  progress_bar.setVisibility(View.GONE);

        }
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:

          //      Log.e("userProfile", json);
                notStartKyc();
               // ragistrationlayout.setVisibility(View.VISIBLE);
                userDetail = new Gson().fromJson(json, ItemUserDetail.class);
                activity.userDetail = userDetail;

                if (Integer.parseInt(userDetail.getData().getVerification_step().trim()) > 1) {
                    isIndian=userDetail.getData().getResidency_type().equals("INDIAN");
                    getPANDetails(userDetail.getData().getPencard());
                }
                if (userDetail.getData().getVerification_step().equals("0"))
                    preRegistered = false;
                else {
                    preRegistered = true;
                    isIndividual = activity.userDetail.getData().getPancard_type().equalsIgnoreCase("INDIVIDUAL");
                }

                break;
            case ResponseType.KYC_Get_PAN_Details:
                panDetails = new Gson().fromJson(json, ItemPanDetails.class);
                Common.SetPreferences(getActivity() ,"kyc_status" ,panDetails.getData().getKyc_status()+"");
                if (panDetails.getData().getKyc_status().equals("1")) {
                    kycStatus = true;
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.UserProfile:
          //      Log.e("userProfile", json);
                break;
            case ResponseType.KYC_Get_PAN_Details:
                kycStatus = false;
                try {
                    org.json.JSONObject obj = new org.json.JSONObject(json);
                    if (obj.has("retry")) {
                        getPANDetails(userDetail.getData().getPencard());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
        }
    }
}
