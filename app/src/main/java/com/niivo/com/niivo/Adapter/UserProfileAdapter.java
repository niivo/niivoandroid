package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.niivo.com.niivo.Model.ProfileUser;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UserProfileAdapter extends BaseAdapter {
    Context contextd;
    ProfileUser list;
    String lang;
    ProfileHolder holder;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));

    public UserProfileAdapter(Context contextd, ProfileUser list) {
        this.contextd = contextd;
        this.list = list;
    }

    @Override
    public int getCount() {

        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.user_profile, parent, false);
            holder = new ProfileHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ProfileHolder) convertView.getTag();
        }

        holder.name.setText(list.getData().get(position).getName().trim());

        try {
            Calendar ct = Calendar.getInstance();
            ct.setTime(getSDF.parse(list.getData().get(position).getDob().trim()));
            holder.dob.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.city.setText(list.getData().get(position).getCa_city().trim());
        holder.state.setText(list.getData().get(position).getCa_state().trim());
        holder.country.setText(list.getData().get(position).getCa_country().trim());
        holder.email.setText(list.getData().get(position).getEmail().trim());
        holder.phone.setText(list.getData().get(position).getMobile().trim());
        holder.occupation.setText(list.getData().get(position).getOccupation_string().trim());
        holder.accountHolderNameET.setText(list.getData().get(position).getAccount_holder_name().trim());
        holder.accountNumberET.setText(list.getData().get(position).getAccount_number().trim());
        holder.ifscET.setText(list.getData().get(position).getIfsc_code().trim());
        holder.acctype.setText(list.getData().get(position).getAccount_type_string().trim());

        return convertView;
    }

    class ProfileHolder  {
        TextView name,city,state,country,email,phone,occupation,dob,accountHolderNameET,accountNumberET,ifscET,acctype,update;

        public ProfileHolder(View v) {
            name=(TextView)v.findViewById(R.id.tv_name);
            city=(TextView)v.findViewById(R.id.tv_city);
            state=(TextView)v.findViewById(R.id.tv_state);
            country=(TextView)v.findViewById(R.id.tv_country);
            email=(TextView)v.findViewById(R.id.tv_email);
            phone=(TextView)v.findViewById(R.id.tv_phone);
            occupation=(TextView)v.findViewById(R.id.tv_occupation);
            dob=(TextView)v.findViewById(R.id.tv_dob);
            accountHolderNameET =(TextView)v.findViewById(R.id.accountHolderNameET);
            accountNumberET=(TextView)v.findViewById(R.id.accountNumberET);
            ifscET=(TextView)v.findViewById(R.id.ifscET);
            acctype=(TextView)v.findViewById(R.id.acctype);
            update=(TextView)v.findViewById(R.id.update);
        }
    }
}
