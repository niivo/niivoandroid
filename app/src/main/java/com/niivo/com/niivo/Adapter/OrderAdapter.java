package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.niivo.com.niivo.Model.RecentOrder;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OrderAdapter extends BaseAdapter{

    Context contextd;
    RecentOrder list;
    OrderHolder holder;
    GetClick click;
    String lang;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));


    public OrderAdapter(Context contextd, RecentOrder list) {
        this.contextd = contextd;
        this.list = list;
    }

    public OrderAdapter setClick(GetClick click) {
        this.click = click;
        return this;
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.recent_oreders, parent, false);
            holder = new OrderHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OrderHolder) convertView.getTag();
        }

        holder.detailSchemeName.setText(list.getData().get(position).getFundname().trim());
        holder.investedDateTV.setText(list.getData().get(position).getInvestdate().trim());
        holder.detailAmountInvested.setText(Common.currencyString(list.getData().get(position).getInvestamount().trim(),true));
        holder.tv_ordertype.setText(list.getData().get(position).getTag().trim());
        holder.unit.setText(list.getData().get(position).getQty().trim());
        return convertView;
    }

    public interface GetClick {
        void onItemClick(int pos);
    }

    class OrderHolder  {
        TextView detailSchemeName, investedDateTV, detailAmountInvested,tv_orderid,tv_ordertype,unit;

        public OrderHolder(View v) {
            detailSchemeName = (TextView)v.findViewById(R.id.detailSchemeName);
            investedDateTV = (TextView)v.findViewById(R.id.investedDateTV);
            detailAmountInvested = (TextView)v.findViewById(R.id.detailAmountInvested);
            tv_orderid=(TextView)v.findViewById(R.id.tv_orderid);
            tv_ordertype=(TextView)v.findViewById(R.id.tv_ordertype);
            unit=(TextView)v.findViewById(R.id.unit);
        }
   }
}







