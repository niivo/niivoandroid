package com.niivo.com.niivo.Fragment.ExpertFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.IntermidiateAdapter;
import com.niivo.com.niivo.Adapter.RecyclerItemClickListener;
import com.niivo.com.niivo.Fragment.InvestHelper.FundDetailAltFragment;
import com.niivo.com.niivo.Model.InvestmentFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;


/**
 * Created by andro on 22/5/17.
 */

public class ExpertDetailFragment extends Fragment implements ResponseDelegate {
    Activity _activity;
    View parentView;
    RecyclerView recyclerView;
    ImageView choice;
    TextView imgDesc;
    //Non ui Vars
    Context contextd;
    private RequestedServiceDataModel requestedServiceDataModel;
    String type = "";
    InvestmentFunds fundList;
    IntermidiateAdapter adapter;
    String lang = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        parentView = inflater.inflate(R.layout.fragment_expert_detail_alt, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(getArguments().getString("title").toString().toUpperCase());
        recyclerView = (RecyclerView) parentView.findViewById(R.id.recfycle_inter);
        imgDesc = (TextView) parentView.findViewById(R.id.imgDesc);
        lang = Common.getLanguage(contextd);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Fragment frag = new FundDetailAltFragment();
                        Bundle b = new Bundle();
                        b.putString("schemeCode", fundList.getData().get(position).getSTARMF_SCHEME_CODE());
                        b.putString("schemeName", lang.equals("en")
                                ? fundList.getData().get(position).getSCHEME_NAME()
                                : lang.equals("hi")
                                ? fundList.getData().get(position).getFUNDS_HINDI()
                                : lang.equals("gu")
                                ? fundList.getData().get(position).getFUNDS_GUJARATI()
                                : lang.equals("mr")
                                ? fundList.getData().get(position).getFUNDS_MARATHI()
                                : fundList.getData().get(position).getSCHEME_NAME());
                        b.putString("fundName", fundList.getData().get(position).getFUND_NAME());
                        b.putString("minimumAmnt", fundList.getData().get(position).getSTARMF_MIN_PUR_AMT());
                 //       b.putString("minimumAmnt", fundList.getData().get(position).getMIN_INIT());
                        b.putString("nav", fundList.getData().get(position).getNAV());
                        b.putString("isin", fundList.getData().get(position).getISIN());
                        b.putString("isSIP", fundList.getData().get(position).getSTARMF_SIP_FLAG().trim());
                        b.putString("from", getArguments().getString("from"));
                        b.putString("minimumPurchase", fundList.getData().get(position).getSTARMF_MIN_PUR_AMT());
                        b.putString("1MonthPref", fundList.getData().get(position).getONE_M_PERF());
                        b.putString("3MonthPref", fundList.getData().get(position).getTHREE_M_PERF());
                        b.putString("6MonthPref", fundList.getData().get(position).getSIX_M_PERF());
                        b.putString("1YearPref", fundList.getData().get(position).getONE_Y_PERF());
                        b.putString("2YearPref", fundList.getData().get(position).getTWO_Y_PERF());
                        b.putString("3YearPref", fundList.getData().get(position).getTHREE_Y_PERF());
                        b.putString("5YearPref", fundList.getData().get(position).getFIVE_Y_PERF());
                        b.putString("7YearPref", fundList.getData().get(position).getSEVEN_Y_PERF());
                        b.putString("10YearPref", fundList.getData().get(position).getTEN_Y_PERF());
                        b.putString("fundClass", fundList.getData().get(position).getFUND_CLASS());
                        b.putString("l1Flag", fundList.getData().get(position).getL1_FLAG());
                        b.putString("fundCategory", fundList.getData().get(position).getFUND_CATEGORY());
                        b.putString("fundCategorization", fundList.getData().get(position).getFUND_CATEGORIZATION());
                        b.putString("fundExpense", fundList.getData().get(position).getEXPENSE_RATIO());
                        b.putString("fundDesciption", fundList.getData().get(position).getDESCRIPTION());
                        b.putString("isCams", fundList.getData().get(position).getSTARMF_RTA_CODE());
                        b.putString("minimumSIPAmnt", fundList.getData().get(position).getSIP_MIN_INSTALLMENT_AMT() == null ? "" : fundList.getData().get(position).getSIP_MIN_INSTALLMENT_AMT());
                        b.putString("SIPMultiplier", (fundList.getData().get(position).getSIP_MULTPL_AMT() == null
                                || fundList.getData().get(position).getSIP_MULTPL_AMT().equals("null")) ? "1" : fundList.getData().get(position).getSIP_MULTPL_AMT());
                        b.putString("SIPDates", fundList.getData().get(position).getSIP_DATES() == null ? "" : fundList.getData().get(position).getSIP_DATES());
                        frag.setArguments(b);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    }
                })
        );
        choice = (ImageView) parentView.findViewById(R.id.choice);
        choice.setImageResource(getArguments().getInt("imgRes"));
        imgDesc.setText(getArguments().getString("imgDesc"));
        type = getArguments().getString("type");
        if (fundList == null)
            getInvestmentFundsList(type);
        else {
            if (adapter == null)
                if (getArguments().getString("type").equals("LONGTERM")) {
                    adapter = new IntermidiateAdapter(fundList, true, contextd);
                } else {
                    adapter = new IntermidiateAdapter(fundList, contextd);
                }
            recyclerView.setAdapter(adapter);
        }
        return parentView;
    }


    void getInvestmentFundsList(String type) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-list.php");
        baseRequestData.setTag(ResponseType.GetFundList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET_woData);
        requestedServiceDataModel.setWebServiceType(type);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        Gson gson = new Gson();
        switch (baseRequestData.getTag()) {
            case ResponseType.GetFundList:
                fundList = gson.fromJson(json, InvestmentFunds.class);
                if (fundList.getData().size() > 0) {
                    if (getArguments().getString("type").equals("LONGTERM")) {
                        adapter = new IntermidiateAdapter(fundList, true, contextd);
                    } else {
                        adapter = new IntermidiateAdapter(fundList, contextd);
                    }
                    recyclerView.setAdapter(adapter);
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetFundList:
                Common.showToast(contextd, message);
                break;
        }
    }
}