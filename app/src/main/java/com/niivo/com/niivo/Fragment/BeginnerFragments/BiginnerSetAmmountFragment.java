package com.niivo.com.niivo.Fragment.BeginnerFragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Model.GoalFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.BeginnerHelper.InputFund;
import com.niivo.com.niivo.Utilities.BeginnerHelper.Investment;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Deepak.
 * set Amount for Beginner
 */

public class BiginnerSetAmmountFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    Activity _activity;
    TextView oneTimeTxt, lumpSumAmnt, goalExampleTxt;
    EditText editYear, editAmnt, editGoalName;
    ImageView goalImg;
    double noYear;
    String json;

    RequestedServiceDataModel requestedServiceDataModel;
    //Non ui Vars
    com.niivo.com.niivo.Utilities.BeginnerHelper.Investment investHelper;
    String type = "", percentage = "", lumpSumArr = "", sipArr = "", lumpsumTxt = "",cateId;
    Context contextd;
    int cnt = 0;
    MainActivity activity;
    List<InputFund> listOfInputFunds;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        View parentView = inflater.inflate(R.layout.fragment_set_amount_biginner, null, false);
        contextd = container.getContext();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        oneTimeTxt = (TextView) parentView.findViewById(R.id.oneTimeTxt2);
        lumpSumAmnt = (TextView) parentView.findViewById(R.id.lumpSumAmnt);
        textView.setText(getArguments().getString("goalNameAlt"));
        Log.d("goalNameAlt..",getArguments().getString("goalNameAlt"));
        oneTimeTxt.setText(getArguments().getString("orderType").equals("ONETIME") ?
                (contextd.getResources().getString(R.string.requiredOneTime)) :
                (contextd.getResources().getString(R.string.requiredSip)));
        type = getArguments().getString("orderType");
        cateId = getArguments().getString("cateId");
        activity = (MainActivity) getActivity();
        TextView button_invest_2 = (TextView) parentView.findViewById(R.id.button_invest_2);
        button_invest_2.setOnClickListener(this);
        //getPercentage();
        editYear = (EditText) parentView.findViewById(R.id.editYear);
        editAmnt = (EditText) parentView.findViewById(R.id.editAmnt);
        goalExampleTxt = (TextView) parentView.findViewById(R.id.exampleTxt);
        editGoalName = (EditText) parentView.findViewById(R.id.editGoalName);
        goalImg = (ImageView) parentView.findViewById(R.id.goalImg);
        goalExampleTxt.setText(contextd.getResources().getString(R.string.forExample) + " '" + getArguments().getString("goalNameExample") + "'");
        goalImg.setImageResource(getArguments().getInt("goalImgRes"));
        editAmnt.setEnabled(false);
        getProfileStatus();
        editYear.addTextChangedListener(new TextWatcher() {
            String temp = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    if (Double.parseDouble(s.toString().trim()) == 0) {
                        editYear.setText("");
                    }
                }
                temp = editAmnt.getText().toString()
                        .replace("\u20B9", "")//rupees Symbol
                        .replace(",", "")//commas
                        .replace(" ", "")//normal space
                        .replace(" ", "");//special space
                if (editYear.getText().toString().trim().length() > 1
                        && editYear.getText().toString().trim().length() == 4
                        && !(Integer.parseInt(editYear.getText().toString().trim()) <= Calendar.getInstance().get(Calendar.YEAR))
                        && !temp.toString().trim().equals("")) {

                        noYear = (Double.parseDouble(editYear.getText().toString().trim()) - (Calendar.getInstance().get(Calendar.YEAR)));
                        getBigineerFunds();
                        investHelper = new Investment(Double.parseDouble(temp.toString().trim()), noYear, Double.parseDouble(percentage), listOfInputFunds, type.equals("ONETIME") ? false : true);
                        if (type.equals("ONETIME")) {
                            lumpSumAmnt.setText(Common.currencyString(investHelper.getLumpSumAmt() + "", false));
                            lumpsumTxt = "\u20B9 " + investHelper.getLumpSumAmt() + "";
                        } else {
                            lumpSumAmnt.setText(Common.currencyString(investHelper.getSipAmt() + "", false));
                            lumpsumTxt = "\u20B9 " + investHelper.getSipAmt() + "";
                        }
                    }else {
                    if (editYear.getText().toString().trim().length() == 4
                            && !(Integer.parseInt(editYear.getText().toString().trim()) <= Calendar.getInstance().get(Calendar.YEAR))){
                        getBigineerFunds();
                     //   editAmnt.setEnabled(true);
                    }
                }
            }
        });

            editAmnt.setText("\u20B9");
            editAmnt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
                @Override
                public void afterTextChanged(Editable s) {

                        if (s.toString().equals("")) {
                            editAmnt.setText("\u20B9");
                            editAmnt.setSelection("\u20B9".length());
                        } else {
                            editAmnt.removeTextChangedListener(this);
                            if (s.toString().contains("\u20B9")) {
                                String temp = s.toString()
                                        .replace("\u20B9", "")//rupees Symbol
                                        .replace(",", "")//commas
                                        .replace(" ", "")//normal space
                                        .replace(" ", "");//special space
                                if (editYear.getText().toString().trim().length() == 4
                                        && editYear.getText().toString().trim().length() > 1
                                        && !(Double.parseDouble(editYear.getText().toString().trim()) <= Calendar.getInstance().get(Calendar.YEAR))
                                        && !temp.isEmpty()) {

                                    double noYear = (Double.parseDouble(editYear.getText().toString().trim()) - (Calendar.getInstance().get(Calendar.YEAR)));

                                    investHelper = new Investment(Double.parseDouble(temp), noYear, Double.parseDouble(percentage), listOfInputFunds, type.equals("ONETIME") ? false : true);
                                    if (type.equals("ONETIME")) {
                                        lumpSumAmnt.setText(Common.currencyString(investHelper.getLumpSumAmt() + "", false));
                                        lumpsumTxt = "\u20B9 " + investHelper.getLumpSumAmt() + "";
                                    } else {
                                        lumpSumAmnt.setText(Common.currencyString(investHelper.getSipAmt() + "", false));
                                        lumpsumTxt = "\u20B9 " + investHelper.getSipAmt() + "";
                                    }
                                }
                                String currency = Common.currencyString(temp, true, false);
                                editAmnt.setText(currency);
                                if (currency.equals("")) {
                                    editAmnt.setText("\u20B9");
                                    editAmnt.setSelection("\u20B9".length());
                                } else
                                    editAmnt.setSelection(currency.toString().length());
                            }
                            editAmnt.addTextChangedListener(this);
                        }
                    }
            });

        return parentView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_invest_2:
                if (validate()) {
                    Fragment frag = null;
                    int[] arr = null;
                    lumpSumArr = Arrays.toString(investHelper.getLumpSumAllocations()).toString();
                    Log.d("lumpSumArr,,,,,",lumpSumArr);
                    sipArr = Arrays.toString(investHelper.getSipAllocations()).toString();
                    if (type.equals("ONETIME")) {
                        if (!lumpSumArr.toString().trim().equals("null")) {
                            arr = investHelper.getLumpSumAllocations();
                            //frag = new InvestmentFundsFragment();
                            Bundle b = new Bundle();
                            b.putString("response", json);
                            b.putString("goalNameAlt",getArguments().getString("goalNameAlt"));
                            frag = new InvestmentFundsFragment();
                        } else
                            frag = null;
                    } else {
                        if (!sipArr.toString().trim().equals("null")) {
                            arr = investHelper.getSipAllocations();
                            frag = new InvestmentFundsFragment();
                        } else
                            frag = null;
                    }
                    if (frag != null) {
                        Bundle b = getArguments();
                        b.putIntArray("fundArr", arr);
                        Log.d("",arr.toString());
                        b.putString("cateId",cateId);
                        b.putString("targetYear", editYear.getText().toString().trim());
                        b.putString("amountAmount", editAmnt.getText().toString().replace("\u20B9", "")//rupees Symbol
                                .replace(",", "")//commas
                                .replace(" ", "")//normal space
                                .replace(" ", ""));
                        b.putString("sipOneTime", lumpsumTxt.toString().trim().split("\u20B9")[1]);
                        b.putString("goalName", editGoalName.getText().toString().trim());
                        b.putString("percentage", percentage);
                        frag.setArguments(b);
                        UiUtils.hideKeyboard(contextd);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    } else {
                        Common.showToast(contextd, contextd.getResources().getString(R.string.enterLittleMoreHeigher));
                    }
                }
                break;
        }
    }

    void getPercentage(String json) {//
        Gson gson = new Gson();
        GoalFunds funds = gson.fromJson(json, GoalFunds.class);
      //  GoalFunds funds = gson.fromJson(getArguments().getString("response"), GoalFunds.class);
        Log.d("GoalFunds funds...", String.valueOf(funds));
        listOfInputFunds = new ArrayList<>();
        int temp = 0;
        for (int i = 0; i < funds.getData().size(); i++) {
            listOfInputFunds.add(new InputFund(funds.getData().get(i).getISIN(),
                 //   funds.getData().get(i).getMIN_INIT() == null ? 0 : (int) Double.parseDouble(funds.getData().get(i).getMIN_INIT().trim()),
                    funds.getData().get(i).getSTARMF_MIN_PUR_AMT() == null ? 0 : (int) Double.parseDouble(funds.getData().get(i).getSTARMF_MIN_PUR_AMT().trim()),
                    funds.getData().get(i).getSIP_MIN_INSTALLMENT_AMT() == null ? 0 : (int) Double.parseDouble(funds.getData().get(i).getSIP_MIN_INSTALLMENT_AMT().trim()), 0));
               if (type.equals("ONETIME")) {
                if (funds.getData().get(i).getSTARMF_MIN_PUR_AMT() == null || (funds.getData().get(i).getSTARMF_MIN_PUR_AMT() + "").equals("null"))
             //       if (funds.getData().get(i).getMIN_INIT() == null || (funds.getData().get(i).getMIN_INIT() + "").equals("null"))
                    temp++;
            } else {
                if (funds.getData().get(i).getSIP_MIN_INSTALLMENT_AMT() == null ||
                        (funds.getData().get(i).getSIP_MIN_INSTALLMENT_AMT() + "").equals("null"))
                    temp++;
            }
        }
        if (temp != 0) {
            fundDataError();
        }
        percentage = funds.getGrowth().trim();
        Log.d("percentage...",percentage);
    }

    boolean validate() {
        boolean b = false;
        if (editGoalName.getText().toString().trim().equals("")) {
            editGoalName.setError(contextd.getResources().getString(R.string.requiredField));
            b = false;
        } else if (editGoalName.getText().toString().trim().equals(getArguments().getString("goalNameAlt"))) {
            editGoalName.setError(contextd.getResources().getString(R.string.invalidGoalName));
            b = false;
        } else if (editYear.getText().toString().trim().equals("")) {
            b = false;
            editYear.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (editYear.getText().toString().trim().length() != 4) {
            b = false;
            editYear.setError(contextd.getResources().getString(R.string.invalidYear));
        } else if (editAmnt.getText().toString().trim().equals("")) {
            b = false;
            editAmnt.setError(contextd.getResources().getString(R.string.requiredField));
        } else if (editAmnt.getText().toString().trim().length() < 2) {
            b = false;
            editAmnt.setError(contextd.getResources().getString(R.string.invalidAmount));
        } else if (Double.parseDouble(editAmnt.getText().toString().replace("\u20B9", "")//rupees Symbol
                .replace(",", "")//commas
                .replace(" ", "")//normal space
                .replace(" ", "").trim()) <= 0) {
            b = false;
            editAmnt.setError(contextd.getResources().getString(R.string.invalidAmount));
        } else if (!editAmnt.getText().toString().trim().equals("")) {
            double noYear = (Double.parseDouble(editYear.getText().toString().trim()) - (Calendar.getInstance().get(Calendar.YEAR)));
            Log.e("No of Year", noYear + "");
            if (noYear <= 0) {
                editYear.setError(contextd.getResources().getString(R.string.invalidYear));
                b = false;
            } else {
                investHelper = new Investment(Double.parseDouble(editAmnt.getText().toString().replace("\u20B9", "")//rupees Symbol
                        .replace(",", "")//commas
                        .replace(" ", "")//normal space
                        .replace(" ", "").trim()), noYear, Double.parseDouble(percentage), listOfInputFunds, type.equals("ONETIME") ? false : true);
                if (type.equals("ONETIME")) {
                    if (investHelper.getLumpSumAmt() == 0) {
                        b = false;
                        Common.showToast(contextd, contextd.getResources().getString(R.string.invalidYearOrAmount));
                    } else if (Double.parseDouble(editAmnt.getText().toString().replace("\u20B9", "")//rupees Symbol
                            .replace(",", "")//commas
                            .replace(" ", "")//normal space
                            .replace(" ", "").trim()) >= investHelper.getLumpSumAmt())
                        b = true;
                    else {
                        editAmnt.setError(contextd.getResources().getString(R.string.amountIsTooLow));
                        b = false;
                    }
                } else {
                    if (investHelper.getSipAmt() == 0) {
                        b = false;
                        Common.showToast(contextd, contextd.getResources().getString(R.string.invalidYearOrAmount));
                    } else if (Double.parseDouble(editAmnt.getText().toString().replace("\u20B9", "")//rupees Symbol
                            .replace(",", "")//commas
                            .replace(" ", "")//normal space
                            .replace(" ", "").trim()) >= investHelper.getSipAmt())
                        b = true;
                    else {
                        editAmnt.setError(contextd.getResources().getString(R.string.amountIsTooLow));
                        b = false;
                    }
                }
            }
        } else
            b = true;
        return b;
    }


    private void fundDataError() {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(contextd, R.style.myDialogTheme);
        builder.setTitle(contextd.getResources().getString(R.string.app_name));
        builder.setMessage(contextd.getResources().getString(R.string.invalidSipDetails));
        builder.setNegativeButton(contextd.getResources().getString(R.string.goToHome), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.getBackToHome();
            }
        });
        android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
//    if(type=="ONETIME"){
//        type = "N";
//    }else{
//        type="Y";
//    }
    void getBigineerFunds() {
    requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
    BaseRequestData baseRequestData = new BaseRequestData();
    baseRequestData.setWebservice("ws-product-intermediate.php");
    baseRequestData.setTag(ResponseType.GoalFundsList);
    baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
    requestedServiceDataModel.putQurry("fund_category", cateId);
    requestedServiceDataModel.putQurry("sip_flag", type) ;
    requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
    requestedServiceDataModel.putQurry("duration", String.valueOf(Double.parseDouble(editYear.getText().toString()) - (Calendar.getInstance().get(Calendar.YEAR))));// String.valueOf(noYear));
    requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
    requestedServiceDataModel.setWebServiceType("GOALFUND");
    requestedServiceDataModel.setLocal(true);
    requestedServiceDataModel.setBaseRequestData(baseRequestData);
    requestedServiceDataModel.execute();
}

    void getProfileStatus() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.UserProfile);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.setWebServiceType("GETPROFILE");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(contextd);

        switch (baseRequestData.getTag()) {
            case ResponseType.GoalFundsList:
                editAmnt.setEnabled(true);
                Log.d("json...",json);
                prefs.edit().putString("response", json).commit();
                getPercentage(json);
                break;
            case ResponseType.UserProfile:
                Log.d("json...1",json);
                JSONObject obj = new JSONObject(json);
                String kycstatus =obj.getJSONObject("data").getString("kyc_status");
                Log.d("kycstatus=========1",kycstatus);
                prefs.edit().putString("kycstatus", kycstatus).commit();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GoalFundsList:
                Common.showToast(contextd, message);
                break;
            case ResponseType.UserProfile:
                editAmnt.setEnabled(true);
                Log.d("json...",json);
                break;
        }
    }
}
