package com.niivo.com.niivo.Fragment.InterMediateFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.IntermidiateAdapter;
import com.niivo.com.niivo.Adapter.RecyclerItemClickListener;
import com.niivo.com.niivo.Fragment.InvestHelper.FundDetailAltFragment;
import com.niivo.com.niivo.Model.InvestmentFunds;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;


/**
 * Created by andro on 22/5/17.
 */

public class IntermidiateFragment extends Fragment implements ResponseDelegate {
    Activity _activity;
    View parentView;
    RecyclerView recyclerView;
    TextView text_top;

    //Non ui Vars
    private RequestedServiceDataModel requestedServiceDataModel;
    IntermidiateAdapter adapter;
    Context contextd;
    InvestmentFunds fundList;
    String lang = "";
    String type = "", tabFrom = "";

    public IntermidiateFragment() {
    }

    public static IntermidiateFragment getInstance(Bundle bD) {
        IntermidiateFragment mfragment = new IntermidiateFragment();
        Bundle b = bD;
        mfragment.setArguments(b);
        return mfragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        contextd = container.getContext();
        parentView = inflater.inflate(R.layout.activity_expert_detail, null, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        text_top = (TextView) parentView.findViewById(R.id.text_top);
        tabFrom = getArguments().getString("tabType").equals("growth") ? "RG" : "RD";
        text_top.setText(tabFrom.equals("RG")
                ? contextd.getResources().getString(R.string.choose_from_the_following_growth)
                : contextd.getResources().getString(R.string.choose_from_following_dividend));
        type = getArguments().getString("type");
        textView.setText(type.equals("EQUITYFUND")
                ? contextd.getResources().getString(R.string.equityFundsAlt)
                : (type.equals("OTHERFUND")
                ? contextd.getResources().getString(R.string.otherfund)
                : (type.equals("DEBTFUNDS")
                ? contextd.getResources().getString(R.string.debtFundsAlt)
                : (type.equals("HYBRIDFUND")
                ? contextd.getResources().getString(R.string.hybridfund)
                : (type.equals("TAXSAVINGFUND")
                ? contextd.getResources().getString(R.string.saveTaxt)
                : (type.equals("NEWFUND")
                ? contextd.getResources().getString(R.string.newfund)
                : contextd.getResources().getString(R.string.newfund)))))));
        lang = Common.getLanguage(contextd);
        recyclerView = (RecyclerView) parentView.findViewById(R.id.recfycle_inter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Fragment frag = new FundDetailAltFragment();
                        Bundle b = new Bundle();
                        b.putString("schemeCode", fundList.getData().get(position).getSTARMF_SCHEME_CODE());
                        b.putString("schemeName", lang.equals("en")
                                ? fundList.getData().get(position).getSCHEME_NAME()
                                : lang.equals("hi")
                                ? fundList.getData().get(position).getFUNDS_HINDI()
                                : lang.equals("gu")
                                ? fundList.getData().get(position).getFUNDS_GUJARATI()
                                : lang.equals("mr")
                                ? fundList.getData().get(position).getFUNDS_MARATHI()
                                : fundList.getData().get(position).getSCHEME_NAME());
                        b.putString("fundName", fundList.getData().get(position).getFUND_NAME());
                        b.putString("minimumAmnt", fundList.getData().get(position).getSTARMF_MIN_PUR_AMT());
                        //     b.putString("minimumAmnt", fundList.getData().get(position).getMIN_INIT());
                        b.putString("nav", fundList.getData().get(position).getNAV());
                        b.putString("isin", fundList.getData().get(position).getISIN());
                        b.putString("isSIP", fundList.getData().get(position).getSTARMF_SIP_FLAG().trim());
                        b.putString("from", getArguments().getString("from"));
                        b.putString("minimumPurchase", fundList.getData().get(position).getSTARMF_MIN_PUR_AMT());
                        b.putString("1MonthPref", fundList.getData().get(position).getONE_M_PERF());
                        b.putString("3MonthPref", fundList.getData().get(position).getTHREE_M_PERF());
                        b.putString("6MonthPref", fundList.getData().get(position).getSIX_M_PERF());
                        b.putString("1YearPref", fundList.getData().get(position).getONE_Y_PERF());
                        b.putString("2YearPref", fundList.getData().get(position).getTWO_Y_PERF());
                        b.putString("3YearPref", fundList.getData().get(position).getTHREE_Y_PERF());
                        b.putString("5YearPref", fundList.getData().get(position).getFIVE_Y_PERF());
                        b.putString("7YearPref", fundList.getData().get(position).getSEVEN_Y_PERF());
                        b.putString("10YearPref", fundList.getData().get(position).getTEN_Y_PERF());
              //          b.putString("fundClass", fundList.getData().get(position).getFUND_CLASS());
                        b.putString("maincategory", fundList.getData().get(position).getMAIN_CATEGORY());
                        b.putString("subcategory", fundList.getData().get(position).getSUB_CATEGORY());

                        b.putString("l1Flag", fundList.getData().get(position).getL1_FLAG());
                        b.putString("fundCategory", fundList.getData().get(position).getFUND_CATEGORY());
                        b.putString("fundCategorization", fundList.getData().get(position).getFUND_CATEGORIZATION());
                        b.putString("fundExpense", fundList.getData().get(position).getEXPENSE_RATIO());
                        b.putString("fundDesciption", fundList.getData().get(position).getDESCRIPTION());
                        b.putString("isCams", fundList.getData().get(position).getSTARMF_RTA_CODE());
                        b.putString("minimumSIPAmnt", fundList.getData().get(position).getSIP_MIN_INSTALLMENT_AMT() == null ? "" : fundList.getData().get(position).getSIP_MIN_INSTALLMENT_AMT());
                        b.putString("SIPMultiplier", (fundList.getData().get(position).getSIP_MULTPL_AMT() == null
                                || fundList.getData().get(position).getSIP_MULTPL_AMT().equals("null")) ? "1" :
                                fundList.getData().get(position).getSIP_MULTPL_AMT());
                        b.putString("SIPDates", fundList.getData().get(position).getSIP_DATES() == null ? "" : fundList.getData().get(position).getSIP_DATES());

                        Log.e("Send", "NAV:" + fundList.getData().get(position).getNAV() +
                                "\nISIN:" + fundList.getData().get(position).getISIN() +
                                "\nisSIP:" + fundList.getData().get(position).getSTARMF_SIP_FLAG().trim() +
                                "\nStarMFSchemeCode:" + fundList.getData().get(position).getSTARMF_SCHEME_CODE() +
                                "\nSchemeName:" + fundList.getData().get(position).getSCHEME_NAME() +
                                "\nSchemeCode:" + fundList.getData().get(position).getSTARMF_SCHEME_CODE() +
                                "\nMinimumAmnt:" + fundList.getData().get(position).getSTARMF_MIN_PUR_AMT());
                              //  "\nMinimumAmnt:" + fundList.getData().get(position).getMIN_INIT());

                        frag.setArguments(b);
                        UiUtils.hideKeyboard(contextd);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    }
                })
        );
        if (fundList == null) {
            Log.d("getString..type",getArguments().getString("type"));
            Log.d("getString..sub_section",getArguments().getString("sub_section"));
            getInterMediatFunds(getArguments().getString("type"), getArguments().getString("sub_section"));

        }else {
            if (adapter == null)
                adapter = new IntermidiateAdapter(fundList, contextd);
            recyclerView.setAdapter(adapter);
        }
        return parentView;
    }

//    void getInterMediatFunds(String type, String sub_section) {
//        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
//        BaseRequestData baseRequestData = new BaseRequestData();
//        baseRequestData.setWebservice("ws-intermediate.php");
//        baseRequestData.setTag(ResponseType.GetFundList);
//        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
//        requestedServiceDataModel.setWebServiceType(type);
//        requestedServiceDataModel.putQurry("type", tabFrom);
//        if (!sub_section.trim().equals(""))
//            requestedServiceDataModel.putQurry("sub_section", sub_section);
//        requestedServiceDataModel.setBaseRequestData(baseRequestData);
//        requestedServiceDataModel.execute();
//    }
    void getInterMediatFunds(String type, String sub_section) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-product-intermediate.php");
        baseRequestData.setTag(ResponseType.GetFundList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType(type);
        requestedServiceDataModel.putQurry("type", tabFrom);
        if (!sub_section.trim().equals(""))
            requestedServiceDataModel.putQurry("sub_section", sub_section);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        Gson gson = new Gson();
        switch (baseRequestData.getTag()) {
            case ResponseType.GetFundList:
                fundList = gson.fromJson(json, InvestmentFunds.class);
                if (fundList.getData().size() > 0) {
                    adapter = new IntermidiateAdapter(fundList, contextd);
                    recyclerView.setAdapter(adapter);
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetFundList:
                Common.showToast(contextd, message);
                break;
        }
    }
}
