package com.niivo.com.niivo.Model;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by deepak on 8/12/17.
 */

public class PastSipList {
    String msg, status;

    ArrayList<SipDues> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SipDues> getData() {
        return data;
    }

    public void setData(ArrayList<SipDues> data) {
        this.data = data;
    }

    public class SipDues {

        String scheme_code;
        String amount;
        String payment_status;
        String scheme_name;

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }

        String dtdate;

        public String getScheme_name() {
            return scheme_name;
        }

        public void setScheme_name(String scheme_name) {
            this.scheme_name = scheme_name;
        }


        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }


        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }


        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }


    }
}
