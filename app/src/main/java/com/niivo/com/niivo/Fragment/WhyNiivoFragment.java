package com.niivo.com.niivo.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 6/11/17.
 */

public class WhyNiivoFragment extends Fragment {
    ImageButton back;
    TextView whyTxt;

    Context contextd;
    String lang = "en";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_why_niivo, container, false);
        contextd = container.getContext();
        whyTxt = (TextView) rootView.findViewById(R.id.whyNiivoTxt);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        title.setText(contextd.getResources().getString(R.string.whyNiivo));
        back = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        back.setVisibility(View.VISIBLE);
        lang = Common.getLanguage(getActivity());
        whyTxt.setText(lang.equals("en") ? (Html.fromHtml("<p ><strong>Why Niivo?</strong></p>" +
                "<p >Niivo offers a completely transparent and easy way to get starting with mutual fund investments. Niivo comes from the Indian word <span lang=\"hi-IN\"><span style=\"font-family: 'Nirmala UI';\">नीव</span> </span>which means &lsquo;base&rsquo; or &lsquo;foundation&rsquo;. Mutual funds are a great way to start investing for the future and Niivo aims to make this activity easy and thus provide a foundation for investing. Currently, investing in mutual funds is a daunting task. Niivo aims to make this as easy as buying other things over smartphones.</p>\n" +
                "<p ><strong>Features:</strong></p>\n" +
                "<ul style=\"list-style-type: disc;\">\n" +
                "<li>\n" +
                "<p>Various investing techniques are available depending on level of expertise</p>\n" +
                "</li>\n" +
                "<li>\n" +
                "<p>Simple KYC procedure</p>\n" +
                "</li>\n" +
                "<li>\n" +
                "<p>Paperless interaction and ease of purchasing and withdrawing money from funds</p>\n" +
                "</li>\n" +
                "<li>\n" +
                "<p>Freedom to choose from a variety of funds from various fund houses</p>\n" +
                "</li>\n" +
                "<li>\n" +
                "<p>Provision to closely monitor how your investment is doing</p>\n" +
                "</li>\n" +
                "</ul>")) :
                lang.equals("hi") ? (Html.fromHtml("<p ><strong>नीवो क्यों?</strong></p>" +
                        "<p >नीवो, म्युचुअल फ़ंड में निवेश शुरू करने के लिए एक पूर्णतः पारदर्शी और आसान तरीका प्रदान करता है। नीवो की व्युत्पत्ति भारतीय शब्द नींव से हुई है जिसका मतलब है 'आधार' या 'बुनियाद'। म्युचुअल फ़ंड भविष्य के लिए निवेश शुरू करने का एक शानदार तरीका है और नीवो का लक्ष्य इस गतिविधि को आसान बनाना है, और इस तरह यह निवेश करने के लिए बुनियाद या नींव उपलब्ध कराता है। वर्तमान में, म्युचुअल फ़ंड में निवेश करना एक कठिन काम है। नीवो का उद्देश्य इसे स्मार्टफ़ोन पर अन्य चीज़ों को खरीदने जितना आसान बनाना है।</p>\n" +
                        "<p ><strong>विशेषताएँ:</strong></p>\n" +
                        "<ul style=\"list-style-type: disc;\">\n" +
                        "<li>\n" +
                        "<p>विशेषज्ञता के स्तर के आधार पर निवेश की विभिन्न तकनीकें उपलब्ध हैं</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>सरल केवाईसी प्रक्रिया</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>कागज रहित इंटरैक्शन और खरीदारी करने और फ़ंड्स से पैसे निकालने में आसानी</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>विभिन्न फ़ंड हाउस के कई फ़ंड्स में से चुनने की स्वतंत्रता</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>अपने निवेश के प्रदर्शन पर नजदीक से निगरानी करने की व्यवस्था</p>\n" +
                        "</li>\n" +
                        "</ul>")) :
                        lang.equals("gu") ? (Html.fromHtml("<p ><strong>શા માટે નીવો?</strong></p>" +
                                "<p>મ્યુચ્યુઅલ ફંડ્સમાં રોકાણની શરૂઆત કરવા માટે નીવો સંપૂર્ણપણે પારદર્શક અને સરળ પદ્ધતિ છે. નીવો શબ્દ મૂળરૂપે હિન્દી શબ્દ ‘नीव’માંથી આવ્યો છે જેનો અર્થ ‘પાયો’ થાય છે. સમૃદ્ધ ભવિષ્ય માટે રોકાણની શરૂઆત કરવા મ્યુચ્યુઅલ ફંડ્સ શ્રેષ્ઠ રીત છે અને નીવો આ પ્રવૃત્તિ વધુ સરળ બનાવવાનો હેતુ ધરાવે છે જેથી–\n" +
                                "રોકાણનો પાયો પુરો પાડે છે. વર્તમાન સમયમાં, મ્યુચ્યુઅલ ફંડ્સમાં રોકાણ કરવું એ મનમાં ભય ઉભો કરતું કામ છે. નીવો આ કામને સ્માર્ટફોન પર જેટલી સરળતાથી અન્ય ચીજોની ખરીદી કરી શકાય એટલું જ સરળ બનાવવા માંગે છે.</p>\n" +
                                "<p ><strong>વિશેષતાઓઃ</strong></p>\n" +
                                "<ul style=\"list-style-type: disc;\">\n" +
                                "<li>\n" +
                                "<p>वતજજ્ઞતાના સ્તરના આધારે રોકાણની વિવિધ ટેકનિક્સ ઉપલબ્ધ છે.</p>\n" +
                                "</li>\n" +
                                "<li>\n" +
                                "<p>કેવાયસીની સરળ પ્રક્રિયા</p>\n" +
                                "</li>\n" +
                                "<li>\n" +
                                "<p>પેપરલેસ સંદેશાવ્યવહાર અને તમારા ફંડ્સમાંથી ખરીદી તેમજ નાણાંનો ઉપાડ ખૂબ સરળતાથી</p>\n" +
                                "</li>\n" +
                                "<li>\n" +
                                "<p>વિવિધ ફંડ હાઉસ દ્વારા ઉપલબ્ધ કરાવાતા વિવિધ ફંડ્સમાંથી કોઈપણ પસંદ કરવાની મુક્તિ</p>\n" +
                                "</li>\n" +
                                "<li>\n" +
                                "<p>તમારું રોકાણ કેવું પરફોર્મન્સ આપી રહ્યું છે તેના પર ઝીણવટપૂર્વક ધ્યાન આપવાની સુવિધા</p>\n" +
                                "</li>\n" +
                                "</ul>")) :
                                (Html.fromHtml("<p ><strong>नीवोच का?</strong></p>" +
                                        "<p >म्युच्युअल फंडात गुंतवणूक करण्यास सुरुवात करण्यासाठी नीवो संपूर्णपणे पारदर्शक आणि सोपा मार्ग उपलब्ध करून देते. नीवो हा शब्द भारतीय नीव ह्या शब्दावरून आलेला आहे, ज्याचा अर्थ ‘आधार’ किंवा ‘पाया’ असा आहे. भविष्यासाठी गुंतवणूक करण्यासाठी म्युच्युअल फंड हा उत्तम मार्ग आहे आणि हे काम सोपे करणे हा नीवोचा उद्देश आहे आणि अशा प्रकारे ते गुंतवणुकीसाठी पाया उपलब्ध करून देतात. सध्या म्युच्युअल फंडात गुंतवणूक करणे हे भिवविणारे काम असते. हे काम, स्मार्टफोनवर इतर गोष्टी खरेदी करण्यासारखेच सोपे करणे हा नीवोचा उद्देश आहे.</p>\n" +
                                        "<p ><strong>वैशिष्ट्ये:</strong></p>\n" +
                                        "<ul style=\"list-style-type: disc;\">\n" +
                                        "<li>\n" +
                                        "<p>प्रावीण्याच्या पातळीनुसार गुंतवणुकीची विविध तंत्रे उपलब्ध आहेत. </p>\n" +
                                        "</li>\n" +
                                        "<li>\n" +
                                        "<p>केवायसीची सोपी प्रक्रिया</p>\n" +
                                        "</li>\n" +
                                        "<li>\n" +
                                        "<p>कागदपत्रांशिवाय व्यवहार आणि खरेदी करण्यात आणि फंडातून पैसे काढण्यात सोपेपणा</p>\n" +
                                        "</li>\n" +
                                        "<li>\n" +
                                        "<p>विविध फंड हाऊसेसकडून वेगवगळ्या फंडांमधून निवड करण्याचे स्वातंत्र्य</p>\n" +
                                        "</li>\n" +
                                        "<li>\n" +
                                        "<p>तुमची गुंतवणूक कशी कामगिरी करीत आहे, ह्यावर जवळून लक्ष ठेवण्याची तरतूद</p>\n" +
                                        "</li>\n" +
                                        "</ul>"))
        );
        return rootView;
    }

}
