package com.niivo.com.niivo.Utils.NetWorkCheck;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.MyApplication;

import java.lang.reflect.Method;

public class NetworkDialogAct extends Activity {
    TextView tryAgainBtn;
    ImageView internetErrorDio;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dio_internet_not_working);

//        this.setFinishOnTouchOutside(false);
        tryAgainBtn = (TextView) findViewById(R.id.tryAgainBtn);
        internetErrorDio = (ImageView) findViewById(R.id.internetErrorDio);
        tryAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NetworkDialogAct.this.finish();
                MyApplication.setNoNetworkDialogShown(false);
                Intent intent = new Intent("tryAgainState");
                sendBroadcast(intent);
            }
        });
        boolean mobileData = getMobileDataEnabled();
        boolean wifiData = getWifiEnabled();

        if (mobileData && wifiData)
            internetErrorDio.setImageResource(R.drawable.no_internet);
        else if (wifiData && !mobileData)
            internetErrorDio.setImageResource(R.drawable.no_wifi_internet);
        else if (!wifiData && mobileData)
            internetErrorDio.setImageResource(R.drawable.no_internet);
        else
            internetErrorDio.setImageResource(R.drawable.internet_error);
    }


    boolean getMobileDataEnabled() {
        try {
            TelephonyManager tm =
                    (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm == null) return false;
            @SuppressLint("PrivateApi")
            Method getMobileDataEnabledMethod = tm.getClass().getDeclaredMethod("getDataEnabled");
            if (null != getMobileDataEnabledMethod) {
                return (boolean) getMobileDataEnabledMethod.invoke(tm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean getWifiEnabled() {
        @SuppressLint("WifiManagerLeak")
        WifiManager manager = (WifiManager) this.getSystemService(WIFI_SERVICE);
        return manager != null && manager.isWifiEnabled();
    }
}