package com.niivo.com.niivo.Utilities.BeginnerHelper;

import java.util.ArrayList;
import java.util.List;

public class AnnualToMonthlyMain {
	public static void main(String[] args) {
		
		//For example user chooses a goal which fetches from the most_persistent table ordered by PERSISTENCE_SCORE descending
		//We fetch the top 3 funds with the maximum persistence scores and take them as inputs while calculating allocations
		//Inputs are - ISIN, Min Lump Sum investment amout, Min Sip investment amount, relevant score
		InputFund fundWithHighestScore = new InputFund("INF740K01060", 1000, 500, 24.002);
		InputFund fundWithSecondHighestScore = new InputFund("INF204K01HY3", 5000, 100, 20.589);
		InputFund fundWithThirdHighestScore = new InputFund("INF277K01451", 5000, 1000, 17.425);
		
		List<InputFund> listOfInputFunds = new ArrayList<InputFund>();
		listOfInputFunds.add(fundWithHighestScore);
		listOfInputFunds.add(fundWithSecondHighestScore);
		listOfInputFunds.add(fundWithThirdHighestScore);
		//Inputs are: Rate of growth, Number of years, Amount required by user
		printForGivenStats(8, 15, 1500000, listOfInputFunds);
		
		printForGivenStats(8, 25, 2500000, listOfInputFunds);
		
		printForGivenStats(10, 10, 1200000, listOfInputFunds);
		
		printForGivenStats(10, 15, 5000000, listOfInputFunds);
		
		printForGivenStats(6, 10, 170000, listOfInputFunds);
		
		printForGivenStats(10, 5, 10000, listOfInputFunds);
	}		
	
	
	private static void printForGivenStats(double annualRatePerc, double timeInYrs, double finalAmt, List<InputFund> listOfInputFunds){
		System.out.println("<--------------->");
		System.out.println("Final Amount required: " + finalAmt);
		Investment investment = new Investment(finalAmt, timeInYrs, annualRatePerc, listOfInputFunds,false);
		int lumpSumAmt = investment.getLumpSumAmt();
		//Put a check here to ensure that whatever required amount enters in the front end, the lumpSumAmt > investment.getLowestLumpSumAmt()
		System.out.println("Lump Sum Amount to paid at " + annualRatePerc +"% annual growth for " + timeInYrs + "yrs: " + lumpSumAmt);
		int[] lumpSumAllocations = investment.getLumpSumAllocations();
		System.out.println("The amount will be divided into " + lumpSumAllocations.length + " fund(s)");
		for(int i = 0; i < lumpSumAllocations.length; i++ ){
			System.out.println("Lump Sum Allocation " + (i+1) + ": " +  lumpSumAllocations[i]);
		}
		
		int sipAmt = investment.getSipAmt();
		//Put a check here to ensure that whatever required amount enters in the front end, the sipAmt > investment.getLowestSipAmt()
		System.out.println("SIP for " + timeInYrs + "yrs: " + sipAmt);
		int[] sipAllocations = investment.getSipAllocations();
		System.out.println("The amount will be divided into " + sipAllocations.length + " fund(s)");
		for(int i = 0; i < sipAllocations.length; i++ ){
			System.out.println("SIP Allocation " + (i+1) + ": " +  sipAllocations[i]);
		}
		System.out.println("<--------------->");
		System.out.println();
		
	}
}
