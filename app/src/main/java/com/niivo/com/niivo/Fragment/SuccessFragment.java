package com.niivo.com.niivo.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;


/**
 * Created by andro on 22/5/17.
 */

public class SuccessFragment extends Fragment implements View.OnClickListener {
    Activity _activity;
    TextView gotoHome;

    LinearLayout ugh, bad, ok, good, awesome;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
       View parentView = inflater.inflate(R.layout.fragment_success, null, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(_activity.getResources().getString(R.string.app_name));
        gotoHome = (TextView) parentView.findViewById(R.id.gotoHome);
        gotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finishAffinity();
            }
        });


        ugh = (LinearLayout) parentView.findViewById(R.id.rateUgh);
        bad = (LinearLayout) parentView.findViewById(R.id.rateBad);
        ok = (LinearLayout) parentView.findViewById(R.id.rateOk);
        good = (LinearLayout) parentView.findViewById(R.id.rateGood);
        awesome = (LinearLayout) parentView.findViewById(R.id.rateAwesome);
        ugh.setOnClickListener(this);
        bad.setOnClickListener(this);
        ok.setOnClickListener(this);
        good.setOnClickListener(this);
        awesome.setOnClickListener(this);

        return parentView;

        //finchk3 5  6   7
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rateUgh:
                ugh.setSelected(true);
                bad.setSelected(false);
                ok.setSelected(false);
                good.setSelected(false);
                awesome.setSelected(false);
                break;
            case R.id.rateBad:
                ugh.setSelected(false);
                bad.setSelected(true);
                ok.setSelected(false);
                good.setSelected(false);
                awesome.setSelected(false);
                break;
            case R.id.rateOk:
                ugh.setSelected(false);
                bad.setSelected(false);
                ok.setSelected(true);
                good.setSelected(false);
                awesome.setSelected(false);
                break;
            case R.id.rateGood:
                ugh.setSelected(false);
                bad.setSelected(false);
                ok.setSelected(false);
                good.setSelected(true);
                awesome.setSelected(false);
                break;
            case R.id.rateAwesome:
                ugh.setSelected(false);
                bad.setSelected(false);
                ok.setSelected(false);
                good.setSelected(false);
                awesome.setSelected(true);
                break;
        }
    }
}
