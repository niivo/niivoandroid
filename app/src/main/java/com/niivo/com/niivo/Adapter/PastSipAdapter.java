package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.niivo.com.niivo.Model.PastSipList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PastSipAdapter extends BaseAdapter {

    Context contextd;
    PastSipList list;
    OrderHolder holder;
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));


    public PastSipAdapter(Context contextd, PastSipList list) {
        this.contextd = contextd;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(contextd).inflate(R.layout.pastsip, parent, false);
            holder = new OrderHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OrderHolder) convertView.getTag();
        }

        Calendar ct = Calendar.getInstance();
        try {
            ct.setTime(getSDF.parse(list.getData().get(position).getDtdate()));
            holder.date.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.scheme.setText(list.getData().get(position).getScheme_name().trim());
        holder.amount.setText(Common.currencyString(list.getData().get(position).getAmount(),false));
          if(list.getData().get(position).getPayment_status().equals("1"))
          {
              holder.status.setText("Paid");
          }else {
              holder.status.setText("UnPaid");
          }
        return convertView;
    }

    class OrderHolder  {
        TextView date,amount,scheme,status;

        public OrderHolder(View v) {
            scheme= (TextView)v.findViewById(R.id.fundName);
            date = (TextView)v.findViewById(R.id.sipPaymentDate);
            status = (TextView)v.findViewById(R.id.payStatus);
            amount=(TextView)v.findViewById(R.id.sipAmount);
        }
    }
}

/*    Context contextd;
    PastSipList list;
    SipPastHolder  holder;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));

    public PastSipAdapter(Context contextd, PastSipList list) {

        this.contextd = contextd;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(contextd).inflate(R.layout.pastsip, parent, false);
            holder = new SipPastHolder(convertView);
        } else {
            holder = (SipPastHolder) convertView.getTag();
        }
        holder.scheme.setText(list.getData().get(position).getScheme_code());
        Log.d("listlist....",list.getData().get(position).getScheme_name());

        //holder.amount.setText(Common.currencyString(list.getData().get(position).getAmount(),false));
        Log.d("listAmount....",list.getData().get(position).getAmount());
        Calendar ct = Calendar.getInstance();
        try {
            ct.setTime(sdf.parse(list.getData().get(position).getDtdate()));
            holder.date.setText(Common.getDateString(ct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
      //  holder.payStatus.setText(list.getData().get(position).getPayment_status());
        if (list.getData().get(position).getPayment_status().equals("1"))
            holder.payStatus.setText("Paid");
        else
            holder.payStatus.setText("UnPaid");
        return convertView;
    }

    public class SipPastHolder {
        TextView scheme, date, amount,payStatus;

        public SipPastHolder(View item) {
            scheme = (TextView) item.findViewById(R.id.fundName);
            date = (TextView) item.findViewById(R.id.sipPaymentDate);
            amount = (TextView) item.findViewById(R.id.sipAmount);
            payStatus = (TextView) item.findViewById(R.id.payStatus);
        }
    }*/
