package com.niivo.com.niivo.Model;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by deepak on 8/12/17.
 */

public class SipDueReports {
    String msg, status;

    ArrayList<SipDues> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SipDues> getData() {
        return data;
    }

    public void setData(ArrayList<SipDues> data) {
        this.data = data;
    }

    public class SipDues {
        String ref_no;
        String sip_registration_no;
        String start_date;
        String scheme_code;
        String frequency;
        String amount;
        String due_date = "";
        String prev_paid_date;
        String installment_paid;
        String total_amount_paid;
        String first_order;
        String fund_name;
        String is_goal;
        String payment_status;

        public String getScheme_name() {
            return scheme_name;
        }

        public void setScheme_name(String scheme_name) {
            this.scheme_name = scheme_name;
        }

        String scheme_name;

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        ArrayList<OrderDetail> order_detail;
        GoalDetails goal_detail;



        public String getIs_goal() {
            return is_goal;
        }

        public void setIs_goal(String is_goal) {
            this.is_goal = is_goal;
        }

        public GoalDetails getGoal_detail() {
            return goal_detail;
        }

        public void setGoal_detail(GoalDetails goal_detail) {
            this.goal_detail = goal_detail;
        }

        public ArrayList<OrderDetail> getOrder_detail() {
            return order_detail;
        }

        public void setOrder_detail(ArrayList<OrderDetail> order_detail) {
            this.order_detail = order_detail;
        }

        public String getRef_no() {
            return ref_no;
        }

        public void setRef_no(String ref_no) {
            this.ref_no = ref_no;
        }

        public String getSip_registration_no() {
            return sip_registration_no;
        }

        public void setSip_registration_no(String sip_registration_no) {
            this.sip_registration_no = sip_registration_no;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getFrequency() {
            return frequency;
        }

        public void setFrequency(String frequency) {
            this.frequency = frequency;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDue_date() {
            return due_date;
        }

        public void setDue_date(String due_date) {
            this.due_date = due_date;
        }

        public String getPrev_paid_date() {
            return prev_paid_date;
        }

        public void setPrev_paid_date(String prev_paid_date) {
            this.prev_paid_date = prev_paid_date;
        }

        public String getInstallment_paid() {
            return installment_paid;
        }

        public void setInstallment_paid(String installment_paid) {
            this.installment_paid = installment_paid;
        }

        public String getTotal_amount_paid() {
            return total_amount_paid;
        }

        public void setTotal_amount_paid(String total_amount_paid) {
            this.total_amount_paid = total_amount_paid;
        }

        public String getFirst_order() {
            return first_order;
        }

        public void setFirst_order(String first_order) {
            this.first_order = first_order;
        }

        public String getFund_name() {
            return fund_name;
        }

        public void setFund_name(String fund_name) {
            this.fund_name = fund_name;
        }
    }

    public class OrderDetail {
        String order_id, order_type, scheme_code, amount, quantity, is_allotment, payment_status, is_goal, scheme_name, sip_registration_no, bse_order_no;

        public String getBse_order_no() {
            return bse_order_no;
        }

        public void setBse_order_no(String bse_order_no) {
            this.bse_order_no = bse_order_no;
        }

        public String getSip_registration_no() {
            return sip_registration_no;
        }

        public void setSip_registration_no(String sip_registration_no) {
            this.sip_registration_no = sip_registration_no;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getIs_allotment() {
            return is_allotment;
        }

        public void setIs_allotment(String is_allotment) {
            this.is_allotment = is_allotment;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getIs_goal() {
            return is_goal;
        }

        public void setIs_goal(String is_goal) {
            this.is_goal = is_goal;
        }

        public String getScheme_name() {
            return scheme_name;
        }

        public void setScheme_name(String scheme_name) {
            this.scheme_name = scheme_name;
        }
    }

    public class GoalDetails {
        String order_id, main_orderid, fund_category, goal_name, investment_type, goal_year, goal_ammount;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getMain_orderid() {
            return main_orderid;
        }

        public void setMain_orderid(String main_orderid) {
            this.main_orderid = main_orderid;
        }

        public String getFund_category() {
            return fund_category;
        }

        public void setFund_category(String fund_category) {
            this.fund_category = fund_category;
        }

        public String getGoal_name() {
            return goal_name;
        }

        public void setGoal_name(String goal_name) {
            this.goal_name = goal_name;
        }

        public String getInvestment_type() {
            return investment_type;
        }

        public void setInvestment_type(String investment_type) {
            this.investment_type = investment_type;
        }

        public String getGoal_year() {
            return goal_year;
        }

        public void setGoal_year(String goal_year) {
            this.goal_year = goal_year;
        }

        public String getGoal_ammount() {
            return goal_ammount;
        }

        public void setGoal_ammount(String goal_ammount) {
            this.goal_ammount = goal_ammount;
        }
    }
}
