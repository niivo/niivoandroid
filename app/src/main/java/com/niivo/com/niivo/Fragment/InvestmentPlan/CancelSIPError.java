package com.niivo.com.niivo.Fragment.InvestmentPlan;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.niivo.com.niivo.Fragment.MyMoneyFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

public class CancelSIPError extends Fragment implements ResponseDelegate {

    RequestedServiceDataModel requestedServiceDataModel;
    Context contextd;
    String order_type, INWARD_TXN_NO;
    TextView thanks_text;
    String msg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.cancelsiperror, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setVisibility(View.INVISIBLE);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.cancelSip));
        Button backToHome = (Button) rootView.findViewById(R.id.backToHome);
        thanks_text = (TextView) rootView.findViewById(R.id.thanks_text);

        Bundle bundle = getArguments();
        order_type = bundle.getString("order_type");
        INWARD_TXN_NO = bundle.getString("INWARD_TXN_NO");

        backToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.frame_container, new MyMoneyFragment()).addToBackStack(null).//TabFragment
                        commit();
            }
        });
        cancelSIP();
        return rootView;
    }

    void cancelSIP() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("order_type", order_type);
        requestedServiceDataModel.putQurry("INWARD_TXN_NO", INWARD_TXN_NO);
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {


    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
    //            Log.d("SIP json..", json);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
                try {

         //           Log.d("SIP json..", json);
                    JSONObject jsonObject = new JSONObject(json);
                    msg = jsonObject.getJSONObject("data").getString("bse_response");

                    String str = msg;
                    thanks_text.setText(str);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}