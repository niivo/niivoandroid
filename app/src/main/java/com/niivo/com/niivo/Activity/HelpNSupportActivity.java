package com.niivo.com.niivo.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Outline;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.Utility;

import org.w3c.dom.Text;

/**
 * Created by deepak on 7/11/17.
 */

public class HelpNSupportActivity extends AppCompatActivity implements View.OnClickListener {
    TextView mailTo, callTo,version;
    ImageButton back;
    ImageView verifiedLogo;
    String appsversion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(HelpNSupportActivity.this, Common.getLanguage(HelpNSupportActivity.this), false);
        setContentView(R.layout.activity_help_and_support);
        mailTo = (TextView) findViewById(R.id.mailTo);
        callTo = (TextView) findViewById(R.id.callTo);
        back = (ImageButton) findViewById(R.id.back);
        verifiedLogo= (ImageView) findViewById(R.id.verifiedLogo);
        version= (TextView) findViewById(R.id.vesrion);
        verifiedLogo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0,0,view.getWidth(),view.getHeight()    );
            }
        });
        mailTo.setOnClickListener(this);
        callTo.setOnClickListener(this);
        back.setOnClickListener(this);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            appsversion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version.setText("Niivo Version  :"+appsversion);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.mailTo:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
              //  intent.setData(Uri.parse("mailto:anshuman.saxena@claroinvestments.com"));
                intent.setData(Uri.parse("mailto:support@niivo.in"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Niivo Support");
                startActivity(Intent.createChooser(intent, "Send Email Using..!"));
                break;
            case R.id.callTo:
               /* Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+917720020355"));
                if (ActivityCompat.checkSelfPermission(HelpNSupportActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Utility.checkPhonePermission(HelpNSupportActivity.this);
                    return;
                }
                startActivity(callIntent);*/
                break;
        }
    }
}
