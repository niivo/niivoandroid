package com.niivo.com.niivo.Utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.*;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.niivo.com.niivo.Utils.NotificationHelpers.FirebaseInstanceIDService;

import net.danlew.android.joda.JodaTimeAndroid;

import org.acra.ACRA;
import org.acra.config.ACRAConfiguration;
import org.acra.config.ACRAConfigurationException;
import org.acra.config.ConfigurationBuilder;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by user47 on 22/5/17.
 */
public  class MyApplication extends MultiDexApplication {

    public static SharedPreferences sharedPreferences, document;
    public static SharedPreferences.Editor editor, docEditor;
    private  Timer timer;
    private static Context context;

    public static boolean isNoNetworkDialogShown() {
        return noNetworkDialogShown;
    }

    public static void setNoNetworkDialogShown(boolean noNetworkDialogShown) {
        MyApplication.noNetworkDialogShown = noNetworkDialogShown;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        MyApplication.context = context;
    }

    public static boolean noNetworkDialogShown = false;

    public static String getToken()
    {
       return  Common.getToken(context);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;
//        AutoErrorReporter.get(this)
//                .setEmailAddresses("deepak.sharma@ninehertzindia.com")
//                .setEmailSubject("Niivo Crash Report")
//                .start();
        JodaTimeAndroid.init(this);
        ACRAConfiguration
                config = null;
        try {
            config = new ConfigurationBuilder(this)
                    .setMailTo("gaurav.tak@ninehertzindia.com")
                    .build();
        } catch (ACRAConfigurationException e) {
            e.printStackTrace();
        }
        if (config != null)
            ACRA.init(this, config);

        sharedPreferences = getSharedPreferences("Niivo", Context.MODE_PRIVATE);
        document = getSharedPreferences("NiivoDocument", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        docEditor = document.edit();

       FirebaseApp.initializeApp(this);
       // String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Common.addToken(FirebaseInstanceIDService.this, refreshedToken+"");
        timer = new Timer();
      //  Common.addToken(context, "123456"+"");

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(FirebaseInstanceId.getInstance()!=null && FirebaseInstanceId.getInstance().getToken()!=null && !FirebaseInstanceId.getInstance().getToken().equalsIgnoreCase(""))
                {
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    Common.addToken(context, refreshedToken+"");
                    cancelTimer();
                    android.util.Log.i("Common Token", "run: "+FirebaseInstanceId.getInstance().getToken());
                    //Log.d("Common Token", "run: "+FirebaseInstanceId.getInstance().getToken());
                    //Common.setPreferences(getApplicationContext(),"firebase_token",FirebaseInstanceId.getInstance().getToken());

                    // timer.cancel();
                }
                else
                {
                    android.util.Log.i("Common Token", "run: "+"Not token");
                    // Log.i(TAG, "run: "+FirebaseInstanceId.getInstance().getToken());


                }
            }
        },0,1000);


    }
    private  void cancelTimer() {
        if(timer!=null)
            timer.cancel();
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static void startGetToken()
    { final Timer timer1 = new Timer();
        timer1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(FirebaseInstanceId.getInstance()!=null && FirebaseInstanceId.getInstance().getToken()!=null && !FirebaseInstanceId.getInstance().getToken().equalsIgnoreCase(""))
                {
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    Common.addToken(context, refreshedToken+"");
                    if(timer1!=null)
                        timer1.cancel();
                    android.util.Log.i("Common Token", "run: "+FirebaseInstanceId.getInstance().getToken());
                    //Log.d("Common Token", "run: "+FirebaseInstanceId.getInstance().getToken());
                    //Common.setPreferences(getApplicationContext(),"firebase_token",FirebaseInstanceId.getInstance().getToken());

                    // timer.cancel();
                }
                else
                {
                    android.util.Log.i("Common Token", "run: "+"Not token");
                    // Log.i(TAG, "run: "+FirebaseInstanceId.getInstance().getToken());


                }
            }
        },0,1000);
    }

}
