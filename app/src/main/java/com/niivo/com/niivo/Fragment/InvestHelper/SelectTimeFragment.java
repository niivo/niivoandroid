package com.niivo.com.niivo.Fragment.InvestHelper;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.TextView;

import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.BeginnerFragments.BiginnerSetAmmountFragment;
import com.niivo.com.niivo.Fragment.TransactionHistory.ViewAllTransactionFragment;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by deepak😘 on 13/6/17.
 */

public class SelectTimeFragment extends Fragment implements ResponseDelegate {
    Context contextd;
    TextView next;
    RadioButton onetime, monthlySIP;
    ImageView signup_logo;

    //Non ui vars
    RequestedServiceDataModel requestedServiceDataModel;
    String from = "", to = "";
    MainActivity activity;
    boolean flag=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_time, container, false);
        contextd = container.getContext();
        activity = (MainActivity) getActivity();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(getArguments().getString("from").equals("biginner") ? getArguments().getString("goalNameAlt") : contextd.getResources().getString(R.string.selectTime));
        onetime = (RadioButton) rootView.findViewById(R.id.oneTime);
        monthlySIP = (RadioButton) rootView.findViewById(R.id.monthlySIP);
        next = (TextView) rootView.findViewById(R.id.btn_next);
        signup_logo = (ImageView) rootView.findViewById(R.id.signup_logo);
        signup_logo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("SIPcheck", monthlySIP.isChecked() + "");
                Log.e("ONETIMEcheck", onetime.isChecked() + "");

                if (onetime.isChecked() || monthlySIP.isChecked()) {
                    openFragment(onetime.isChecked() ? true : false);
                } else {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.selectTypeOfInvestment));
                }
            }
        });
        yearsd = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            yearsd.add((Integer.parseInt((Calendar.getInstance().get(Calendar.YEAR) + i + 1) + "")) + "");
        }
        if (!getArguments().getString("from").equals("biginner")) {
            if (!getArguments().getString("isSIP").equals("Y")) {
                monthlySIP.setVisibility(View.GONE);
            }
        }
        yearText = new String[yearsd.size()];
        yearText = yearsd.toArray(yearText);
        monthlySIP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                   /* if (activity.userDetail.getData().getKyc_status().equalsIgnoreCase("COMPLETE")
                            &&activity.userDetail.getData().getIs_mendate().equalsIgnoreCase("NOT REGISTERED")) {
                        showMandateAlert();
                    }*/
                     if(activity.userDetail.getData()!=null && activity.userDetail.getData().getKyc_status()!=null && activity.userDetail.getData()!=null&& activity.userDetail.getData().getIs_mendate()!=null)
                    {
                        if (activity.userDetail.getData().getKyc_status().equalsIgnoreCase("COMPLETE")
                                &&activity.userDetail.getData().getIs_mendate().equalsIgnoreCase("NOT REGISTERED")) {
                            showMandateAlert();
                        }

                        else if (activity.userDetail.getData().getKyc_status().equalsIgnoreCase("NOT STARTED")
                                &&activity.userDetail.getData().getIs_mendate().equalsIgnoreCase("NOT REGISTERED")) {
                            showKycAlert();
                        }
                    }else if(activity.userDetail.getData()==null || activity.userDetail.getData().getKyc_status()==null || activity.userDetail.getData()==null || activity.userDetail.getData().getIs_mendate()==null)
                     {
                         showMandateAlert();
                     }
                }
            }
        });

        SharedPreferences prefss = PreferenceManager.getDefaultSharedPreferences(contextd);
        prefss.edit().putString("flag", String.valueOf(flag)).commit();
        String kycstatusselect = activity.userDetail.getData().getKyc_status();
        prefss.edit().putString("kycstatusselect", kycstatusselect).commit();
        return rootView;
    }


    Dialog mandateAlert;
    ImageView close;
    Button createNow;

    void showMandateAlert() {
        mandateAlert = new Dialog(contextd);
        mandateAlert.setContentView(R.layout.dio_e_mandate_alert);
        close = (ImageView) mandateAlert.findViewById(R.id.closeBtn);
        createNow = (Button) mandateAlert.findViewById(R.id.createNowBtn);
        mandateAlert.setCancelable(false);
        Window window = mandateAlert.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(wlp);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandateAlert.dismiss();
            }
        });
        createNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandateAlert.dismiss();
                activity.goToEmandate();
            }
        });
        mandateAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mandateAlert.show();
    }
    void showKycAlert() {
        mandateAlert = new Dialog(contextd);
        mandateAlert.setContentView(R.layout.dio_e_mandate_alert);

        close = (ImageView) mandateAlert.findViewById(R.id.closeBtn);
        createNow = (Button) mandateAlert.findViewById(R.id.createNowBtn);
        TextView message = (TextView) mandateAlert.findViewById(R.id.message);
        message.setText(getString(R.string.kyc_alert));
        createNow.setText(getString(R.string.kyc_registation));

        mandateAlert.setCancelable(false);
        Window window = mandateAlert.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(wlp);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandateAlert.dismiss();
            }
        });
        createNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mandateAlert.dismiss();
                activity.goToKyc();
            }
        });
        mandateAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mandateAlert.show();
    }

    void openFragmentt(boolean oneTime) {
        Fragment frag = null;
        if (oneTime) {
            if (getArguments().getString("from").equals("biginner")) {

                FragmentTransaction transection=getFragmentManager().beginTransaction();
                BiginnerSetAmmountFragment mfragment = new BiginnerSetAmmountFragment();
                Bundle bundle=new Bundle();
                bundle.putString("orderType", "ONETIME");
                bundle.putString("cateId",getArguments().getString("cateId"));
                bundle.putString("goalNameAlt",getArguments().getString("goalNameAlt"));
                mfragment.setArguments(bundle); //data being send to SecondFragment
                transection.replace(R.id.frame_container, mfragment);
                transection.commit();
                // Fragment fragg = new BiginnerSetAmmountFragment();
//                Bundle b = getArguments();
//                b.putString("orderType", "ONETIME");
//                b.putString("cateId",   getArguments().getString("cateId"));
//                b.putString("goalNameAlt", getArguments().getString("goalNameAlt"));
//                Log.d("goalnameO",getArguments().getString("goalNameAlt"));
//                //frag.setArguments(b);
//                UiUtils.hideKeyboard(contextd);
//                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
//                        .replace(R.id.frame_container, new BiginnerSetAmmountFragment()).addToBackStack(null).
//                        commit();
//                getBigineerFunds(getArguments().getString("cateId"), false);
//                frag = null;
            } else {
                frag = new SetInvestAmountFragment();
                Bundle b = getArguments();
                b.putString("orderType", "ONETIME");
                frag.setArguments(b);
            }
        } else {
            if (getArguments().getString("from").equals("biginner")) {
//                getBigineerFunds(getArguments().getString("cateId"), true);
//                frag = null;
                FragmentTransaction transection=getFragmentManager().beginTransaction();
                BiginnerSetAmmountFragment mfragment = new BiginnerSetAmmountFragment();
                Bundle bundle=new Bundle();
                bundle.putString("orderType", "MONTHLY");
                bundle.putString("cateId",   getArguments().getString("cateId"));
                bundle.putString("goalNameAlt", getArguments().getString("goalNameAlt"));
                bundle.putString("goalNameAlt", getArguments().getString("goalNameAlt"));
                mfragment.setArguments(bundle); //data being send to SecondFragment
                transection.replace(R.id.frame_container, mfragment);
                transection.commit();
            } else {
                frag = null;
                setTargetYear();
            }
        }

        if (frag != null) {
            UiUtils.hideKeyboard(contextd);
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, frag).addToBackStack(null).
                    commit();
        }
    }

    void openFragment(boolean oneTime) {
        Fragment frag = null;
        if (oneTime) {
            if (getArguments().getString("from").equals("biginner")) {
                getBigineerFunds(getArguments().getString("cateId"), false);
                frag = null;
            } else {
                frag = new SetInvestAmountFragment();
                Bundle b = getArguments();
                b.putString("orderType", "ONETIME");
                frag.setArguments(b);
            }
        } else {
            if (getArguments().getString("from").equals("biginner")) {
                getBigineerFunds(getArguments().getString("cateId"), true);
                frag = null;
            } else {
                frag = null;
                setTargetYear();
            }
        }

        if (frag != null) {
            UiUtils.hideKeyboard(contextd);
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.frame_container, frag).addToBackStack(null).
                    commit();
        }
    }


    AlertDialog alert;
    Calendar calendar, temp;
    TextView fromMonth, toMonth;
    EditText targetYear;
    SimpleDateFormat sdmonth = new SimpleDateFormat("MMM-yyyy", new Locale("en"));


    void setTargetYear() {
        calendar = Calendar.getInstance();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dio_set_target_year, null);
        dialogBuilder.setView(dialogView);

        TextView submit = (TextView) dialogView.findViewById(R.id.submit);
        targetYear = (EditText) dialogView.findViewById(R.id.targetYear);
        fromMonth = (TextView) dialogView.findViewById(R.id.fromMonth);
        toMonth = (TextView) dialogView.findViewById(R.id.toMonth);
        fromMonth.setText(contextd.getResources().getString(R.string.fromd) + "\n" + sdmonth.format(calendar.getTime()));
        from = sdmonth.format(calendar.getTime());
        toMonth.setVisibility(View.GONE);
        fromMonth.setVisibility(View.GONE);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (targetYear.getText().toString().trim().equals("")) {
                    targetYear.setError(contextd.getResources().getString(R.string.requiredField));
                } else if (targetYear.getText().toString().trim().equals("0")) {
                    targetYear.setError(contextd.getResources().getString(R.string.invalidNumberOfMonths));
                } else {
                    String months = targetYear.getText().toString().trim();
                    Fragment frag = new SetInvestAmountFragment();
                    Bundle b = getArguments();
                    b.putString("orderType", "MONTHLY");
                    b.putString("months",   targetYear.getText().toString().trim());
                    b.putString("sipFrom",  from);
                    b.putString("sipTo",    to);
                    frag.setArguments(b);
                    UiUtils.hideKeyboard(contextd);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.frame_container, frag).addToBackStack(null).
                            commit();
                    alert.dismiss();
                }
            }
        });
        targetYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                temp = Calendar.getInstance();
                if ((targetYear.getText().toString().trim().equals("0"))) {
                    toMonth.setVisibility(View.GONE);
                    fromMonth.setVisibility(View.GONE);
                } else if (!targetYear.getText().toString().trim().equals("")) {
                    temp.add(Calendar.MONTH, Integer.parseInt(targetYear.getText().toString().trim()));
                    toMonth.setText(contextd.getResources().getString(R.string.toD) + "\n" + sdmonth.format(temp.getTime()));
                    to = sdmonth.format(temp.getTime());
                    toMonth.setVisibility(View.VISIBLE);
                    fromMonth.setVisibility(View.VISIBLE);
                } else {
                    toMonth.setVisibility(View.GONE);
                    fromMonth.setVisibility(View.GONE);
                }
            }
        });
        toMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMonthYearPickerDialog(0, 0);
            }
        });
        alert = dialogBuilder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alert.show();
        targetYear.setText("180");
    }

    ArrayList<String> yearsd;
    String[] monthText = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}, yearText;
    int monthPosd = -1, yearPosd = -1;
    SimpleDateFormat sdfMonth = new SimpleDateFormat("MMM", new Locale("en")),
            sdfYer = new SimpleDateFormat("yyyy", new Locale("en")),
            sdf = new SimpleDateFormat("dd-MMM-yyyy", new Locale("en"));
    Calendar temp2;
    AlertDialog alertDialog = null;
    String noOfMonth = "";

    void showMonthYearPickerDialog(int monthPos, int yearPos) {
        if (alertDialog == null) {
            AlertDialog.Builder builder;
            final TextView selectedMonth;
            builder = new AlertDialog.Builder(contextd, R.style.myAlertDialogTheme);
            View cust = LayoutInflater.from(contextd).inflate(R.layout.dio_month_year_picker, null);
            selectedMonth = (TextView) cust.findViewById(R.id.selectedMonth);
            final NumberPicker month = (NumberPicker) cust.findViewById(R.id.month),
            year = (NumberPicker) cust.findViewById(R.id.year);
            month.setMinValue(0);
            month.setMaxValue(monthText.length - 1);
            year.setMinValue(0);
            year.setMaxValue(yearText.length - 1);
            month.setWrapSelectorWheel(false);
            year.setWrapSelectorWheel(false);
            month.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            month.setDisplayedValues(monthText);
            year.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            year.setDisplayedValues(yearText);
            if (monthPos == -1)
                selectedMonth.setText(monthText[0] + ", " + yearText[0]);
            else {
                selectedMonth.setText(monthText[monthPos] + ", " + yearText[yearPos]);
                month.setValue(monthPos);
                year.setValue(yearPos);
            }

            month.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    selectedMonth.setText(monthText[picker.getValue()] + ", " + yearText[year.getValue()]);
                }
            });
            year.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    selectedMonth.setText(monthText[month.getValue()] + ", " + yearText[picker.getValue()]);
                }
            });
            builder.setView(cust);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    temp2 = Calendar.getInstance();
                    String date = "01-" + monthText[month.getValue()] + "-" + yearText[year.getValue()];
                    try {
                        temp2.setTime(sdf.parse(date));
                        Log.e("to:", sdf.format(temp2.getTime()));
                        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MMM-yyyy").withLocale(new Locale("en"));
                        DateTime from = dtf.parseDateTime(date),
                         to = DateTime.now();

                        Log.e("Dates", "From:" + dtf.print(from) + "\nTO:" + dtf.print(to));
                        noOfMonth = (Months.monthsBetween(to, from).getMonths() + 1) + "";
                        Log.e("No of Months:", noOfMonth + "");
                        targetYear.setText(noOfMonth);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    monthPosd = month.getValue();
                    yearPosd = year.getValue();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog = builder.create();
            alertDialog.show();
            Calendar ct = Calendar.getInstance();
            ct.add(Calendar.MONTH, 180);

            String mont = sdfMonth.format(ct.getTime()), yer = sdfYer.format(ct.getTime());
            int pos1 = 0, pos2 = 0;
            for (int i = 0; i < monthText.length; i++) {
                if (mont.equals(monthText[i]))
                    pos1 = i;
            }
            for (int i = 0; i < yearText.length; i++) {
                if (yer.equals(yearText[i]))
                    pos2 = i;
            }
            selectedMonth.setText(monthText[pos1] + ", " + yearText[pos2]);
            month.setValue(pos1);
            year.setValue(pos2);
        } else {
            alertDialog.show();
        }
    }


    void getBigineerFundss(String cateId, boolean sip) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-list.php");
        baseRequestData.setTag(ResponseType.GoalFundsList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("fund_category", cateId);
        requestedServiceDataModel.putQurry("sip_flag", onetime.isChecked() ? "N" : "Y");
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("BIGGNERS");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }
void getBigineerFundsss(String cateId, boolean sip) {
    requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
    BaseRequestData baseRequestData = new BaseRequestData();
    baseRequestData.setWebservice("ws-product-intermediate.php");
    baseRequestData.setTag(ResponseType.GoalFundsList);
    baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
    requestedServiceDataModel.putQurry("fund_category", cateId);
    requestedServiceDataModel.putQurry("sip_flag", onetime.isChecked() ? "N" : "Y");
    requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
    requestedServiceDataModel.putQurry("duration", "2");
    requestedServiceDataModel.putQurry("device_token", Common.getToken(getActivity().getApplicationContext()));
    requestedServiceDataModel.setWebServiceType("GOALFUND");
    requestedServiceDataModel.setLocal(true);
    requestedServiceDataModel.setBaseRequestData(baseRequestData);
    requestedServiceDataModel.execute();
}
void getBigineerFunds(String cateId, boolean sip)
{
    Fragment frag = new BiginnerSetAmmountFragment();
    Bundle b = getArguments();
    b.putString("orderType", onetime.isChecked() ? "ONETIME" : "MONTHLY");
    b.putString("fund_category", cateId);
    b.putString("sip_flag", onetime.isChecked() ? "N" : "Y");
    frag.setArguments(b);
    UiUtils.hideKeyboard(contextd);
    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
            .replace(R.id.frame_container, frag).addToBackStack(null).
            commit();
}


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GoalFundsList:
                Fragment frag = new BiginnerSetAmmountFragment();
                Bundle b = getArguments();
                b.putString("response", json);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    if (jsonObject.getJSONArray("data").length() != 3) {
                        Common.showDialog(contextd, contextd.getResources().getString(R.string.numberOfFundsNotEnough));
                    } else {
                        b.putString("orderType", onetime.isChecked() ? "ONETIME" : "MONTHLY");
                        frag.setArguments(b);
                        UiUtils.hideKeyboard(contextd);
                        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                                .replace(R.id.frame_container, frag).addToBackStack(null).
                                commit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GoalFundsList:
            //    Log.e("res", json);
                Common.showDialog(contextd, contextd.getResources().getString(R.string.numberOfFundsNotEnough));
                break;
        }
    }
}
