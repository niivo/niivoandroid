package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;

/**
 * Created by deepak on 28/8/18.
 */

public class TestClubedAddapter extends RecyclerView.Adapter {
    Context contextd;
    GetClick click;

    public TestClubedAddapter(Context contextd, GetClick click) {
        this.contextd = contextd;
        this.click = click;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View clubbbedScheme = LayoutInflater.from(contextd).inflate(R.layout.item_clubed_scheme, parent, false);
        View clubbedGoal = LayoutInflater.from(contextd).inflate(R.layout.item_invested_with_status, parent, false);
        return viewType == 0 ? new ClubedSchemeHolder(clubbbedScheme) : new ClubedGoalHolder(clubbedGoal);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ClubedGoalHolder) {
            ClubedGoalHolder goalHolder = (ClubedGoalHolder) holder;
            goalHolder.title.setText("Honda City");
            goalHolder.ivLogo.setImageResource(R.drawable.buyingcar);
            goalHolder.type.setText("GOAL");
            goalHolder.fundReturn.setText("32.5%");
            goalHolder.percentPortFolio.setText("32.1%");
            goalHolder.amntInvested.setText("\u20B9 1,25,000");
            goalHolder.invesdDate.setText("17th Apr 2018");
            goalHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click != null)
                        click.onGoalClick(position);
                }
            });
        } else if (holder instanceof ClubedSchemeHolder) {
            ClubedSchemeHolder schemeHolder = (ClubedSchemeHolder) holder;
            schemeHolder.title.setText("ICICI Prudential Multicap Fund - Growth");
            schemeHolder.fundReturn.setText("32.5%");
            schemeHolder.percentPortFolio.setText("32.1%");
            schemeHolder.amntInvested.setText("\u20B9 1,25,000");
            schemeHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click != null)
                        click.onSchemeClick(position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class ClubedSchemeHolder extends RecyclerView.ViewHolder {
        TextView title, amntInvested, type, percentPortFolio, fundReturn;
        ImageView ivLogo;

        public ClubedSchemeHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            type = (TextView) itemView.findViewById(R.id.type);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
        }
    }

    class ClubedGoalHolder extends RecyclerView.ViewHolder {
        TextView title, amntInvested, type, percentPortFolio, fundReturn, status, invesdDate;
        ImageView ivLogo;

        public ClubedGoalHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            status = (TextView) itemView.findViewById(R.id.payStatus);
            type = (TextView) itemView.findViewById(R.id.type);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
            invesdDate = (TextView) itemView.findViewById(R.id.investedDateTV);
        }
    }

    public interface GetClick {
        void onSchemeClick(int pos);

        void onGoalClick(int pos);
    }
}
