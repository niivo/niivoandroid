package com.niivo.com.niivo.Utilities.CartHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by deepak on 12/12/17.
 */

public class CartDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NiivoDB";
    private static final String Table_Cart = "MyCart";
    String id = "id",
            scheme_name = "scheme_name",
            amount = "amount",
            minAmount = "minAmount",
            orderType = "orderType",
            type = "type",
            scheme_code = "scheme_code",
            added_date = "added_date",
            target_year = "target_year",
            nav = "nav",
            qty = "qty",
            numberofmonths = "numberofmonths",
            goal_name = "goal_name",
            goal_amount = "goal_amount",
            schem_json = "schem_json",
            cate_Img = "cate_Img",
            goal_cate_id = "goal_cate_id";

    public CartDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + Table_Cart + " (" +
                id + " INTEGER PRIMARY KEY, " +
                scheme_name + " TEXT, " +
                amount + " TEXT, " +
                minAmount + " TEXT, " +
                orderType + " TEXT, " +
                type + " TEXT, " +
                scheme_code + " TEXT, " +
                added_date + " TEXT, " +
                target_year + " TEXT, " +
                nav + " TEXT, " +
                qty + " TEXT, " +
                numberofmonths + " TEXT, " +
                goal_name + " TEXT, " +
                goal_amount + " TEXT, " +
                schem_json + " TEXT, " +
                cate_Img + " TEXT, " +
                goal_cate_id + " TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Table_Cart);
        onCreate(db);
    }

    public void addFund(CartModle cartItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(scheme_name, cartItem.getScheme_name());
        values.put(amount, cartItem.getAmount());
        values.put(minAmount, cartItem.getMinAmount());
        values.put(orderType, cartItem.getOrderType());
        values.put(type, cartItem.getType());
        values.put(scheme_code, cartItem.getScheme_code());
        values.put(added_date, cartItem.getAdded_date());
        values.put(target_year, cartItem.getTarget_year());
        values.put(nav, cartItem.getNav());
        values.put(qty, cartItem.getQty());
        values.put(numberofmonths, cartItem.getNumberofmonths());
        values.put(schem_json, cartItem.getSchem_json());

        db.insert(Table_Cart, null, values);
        db.close();
    }

    public void addGoal(CartModle cartItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(scheme_code, cartItem.getScheme_code());
        values.put(orderType, cartItem.getOrderType());
        values.put(type, cartItem.getType());
        values.put(added_date, cartItem.getAdded_date());
        values.put(target_year, cartItem.getTarget_year());
        values.put(numberofmonths, cartItem.getNumberofmonths());
        values.put(goal_name, cartItem.getGoal_name());
        values.put(goal_amount, cartItem.getGoal_amount());
        values.put(schem_json, cartItem.getSchem_json());
        values.put(cate_Img, cartItem.getCateImage());
        values.put(goal_cate_id, cartItem.getGoal_cate_id());

        db.insert(Table_Cart, null, values);
        db.close();
    }


    public int updateFund(CartModle cartItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(scheme_name, cartItem.getScheme_name());
        values.put(amount, cartItem.getAmount());
        values.put(minAmount, cartItem.getMinAmount());
        values.put(orderType, cartItem.getOrderType());
        values.put(type, cartItem.getType());
        values.put(scheme_code, cartItem.getScheme_code());
        values.put(added_date, cartItem.getAdded_date());
        values.put(target_year, cartItem.getTarget_year());
        values.put(nav, cartItem.getNav());
        values.put(qty, cartItem.getQty());
        values.put(numberofmonths, cartItem.getNumberofmonths());


        int res = -1;
        res = db.update(Table_Cart, values, scheme_code + " = ?",
                new String[]{String.valueOf(cartItem.getScheme_code())});
        db.close();
        if (res != -1)
            return res;
        else
            return -1;
    }


    public ArrayList<CartModle> getAllFunds() {
        ArrayList<CartModle> list = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + Table_Cart, null);

        Log.e("columnCount", cursor.getColumnCount() + "");
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            Log.e("coulumn" + i + " ", cursor.getColumnName(i));
        }
        if (cursor.moveToFirst()) {
            do {
                CartModle cart = new CartModle(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getInt(15),
                        cursor.getString(16));
                list.add(cart);
            } while (cursor.moveToNext());
        }
        db.close();
        return list;
    }

    public void deleteFund(String schemeCode) {
        SQLiteDatabase db = this.getWritableDatabase();
        int count = db.delete(Table_Cart, scheme_code + " = ?",
                new String[]{String.valueOf(schemeCode)});
        Log.e("Deleted from Cart", count + "");
        db.close();
    }

    public CartModle getItemFromCart(String FschemeCode) {
        if (FschemeCode != null || !FschemeCode.equals("")) {
            CartModle cart = new CartModle();
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.query(Table_Cart, new String[]{scheme_name, amount, orderType, type}, scheme_code + "=?",
                    new String[]{String.valueOf(FschemeCode)}, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                cart.setScheme_name(cursor.getString(0));
                cart.setAmount(cursor.getString(1));
                cart.setOrderType(cursor.getString(2));
                cart.setType(cursor.getString(3));
            }
            if (cursor.getCount() > 0)
                return cart;
            else
                return null;
        } else
            return null;
    }
}
