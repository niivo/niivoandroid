package com.niivo.com.niivo.Fragment.InvestmentPlan;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Fragment.InvestHelper.RedirectringFragment;
import com.niivo.com.niivo.Fragment.TransactionHistory.ViewAllTransactionFragment;
import com.niivo.com.niivo.Fragment.Withdraw.WithdrawFragmentNew;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class InvestedSchemeDetailFragmentNew extends Fragment implements ResponseDelegate {
    Context contextd;
    TextView ammountInvested, detailInvestmentDate, nav, detailNumberOfUnit, detailFolio, investamt,schemeName;
    TextView withdrawbtn,viewAll,cancelsip;
    MainActivity activity;

    String INWARD_TXN_NO,order_type,BSE_SCHEME_CODE,foliono,order_status;
    public static final String TAG="InvestedSchemeDetailFragmentNew";
    AlertDialog.Builder builder;


    RequestedServiceDataModel requestedServiceDataModel;
    Bundle bundle=new Bundle();

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.investment_details, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ImageButton icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setVisibility(View.VISIBLE);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.myInvestment));
        activity = (MainActivity) getActivity();
        ammountInvested = (TextView) rootView.findViewById(R.id.detailAmountInvested);
        detailFolio = (TextView) rootView.findViewById(R.id.detailFolio);
        detailInvestmentDate = (TextView) rootView.findViewById(R.id.detailInvestmentDate);
        nav = (TextView) rootView.findViewById(R.id.tv_nav);
        detailNumberOfUnit = (TextView) rootView.findViewById(R.id.detailNumberOfUnit);
        investamt = (TextView) rootView.findViewById(R.id.investamt);
        schemeName = (TextView) rootView.findViewById(R.id.detailSchemeName);
        withdrawbtn = (TextView) rootView.findViewById(R.id.withdrawbtn);
        viewAll =(TextView)rootView.findViewById(R.id.viewAll);
        cancelsip=(TextView)rootView.findViewById(R.id.cancelSip);

        order_type = getArguments().getString("Order_type");
        INWARD_TXN_NO = getArguments().getString("INWARD_TXN_NO");
        BSE_SCHEME_CODE=getArguments().getString("BSE_SCHEME_CODE");
        order_status=getArguments().getString("order_status");


//        if (order_status.equals("Cancel")){
//            Log.i(TAG,"enter in else if");
//            cancelsip.setVisibility(View.INVISIBLE);
//            //  withdrawbtn.setGravity(Gravity.CENTER);
//
//        } else
            if(order_type.equals("XSIP") && order_status.equals("Active") || (order_type.equals("SIP"))  && order_status.equals("Active")){

            cancelsip.setVisibility(View.VISIBLE);
        }else {

            cancelsip.setVisibility(View.INVISIBLE);
        }

        builder = new AlertDialog.Builder(contextd);
        cancelsip.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sipCacelationPop();

                    }
                });

        withdrawbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WithdrawFragmentNew mfragment = new WithdrawFragmentNew();
//                foliono = detailFolio.getText().toString();
//                Bundle bundle=new Bundle();
//                bundle.putString("foliono",foliono);
//                bundle.putString("BSE_SCHEME_CODE",BSE_SCHEME_CODE);
//                Log.d("value of Bundle", String.valueOf(foliono));
//                mfragment.setArguments(bundle); //data being send to SecondFragment
//                transection.replace(R.id.frame_container, mfragment);
//                transection.commit();
                foliono = detailFolio.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString("foliono",foliono);
                bundle.putString("BSE_SCHEME_CODE",BSE_SCHEME_CODE);
                getFolioWithdraw(foliono,BSE_SCHEME_CODE);
            }
        });
        viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transection=getFragmentManager().beginTransaction();
                ViewAllTransactionFragment mfragment = new ViewAllTransactionFragment();
                foliono = detailFolio.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString("foliono",foliono);
                bundle.putString("BSE_SCHEME_CODE",BSE_SCHEME_CODE);
                Log.d("value of Bundle", String.valueOf(foliono));
                mfragment.setArguments(bundle); //data being send to SecondFragment
                transection.replace(R.id.frame_container, mfragment);
                transection.commit();
            }
        });

        ammountInvested.setText(Common.currencyString(getArguments().getString("currentamount"),true));

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr=getArguments().getString("detailInvestmentDate");
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        detailInvestmentDate.setText(outputDateStr);
        investamt.setText(Common.currencyString(getArguments().getString("investment_amount"),true));
        detailNumberOfUnit.setText(getArguments().getString("detailNumberOfUnit"));
        String reepes=getResources().getString(R.string.Rs);
        nav.setText(reepes+" "+getArguments().getString("nav"));
        detailFolio.setText(getArguments().getString("detailFolio"));
        schemeName.setText(getArguments().getString("schemeName"));
        return rootView;

    }
    private void getFolioWithdraw(String folio_no,String BSE_SCHEME_CODE) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.WithdrawList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("WITHDRAW");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("folio_no", folio_no);
        requestedServiceDataModel.putQurry("BSE_SCHEME_CODE", BSE_SCHEME_CODE);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    void cancelSIP() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.CancelSIP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("order_type",order_type);
        requestedServiceDataModel.putQurry("INWARD_TXN_NO",INWARD_TXN_NO);
        requestedServiceDataModel.setWebServiceType("SIP-CANCEL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    AlertDialog b;
    public void sipCacelationPop() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.popupsipcancel, null);
        dialogBuilder.setView(dialogView);

        TextView invest_cancel = (TextView) dialogView.findViewById(R.id.invest_cancel);

        invest_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();

            }
        });
        TextView button_popup = (TextView) dialogView.findViewById(R.id.button_popup);
        button_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                cancelSIP();
            }
        });

        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }
  //  AlertDialog b;
    public void Withdrawpop(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.withdraw_popup, null);
        dialogBuilder.setView(dialogView);

        TextView withdrawpop = (TextView) dialogView.findViewById(R.id.withdrawpop);
        withdrawpop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        b.show();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {
        FragmentTransaction transection=getFragmentManager().beginTransaction();
        switch (baseRequestData.getTag()) {
            case ResponseType.CancelSIP:
       //         Log.e("CancelSIP res", json);
              //  FragmentTransaction transection=getFragmentManager().beginTransaction();
                Cancel_Sip mfragment = new Cancel_Sip();
                bundle.putString("order_type",order_type);
                bundle.putString("INWARD_TXN_NO",INWARD_TXN_NO);
                mfragment.setArguments(bundle);
                transection.replace(R.id.frame_container, mfragment);
                transection.commit();
                break;
            case ResponseType.WithdrawList:
                WithdrawFragmentNew withdraw = new WithdrawFragmentNew();
                foliono = detailFolio.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString("foliono",foliono);
                bundle.putString("BSE_SCHEME_CODE",BSE_SCHEME_CODE);
                Log.d("value of Bundle", String.valueOf(foliono));
                withdraw.setArguments(bundle); //data being send to SecondFragment
                transection.replace(R.id.frame_container, withdraw);
                transection.commit();
             //   getFolioWithdraw(foliono,BSE_SCHEME_CODE);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {

            case ResponseType.CancelSIP:
                FragmentTransaction transection=getFragmentManager().beginTransaction();
                CancelSIPError mfragment = new CancelSIPError();
                Bundle bundle=new Bundle();
                bundle.putString("order_type",order_type);
                bundle.putString("INWARD_TXN_NO",INWARD_TXN_NO);
                mfragment.setArguments(bundle);
                transection.replace(R.id.frame_container, mfragment);
                transection.commit();

                break;
            case ResponseType.WithdrawList:
                Withdrawpop ();
            //Common.showToast(contextd, message);
                break;
        }
    }
}



