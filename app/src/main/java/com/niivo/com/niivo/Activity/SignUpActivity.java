package com.niivo.com.niivo.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Outline;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.AppConstants;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.RightGravityTextInputLayout;
import com.niivo.com.niivo.Utils.Utility;
import com.niivo.com.niivo.web_view.WebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity implements ResponseDelegate {
    TextInputEditText signupPhone, password, confirmPass;
    ImageView imgLogo;
    RightGravityTextInputLayout phoneTIL, passwordTIL, confirmPassTIL;


    private RequestedServiceDataModel requestedServiceDataModel;
    String otp = "";
    public static String otpWhere = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(SignUpActivity.this, Common.getLanguage(SignUpActivity.this), false);
        setContentView(R.layout.activity_sign_up);
        imgLogo = (ImageView) findViewById(R.id.logo);
        Button signUp = (Button) findViewById(R.id.signup_button_signup);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate())
                 //   if (Utility.checkSMSPermission(SignUpActivity.this)) {
                        signup();
                 //   } else {
                   //     Common.showToast(SignUpActivity.this, SignUpActivity.this.getResources().getString(R.string.permisionNotGranted));
                //    }

            }
        });
        TextView terms = (TextView) findViewById(R.id.text_terms);
        terms.setText(Html.fromHtml("<u>"+SignUpActivity.this.getResources().getString(R.string.terms)+"</u>"));
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, WebViewActivity.class);
                intent.putExtra(AppConstants.WEB_PAGE_TYPE,2);
                intent.setData(Uri.parse(AppConstants.TERMS_CONDITIONS));
                startActivity(intent);
            }
        });

        TextView privascy = (TextView) findViewById(R.id.text_privacy);
        privascy.setText(Html.fromHtml("<u>"+SignUpActivity.this.getResources().getString(R.string.privacy)+"</u>"));
        privascy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, WebViewActivity.class);
                intent.putExtra(AppConstants.WEB_PAGE_TYPE,1);
                intent.setData(Uri.parse(AppConstants.PRIVACY_POLICY));
                startActivity(intent);
            }
        });
        TextView text_signin = (TextView) findViewById(R.id.text_signin);
        text_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });
        signupPhone = (TextInputEditText) findViewById(R.id.signup_phone_number);
        password = (TextInputEditText) findViewById(R.id.edit_password);
        confirmPass = (TextInputEditText) findViewById(R.id.edit_con_password);
        phoneTIL = (RightGravityTextInputLayout) findViewById(R.id.phoneTIL);
        passwordTIL = (RightGravityTextInputLayout) findViewById(R.id.passwordTIL);
        confirmPassTIL = (RightGravityTextInputLayout) findViewById(R.id.confirmPassTIL);
        setUpUiChanges();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.READ_SMS:
               // if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    signup();
            //    } else {
                    //code for deny
            //    }
                break;
        }
    }

    boolean validate() {
        boolean b = false;
        if (signupPhone.getText().toString().trim().equals("")) {
            phoneTIL.setError(SignUpActivity.this.getResources().getString(R.string.requiredField));
            b = false;
        } else if (signupPhone.getText().toString().trim().length() != 10) {
            phoneTIL.setError(SignUpActivity.this.getResources().getString(R.string.invalidPhoneNumber));
            b = false;
        } else if (password.getText().toString().trim().equals("")) {
            passwordTIL.setError(SignUpActivity.this.getResources().getString(R.string.requiredField));
            b = false;
        } else if (password.getText().toString().trim().length() < 6) {
            passwordTIL.setError(SignUpActivity.this.getResources().getString(R.string.useAtLeast6Digits));
            b = false;
        } else if (password.getText().toString().trim().length() > 16) {
            passwordTIL.setError(SignUpActivity.this.getResources().getString(R.string.notMoreThan16Digits));
            b = false;
        } else if (confirmPass.getText().toString().trim().equals("")) {
            confirmPassTIL.setError(SignUpActivity.this.getResources().getString(R.string.requiredField));
            b = false;
        } else if (confirmPass.getText().toString().trim().length() < 6) {
            confirmPassTIL.setError(SignUpActivity.this.getResources().getString(R.string.useAtLeast6Digits));
            b = false;
        } else if (confirmPass.getText().toString().trim().length() > 16) {
            confirmPassTIL.setError(SignUpActivity.this.getResources().getString(R.string.notMoreThan16Digits));
            b = false;
        } else if (!confirmPass.getText().toString().trim().equals(password.getText().toString().trim())) {
            confirmPassTIL.setError(SignUpActivity.this.getResources().getString(R.string.passwordMismatch));
            b = false;
        } else {
            b = true;
        }
        return b;
    }

    void signup() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.SIGNUP);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", signupPhone.getText().toString().trim());
        requestedServiceDataModel.putQurry("password", password.getText().toString().trim());
        requestedServiceDataModel.putQurry("default_lang", Common.sendLanguage(SignUpActivity.this));
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("SENDOTP");
        requestedServiceDataModel.setLocal(true);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.SIGNUP:
     //           Log.d("res", json);
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    Common.SetPreferences(this, "mobile", jsonObject.getJSONObject("data").getString("mobile"));
                    otp = jsonObject.getJSONObject("data").getString("otp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                otpWhere = "signup";
                Intent intent = new Intent(SignUpActivity.this, OTPActivity.class).putExtra("otp", otp);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.SIGNUP:
                Common.showToast(SignUpActivity.this,
                        SignUpActivity.this.getResources().getString(R.string.mobile_number_already_exist));
                break;
        }
    }

    void setUpUiChanges() {
        imgLogo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        signupPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                phoneTIL.setErrorEnabled(false);
                phoneTIL.setError(null);

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                passwordTIL.setErrorEnabled(false);
                passwordTIL.setError(null);
            }
        });
        confirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                confirmPassTIL.setErrorEnabled(false);
                confirmPassTIL.setError(null);
            }
        });
    }
}
