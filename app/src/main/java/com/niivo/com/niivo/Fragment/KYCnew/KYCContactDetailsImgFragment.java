package com.niivo.com.niivo.Fragment.KYCnew;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.niivo.com.niivo.Adapter.ProofTypeAdapter;
import com.niivo.com.niivo.BuildConfig;
import com.niivo.com.niivo.Model.ItemProofTypesList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.AWS_Helper.ProgressViewDialog;
import com.niivo.com.niivo.Utils.AWS_Helper.S3UploadService;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.PickerHelper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * Created by deepak on 24/9/18.
 */

public class KYCContactDetailsImgFragment extends Fragment implements View.OnClickListener, ResponseDelegate {
    TextView continueBtn;
    AppCompatSpinner addressProofSpinner;
    LinearLayout frontImgBtn, backImgBtn;
    ImageView proofImgFront, proofImgBack;
    //Non ui vars
    Context contextd;
    RequestedServiceDataModel requestedServiceDataModel;
    ItemProofTypesList proofTypesList;
    TextInputEditText postalidET;
    boolean isFrontUploaded = false, isBackUploaded = false, isBack = false;
    String addressProofFrontRef = "", addressProofBackRef = "", doc_type = "", doc_typeId = "";
    File imgFileFront = null, imgFileBack = null;
    PickerHelper helper;
    ProgressViewDialog progressViewDialog;
    int type = 0;
    private static final int PICK_IMAGE_CAMERA = 111;
    private static final int PICK_IMAGE_GALLERY = 222;
    Uri mHighQualityImageUri=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_kyc_contact_details_image, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.contact_details);
        contextd = container.getContext();
        continueBtn = (TextView) rootView.findViewById(R.id.continueBtn);
        addressProofSpinner = (AppCompatSpinner) rootView.findViewById(R.id.addressProofSpinner);
        frontImgBtn = (LinearLayout) rootView.findViewById(R.id.frontImgBtn);
        backImgBtn = (LinearLayout) rootView.findViewById(R.id.backImgBtn);
        postalidET = (TextInputEditText)rootView.findViewById(R.id.postalidET);
        proofImgFront = (ImageView) rootView.findViewById(R.id.proofFrontImg);
        proofImgBack = (ImageView) rootView.findViewById(R.id.proofBackImg);
        continueBtn.setOnClickListener(this);
        getProofTypeList();
        addressProofSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                postalidET.setHint("Please enter " +proofTypesList.getData().get(position).getFRONTEND_OPTION_NAME() + " ID");
                if (proofTypesList.getData().get(position).getHAS_BACK().equalsIgnoreCase("n")) {
                    doc_typeId = proofTypesList.getData().get(position).getNIIVO_CODE();
                    doc_type = proofTypesList.getData().get(position).getFRONTEND_OPTION_NAME();
                    backImgBtn.setVisibility(View.GONE);
                    isBack = false;
                } else {
                    isBack = true;
                    backImgBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        frontImgBtn.setOnClickListener(this);
        backImgBtn.setOnClickListener(this);
        doPermissionWork();
        return rootView;
    }

    void doPermissionWork() {
        progressViewDialog = ProgressViewDialog.with(contextd);
        progressViewDialog.setCancelable(false);
        helper = PickerHelper.with(contextd, KYCContactDetailsImgFragment.this);
        if ((ContextCompat.checkSelfPermission(contextd, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(contextd, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, 125);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBtn:
                if (imgFileFront == null) {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.please_add_front_image_of_address));
                } else if (isBack && imgFileBack == null) {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.please_add_back_image_of_address));
                }
                else if(postalidET.getText().toString().equalsIgnoreCase(""))
                {
                    Common.showToast(contextd, contextd.getResources().getString(R.string.please_enter_id));

                }else {
                    progressViewDialog.show();
                    LocalBroadcastManager.getInstance(contextd).registerReceiver(uploadReciever, new IntentFilter("onUploadStatus"));
                    Intent i = new Intent(contextd, S3UploadService.class);
                    i.putExtra("userId", getArguments().getString("userId"));
                    i.putExtra("fileType", "ADDRESS_PROOF");
                    i.putExtra("file", imgFileFront);
                    contextd.startService(i);

                }
                break;
            case R.id.frontImgBtn:
                type = 1;
                selectImage();
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.OFF)
//                        .setAllowFlipping(false)
//                        .setAutoZoomEnabled(true)
//                        .setBorderCornerColor(Color.parseColor("#42E598"))
//                        .setBorderLineColor(Color.parseColor("#3CB5B9"))
//                        .setMinCropResultSize(640, 360)
//                        .setAspectRatio(16, 9)
//                        .start(contextd, this);
                break;
            case R.id.backImgBtn:
                type = 2;
                selectImage();
//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.OFF)
//                        .setAllowFlipping(false)
//                        .setAutoZoomEnabled(true)
//                        .setBorderCornerColor(Color.parseColor("#42E598"))
//                        .setBorderLineColor(Color.parseColor("#3CB5B9"))
//                        .setMinCropResultSize(640, 360)
//                        .setAspectRatio(16, 9)
//                        .start(contextd, this);
                break;
        }
    }
    private Uri generateTimeStampPhotoFileUri() {
        Uri photoFileUri = null;
        File outputDir = getPhotoDirectory();
        if (outputDir != null) {
            File photoFile = new File(outputDir,  System.currentTimeMillis()+".jpg");
            //photoFileUri = Uri.fromFile(photoFile);
            photoFileUri =  FileProvider.getUriForFile(contextd, BuildConfig.APPLICATION_ID + ".provider",photoFile);
        }
        return photoFileUri;
    }

    private File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageStagte = Environment.getExternalStorageState();
        if (externalStorageStagte.equals(Environment.MEDIA_MOUNTED)) {
            File photoDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            outputDir = new File(photoDir, getString(R.string.app_name));
            if (!outputDir.exists())
                if (!outputDir.mkdirs()) {
                    Toast.makeText(contextd, "Failed to create directory " + outputDir.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    outputDir = null;
                }
        }
        return outputDir;
    }

    private void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(contextd);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        mHighQualityImageUri = generateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                       // intent.setClassName("com.android.camera", "com.android.camera.Camera");
                        //intent.setClassName("com.android.camera", "com.android.camera.Camera");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mHighQualityImageUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICK_IMAGE_GALLERY);
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
        // CropImage.ActivityResult result = CropImage.getActivityResult(data);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK && mHighQualityImageUri != null) {
            Uri resultUri = mHighQualityImageUri;
            doc_typeId = proofTypesList.getData().get(addressProofSpinner.getSelectedItemPosition()).getNIIVO_CODE();
            doc_type = proofTypesList.getData().get(addressProofSpinner.getSelectedItemPosition()).getFRONTEND_OPTION_NAME();
            //  Uri resultUri = result.getUri();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                bitmap = helper.getResizedBitmap(bitmap, 1920);
                bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                if (type == 1) {
                    imgFileFront = helper.saveBitmapToFile(bitmap, doc_type.replace(" ", "_").toLowerCase() + "_front");
                } else
                    imgFileBack = helper.saveBitmapToFile(bitmap, doc_type.replace(" ", "_").toLowerCase() + "_back");
                Picasso.with(contextd)
                        .load(type == 1 ? imgFileFront : imgFileBack)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .into(type == 1 ? proofImgFront : proofImgBack);
                frontImgBtn.setVisibility(imgFileFront != null ? View.GONE : View.VISIBLE);
                backImgBtn.setVisibility((imgFileBack != null
                        || !isBack) ? View.GONE : View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && data!= null) {

              Uri resultUri = data.getData();
            doc_typeId = proofTypesList.getData().get(addressProofSpinner.getSelectedItemPosition()).getNIIVO_CODE();
            doc_type = proofTypesList.getData().get(addressProofSpinner.getSelectedItemPosition()).getFRONTEND_OPTION_NAME();
            //  Uri resultUri = result.getUri();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(contextd.getApplicationContext().getContentResolver(), resultUri);
                bitmap = helper.getResizedBitmap(bitmap, 1920);
                bitmap = helper.rotateImageIfRequired(contextd,bitmap,resultUri);
                if (type == 1) {
                    imgFileFront = helper.saveBitmapToFile(bitmap, doc_type.replace(" ", "_").toLowerCase() + "_front");
                } else
                    imgFileBack = helper.saveBitmapToFile(bitmap, doc_type.replace(" ", "_").toLowerCase() + "_back");
                Picasso.with(contextd)
                        .load(type == 1 ? imgFileFront : imgFileBack)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .into(type == 1 ? proofImgFront : proofImgBack);
                frontImgBtn.setVisibility(imgFileFront != null ? View.GONE : View.VISIBLE);
                backImgBtn.setVisibility((imgFileBack != null
                        || !isBack) ? View.GONE : View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }
    void getProofTypeList() {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-mapping.php");
        baseRequestData.setTag(ResponseType.GetProofType);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("POACODE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }


    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetProofType:
         //       Log.e("getProofs", json);
                proofTypesList = new Gson().fromJson(json, ItemProofTypesList.class);
                addressProofSpinner.setAdapter(new ProofTypeAdapter(contextd, proofTypesList));
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.GetProofType:
          //      Log.e("getProofs", json);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(contextd).unregisterReceiver(uploadReciever);
        Intent i = new Intent();
        i.putExtra("addressProofFrontRef", addressProofFrontRef);
        i.putExtra("addressProofBackRef", addressProofBackRef);
        i.putExtra("doc_type", doc_type);
        i.putExtra("doc_typeId", doc_typeId);
        i.putExtra("doc_proof_id", postalidET.getText().toString());
        Fragment fragment = getTargetFragment();
        fragment.onActivityResult(121, isFrontUploaded ?
                RESULT_OK : Activity.RESULT_CANCELED, i);
    }

    BroadcastReceiver uploadReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressViewDialog.dismiss();
            if (intent.getStringExtra("state").equals("COMPLETED")) {
                if (!isFrontUploaded) {
                    isFrontUploaded = true;
                    addressProofFrontRef = intent.getStringExtra("panRef");
                } else {
                    addressProofBackRef = intent.getStringExtra("panRef");
                    isBackUploaded = true;
                }
                if (isBack && !isBackUploaded) {
                    isBackUploaded = false;
                    progressViewDialog.show();
                    Intent i = new Intent(contextd, S3UploadService.class);
                    i.putExtra("userId", getArguments().getString("userId"));
                    i.putExtra("fileType", "ADDRESS_PROOF");
                    i.putExtra("file", imgFileBack);
                    contextd.startService(i);
                } else {
                    progressViewDialog.dismiss();
                    getActivity().onBackPressed();
                }
            } else {
                Common.showToast(contextd, contextd.getResources().getString(R.string.upload_failed_try_again));
            }
        }
    };
}
