package com.niivo.com.niivo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by deepak on 11/7/18.
 */

public class TestActivity extends AppCompatActivity implements ResponseDelegate {

    private RequestedServiceDataModel requestedServiceDataModel;
    private String amount;
    private String schemeName;
    private String orderIdValueForProcessing;
    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        try {
            url = "http://172.104.187.141/production/public/api/services/thank-you.php?order_id=2351543993276";
            String[] urlOrderId = url.trim().split("\\?");
            if (urlOrderId.length > 1) {
                String[] orderIdValue = urlOrderId[1].split("&");
                //Log.i("onClick: ", orderIdValue[0].split("=")[1]);
                orderIdValueForProcessing = orderIdValue[0].split("=")[1];
                Log.i("onClickUrlId: ", orderIdValueForProcessing + "");
                getOrderDetailsValues();
            }
        }
        catch (Exception e)
        {

        }
    }

    private void getOrderDetailsValues() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderDetail);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
      //  requestedServiceDataModel.putQurry("userid", Common.getPreferences(getApplicationContext(), "userID"));
        requestedServiceDataModel.putQurry("order_id", orderIdValueForProcessing);
        requestedServiceDataModel.setWebServiceType("GETORDERDETAIL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderDetail:
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    amount = jsonObject.getJSONObject("data").getString("amount");
                    schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
                    String str = "Thank you for investing "+"Rs."+amount+" in "+schemeName;
              //      Log.i("Url Response", "onSuccess: "+str);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

    }

}
