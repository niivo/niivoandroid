package com.niivo.com.niivo.Fragment.Withdraw;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utilities.UiUtils;
import com.niivo.com.niivo.Utils.Common;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Locale;

public class WithdrawFragmentNew extends Fragment implements ResponseDelegate {
    Activity _activity;
    View parentView;
    ImageButton icon_navi;
    TextView fundGoalName, investedAmnt,validationtext, currentAmnt, withdrawableAmnt, errorTxt, remainingBal, wdWithdrawUnit, wdWithdAmt,next_withdraw,clickme;
    EditText withdrawAmount, textWithdrawunit;
    RadioButton withdrawFull, withdrawPartial, RBwithdrawAmount, RBwithdrawUnit;
    RadioGroup withdraw;
    LinearLayout withdrawlayout,unitlayout,amountlayout;
    //Non ui Vars
    Context contextd;
    MainActivity activity;
    String schemeCode = "", id = "", lang = "",order_id,fullamount,fullunits,INWARD_TXN_NO;;
    float val;
    RequestedServiceDataModel requestedServiceDataModel;
    public static final String TAG = "WithdrawFragmentNew";

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _activity = getActivity();
        contextd = container.getContext();
        parentView = inflater.inflate(R.layout.withdraw_fragment, null, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        icon_navi = (ImageButton) toolbar.findViewById(R.id.icon_navi);
        icon_navi.setVisibility(View.VISIBLE);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        textView.setText(contextd.getResources().getString(R.string.withDraw));
        lang = Common.getLanguage(contextd);
        activity = (MainActivity) getActivity();
        next_withdraw = (TextView) parentView.findViewById(R.id.next_withdraw);
        next_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiUtils.hideKeyboard(contextd);
                if (validate()){
//                    Intent intent = new Intent(getActivity(), WithDrawOTPActivity.class);
//                    intent.putExtra("amount", withdrawFull.isChecked() ? (totalamt + "")
//                            : RBwithdrawAmount.isChecked()
//                            ? withdrawAmount.getText().toString().trim() +"="+"amount"
//                            : textWithdrawunit.getText().toString().trim()+"="+"units");
//
//                    intent.putExtra("order_id", order_id);
//                    intent.putExtra("schemeCode", schemeCode);
//                    intent.putExtra("fullamount",fullamount);
//                    intent.putExtra("fullunits",fullunits);
//                    intent.putExtra("id", id);
//                    intent.putExtra("INWARD_TXN_NO",INWARD_TXN_NO);
//                    intent.putExtra("full_withdraw", withdrawFull.isChecked() ? "Y" : "N");
//                    startActivity(intent);
//                    getActivity().finish();
                   //startActivityForResult(intent, 101);

                    FragmentTransaction transection=getFragmentManager().beginTransaction();
                    WithdrawOTP mfragment = new WithdrawOTP();
                    Bundle bundle=new Bundle();
                    bundle.putString("amount", withdrawFull.isChecked() ? (totalamt + "")
                            : RBwithdrawAmount.isChecked()
                            ? withdrawAmount.getText().toString().trim() +"="+"amount"
                            : textWithdrawunit.getText().toString().trim()+"="+"units");
                    bundle.putString("order_id", order_id);
                    bundle.putString("schemeCode", schemeCode);
                    bundle.putString("fullamount",fullamount);
                    bundle.putString("fullunits",fullunits);
                    bundle.putString("id", id);
                    bundle.putString("INWARD_TXN_NO",INWARD_TXN_NO);
                    bundle.putString("full_withdraw", withdrawFull.isChecked() ? "Y" : "N");
                    mfragment.setArguments(bundle);
                    transection.replace(R.id.frame_container, mfragment);
                    transection.commit();
                }
            }
        });

        validationtext =(TextView) parentView.findViewById(R.id.validationtext);
        fundGoalName = (TextView) parentView.findViewById(R.id.wdFundName);
        investedAmnt = (TextView) parentView.findViewById(R.id.wdFundAmntInvested);
        currentAmnt = (TextView) parentView.findViewById(R.id.wdFundAmntCurrent);
//        withdrawableAmnt = (TextView) parentView.findViewById(R.id.wdWithdrawableAmnt);
 //       errorTxt = (TextView) parentView.findViewById(R.id.wdErrorTxt);
        remainingBal = (TextView) parentView.findViewById(R.id.wdRemainingBal);

        withdrawAmount = (EditText) parentView.findViewById(R.id.wdWithdAmount);
        textWithdrawunit = (EditText) parentView.findViewById(R.id.wdWithdrawunit);
        textWithdrawunit.setEnabled(false);
        withdrawAmount.setEnabled(true);

        withdrawFull = (RadioButton) parentView.findViewById(R.id.withdrawFull);
        withdrawPartial = (RadioButton) parentView.findViewById(R.id.withdrawPartial);

        withdraw = (RadioGroup)parentView.findViewById(R.id.withdraw);
        RBwithdrawAmount = (RadioButton) parentView.findViewById(R.id.RBwithdrawAmount);
        RBwithdrawUnit = (RadioButton) parentView.findViewById(R.id.RBwithdrawAmount);

        wdWithdAmt = (TextView) parentView.findViewById(R.id.wdWithdAmt);
        wdWithdrawUnit = (TextView) parentView.findViewById(R.id.wdWithdrawUnit);

        amountlayout= (LinearLayout) parentView.findViewById(R.id.amountlayout);
        unitlayout = (LinearLayout) parentView.findViewById(R.id.unitlayout);
        withdrawlayout = (LinearLayout) parentView.findViewById(R.id.withdrawlayout);
        withdrawlayout.setVisibility(View.GONE);

        withdrawFull.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    withdrawlayout.setVisibility(View.GONE);
                    Common.hideKeyboard(_activity);
                    textWithdrawunit.getText().clear();
                    withdrawAmount.getText().clear();
                    //textWithdrawunit.setError(null);
                    //withdrawAmount.setError(null);

                   } else {
                    withdrawlayout.setVisibility(View.VISIBLE);
                    textWithdrawunit.getText().clear();
                    withdrawAmount.getText().clear();
                    //textWithdrawunit.setError(null);
                    //withdrawAmount.setError(null);
                }
            }
        });
        withdraw.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.RBwithdrawAmount:
                        textWithdrawunit.setEnabled(false);
                        withdrawAmount.setEnabled(true);
                        textWithdrawunit.getText().clear();
                        textWithdrawunit.setError(null);
                        validationtext.setText("");
                        break;
                    case R.id.RBwithdrawUnit:
                        withdrawAmount.setEnabled(false);
                        textWithdrawunit.setEnabled(true);
                        withdrawAmount.getText().clear();
                        withdrawAmount.setError(null);
                        validationtext.setText("");
                        break;
                }
            }
        });

        withdrawAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validationtext.setText("");
            }

            @SuppressLint("StringFormatInvalid")
            @Override
            public void afterTextChanged(Editable s) {
                if (!withdrawAmount.getText().toString().trim().equals("")) {
                    if (Float.parseFloat(withdrawAmount.getText().toString().trim()) < bseminamount) {
                        //withdrawAmount.setError(contextd.getResources().getString(R.string.minimumamount, bseminamount));
                        validationtext.setText(contextd.getResources().getString(R.string.minimumamount, bseminamount));
                    } else if (Float.parseFloat(withdrawAmount.getText().toString().trim()) > totalamt) {
                      //  withdrawAmount.setError(contextd.getResources().getString(R.string.totalamount, totalamt));
                        validationtext.setText(contextd.getResources().getString(R.string.totalamount, totalamt));
//                        errorTxt.setText("");
                    }
                    if (Float.parseFloat(withdrawAmount.getText().toString().trim()) > totalamt){
                        remainingBal.setText("0");
                    }else {
                        remainingBal.setText(String.format(new Locale("en"), "%.2f", (totalCrnt - Float.parseFloat(withdrawAmount.getText().toString().trim()))) + "");
                    }
                } else if (withdrawAmount.getText().toString().startsWith("0")) {
                } else{
                    validationtext.setText("");
                    remainingBal.setText(tCr + "");
                }
            }
        });
        textWithdrawunit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validationtext.setText("");
            }
            @SuppressLint({"StringFormatMatches", "StringFormatInvalid"})
            @Override
            public void afterTextChanged(Editable s) {
               // withdrawAmount.getText().clear();
                if (!textWithdrawunit.getText().toString().trim().equals("")) {
                    val = Float.parseFloat(textWithdrawunit.getText().toString().trim());
                    float minunit = bseminamount/nav;
                    if (val < minunit ) {
                        //textWithdrawunit.setError(contextd.getResources().getString(R.string.minimumunits, minunit));
                        validationtext.setText(contextd.getResources().getString(R.string.minimumunits, minunit));

//                        errorTxt.setText("");
                    } else  if(Float.parseFloat(textWithdrawunit.getText().toString().trim()) > holdingunits){
                        //textWithdrawunit.setError(contextd.getResources().getString(R.string.totalunits, holdingunits));
                        validationtext.setText(contextd.getResources().getString(R.string.totalunits, holdingunits));
//                        errorTxt.setText("");
                    }
                    if (Float.parseFloat(textWithdrawunit.getText().toString().trim()) > holdingunits) {
                        remainingBal.setText("0");
                    }else {
                        remainingBal.setText(String.format(new Locale("en"), "%.2f", (holdingunits - Float.parseFloat(textWithdrawunit.getText().toString().trim()))) + "");

                    }
                    } else if (textWithdrawunit.getText().toString().startsWith("0"))
                    {
                        validationtext.setText("");

                } else {
                    validationtext.setText("");
                    remainingBal.setText(tCr + "");

                }
            }
        });

        Bundle mBundle = new Bundle();
        mBundle = getArguments();
        String val = mBundle.getString("foliono");
        String BSE_SCHEME_CODE= mBundle.getString("BSE_SCHEME_CODE");
        String folio_no = String.valueOf(val);
        getFolioWithdraw(folio_no,BSE_SCHEME_CODE);
        return parentView;

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 101) {
//            if (resultCode == 101) {
//                if (data.getStringExtra("done").equals("done")) {
//                    activity.getBackToPortfolio();
//                }
//            }
//        }
//    }


    @SuppressLint("StringFormatInvalid")
    boolean validate() {
        boolean b = false;

        if (withdrawFull.isChecked()) {
            b = true;
        } else {
            if (RBwithdrawAmount.isChecked()) {
                b = true;
                if (withdrawAmount.getText().toString().trim().equals("")) {
                    //withdrawAmount.setError(contextd.getResources().getString(R.string.requiredField));
                    validationtext.setText(contextd.getResources().getString(R.string.requiredField));
                    b = false;
                } else if (withdrawAmount.getText().toString().trim().length() <= 1) {
                    //withdrawAmount.setError(contextd.getResources().getString(R.string.invalidAmount));
                    validationtext.setText(contextd.getResources().getString(R.string.invalidAmount));
//                    errorTxt.setText(contextd.getResources().getString(R.string.youHaveToChoose));
                    b = false;
//            } else if (Float.parseFloat(withdrawAmount.getText().toString()) > totalCrnt) {
//                b = false;
//                withdrawAmount.setError(contextd.getResources().getString(R.string.invalidAmount));
//                errorTxt.setText(contextd.getResources().getString(R.string.ammountCannotExceed));
                    } else if (Float.parseFloat(withdrawAmount.getText().toString().trim()) < bseminamount) {
                       // withdrawAmount.setError(contextd.getResources().getString(R.string.minimumamount, bseminamount));
                     validationtext.setText(contextd.getResources().getString(R.string.minimumamount, bseminamount));
                       b = false;
                    } else if (Float.parseFloat(withdrawAmount.getText().toString().trim()) > totalamt) {
                    //withdrawAmount.setError(contextd.getResources().getString(R.string.totalamount, totalamt));
                    validationtext.setText(contextd.getResources().getString(R.string.totalamount, totalamt));
                    b = false;
                } else {
                        b = true;
                        validationtext.setText("");
                    }
                } else {
                    float minnunit = bseminamount / nav;
                    if (textWithdrawunit.getText().toString().trim().equals("")) {
                        try {
                            val = Float.parseFloat(textWithdrawunit.getText().toString().trim());
                        } catch(NumberFormatException ex) {
                            val = (float) 0.0; // default ??
                        }
                       // textWithdrawunit.setError(contextd.getResources().getString(R.string.requiredField));
                        validationtext.setText(contextd.getResources().getString(R.string.requiredField));
                        b = false;
                    }else if (val < minnunit){
                        //textWithdrawunit.setError(contextd.getResources().getString(R.string.minimumunits, minnunit));
                        validationtext.setText(contextd.getResources().getString(R.string.minimumunits, minnunit));
                        errorTxt.setText("");
                        b = false;
                    }else if(Float.parseFloat(textWithdrawunit.getText().toString().trim()) > holdingunits) {
                       // textWithdrawunit.setError(contextd.getResources().getString(R.string.totalunits, holdingunits));
                        validationtext.setText(contextd.getResources().getString(R.string.totalunits, holdingunits));
//                        errorTxt.setText("");
                        b = false;
                    }
//                 textWithdrawunit.setError(contextd.getResources().getString(R.string.requiredField));
//                b = false;
                    else{
                        b = true;
                        validationtext.setText("");
                    }
                }
            }
            return b;
    }

    private void getFolioWithdraw(String folio_no,String BSE_SCHEME_CODE) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.WithdrawList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("WITHDRAW");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("folio_no", folio_no);
        requestedServiceDataModel.putQurry("BSE_SCHEME_CODE", BSE_SCHEME_CODE);

        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.executeWithoutProgressbar();
    }

    float maxamount=0,minunits=0,maxunits=0,totalCrnt=0,totalamt=0,nav=0,holdingunits=0,bseminamount=0;
    String tCr = "";

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) throws JSONException {

    //    Log.i(TAG,"enter in OnSuccess");
    //    Log.e("Resholdings2", json);
        switch (baseRequestData.getTag()) {
            case ResponseType.WithdrawList://MyMoney:
         //       Log.e("res", json);
                try {
                    JSONObject odt = new JSONObject(json);
//                    fundGoalName.setText(odt.getJSONObject("data").getString("is_goal").equals("0")
//                            ? (lang.equals("en")
//                            ? odt.getJSONObject("data").getString("FUND_NAME")
//                            : lang.equals("hi")
//                            ? odt.getJSONObject("data").getString("FUNDS_HINDI")
//                            : lang.equals("gu")
//                            ? odt.getJSONObject("data").getString("FUNDS_GUJARATI")
//                            : lang.equals("mr")
//                            ? odt.getJSONObject("data").getString("FUNDS_MARATHI")
//                            : odt.getJSONObject("data").getString("FUND_NAME"))
//                            : odt.getJSONObject("goal_detail").getString("goal_name"));

                    fundGoalName.setText(odt.getJSONObject("data").getString("FUND_NAME"));
                    wdWithdAmt.setText(Common.currencyString(odt.getJSONObject("data").getString("HOLDING_AMOUNT"), true));
                    investedAmnt.setText(Common.currencyString(odt.getJSONObject("data").getString("investedamount"), true));

                    wdWithdrawUnit.setText(odt.getJSONObject("data").getString("HOLDING_UNITS"));
//                    String b= String.format(String.valueOf(a));
//                    wdWithdrawUnit.setText
                    //wdWithdrawUnit.setText(Common.currencyString(odt.getJSONObject("data").getString("HOLDING_UNITS"), false));
                  //  wdWithdrawUnit.setText(odt.getJSONObject("data").getString("HOLDING_UNITS"));
                    currentAmnt.setText(Common.currencyString(odt.getJSONObject("data").getString("HOLDING_AMOUNT"), false));

                    schemeCode = String.valueOf((odt.getJSONObject("data").getString("scheme_code")));
                    id = String.valueOf(((odt.getJSONObject("data").getString("id"))));
                    order_id=String.valueOf(((odt.getJSONObject("data").getString("order_id"))));
                    fullamount=String.valueOf(((odt.getJSONObject("data").getString("HOLDING_AMOUNT"))));
                    fullunits=String.valueOf(((odt.getJSONObject("data").getString("HOLDING_UNITS"))));

                    nav = (Float.parseFloat(odt.getJSONObject("data").getString("NAV")));
                    holdingunits = (Float.parseFloat(odt.getJSONObject("data").getString("HOLDING_UNITS")));
                    totalamt = (Float.parseFloat(odt.getJSONObject("data").getString("HOLDING_AMOUNT")));

                    bseminamount = (Float.parseFloat(odt.getJSONObject("data").getString("BSE_REDEMPTION_AMT_MIN")));
                    maxamount= (Float.parseFloat(odt.getJSONObject("data").getString("BSE_REDEMTION_AMT_MAX")));
                    maxunits = (Float.parseFloat(odt.getJSONObject("data").getString("BSE_MAX_REDEMPTION_QTY")));
//                    minunits = (Float.parseFloat(odt.getJSONObject("data").getString("BSE_MIN_REDEMPTION_QTY")));

                    INWARD_TXN_NO=String.valueOf((odt.getJSONObject("data").getString("INWARD_TXN_NO")));

                    totalCrnt = nav * holdingunits;
      //              Log.e("Response totalCrnt", String.valueOf(totalCrnt));
       //             Log.e("res_minamount", String.valueOf(bseminamount));
      //              Log.e("res_maxamount", String.valueOf(maxamount));
       //             Log.e("res_maxunits", String.valueOf(maxunits));
      //              Log.e("res_minunits", String.valueOf(minunits));
//                   errorTxt.setText("");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
            switch (baseRequestData.getTag()) {
                case ResponseType.WithdrawList:
             //       Log.e("res", json);
                    Common.showToast(contextd, message);
                    break;
        }
    }
}
