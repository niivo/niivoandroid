package com.niivo.com.niivo.Model;

/**
 * Created by deepak on 9/6/17.
 */

public class ItemFundsInGoal {
    String title;
    int logo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public ItemFundsInGoal(String title, int logo) {
        this.title = title;
        this.logo = logo;
    }
}
