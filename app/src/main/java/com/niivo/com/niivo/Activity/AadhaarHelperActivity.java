package com.niivo.com.niivo.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 9/10/17.
 */

public class AadhaarHelperActivity extends AppCompatActivity {
    WebView helperWebView;
    ImageView back;

    //Non ui Vars
    String rawWebData = "";
    String aaDharDetails = "";
    ProgressDialog pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(AadhaarHelperActivity.this, Common.getLanguage(AadhaarHelperActivity.this), false);
        setContentView(R.layout.activity_aadhar_helper_act);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        helperWebView = (WebView) findViewById(R.id.helperWebView);
        rawWebData = getIntent().getStringExtra("rawHtml");
        pd = new ProgressDialog(this);
        pd.setMessage(this.getResources().getString(R.string.loadingPleaseWait));
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();
        loadWebData(rawWebData.replace('\n', ' ').trim());
    }

    void loadWebData(String rawData) {
        helperWebView.getSettings().setJavaScriptEnabled(true);
        helperWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        helperWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        helperWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        helperWebView.getSettings().setSupportMultipleWindows(true);
        helperWebView.setWebChromeClient(new CustomChromeClent());
        helperWebView.setWebViewClient(new CustomWebClient());
        helperWebView.loadData(rawData, "text/html", "UTF-8");
    }

    class CustomWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e("pageStart", url.trim());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("UrlLoading", url);
            view.loadUrl(url);
            if (!pd.isShowing())
                pd.show();
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
            Log.e("pageFinished", url.trim());
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            pd.dismiss();
            Log.e("receivedError", error.getDescription().toString().trim());
        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
            pd.dismiss();
            Log.e("HttpAuthRequest", host.trim());
        }
    }

    class CustomChromeClent extends WebChromeClient {


        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {


            WebView newWebView = new WebView(AadhaarHelperActivity.this);
            newWebView.getSettings().setJavaScriptEnabled(true);
            newWebView.getSettings().setSupportZoom(true);
            newWebView.getSettings().setBuiltInZoomControls(true);
            newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            newWebView.getSettings().setSupportMultipleWindows(true);
            view.addView(newWebView);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();
            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    try {
                        Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!pd.isShowing())
                        pd.show();
                    Log.e("NewUrl", url);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.e("NewUrl", url);
                    try {
                        Log.e("originalUrl", view.getOriginalUrl().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    view.loadUrl(url);
                    if (!pd.isShowing())
                        pd.show();
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    Log.e("Chrome->WebView->FinishedUrl", url);
                    pd.dismiss();
                    view.evaluateJavascript(" document.getElementById('strResponse').value; ",
                            new ValueCallback<String>() {
                                @Override
                                public void onReceiveValue(String html) {
                                    aaDharDetails = html;
                                    if (aaDharDetails.toString().trim().length() > 5) {
                                        Intent i = new Intent();
                                        i.putExtra("rawBase", aaDharDetails);
                                        setResult(113, i);
                                        AadhaarHelperActivity.this.finish();
                                    }
                                }
                            });
                }
            });
            return true;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && helperWebView.canGoBack()) {
            helperWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
