package com.niivo.com.niivo.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deepak on 29/9/17.
 */

public class OrderedList implements Serializable{
    String status, msg, total_current_value, total_amount;
    ArrayList<OrderedItem> data;

    public String getTotal_current_value() {
        return total_current_value;
    }

    public void setTotal_current_value(String total_current_value) {
        this.total_current_value = total_current_value;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<OrderedItem> getData() {
        return data;
    }

    public void setData(ArrayList<OrderedItem> data) {
        this.data = data;
    }

    public static class OrderedItem implements Serializable {
        public OrderedItem() {
        }

        String id, order_id, order_type, dtdate, userid, scheme_code, amount, bse_order_no,
                order_status, failed_reson, payment_status, remarks, FUND_NAME, is_goal,
                current_value, fund_invested, nav, quantity, is_allotment, withdrawQty, withdrawAmt,
                requested, full_withdraw , is_childorder,folio_no,
                FUNDS_GUJARATI, FUNDS_HINDI, FUNDS_MARATHI,sip_registration_date;
        boolean isGroupEntries;

        public boolean isGroupEntries() {
            return isGroupEntries;
        }

        public void setGroupEntries(boolean groupEntries) {
            isGroupEntries = groupEntries;
        }

        ArrayList<OrderedItem> orderedItemArrayListSub = new ArrayList<>();

        public ArrayList<OrderedItem> getOrderedItemArrayListSub() {
            return orderedItemArrayListSub;
        }

        public void setOrderedItemArrayListSub(ArrayList<OrderedItem> orderedItemArrayList) {
            this.orderedItemArrayListSub = orderedItemArrayList;
        }

        public String getSip_registration_date() {
            return sip_registration_date;
        }

        public void setSip_registration_date(String sip_registration_date) {
            this.sip_registration_date = sip_registration_date;
        }

        public String getFUNDS_GUJARATI() {
            return FUNDS_GUJARATI;
        }

        public void setFUNDS_GUJARATI(String FUNDS_GUJARATI) {
            this.FUNDS_GUJARATI = FUNDS_GUJARATI;
        }

        public String getFUNDS_HINDI() {
            return FUNDS_HINDI;
        }

        public void setFUNDS_HINDI(String FUNDS_HINDI) {
            this.FUNDS_HINDI = FUNDS_HINDI;
        }

        public String getFUNDS_MARATHI() {
            return FUNDS_MARATHI;
        }

        public void setFUNDS_MARATHI(String FUNDS_MARATHI) {
            this.FUNDS_MARATHI = FUNDS_MARATHI;
        }

        GoalDetial goal_detail;
        ArrayList<GoalScheme> order_detail;

        public String getFull_withdraw() {
            return full_withdraw;
        }

        public void setFull_withdraw(String full_withdraw) {
            this.full_withdraw = full_withdraw;
        }

        public String getWithdrawQty() {
            return withdrawQty;
        }

        public void setWithdrawQty(String withdrawQty) {
            this.withdrawQty = withdrawQty;
        }

        public String getWithdrawAmt() {
            return withdrawAmt;
        }

        public void setWithdrawAmt(String withdrawAmt) {
            this.withdrawAmt = withdrawAmt;
        }

        public String getRequested() {
            return requested;
        }

        public void setRequested(String requested) {
            this.requested = requested;
        }

        public String getIs_allotment() {
            return is_allotment;
        }

        public void setIs_allotment(String is_allotment) {
            this.is_allotment = is_allotment;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getNav() {
            return nav;
        }

        public void setNav(String nav) {
            this.nav = nav;
        }

        public String getCurrent_value() {
            return current_value;
        }

        public void setCurrent_value(String current_value) {
            this.current_value = current_value;
        }

        public String getFund_invested() {
            return fund_invested;
        }

        public void setFund_invested(String fund_invested) {
            this.fund_invested = fund_invested;
        }

        public ArrayList<GoalScheme> getOrder_detail() {
            return order_detail;
        }

        public void setOrder_detail(ArrayList<GoalScheme> order_detail) {
            this.order_detail = order_detail;
        }

        public String getIs_goal() {
            return is_goal;
        }

        public void setIs_goal(String is_goal) {
            this.is_goal = is_goal;
        }

        public GoalDetial getGoal_detail() {
            return goal_detail;
        }

        public void setGoal_detail(GoalDetial goal_detail) {
            this.goal_detail = goal_detail;
        }

        public String getFUND_NAME() {
            return FUND_NAME;
        }

        public void setFUND_NAME(String FUND_NAME) {
            this.FUND_NAME = FUND_NAME;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBse_order_no() {
            return bse_order_no;
        }

        public void setBse_order_no(String bse_order_no) {
            this.bse_order_no = bse_order_no;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getFailed_reson() {
            return failed_reson;
        }

        public void setFailed_reson(String failed_reson) {
            this.failed_reson = failed_reson;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getIs_childorder() {
            return is_childorder;
        }

        public void setIs_childorder(String is_childorder) {
            this.is_childorder = is_childorder;
        }

        public String getFolio_no() {
            return folio_no;
        }

        public void setFolio_no(String folio_no) {
            this.folio_no = folio_no;
        }
    }


    public class GoalDetial {
        String order_id, main_orderid, fund_category, goal_name, investment_type, goal_year, goal_ammount, dtdate;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getMain_orderid() {
            return main_orderid;
        }

        public void setMain_orderid(String main_orderid) {
            this.main_orderid = main_orderid;
        }

        public String getFund_category() {
            return fund_category;
        }

        public void setFund_category(String fund_category) {
            this.fund_category = fund_category;
        }

        public String getGoal_name() {
            return goal_name;
        }

        public void setGoal_name(String goal_name) {
            this.goal_name = goal_name;
        }

        public String getInvestment_type() {
            return investment_type;
        }

        public void setInvestment_type(String investment_type) {
            this.investment_type = investment_type;
        }

        public String getGoal_year() {
            return goal_year;
        }

        public void setGoal_year(String goal_year) {
            this.goal_year = goal_year;
        }

        public String getGoal_ammount() {
            return goal_ammount;
        }

        public void setGoal_ammount(String goal_ammount) {
            this.goal_ammount = goal_ammount;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }
    }

    public class GoalScheme {
        String id, order_id, order_type, dtdate, userid, scheme_code, amount, bse_order_no, order_status,
                failed_reson, payment_status, remarks, FUND_NAME, nav, quantity;

        public String getNav() {
            return nav;
        }

        public void setNav(String nav) {
            this.nav = nav;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getOrder_type() {
            return order_type;
        }

        public void setOrder_type(String order_type) {
            this.order_type = order_type;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBse_order_no() {
            return bse_order_no;
        }

        public void setBse_order_no(String bse_order_no) {
            this.bse_order_no = bse_order_no;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getFailed_reson() {
            return failed_reson;
        }

        public void setFailed_reson(String failed_reson) {
            this.failed_reson = failed_reson;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getFUND_NAME() {
            return FUND_NAME;
        }

        public void setFUND_NAME(String FUND_NAME) {
            this.FUND_NAME = FUND_NAME;
        }
    }
}
