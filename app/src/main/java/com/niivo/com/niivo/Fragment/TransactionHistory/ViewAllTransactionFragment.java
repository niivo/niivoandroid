package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.niivo.com.niivo.Activity.MainActivity;
import com.niivo.com.niivo.Adapter.TransactionAdapter;
import com.niivo.com.niivo.Model.ViewTransaction;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

public class ViewAllTransactionFragment extends Fragment implements ResponseDelegate {
    Context contextd;
    MainActivity activity;
    RequestedServiceDataModel requestedServiceDataModel;
    ListView transactionlist;
    ViewTransaction viewTransaction;
    TransactionAdapter transactionAdapter;
    TextView detailSchemeName,detailAmountInvested,  detailFolio, detailNumberOfUnit,tv_nav,tv_orderstatus,tv_orderid;
    String val,scheme_code,folio_no,scheme_codee;
    SwipeRefreshLayout swipeRefreshLayout;

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contextd = container.getContext();
        View rootView = inflater.inflate(R.layout.fragment_transaction_list, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_title);
        transactionlist = (ListView) rootView.findViewById(R.id.transactionlist);
        textView.setText(contextd.getResources().getString(R.string.myTransaction));
        activity = (MainActivity) getActivity();
        detailSchemeName = (TextView)rootView.findViewById(R.id.detailSchemeName);
        detailAmountInvested = (TextView)rootView.findViewById(R.id.detailAmountInvested);
        detailFolio = (TextView)rootView.findViewById(R.id.detailFolio);
        tv_nav = (TextView)rootView.findViewById(R.id.tv_nav);
        detailNumberOfUnit = (TextView)rootView.findViewById(R.id.detailNumberOfUnit);
        tv_orderstatus = (TextView)rootView.findViewById(R.id.tv_orderstatus);
        tv_orderid = (TextView)rootView.findViewById(R.id.tv_orderid);
//        swipeRefreshLayout =(SwipeRefreshLayout)rootView.findViewById(R.id.pullToRefresh);
//        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.textGreen));

        Bundle mBundle = new Bundle();
        mBundle = getArguments();
        val = mBundle.getString("foliono");
        scheme_code= mBundle.getString("BSE_SCHEME_CODE");
        folio_no = String.valueOf(val);
        scheme_codee = String.valueOf(scheme_code);
        getFolioViewTransaction(folio_no,scheme_codee);
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                getFolioViewTransaction(folio_no,scheme_codee); // your code
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });
        return rootView;
    }

    private void getFolioViewTransaction(String folio_no,String scheme_codee) {
        requestedServiceDataModel = new RequestedServiceDataModel(contextd, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-holdings.php");
        baseRequestData.setTag(ResponseType.HoldingList);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.setWebServiceType("VIEWTRANX");
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(contextd, "userID"));
        requestedServiceDataModel.putQurry("folio_no",folio_no); //Common.getPreferences(contextd, "folio_no"));
        requestedServiceDataModel.putQurry("scheme_code",scheme_codee);
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {
    }
    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData)  {
       // Log.e("AllTransaction onSuccess", json);
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                Gson gson = new Gson();
                viewTransaction = gson.fromJson(json, ViewTransaction.class);
                transactionAdapter = new TransactionAdapter(contextd, viewTransaction);
                transactionlist.setAdapter(transactionAdapter);
                break;
        }
    }
    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.HoldingList:
                Common.showToast(contextd, contextd.getResources().getString(R.string.noTransaction));
                break;
        }
    }
}
