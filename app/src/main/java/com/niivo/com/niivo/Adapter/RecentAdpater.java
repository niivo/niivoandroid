package com.niivo.com.niivo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niivo.com.niivo.Model.InvestmentList;
import com.niivo.com.niivo.Model.RecentOrder;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class RecentAdpater extends RecyclerView.Adapter {
    Context contextd;
    private Activity activity;
    RecentOrder lists;
    String lang = "";
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));

    public RecentAdpater(Context contextd,  RecentOrder lists) {
        this.contextd = contextd;
        this.lists = lists;
        lang = Common.getLanguage(contextd);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(contextd).inflate(R.layout.recent_oreders, parent, false);
        return new RecentHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
       RecentHolder recentholder = (RecentHolder) holder;
        recentholder.detailSchemeName.setText(lists.getData().get(position).getFundname());
        recentholder.investedDateTV.setText(lists.getData().get(position).getInvestdate());
        recentholder.detailAmountInvested.setText(lists.getData().get(position).getInvestamount());
        recentholder.tv_ordertype.setText(lists.getData().get(position).getOrdertype());
    }

    @Override
    public int getItemCount() {
        return lists.getData().size();
    }
    private class RecentHolder extends RecyclerView.ViewHolder {
        TextView detailSchemeName, investedDateTV, detailAmountInvested, tv_ordertype;

        public RecentHolder(View view) {
            super(view);
            detailSchemeName = (TextView) view.findViewById(R.id.detailSchemeName);
            investedDateTV = (TextView) view.findViewById(R.id.investedDateTV);
            detailAmountInvested = (TextView) view.findViewById(R.id.detailAmountInvested);
            tv_ordertype = (TextView) view.findViewById(R.id.tv_ordertype);

        }
    }
}
