package com.niivo.com.niivo.Requests;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.niivo.com.niivo.Activity.SignInActivity;
import com.niivo.com.niivo.Activity.SplashActivity;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;
import com.niivo.com.niivo.Utils.Log;
import com.niivo.com.niivo.Utils.MyApplication;
import com.niivo.com.niivo.Utils.NetWorkCheck.NetworkDialogAct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServerRequest {

    private Context context;
    private RequestedServiceDataModel requestedServiceDataModel;
    private Type responseClass = null;
    private Dialog progressdialog;
    private Dialog progressdialogtrasacttion;


    public ServerRequest(Context context, RequestedServiceDataModel serviceRequests) {

        this.context = context;
        this.requestedServiceDataModel = serviceRequests;
        progressdialog = new Dialog(context);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.view_progress);
//        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // Set the progress dialog background color transparent
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // pDialog = new ProgressDialog(context);
        // pDialog.setMessage("Loading..");
    //    progressdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    //    progressdialog.setMessage("Loading...");
        progressdialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        progressdialogtrasacttion = new Dialog(context);
        progressdialogtrasacttion.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialogtrasacttion.setContentView(R.layout.view_progress_transaction);
//        progressdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // Set the progress dialog background color transparent
        progressdialogtrasacttion.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // pDialog = new ProgressDialog(context);
        // pDialog.setMessage("Loading..");
        //    progressdialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //    progressdialog.setMessage("Loading...");
        progressdialogtrasacttion.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);


    }
    public void executeRequestTransaction() throws UnsupportedEncodingException, JSONException {
        showProgressBartransaction();
       // progressStart(requestedServiceDataModel.getBaseRequestData());
        progressStarttransaction(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;


        // String Url = "http://employeetracker.us/niivo/services/";
        //String LocalUrl = "http://192.168.0.2/niivo/services/";
//        String AadharUrl = "http://http://172.104.187.141/production/public/api/";
//        String liveUrl = "http://http://172.104.187.141/production/public/api/services/";
//        String jsonUrl = "http://http://172.104.187.141/production/public/api/uploads/json/";
        String AadharUrlv2 = "http://172.104.187.141/production/public/api/";
        String liveUrlv2 = "http://172.104.187.141/production/public/api/services/";
        String jsonUrlv2 = "http://172.104.187.141/production/public/api/uploads/json/";


        requestedServiceDataModel.setLocal(true);
        userService = ServiceGenerator.createService(RetroDataRequest.class, requestedServiceDataModel.isLocal() ? liveUrlv2 : liveUrlv2);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.AAdharApi)
            userService = ServiceGenerator.createService(RetroDataRequest.class, AadharUrlv2);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.GetJson)
            userService = ServiceGenerator.createService(RetroDataRequest.class, jsonUrlv2);

        showProgressBartransaction();
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET_woData) {
            call = userService.dataRequestGetWo(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
        } else if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());

            System.out.print("url" + call.request().url().toString());
        } else if (requestedServiceDataModel.getFile() == null) {
            call = userService.dataRequest(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());
            android.util.Log.e("PostData", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("PostUrl", call.request().url().toString());
        } else {
            android.util.Log.e("data is", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("data is", requestedServiceDataModel.getFile() + "");
            call = userService.dataRequestMultiPart(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());

            System.out.print("url" + call.request().url().toString());
        }
        Log.log("url", call.request().url().toString());
        System.out.print("url" + call.request().url().toString());

        call.enqueue(stringCallback);

    }
    public void executeRequest() throws UnsupportedEncodingException, JSONException {
        showProgressBar();
        progressStart(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;


        // String Url = "http://employeetracker.us/niivo/services/";
        //String LocalUrl = "http://192.168.0.2/niivo/services/";
//        String AadharUrl = "http://http://172.104.187.141/production/public/api/";
//        String liveUrl = "http://http://172.104.187.141/production/public/api/services/";
//        String jsonUrl = "http://http://172.104.187.141/production/public/api/uploads/json/";
        String AadharUrlv2 = "http://172.104.187.141/production/public/api/";
        String liveUrlv2 = "http://172.104.187.141/production/public/api/services/";
        String jsonUrlv2 = "http://172.104.187.141/production/public/api/uploads/json/";


        requestedServiceDataModel.setLocal(true);
        userService = ServiceGenerator.createService(RetroDataRequest.class, requestedServiceDataModel.isLocal() ? liveUrlv2 : liveUrlv2);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.AAdharApi)
            userService = ServiceGenerator.createService(RetroDataRequest.class, AadharUrlv2);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.GetJson)
            userService = ServiceGenerator.createService(RetroDataRequest.class, jsonUrlv2);
        showProgressBar();
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET_woData) {
            call = userService.dataRequestGetWo(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
        } else if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
            System.out.print("url" + call.request().url().toString());
        }
        else if (requestedServiceDataModel.getFile() == null) {
            call = userService.dataRequest(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());
            android.util.Log.e("PostData", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("PostUrl", call.request().url().toString());
        } else {
            android.util.Log.e("data is", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("data is", requestedServiceDataModel.getFile() + "");
            call = userService.dataRequestMultiPart(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());

            System.out.print("url" + call.request().url().toString());
        }
        Log.log("url", call.request().url().toString());
        System.out.print("url" + call.request().url().toString());

        call.enqueue(stringCallback);

    }

    public void pythonServiceExcute() throws UnsupportedEncodingException, JSONException {
        showProgressBar();
        progressStart(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;


      //  String PancardService = "http://192.168.50.142:65000/template/";
        String PancardService = "http://t03.uat.cmots.com/";

        requestedServiceDataModel.setLocal(true);
        userService = ServiceGenerator.createService(RetroDataRequest.class, requestedServiceDataModel.isLocal() ? PancardService : PancardService);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.AAdharApi)
            userService = ServiceGenerator.createService(RetroDataRequest.class, PancardService);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.GetJson)
            userService = ServiceGenerator.createService(RetroDataRequest.class, PancardService);
        showProgressBar();
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET_woData) {
            call = userService.dataRequestGetWo(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
        } else if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
            System.out.print("url" + call.request().url().toString());
        }
        else if (requestedServiceDataModel.getFile() == null) {
            call = userService.dataRequest(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());
            android.util.Log.e("PostData", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("PostUrl", call.request().url().toString());
        } else {
            android.util.Log.e("data is", requestedServiceDataModel.getQurry() + "");
            android.util.Log.e("data is", requestedServiceDataModel.getFile() + "");
            call = userService.dataRequestMultiPart(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());

            System.out.print("url" + call.request().url().toString());
        }
        Log.log("url", call.request().url().toString());
        System.out.print("url" + call.request().url().toString());

        call.enqueue(stringCallback);

    }

    public void executeRequestWithoutProgressbar() throws UnsupportedEncodingException, JSONException {

//        showProgressBar();
        progressStart(requestedServiceDataModel.getBaseRequestData());
        RetroDataRequest userService;
        String Url = "http://employeetracker.us/niivo/services/";
        // String LocalUrl = "http://192.168.0.2/niivo/services/";
//        String AadharUrl = "http://http://172.104.187.141/production/public/api/";
//        String liveUrl = "http://http://172.104.187.141/production/public/api/services/";
//        String jsonUrl = "http://http://172.104.187.141/production/public/api/uploads/json/";
        String AadharUrlv2 = "http://172.104.187.141/production/public/api/";
        //    String liveUrlv2 = "http://182.104.187.141/v2/services/";
        String liveUrlv2 = "http://172.104.187.141/production/public/api/services/";
        String jsonUrlv2 = "http://172.104.187.141/production/public/api/uploads/json/";
        requestedServiceDataModel.setLocal(true);
        userService = ServiceGenerator.createService(RetroDataRequest.class, requestedServiceDataModel.isLocal() ? liveUrlv2 : liveUrlv2);
        if (requestedServiceDataModel.isLocal() && requestedServiceDataModel.getBaseRequestData().getTag() == ResponseType.AAdharApi)
            userService = ServiceGenerator.createService(RetroDataRequest.class, AadharUrlv2);
        Call<String> call = null;
        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET_woData) {
            call = userService.dataRequestGetWo(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
        } else if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            call = userService.dataRequestGet(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction());
            System.out.print("url" + call.request().url().toString());

        } else if (requestedServiceDataModel.getFile() == null) {
            android.util.Log.e("data is", requestedServiceDataModel.getQurry() + "");
            call = userService.dataRequest(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), requestedServiceDataModel.getQurry());

            System.out.print("url" + call.request().url().toString());
        } else {
            System.out.print("url" + call.request().url().toString());
            call = userService.dataRequestMultiPart(Common.getToken(context),
                    requestedServiceDataModel.getBaseRequestData().getWebservice(), getAction(), getMultiPartDataText(), getMultiPartDataImage());

            android.util.Log.d("da", call.request().url().toString());
            System.out.print("url" + call.request().url().toString());
        }

        android.util.Log.v("URL", call.request().url().toString());
        android.util.Log.e("URL", call.request().url().toString());
        System.out.print("url" + call.request().url().toString());
        call.enqueue(stringCallback);

    }
    private void showProgressBartransaction() {

        /*CharSequence title = context.getResources().getString(R.string.app_name);
        CharSequence msg = context.getResources().getString(R.string.loading);*/

        // progressdialog = new TransparentProgressDialog(context);

        progressdialogtrasacttion.setCancelable(false);
        try {

            progressdialogtrasacttion.show();
            progressdialog.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void progressStarttransaction(BaseRequestData requestData) {


    }

    private void progressEndtransaction(BaseRequestData requestData) {
        if (progressdialogtrasacttion.isShowing()) {
            try {
                progressdialogtrasacttion.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void showProgressBar() {

        /*CharSequence title = context.getResources().getString(R.string.app_name);
        CharSequence msg = context.getResources().getString(R.string.loading);*/

        // progressdialog = new TransparentProgressDialog(context);

        progressdialog.setCancelable(false);
        try {

            progressdialogtrasacttion.hide();
            progressdialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void progressStart(BaseRequestData requestData) {


    }

    private void progressEnd(BaseRequestData requestData) {
        if (progressdialog.isShowing()) {
            try {
                progressdialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private BroadcastReceiver netHelpReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            android.util.Log.e("tryAgainReq", "" + intent.getAction());
            context.unregisterReceiver(this);
            ServerRequest.this.requestedServiceDataModel.execute();
        }
    };
    Callback<String> stringCallback = new Callback<String>() {
        private String s;
        private Response response;

        @Override
        public void onResponse(Call<String> s, Response<String> response) {

            progressEndtransaction(requestedServiceDataModel.getBaseRequestData());
            progressEnd(requestedServiceDataModel.getBaseRequestData());
            Log.log(response.body() + "");
            android.util.Log.e("res", response.body() + "");
            android.util.Log.e("response", response.body() + "");
            try {
                JSONObject jsonObject = new JSONObject(response.body());
                android.util.Log.d("res", response.body() + "");
                String status = jsonObject.has("status") ? (jsonObject.getString("status")) : "true";
                String message = jsonObject.has("msg") ? (jsonObject.getString("msg")) : "No message";

                if (status.equalsIgnoreCase("true")) {
                    JSONObject jsonData = null;
                    if (jsonObject.has("userid")) {
                        Common.SetPreferences(context, "userid", jsonObject.getString("userid"));
                    }

//                    if (jsonObject.has("data")) {
//                        if (jsonObject.getJSONObject("data") != null) {
//                            jsonData = jsonObject.getJSONObject("data");
//                            if (requestedServiceDataModel.getResponseDelegate() != null) {
//                                requestedServiceDataModel.getResponseDelegate().onSuccess(jsonData.toString(), requestedServiceDataModel.getBaseRequestData());
//                            }
//                        }
//                    } else {
                        requestedServiceDataModel.getResponseDelegate().onSuccess(message, jsonObject.toString(), requestedServiceDataModel.getBaseRequestData());
//                    }


                } else if (status.equals("deactive")) {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                    context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();
                    Common.showToast(context, message);
                    Intent intent = new Intent(context, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else if ((status.equals("false")) && (message.contains("<logout>"))) {
                    String s1 = message;
                    String[] split = s1.split("<");
                    String firstSubString = split[0];
                    String secondSubString = split[1];
                    if (!firstSubString.trim().equals("")) {
                        Common.showToast(context, firstSubString);
                    }
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                    context.getSharedPreferences("prefs_login", Activity.MODE_PRIVATE).edit().clear().commit();

                    Intent intent = new Intent(context, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                    requestedServiceDataModel.getResponseDelegate().onFailure(message, jsonObject.toString(), requestedServiceDataModel.getBaseRequestData());
                }
            } catch (Exception e) {
                android.util.Log.e("RESPONSE", "BODY:\t" + response.body()
                        + "\n\nMESSAGE:\t" + response.message()
                        + "\n\nERROR BODY:\t" + response.errorBody()
                        + "\n\nCODE:\t" + response.code()
                        + "\n\nHEADERS:\t" + response.headers()
                        + "\n\nIsSuccessful:\t" + response.isSuccessful()
                        + "\n\nRAW:\t" + response.raw());
                Log.log("exception is>>>>>>>>", e + "");
                requestedServiceDataModel.getResponseDelegate().onFailure(response.message() + "", response.message() + "", requestedServiceDataModel.getBaseRequestData());
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            progressEnd(requestedServiceDataModel.getBaseRequestData());
            progressEndtransaction(requestedServiceDataModel.getBaseRequestData());

            t.printStackTrace();
            if (t instanceof IOException || t instanceof UnknownHostException
                    || t instanceof SocketTimeoutException) {
                context.registerReceiver(netHelpReceiver, new IntentFilter("tryAgainState"));
                if (!MyApplication.isNoNetworkDialogShown()) {
                    MyApplication.setNoNetworkDialogShown(true);
                    context.startActivity(new Intent(context, NetworkDialogAct.class));
                }
                requestedServiceDataModel.getResponseDelegate().onNoNetwork(context.getResources().getString(R.string.checkNetworkConnection)
                        , requestedServiceDataModel.getBaseRequestData());
            }
        }
    };

    class ClickListener implements DialogInterface.OnClickListener {
        ServerRequest request;

        public ClickListener(ServerRequest request) {
            this.request = request;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            try {
                request.executeRequest();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static void showToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public HashMap<String, String> getAction() {
        HashMap<String, String> qurryMap = new HashMap<>();
        ArrayList<String> strings = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            strings.add(stringStringEntry.getKey());
            if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
                try {
                    jsonObject.put(stringStringEntry.getKey(), stringStringEntry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        android.util.Log.e("arra", jsonObject.toString());
        JSONArray jsonArray = new JSONArray();
        strings.add(requestedServiceDataModel.getWebServiceType().toLowerCase());
//        qurryMap.put("signature", SignatureCommonMenthods.getSignatureForAPI(strings));
        if (!(requestedServiceDataModel.getWebServiceType().toLowerCase().equals("")) || requestedServiceDataModel.getWebServiceType().toLowerCase() != null)
            qurryMap.put("type", requestedServiceDataModel.getWebServiceType());

        if (requestedServiceDataModel.getBaseRequestData().getServiceType() == Constant.SERVICE_TYPE_GET) {
            jsonArray.put(jsonObject);
            android.util.Log.e("arra", jsonArray.toString());
            qurryMap.put("data", jsonArray.toString());
//            requestedServiceDataModel.setQurry(new HashMap<String, String>());
        }
        return qurryMap;
    }

    public HashMap<String, RequestBody> getMultiPartDataText() {
        HashMap<String, RequestBody> bodyHashMap = new HashMap<>();
        for (Map.Entry<String, String> stringStringEntry : requestedServiceDataModel.getQurry().entrySet()) {
            bodyHashMap.put(stringStringEntry.getKey(), RequestBodyUtils.getRequestBodyString(stringStringEntry.getValue()));
        }
        return bodyHashMap;
    }

    public MultipartBody.Part getMultiPartDataImage() {
        List<MultipartBody.Part> bodyHashMap = new ArrayList<>();
        for (Map.Entry<String, File> stringStringEntry : requestedServiceDataModel.getFile().entrySet()) {
            bodyHashMap.add(RequestBodyUtils.getRequestBodyImage(stringStringEntry.getValue(), stringStringEntry.getKey()));
        }
        if (bodyHashMap.size() > 0) {
            return bodyHashMap.get(0);
        }
        return null;
    }

}
