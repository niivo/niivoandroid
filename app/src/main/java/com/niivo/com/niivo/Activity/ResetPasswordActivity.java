package com.niivo.com.niivo.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.Utils.Common;

/**
 * Created by deepak on 28/6/17.
 */

public class ResetPasswordActivity extends Activity implements ResponseDelegate {
    EditText password, confirmPassword;
    Button done;


    private RequestedServiceDataModel requestedServiceDataModel;
    String mobile = "", newPassword = "", otp = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(ResetPasswordActivity.this, Common.getLanguage(ResetPasswordActivity.this), false);
        setContentView(R.layout.activity_reset_password);
        password = (EditText) findViewById(R.id.resetPassword);
        confirmPassword = (EditText) findViewById(R.id.resetConfirmPassword);
        done = (Button) findViewById(R.id.button_done);
        otp = getIntent().getStringExtra("otp");
        mobile = getIntent().getStringExtra("mobile");
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {

                    newPassword = password.getText().toString().trim();
                    resetPassword();
                }
            }
        });
    }

    boolean validate() {
        boolean b = false;
        if (password.getText().toString().trim().equals("")) {
            b = false;
            password.setError(ResetPasswordActivity.this.getResources().getString(R.string.requiredField));
        } else {
            if (password.getText().toString().trim().length() < 6) {
                password.setError(ResetPasswordActivity.this.getResources().getString(R.string.useAtLeast6Digits));
                b = false;
            } else {
                if (password.getText().toString().trim().length() > 16) {
                    password.setError(ResetPasswordActivity.this.getResources().getString(R.string.notMoreThan16Digits));
                    b = false;
                } else {
                    if (confirmPassword.getText().toString().trim().equals("")) {
                        confirmPassword.setError(ResetPasswordActivity.this.getResources().getString(R.string.requiredField));
                        b = false;
                    } else {
                        if (confirmPassword.getText().toString().trim().length() < 6) {
                            confirmPassword.setError(ResetPasswordActivity.this.getResources().getString(R.string.useAtLeast6Digits));
                            b = false;
                        } else {
                            if (confirmPassword.getText().toString().trim().length() > 16) {
                                confirmPassword.setError(ResetPasswordActivity.this.getResources().getString(R.string.notMoreThan16Digits));
                                b = false;
                            } else {
                                if (!confirmPassword.getText().toString().trim().equals(password.getText().toString().trim())) {
                                    confirmPassword.setError(ResetPasswordActivity.this.getResources().getString(R.string.passwordMismatch));
                                    b = false;
                                } else {
                                    b = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return b;
    }

    void resetPassword() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-user.php");
        baseRequestData.setTag(ResponseType.ResetPass);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_GET);
        requestedServiceDataModel.putQurry("mobile", mobile);
        requestedServiceDataModel.putQurry("otp", otp);
        requestedServiceDataModel.putQurry("newpass", newPassword);
        requestedServiceDataModel.setWebServiceType("PASSCHANGE");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
        requestedServiceDataModel.execute();
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ResetPass:
              //  Log.d("res", json);
                Intent intent = new Intent(ResetPasswordActivity.this, CommonSignUpSignInActivity.class);
                startActivity(intent);
                finishAffinity();
                Common.showToast(ResetPasswordActivity.this, message);
                break;
        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.ResetPass:
            //    Log.d("res", json);
                break;
        }
        Common.showToast(this, message);
    }
}
