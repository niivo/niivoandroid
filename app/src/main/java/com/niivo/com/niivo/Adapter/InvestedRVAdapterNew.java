package com.niivo.com.niivo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.niivo.com.niivo.Model.InvestmentList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class InvestedRVAdapterNew  extends RecyclerView.Adapter implements Filterable {

    Context contextd;
    InvestmentList list;
    String lang = "";
    GetClick click;

   // SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));
    public InvestedRVAdapterNew(Context contextd, InvestmentList list) {

        this.contextd = contextd;
        this.list = list;
        lang = Common.getLanguage(contextd);
    }



    public InvestedRVAdapterNew setClick(GetClick click) {
        this.click = click;
        return this;
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    public interface GetClick {
        void onItemClick(int pos);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invested_with_status, parent, false);
        return new InvestViewHolder(itemView);
    }


        @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof InvestViewHolder) {
            InvestViewHolder investholder = (InvestViewHolder) holder;
            investholder.title.setText(list.getData().get(position).getFundname());
            investholder.amntInvested.setText(Common.currencyString(list.getData().get(position).getAmount(),true));

            if((list.getData().get(position).getOrder_status().equals("Active"))
                    || list.getData().get(position).getOrder_status().equals("Cancelled"))
               {
                investholder.status.setText("SIP "+list.getData().get(position).getOrder_status());
               }else {
                investholder.status.setText(list.getData().get(position).getOrder_status());
            }

            investholder.percentPortFolio.setText(list.getData().get(position).getPortfolio_percent());
            investholder.fundReturn.setText(list.getData().get(position).getReturns()+" % ");

            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
            String inputDateStr=list.getData().get(position).getDtdate();
            Date date = null;
            try {
                date = inputFormat.parse(inputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String outputDateStr = outputFormat.format(date);

            investholder.invesdDate.setText(outputDateStr);
           // investholder.invesdDate.setText(list.getData().get(position).getDtdate());

            investholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click != null)
                        click.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount()
    {
        return list.getData().size();
    }
    private class InvestViewHolder extends RecyclerView.ViewHolder {
        TextView title, amntInvested, percentPortFolio, fundReturn, status, invesdDate;
        ImageView ivLogo;

        public InvestViewHolder(View view) {
            super(view);
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            status = (TextView) itemView.findViewById(R.id.payStatus);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
            invesdDate = (TextView) itemView.findViewById(R.id.investedDateTV);

        }
    }


  /*  @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    movieListFiltered = movieList;
                } else {
                    List<InvestmentList> filteredList = new ArrayList<>();
                    for (InvestmentList movie : movieList) {
                        if (movie.getData().contains(charString.toLowerCase())) {
                            filteredList.add(movie);
                        }
                    }
                    movieListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = movieListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                movieListFiltered = (ArrayList<InvestmentList>) filterResults.values;

                notifyDataSetChanged();
            }
        };*/
 //   }
//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    movieListFiltered = movieList;
//                } else {
//                    List<Movie> filteredList = new ArrayList<>();
//                    for (Movie movie : movieList) {
//                        if (movie.getTitle().toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(movie);
//                        }
//                    }
//                    movieListFiltered = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = movieListFiltered;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                movieListFiltered = (ArrayList<Movie>) filterResults.values;
//
//                notifyDataSetChanged();
//            }
//        };
//    }

}
