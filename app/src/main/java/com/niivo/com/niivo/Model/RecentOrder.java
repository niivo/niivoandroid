package com.niivo.com.niivo.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class RecentOrder implements Serializable {

    String status;
    String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ArrayList<RecentOrderList> getData() {
        return data;
    }

    public void setData(ArrayList<RecentOrderList> data) {
        this.data = data;
    }

    ArrayList<RecentOrderList> data;

    public class RecentOrderList {
        String fundname;
        String investdate;
        String investamount;
        String status;
        String tag;
        String ordertype;
        String orderno;
        String is_goal;
        String qty;

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }



        public String getIs_goal() {
            return is_goal;
        }

        public void setIs_goal(String is_goal) {
            this.is_goal = is_goal;
        }

        public GoalDetial getGoal_detail() {
            return goal_detail;
        }

        public void setGoal_detail(GoalDetial goal_detail) {
            this.goal_detail = goal_detail;
        }

        GoalDetial goal_detail;


        String FUNDS_GUJARATI;
        String FUNDS_HINDI;
        String FUNDS_MARATHI;

        public String getFUNDS_GUJARATI() {
            return FUNDS_GUJARATI;
        }

        public void setFUNDS_GUJARATI(String FUNDS_GUJARATI) {
            this.FUNDS_GUJARATI = FUNDS_GUJARATI;
        }

        public String getFUNDS_HINDI() {
            return FUNDS_HINDI;
        }

        public void setFUNDS_HINDI(String FUNDS_HINDI) {
            this.FUNDS_HINDI = FUNDS_HINDI;
        }

        public String getFUNDS_MARATHI() {
            return FUNDS_MARATHI;
        }

        public void setFUNDS_MARATHI(String FUNDS_MARATHI) {
            this.FUNDS_MARATHI = FUNDS_MARATHI;
        }

        public String getOrdertype() {
            return ordertype;
        }

        public void setOrdertype(String ordertype) {
            this.ordertype = ordertype;
        }

        public String getOrderno() {
            return orderno;
        }

        public void setOrderno(String orderno) {
            this.orderno = orderno;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }
//        public GoalDetial getGoal_detail() {
//            return goal_detail;
//        }
//
//        public void setGoal_detail(GoalDetial goal_detail) {
//            this.goal_detail = goal_detail;
//        }

        public String getFundname() {
            return fundname;
        }

        public void setFundname(String fundname) {
            this.fundname = fundname;
        }

        public String getInvestdate() {
            return investdate;
        }

        public void setInvestdate(String investdate) {
            this.investdate = investdate;
        }

        public String getInvestamount() {
            return investamount;
        }

        public void setInvestamount(String investamount) {
            this.investamount = investamount;
        }
    }

    public class GoalDetial {

        String id;
        String order_id;
        String main_orderid;
        String fund_category;
        String goal_name;
        String investment_type;
        String goal_year;
        String goal_ammount;
        String scheme_code;
        String scheme_name;
        String scheme_amount;
        String dtdate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getMain_orderid() {
            return main_orderid;
        }

        public void setMain_orderid(String main_orderid) {
            this.main_orderid = main_orderid;
        }

        public String getFund_category() {
            return fund_category;
        }

        public void setFund_category(String fund_category) {
            this.fund_category = fund_category;
        }

        public String getGoal_name() {
            return goal_name;
        }

        public void setGoal_name(String goal_name) {
            this.goal_name = goal_name;
        }

        public String getInvestment_type() {
            return investment_type;
        }

        public void setInvestment_type(String investment_type) {
            this.investment_type = investment_type;
        }

        public String getGoal_year() {
            return goal_year;
        }

        public void setGoal_year(String goal_year) {
            this.goal_year = goal_year;
        }

        public String getGoal_ammount() {
            return goal_ammount;
        }

        public void setGoal_ammount(String goal_ammount) {
            this.goal_ammount = goal_ammount;
        }

        public String getScheme_code() {
            return scheme_code;
        }

        public void setScheme_code(String scheme_code) {
            this.scheme_code = scheme_code;
        }

        public String getScheme_name() {
            return scheme_name;
        }

        public void setScheme_name(String scheme_name) {
            this.scheme_name = scheme_name;
        }

        public String getScheme_amount() {
            return scheme_amount;
        }

        public void setScheme_amount(String scheme_amount) {
            this.scheme_amount = scheme_amount;
        }

        public String getDtdate() {
            return dtdate;
        }

        public void setDtdate(String dtdate) {
            this.dtdate = dtdate;
        }
    }
}
