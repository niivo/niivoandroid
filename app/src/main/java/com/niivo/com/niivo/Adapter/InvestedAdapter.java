package com.niivo.com.niivo.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.niivo.com.niivo.Model.OrderedList;
import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Utils.Common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by deepak on 8/6/17.
 */

public class InvestedAdapter extends BaseAdapter {
    Context contextd;
    OrderedList list;
    InvestedHolder holder;
    boolean isInvestment = false;
    boolean isPending = false;
    String lang = "";
    SimpleDateFormat getSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));
    public InvestedAdapter(Context context, OrderedList res, boolean inv) {
        this.contextd = context;
        this.list = res;
        this.isInvestment = inv;
        lang = Common.getLanguage(contextd);
    }

    public InvestedAdapter(Context context, OrderedList res, boolean inv, boolean isPending) {
        this.contextd = context;
        this.list = res;
        this.isInvestment = inv;
        this.isPending = isPending;
        lang = Common.getLanguage(contextd);
    }

    @Override
    public int getCount() {
        return list.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return list.getData().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       try {
           if (convertView == null) {
               convertView = LayoutInflater.from(contextd).inflate(R.layout.item_invested, parent, false);
               holder = new InvestedHolder(convertView);
               convertView.setTag(holder);
           } else {
               holder = (InvestedHolder) convertView.getTag();
           }
           convertView = LayoutInflater.from(contextd).inflate(R.layout.item_invested, parent, false);
           holder = new InvestedHolder(convertView);
           convertView.setTag(holder);

           if (list.getData().get(position).getIs_goal() != null) {
               holder.title.setText(list.getData().get(position).getIs_goal().trim().equals("1") ?
                       list.getData().get(position).getGoal_detail().getGoal_name() :
                       (lang.equals("en")
                               ? list.getData().get(position).getFUND_NAME()
                               : lang.equals("hi")
                               ? list.getData().get(position).getFUNDS_HINDI()
                               : lang.equals("gu")
                               ? list.getData().get(position).getFUNDS_GUJARATI()
                               : lang.equals("mr")
                               ? list.getData().get(position).getFUNDS_MARATHI()
                               : list.getData().get(position).getFUND_NAME()));
           } else {
               holder.title.setText((lang.equals("en")
                       ? list.getData().get(position).getFUND_NAME()
                       : lang.equals("hi")
                       ? list.getData().get(position).getFUNDS_HINDI()
                       : lang.equals("gu")
                       ? list.getData().get(position).getFUNDS_GUJARATI()
                       : lang.equals("mr")
                       ? list.getData().get(position).getFUNDS_MARATHI()
                       : list.getData().get(position).getFUND_NAME()));
           }
           holder.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, (isInvestment ? R.drawable.stay : 0), 0);

           holder.pendingV.setVisibility(isPending ? View.GONE : View.VISIBLE);
           try {
               Calendar ct = Calendar.getInstance();
               ct.setTime(getSDF.parse(list.getData().get(position).getDtdate().trim()));
               holder.investedDateTV.setText(Common.getDateString(ct));
           } catch (ParseException e) {
               e.printStackTrace();
           }
           if (list.getData().get(position).getWithdrawQty().equals("0")) {
               float total = Float.parseFloat(list.getTotal_amount().trim()),
                       amountInvested = Float.parseFloat(list.getData().get(position).getAmount());
               holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested) / total) * 100) + "%");
               holder.amntInvested.setText(Common.currencyString(list.getData().get(position).getAmount(), false));
           } else {
               float total = Float.parseFloat(list.getTotal_amount().trim()),
                       amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                       withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt());
               holder.percentPortFolio.setText(String.format(new Locale("en"), "%.1f", ((amountInvested - withdrawAmnt) / total) * 100) + "%");
               holder.amntInvested.setText(Common.currencyString(((int) (amountInvested - withdrawAmnt)) + "", false));
           }

           if (list.getData().get(position).getIs_goal().trim().equals("1")) {
               int cnt = 0;
               for (int i = 0; i < list.getData().get(position).getOrder_detail().size(); i++) {
                   try {
                       cnt += Integer.parseInt(list.getData().get(position).getOrder_detail().get(i).getAmount().trim());
                   } catch (NumberFormatException e) {
                       e.printStackTrace();
                       cnt += 0;
                   }
               }
               float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) / (float) cnt) * 100;
               holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret % 100)) + "%");
               if (list.getData().get(position).getIs_allotment().equals("0"))
                   holder.fundReturn.setText("0%");
               holder.amntInvested.setText(Common.currencyString(cnt + "", false));
               if (list.getData().get(position).getGoal_detail().getFund_category().equals("1")) {
                   holder.ivLogo.setImageResource(R.drawable.retirementfund);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("2")) {
                   holder.ivLogo.setImageResource(R.drawable.childseducation);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("3")) {
                   holder.ivLogo.setImageResource(R.drawable.childswedding);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("4")) {
                   holder.ivLogo.setImageResource(R.drawable.vacation);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("5")) {
                   holder.ivLogo.setImageResource(R.drawable.buyingcar);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("6")) {
                   holder.ivLogo.setImageResource(R.drawable.buyinghouse);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("7")) {
                   holder.ivLogo.setImageResource(R.drawable.startingbusiness);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("8")) {
                   holder.ivLogo.setImageResource(R.drawable.startinganemergencyfund);
               } else if (list.getData().get(position).getGoal_detail().getFund_category().equals("9")) {
                   holder.ivLogo.setImageResource(R.drawable.wealthcreation);
               } else {
                   holder.ivLogo.setImageResource(R.drawable.rupee);
               }
               holder.type.setText(contextd.getResources().getString(R.string.goal));
           } else {
               float ret = (Float.parseFloat(list.getData().get(position).getCurrent_value().trim()) - (Float.parseFloat(list.getData().get(position).getAmount().trim())));
               ret = ret / (Float.parseFloat(list.getData().get(position).getAmount().trim())) * 100;
               Log.e("return", ret + "");
               if (list.getData().get(position).getIs_allotment().equals("0"))
                   holder.fundReturn.setText("0%");
               else {
                   if (list.getData().get(position).getWithdrawQty().equals("0")) {
                       holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                   } else {
                       float amountInvested = Float.parseFloat(list.getData().get(position).getAmount()),
                               withdrawAmnt = Float.parseFloat(list.getData().get(position).getWithdrawAmt()),
                               wthdrawUnit = Float.parseFloat(list.getData().get(position).getWithdrawQty()),
                               investedUnit = Float.parseFloat(list.getData().get(position).getQuantity()),
                               nav = Float.parseFloat(list.getData().get(position).getNav().trim());
                       ret = (nav * (investedUnit - wthdrawUnit)) - (amountInvested - withdrawAmnt);
                       ret = ret / (amountInvested - withdrawAmnt) * 100;
                       holder.fundReturn.setText(String.format(new Locale("en"), "%.1f", (ret)) + "%");
                   }
               }
               holder.ivLogo.setImageResource(R.drawable.rupee);
               holder.type.setText(contextd.getResources().getString(R.string.fund));
           }

           return convertView;
       }
      catch (Exception e)
       {
           convertView = LayoutInflater.from(contextd).inflate(R.layout.item_invested, parent, false);
           holder = new InvestedHolder(convertView);
           convertView.setTag(holder);


           return convertView;
       }
    }

    class InvestedHolder {
        TextView title, amntInvested, type, percentPortFolio, fundReturn, investedDateTV;
        ImageView ivLogo;
        LinearLayout pendingV;

        public InvestedHolder(View itemView) {
            title = (TextView) itemView.findViewById(R.id.fund_title);
            amntInvested = (TextView) itemView.findViewById(R.id.amntInvested);
            ivLogo = (ImageView) itemView.findViewById(R.id.logo);
            type = (TextView) itemView.findViewById(R.id.type);
            percentPortFolio = (TextView) itemView.findViewById(R.id.portFolioPercent);
            fundReturn = (TextView) itemView.findViewById(R.id.fundReturn);
            pendingV = (LinearLayout) itemView.findViewById(R.id.pendingV);
            investedDateTV = (TextView) itemView.findViewById(R.id.investedDateTV);
        }
    }
}
