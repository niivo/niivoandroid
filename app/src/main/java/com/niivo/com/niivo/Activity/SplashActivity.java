package com.niivo.com.niivo.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Outline;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;

import com.niivo.com.niivo.R;
import com.niivo.com.niivo.Requests.BaseRequestData;
import com.niivo.com.niivo.Requests.Constant;
import com.niivo.com.niivo.Requests.RequestedServiceDataModel;
import com.niivo.com.niivo.Requests.ResponseDelegate;
import com.niivo.com.niivo.Requests.ResponseType;
import com.niivo.com.niivo.TestActivity;
import com.niivo.com.niivo.Utils.Common;
import com.vansuita.library.CheckNewAppVersion;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends Activity implements ResponseDelegate {
    Intent intent;
    Runnable runab;
    Handler handler;
    CheckNewAppVersion st;


    ImageView imgLogo;
    private RequestedServiceDataModel requestedServiceDataModel;
    private String orderIdValueForProcessing;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.setLanguage(SplashActivity.this, Common.getLanguage(SplashActivity.this), false);
        setContentView(R.layout.activity_splash_activity);
       //  String url = "hello.com?id=we123";

      //  String[] str = url.trim().split("\\?");
      //  Log.i( "onCreate: ",str[1]);
        url = "http://172.104.187.141/production/public/api/services/thank-you.php?order_id=2351543993276";
        String[] urlOrderId = url.trim().split("\\?");
        String[] orderIdValue = urlOrderId[1].split("&");
        Log.i("onClick: ",orderIdValue[0].split("=")[1]);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        handler = new Handler();
        st = new CheckNewAppVersion(SplashActivity.this).setOnTaskCompleteListener(new CheckNewAppVersion.ITaskComplete() {
            @Override
            public void onTaskComplete(CheckNewAppVersion.Result result) {
                if (result.hasNewVersion()) {
                    if (handler != null && runab != null)
                        handler.removeCallbacks(runab);
                    ShowUpdateDialog();
                }
            }
        });
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        imgLogo.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getWidth());
            }
        });
        st.execute();
        runab = new Runnable() {
            @Override
            public void run() {
                if (!st.isCancelled()) {
                    st.cancel(true);
                    Log.e("NewVersion", "checking Cancled..!");
                }
                if ((Common.getPreferences(SplashActivity.this, "userID").equals(""))) {
                    intent = new Intent(SplashActivity.this,ChooseLanguageActivity.class); //IndroductionActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                  intent = new Intent(SplashActivity.this, MainActivity.class);
                  //  intent = new Intent(SplashActivity.this, TestActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        handler.postDelayed(runab, 2000);
    }

    void ShowUpdateDialog() {
        new AlertDialog.Builder(SplashActivity.this, R.style.myDialogTheme)
                .setTitle(SplashActivity.this.getResources().getString(R.string.app_name))
                .setMessage(SplashActivity.this.getResources().getString(R.string.newVersionCheck))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(myAppLinkToMarket);
                        } catch (ActivityNotFoundException e) {
                            Common.showToast(SplashActivity.this, "Unable to find market app");
                        }
                    }
                })
                .show().setCancelable(false);
    }

    private void getOrderDetailsValues() {
        requestedServiceDataModel = new RequestedServiceDataModel(this, this);
        BaseRequestData baseRequestData = new BaseRequestData();
        baseRequestData.setWebservice("ws-order.php");
        baseRequestData.setTag(ResponseType.OrderDetail);
        baseRequestData.setServiceType(Constant.SERVICE_TYPE_POST);
        requestedServiceDataModel.putQurry("userid", Common.getPreferences(getApplicationContext(), "userID"));
        requestedServiceDataModel.putQurry("order_id:", orderIdValueForProcessing);
        requestedServiceDataModel.putQurry("device_token", Common.getToken(getApplicationContext()));
        requestedServiceDataModel.setWebServiceType("GETORDERDETAIL");
        requestedServiceDataModel.setBaseRequestData(baseRequestData);
    }

    @Override
    public void onNoNetwork(String message, BaseRequestData baseRequestData) {

    }

    @Override
    public void onSuccess(String message, String json, BaseRequestData baseRequestData) {
        switch (baseRequestData.getTag()) {
            case ResponseType.OrderDetail:
                try {
                    JSONObject jsonObject = new JSONObject(json);
                   String amount = jsonObject.getJSONObject("data").getString("amount");
                    String  schemeName = jsonObject.getJSONObject("data").getString("scheme_name");
                    String str = "Thank you for investing "+"Rs."+amount+" in "+schemeName;
               //     Log.i("Url Response", "onSuccess: "+str);
                   // thanks_text.setText(str);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    @Override
    public void onFailure(String message, String json, BaseRequestData baseRequestData) {

    }
}
