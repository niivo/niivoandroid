package com.niivo.com.niivo.Fragment.TransactionHistory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.niivo.com.niivo.R;

/**
 * Created by deepak on 8/2/18.
 */

public class PendingFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView title;
    //non ui vars
    Context contextd;
    String type = "";

    public PendingFragment() {
    }

    public static PendingFragment getInstance(Bundle bD) {
        PendingFragment mfragment = new PendingFragment();
        Bundle b = bD;
        mfragment.setArguments(b);
        return mfragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaction, container, false);
        contextd = getActivity();
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        setScreenInfo();
        return rootView;
    }

    void setScreenInfo() {
        setTabs();
        viewPager.setAdapter(new PagerAdapter(this.getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        if (type.equals("sip")) {
            title.setText(contextd.getResources().getString(R.string.sipInstallments));
            tabLayout.getTabAt(1).select();
            //call SIP API here for upcoming
        } else if (type.equals("history")) {
            title.setText(contextd.getResources().getString(R.string.transactions));
            tabLayout.getTabAt(0).select();
            //call History API for past
        }
    }

    void setTabs() {
        for (int i = 0; i < 2; i++) {
            tabLayout.addTab(tabLayout.newTab(), i);
            if (i == 0) {
                tabLayout.getTabAt(i).setText(R.string.pending_purchase);
            } else if (i == 1) {
                tabLayout.getTabAt(i).setText(R.string.pending_withdrawls);
            }
        }
    }


    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Bundle b = new Bundle();
                b.putAll(PendingFragment.this.getArguments());
                b.putString("orderStatus", "CONFIRM");
                b.putString("paymentStatus", "-1");
                return TransactionListFragment.getInstance(b);
            } else if (position == 1) {
                Bundle b2 = new Bundle();
                b2.putAll(PendingFragment.this.getArguments());
                b2.putString("orderStatus", "CONFIRM");
                b2.putString("paymentStatus", "-1");
                return TransactionListFragment.getInstance(b2);
            } else return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int i) {
            if (i == 0) {
                return "Pending Purchases";
            } else if (i == 1) {
                return "Pending Withdrawals";
            } else
                return "";
        }
    }
}
